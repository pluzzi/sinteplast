import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IsotopeModule} from 'ngx-isotope';
import {pgCardSocialModule} from '../@pages/components/card-social/card-social.module';
import {RouterModule} from '@angular/router';
import {pgCardModule} from '../@pages/components/card/card.module';
import {pgTabsModule} from '../@pages/components/tabs/tabs.module';
import {NotificationComponent} from '@qavant/notification/notification.component';
import {CardComponent} from '@qavant/card/card.component';
import {ReactionsComponent} from '@qavant/reactions/reactions.component';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {TruncateModule} from 'ng2-truncate';
import {CardDetailComponent} from '@qavant/card-detail/card-detail.component';
import {CardSuggestionComponent} from '@qavant/card-suggestion/card-suggestion.component';
import {CardGalleryComponent } from '@qavant/card-gallery/card-gallery.component';
import {ModalGalleryComponent } from '@qavant/modal-gallery/modal-gallery.component';
import {VgCoreModule} from 'videogular2/core';
import {VgControlsModule} from 'videogular2/controls';
import {VgOverlayPlayModule} from 'videogular2/overlay-play';
import {VgBufferingModule} from 'videogular2/buffering';
// import {SwiperModule, SWIPER_CONFIG, SwiperConfigInterface } from 'ngx-swiper-wrapper';
import {pgSliderModule } from 'app/@pages/components/slider/slider.module';
import {TabsModule} from 'ngx-bootstrap/tabs';
import {
  ModalModule,
  CollapseModule,
  BsDropdownModule} from 'ngx-bootstrap';
import {CarouselModule} from 'ngx-bootstrap/carousel';
import {NgsRevealModule} from 'ng-scrollreveal';
import {SharedModule} from 'app/@pages/components/shared.module';
// const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
//   direction: 'horizontal',
//   slidesPerView: 'auto'
// };
import {FlexLayoutModule} from '@angular/flex-layout';
import {SwiperModule, SwiperConfigInterface, SWIPER_CONFIG} from 'ngx-swiper-wrapper';
import {SafePipe} from 'app/qavant-pipes/safe/safe.pipe';
const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  observer: true,
  direction: 'horizontal',
  threshold: 50,
  spaceBetween: 5,
  slidesPerView: 1,
  centeredSlides: true
};
@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    RouterModule,
    TruncateModule,
    IsotopeModule,
    // pgCardSocialModule,
    // pgCardModule,
    // pgTabsModule,
    pgSliderModule,
    SwiperModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    FlexLayoutModule,
    TabsModule,
    ModalModule.forRoot(),
    CollapseModule.forRoot(),
    BsDropdownModule.forRoot(),
    NgsRevealModule.forRoot(),
    CarouselModule.forRoot()
  ],
  declarations: [
    NotificationComponent,
    CardComponent,
    CardDetailComponent,
    ReactionsComponent,
    CardSuggestionComponent,
    CardGalleryComponent,
    ModalGalleryComponent,
    SafePipe,
  ],
  exports: [
    SharedModule,
    CommonModule,
    FormsModule,
    IsotopeModule,
    pgCardSocialModule,
    pgCardModule,
    pgTabsModule,
    RouterModule,
    TruncateModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    SwiperModule,
    pgSliderModule,
    ModalModule,
    CollapseModule,
    BsDropdownModule,
    CarouselModule,
    NgsRevealModule,
    FlexLayoutModule,
    TabsModule,
    NotificationComponent,
    CardComponent,
    CardDetailComponent,
    ReactiveFormsModule,
    ReactionsComponent,
    CardGalleryComponent,
    ModalGalleryComponent,
    SafePipe,
  ],
  providers: [{
    provide: SWIPER_CONFIG,
    useValue: DEFAULT_SWIPER_CONFIG
  }],
})
export class QavantSharedModule { }
