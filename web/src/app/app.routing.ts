import {Routes} from '@angular/router';
// Layouts
import {
  CondensedComponent,
  // BlankComponent,
  // CorporateLayout,
  // SimplyWhiteLayout,
  // ExecutiveLayout,
  // CasualLayout ,
  // BlankCasualComponent,
  BlankCorporateComponent,
  // BlankSimplywhiteComponent
} from './@pages/layouts';
import {AuthGuard} from '@services/security/auth-guard.service';

export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: 'session/login',
    pathMatch: 'full'
  },
  {
    path: '',
    component: BlankCorporateComponent,
    children: [{
      path: 'session',
      loadChildren: '@qavant/session/session.module#SessionModule'
      },
    ]
  },
  {
    path: '',
    canActivate: [AuthGuard],
    component: CondensedComponent,
    children: [
      {path: 'muro', loadChildren: '@qavant/wall/wall.module#WallModule'},
      {path: 'extra', loadChildren: '@qavant/extra/extra.module#ExtraModule'},
      {path: 'perfil', loadChildren: '@qavant/profile/profile.module#ProfileModule'},
      {path: 'noticias', loadChildren: '@qavant/news/news.module#NewsModule'},
      {path: 'cumples', loadChildren: '@qavant/birthdays/birthdays.module#BirthdaysModule'},
      {path: 'quien-es-quien', loadChildren: '@qavant/whoiswho/whoiswho.module#WhoiswhoModule'},
      {path: 'documentos', loadChildren: '@qavant/documents/documents.module#DocumentsModule'},
      {path: 'informacion', loadChildren: '@qavant/information/information.module#InformationModule'},
      {path: 'beneficios', loadChildren: '@qavant/benefits/benefits.module#BenefitsModule'},
      {path: 'terminos', loadChildren: '@qavant/term/term.module#TermModule'},
      {path: 'capacitaciones', loadChildren: '@qavant/trainings/trainings.module#TrainingsModule'},
      {path: 'formularios', loadChildren: '@qavant/forms-polls/forms-polls.module#FormsPollsModule'},
      {path: 'encuestas', loadChildren: '@qavant/forms-polls/forms-polls.module#FormsPollsModule'},
      {path: '**', redirectTo: 'muro'}
    ],
  },

  // {
  //   path: '',
  //   component: CondensedComponent,
  //   children: [{
  //     path: 'muro',
  //     loadChildren: '@qavant/wall/wall.module#WallModule'
  //   }],
  // },
  // {
  //   path: '',
  //   component: CondensedComponent,
  //   children: [{
  //     path: 'extra',
  //     loadChildren: '@qavant/extra/extra.module#ExtraModule'
  //   }]
  // },

  // {
  //   path: 'casual',
  //   data: {
  //       breadcrumb: 'Home'
  //   },
  //   component: CasualLayout
  // },
  // {
  //   path: 'executive',
  //   data: {
  //       breadcrumb: 'Home'
  //   },
  //   component: ExecutiveLayout
  // },
  // {
  //   path: 'simplywhite',
  //   data: {
  //       breadcrumb: 'Home'
  //   },
  //   component: SimplyWhiteLayout
  // },
  // {
  //   path: 'corporate',
  //   data: {
  //       breadcrumb: 'Home'
  //   },
  //   component: CorporateLayout
  // },
];
