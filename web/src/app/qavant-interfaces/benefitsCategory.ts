export interface BenefitsCategory {
  Id: number;
  Name: string;
  Enabled: boolean;
  Icon: string;
  BenefitCategories: any;
}
