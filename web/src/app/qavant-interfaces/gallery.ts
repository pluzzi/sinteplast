export interface Gallery {
    Categoria: string;
    CategoriaId: number;
    Copete: string;
    Destacada: boolean;
    ElementosMultimedia: any;
    FechaAlta: string;
    Id: number;
    ImagenPortada: string;
    ReaccionId: number;
    Titulo: string;
    TotalReacciones: number;
}
