export interface Information {
  CategoriaId: number;
  Copete: string;
  Destacada: boolean;
  EmpresaId: number;
  Habilitado: boolean;
  Html: string;
  Icono: any;
  Id: number;
  Imagen: string;
  Orden: number;
  Titulo: string;
}
