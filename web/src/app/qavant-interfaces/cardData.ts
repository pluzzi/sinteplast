export interface CardData {
    Id: number;
    Contenido?: string;
    FechaAlta: string;
    Imagen: string;
    Tipo: string;
    Titulo: string;
    Copete: string;
    Destacada: boolean;
    DescRegistro: string;
    ReaccionId: number;
    TotalReacciones: number;
}

