export interface Reaction {
    ReaccionId: number;
    EntidadId: number;
    Modulo: string;
}