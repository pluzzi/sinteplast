import {StorageService} from '@services/storage/storage.service';
import {HttpHeaders, HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, Subject } from 'rxjs';
import {isNullOrUndefined} from 'util';
@Injectable()
export class SettingsService {
  private has_settings = new Subject<boolean>();
  private hash_activate_user: string;
  private localSettings: any;
  private apiSettings: any;
  redirectAfterAuth: string;
  domain: string;
  constructor(
    private storageService: StorageService,
    private http: HttpClient,
    ) {
    this.hash_activate_user = '95153659a5461d15c00f1d83566d65f2';
    this.redirectAfterAuth = '/muro';
  }

  hasSettings(): boolean {
    return ( !isNullOrUndefined(this.apiSettings) );
  }

  getLocalSettings(): any {
    return this.http.get('assets/settings/config.json');
  }

  getApiSettings(): any {
    return this.http.post(`${this.getApiUrl()}tenant/websettings/`, {});
  }

  getApiUrl(): string {
    return this.localSettings.app.end_point;
  }

  getReactions(): any {
    return this.apiSettings.reacciones;
  }

  sessionObserver(): Observable<boolean> {
    return this.has_settings.asObservable();
  }

  setLocalSettings(data: any): void {
    this.localSettings = data;
  }

  setApiSettings(data: any): void {
    this.apiSettings = data;
  }

  setSetting(value: boolean): void {
    this.has_settings.next(value);
  }

  getActiveMenu(): any {
    return this.apiSettings.menu;
  }

  getHasActivateUser(): string {
    return this.hash_activate_user;
  }
  // Redirige despues de ingresar al sistema
  getPathAfterAuth(): string {
    return this.redirectAfterAuth;
  }

  getAppHeaders(): any {
    const apiHeaders = new HttpHeaders().set('x-token', this.storageService.getToken());
    return apiHeaders;
  }

  getTitle(): string {
    return this.apiSettings.global.SiteTitle;
  }

  getTitleSectionStyle(): any {
    if ( this.localSettings.design.titleSection.setPrimaryColorApi ) {
      const styles: any = this.localSettings.design.titleSection;
      styles.color = this.getPrimaryColor();
      styles['border-bottom'] = `1px solid ${this.getPrimaryColor()}`;
      return styles;
    } else {
      return this.localSettings.design.titleSection;
    }
  }

  getTitleStyle(): any {
    const styles = this.localSettings.design.title_section_detail;
    // styles.color = 'red';
    return styles;
  }

  getTitleDetailStyle(): any {
    return this.localSettings.design.title_section_detail;
    // const styles = {
    //   'color': this.settings.theme.TitleSectionDetailColor,
    //   'font-size': this.settings.theme.TitleSectionDetailFontSize,
    //   'line-height': this.settings.theme.TitleSectionDetailLineHeight
    // };
    // return styles;
  }

  getCardTitleStyle(): any {
    if ( this.localSettings.design.cardTitle.setPrimaryColorApi ) {
      const styles = this.localSettings.design.cardTitle;
      styles.color = this.getPrimaryColor();
      return styles;
    } else {
      return this.localSettings.design.cardTitle;
    }
  }

  getPrimaryColor(): any {
    return this.apiSettings.theme.PrimaryColor;
  }

  getSecundaryColor(): any {
    return this.apiSettings.theme.SecondaryColor;
  }

  getLogo(): string {
    return this.apiSettings.theme.Logo;
  }

  getNegativeLogo(): string {
    return this.apiSettings.theme.NegativeLogo;
  }

  hasNegativeLogo(): boolean {
    return ( this.apiSettings.theme.NegativeLogo !== '' );
  }

  getWallTitle(): string {
    return this.apiSettings.global.WallTitle;
  }

  getWallSubtitle(): string {
    return this.apiSettings.global.WallSubtitle;
  }

  getWallImage(): any {
    return this.apiSettings.theme.WallBgImage;
  }

  getLoginTitle(): string {
    return this.apiSettings.global.LoginTitle;
  }

  getLoginSubTitle(): string {
    return this.apiSettings.global.LoginSubtitle;
  }

  getLoginTitleStyle(): any {
    return this.localSettings.design.login.title;
  }

  getLoginSubTitleStyle(): any {
    return this.localSettings.design.login.sub_title;
  }

  hasBackgroundLoginImage(): any {
    if ( this.hasSettings() ) {
      // return (this.settings.design.images.login_background_image !== '');
      return (this.apiSettings.theme.LoginBgImage !== '');
    } else {
      return '';
    }
  }

  getBackgroundLoginImage(): string {
    return this.apiSettings.theme.LoginBgImage;
  }

  getBtnActionStyle(): any {
    return this.localSettings.design.ux.btn.action;
  }

  getBtnActionApiStyle(): any {
    const style: any = {
      'background-color': this.getPrimaryColor(),
      'border-color' : this.getPrimaryColor(),
      'color': '#fff'
    };
    return style;
  }

  getBtnCancelStyle(): any {
    return this.localSettings.design.ux.btn.cancel;
  }
}
