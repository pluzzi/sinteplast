import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Subject, Observable} from 'rxjs';
import {SettingsService} from '@services/settings/settings.service';
import {Gallery} from '@interface/gallery';
import { isNullOrUndefined } from 'util';

@Injectable({
  providedIn: 'root'
})
export class GalleryService {
  private modal = new Subject<boolean>();
  private id: number;
  constructor(
    private http: HttpClient,
    private settingsService: SettingsService
    ) {}

  modalObserver(): Observable<boolean> {
    return this.modal.asObservable();
  }

  openModal(value: boolean): void {
    this.modal.next(value);
  }

  // setGalleryById(data: any): void {
  //   if ( !isNullOrUndefined(data) ) {
  //     this.galeryById = data;
  //   }
  // }

  // getDataGaleryById(): any {
  //   return this.galeryById;
  // }

  setId(id: number): void {
    this.id = id;
  }

  getGalleryById(): any {
    return this.http.get(
      `${this.settingsService.getApiUrl()}galerias/${this.id}`,
      {headers: this.settingsService.getAppHeaders()}
    );
  }

  getGallery(): Observable<Gallery[]> {
    return this.http.get<Gallery[]>(
      `${this.settingsService.getApiUrl()}galerias`,
      {headers: this.settingsService.getAppHeaders()}
    );
  }

  getCategories(): any {
    return this.http.get(
      `${this.settingsService.getApiUrl()}categorias/modulo/galer`,
      {headers: this.settingsService.getAppHeaders()}
    );
  }

  filterByCategory(id: number): any {
    return this.http.get(
      `${this.settingsService.getApiUrl()}galerias/categoria/${id}`,
      {headers: this.settingsService.getAppHeaders()}
    );
  }
}
