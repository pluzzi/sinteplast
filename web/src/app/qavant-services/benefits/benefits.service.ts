import {Benefits} from '@interface/benefits';
import {map } from 'rxjs/operators/map';
import {Observable} from 'rxjs/index';
import {HttpClient} from '@angular/common/http';
import {SettingsService} from '@services/settings/settings.service';
import {Injectable} from '@angular/core';
import {BenefitsCategory} from '@interface/benefitsCategory';

@Injectable({
  providedIn: 'root'
})
export class BenefitsService {
  allBenefits: Benefits[];
  constructor(
    private http: HttpClient,
    private settingsService: SettingsService,
  ) {  }

  getAllBenefits(): Observable<Benefits[]> {
    return this.http.get<Benefits[]>(
      `${this.settingsService.getApiUrl()}benefits/all`,
      {headers: this.settingsService.getAppHeaders()}
    ).pipe( map(response => this.setBenefits(response)));
  }

  private setBenefits(data: Benefits[]) {
    this.allBenefits = data;
    return data;
  }

  getStorageBenefits(): Benefits[] {
    return this.allBenefits;
  }

  getBenefitsById(id: number): Observable<Benefits> {
    return this.http.get<Benefits>(
      `${this.settingsService.getApiUrl()}benefits/${id}`,
      {headers: this.settingsService.getAppHeaders()}
    );
  }

  getCategories(): Observable<BenefitsCategory[]> {
    return this.http.get<BenefitsCategory[]>(
      `${this.settingsService.getApiUrl()}categorias/ModuloId/8`,
      {headers: this.settingsService.getAppHeaders()}
    );
  }

  getBenefitByCategory(id: number): Observable<Benefits[]> {
    return this.http.get<Benefits[]>(
      `${this.settingsService.getApiUrl()}benefits/bycategory/${id}`,
      {headers: this.settingsService.getAppHeaders()}
    );
  }

  like(id: number): any {
    return this.http.get(
      `${this.settingsService.getApiUrl()}benefits/${id}/like`,
      {headers: this.settingsService.getAppHeaders()}
    );
  }
}
