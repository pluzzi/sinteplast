import {map} from 'rxjs/operators/map';
import {Observable } from 'rxjs/index';
import {SettingsService } from '@services/settings/settings.service';
import {HttpClient } from '@angular/common/http';
import {Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class DocumentsService {
  categories: any = [];
  constructor(
    private http: HttpClient,
    private settingsService: SettingsService,
  ) { }

  getDocuments(): Observable<Document[]> {
    return this.http.get<Document[]>(
      `${this.settingsService.getApiUrl()}documents/all`,
      {headers: this.settingsService.getAppHeaders()}
    ).pipe( map(res => this.setformatDocuments(res) ));
  }

  getDocumentsByCategory(id: number): Observable<Document[]> {
    return this.http.get<Document[]>(
      `${this.settingsService.getApiUrl()}documentos/all/bycategoriaId/${id}`,
      {headers: this.settingsService.getAppHeaders()}
    ).pipe( map(res => this.setformatDocuments(res) ));
  }

  getCategories(): any {
    return this.http.get(
      `${this.settingsService.getApiUrl()}categorias/ModuloId/1`,
      {headers: this.settingsService.getAppHeaders()}
    ).pipe( map(res => this.setCategories(res) ));
  }

  filterBy(id: number): any {
    return this.http.get(
      `${this.settingsService.getApiUrl()}documentos/categoriaIdlist/${id}`,
      {headers: this.settingsService.getAppHeaders()}
    ).pipe( map(res => this.setformatDocuments(res) ));
  }

  setCategories(data): void {
    this.categories = data;
    for ( const cat of this.categories ) {
      cat.nameFormat = '';
      const str = Array.from(cat.Nombre);
      for (let i = 0; i < str.length; i++) {
        if ( str[i] === '-' ) {
          str[i] = '  &nbsp;';
        }
        if ( str[i] !== undefined ) {
          cat.nameFormat += str[i];
        }
      }
    }
    return data;
  }

  private setformatDocuments(data): any {
    let docs = [];
    ( data instanceof Array ) ? docs = data : docs.push(data);
    for ( const doc of docs ) {
      if ( doc.DocIcon === undefined || doc.DocIcon === '') {
        doc.DocIcon = 'fal fa-file';
      } else {
      }
      doc.Link = `<a href="${doc.FilePath}" target="_blank" class="download-icon">
      <i class="fas fa-download fa-2x" aria-hidden="true" title="Descargar"></i></a>`;
      doc.icon = `<i  class="fa ${doc.DocIcon} fa-2x" aria-hidden="true" title="${doc.FileTypeLabel}"></i>`;
    }
    return docs;
  }

  getById(id: number): Observable<Document[]> {
    return this.http.get<Document[]>(
      `${this.settingsService.getApiUrl()}documentos/${id}`,
      {headers: this.settingsService.getAppHeaders()}
    ).pipe( map(res => this.setformatDocuments(res) ));
  }

}
