import {Injectable} from '@angular/core';
import {isNullOrUndefined} from 'util';
import {Subject, Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AppService {
  previousPage: string;
  // isLoading = new Subject<boolean>();
  loginPage = '/session/login';
  homePage = '/muro';
  constructor() { }

  // observableGif(): Observable<boolean> {
  //   return this.isLoading.asObservable();
  // }

  // setLoading(status: boolean): void {
  //   this.isLoading.next(status);
  // }

  setPreviousPage(path: string): void {
    this.previousPage = path;
  }

  getPreviousPage(): string {
    if ( isNullOrUndefined(this.previousPage) ) {
      return this.homePage;
    } else {
      return this.previousPage;
    }
  }

  // loading(): boolean {
  //   return this.isLoading;
  // }

}
