import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {StorageService} from '@services/storage/storage.service';
import {SettingsService} from '@services/settings/settings.service';
import {CardData} from '@interface/cardData';
import 'rxjs/add/operator/map';
@Injectable({
  providedIn: 'root'
})

export class WallService {
  allNews: CardData[];
  private api_url: string;
  constructor(
    private http: HttpClient,
    private settingsService: SettingsService,
    private storageService: StorageService,
  ) {  }

  getWallNews(): Observable<CardData[]> {
    const token = {'x-token' : this.storageService.getToken()};
    const apiHeaders = new HttpHeaders().set('x-token', this.storageService.getToken());
    return this.http.get<CardData[]>(
      `${this.settingsService.getApiUrl()}muro/all`, {headers: apiHeaders});
  }

  setWallData(data: CardData[]): void {
    this.allNews = data;
  }

  getAllNews(): CardData[] {
    return this.allNews;
  }
}
