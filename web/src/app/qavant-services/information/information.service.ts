import {Information} from '@interface/information';
import {Observable} from 'rxjs/index';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import {isNullOrUndefined, isArray} from 'util';
import {SettingsService} from '@services/settings/settings.service';
@Injectable({
  providedIn: 'root'
})
export class InformationService {
  private api_url: string;
  tree: any = [];
  informations: Information[];
  constructor(
    private http: HttpClient,
    private settingsService: SettingsService,
  ) { }

  setAllInformation(info: Information[]) {
    this.informations = info;
    if ( this.tree.length > 0 ) {
      for ( let t = 0; t < this.tree.length; t++) {
        for ( let i = 0; i < this.informations.length; i++ ) {
          if ( this.tree[t].id === this.informations[i].CategoriaId ) {
            if ( !isNullOrUndefined(this.tree[t].children) ) {
              this.tree[t].children.push(
                {
                  name: this.informations[i].Titulo,
                  id: this.informations[i].Id,
                  info: true,
                  children: [],
                }
              );
            }
          } else {
            if (this.tree[t].children.length > 0 ) {
              this.recursive(this.tree[t].children, this.informations[i], true);
            }
          }
        }
      }
    }
  }

  getInformationRequest(): Observable<Information[]> {
    return this.http.get<Information[]>(
      `${this.settingsService.getApiUrl()}paginasInfo`,
      {headers: this.settingsService.getAppHeaders()}
    );
  }

  getById(id: number): Observable<Information> {
    return this.http.get<Information>(
      `${this.settingsService.getApiUrl()}infoById/${id}`,
      {headers: this.settingsService.getAppHeaders()}
    );
  }

  getInformationById(id: number): Observable<any> {
    return this.http.get(
      `${this.settingsService.getApiUrl()}infoPaginas/categoriaId/${id}`,
      {headers: this.settingsService.getAppHeaders()}
    );
  }

  getCategories(): Observable<any> {
    return this.http.get(
      `${this.settingsService.getApiUrl()}categorias/modulo/INFUT/false`,
      {headers: this.settingsService.getAppHeaders()}
      ).pipe( map (response => this.setTree(response)));
    }

  setTree(data: any): any {
    this.tree = [];
    for ( let i = 0; i < data.length; i++ ) {
      if ( isNullOrUndefined(data[i].PadreCategoriaId) ) {
        this.tree.push(this.getModelComponentUx(data[i], false));
      } else {
        for (let t = 0; t < this.tree.length; t++) {
          if ( data[i].PadreCategoriaId === this.tree[t].id) {
            this.tree[t].children.push(this.getModelComponentUx(data[i], false));
            break;
          } else {
            if (this.tree[t].children.length > 0) {
              this.recursive(this.tree[t].children, data[i], false);
            }
          }
        }
      }
    }
    return this.tree;
  }

  recursive(parent: any, children: any, setInfo: boolean): void {
    if ( isArray(parent) ) {
      const childrenId = this.setParentIdName(children, setInfo);
      for ( let i = 0; i < parent.length; i++ ) {
        if ( parent[i].id === childrenId && parent[i].hasChildren) {
          const c = this.getModelComponentUx(children, setInfo);
          parent[i].children.push(c);
          break;
        } else {
          if ( !isNullOrUndefined(parent[i].children) ) {
            if ( parent[i].children.length > 0 ) {
              this.recursive(parent[i].children, children, setInfo);
            }
          }
        }
      }
    }
  }

  setParentIdName(data: any, setInfo: boolean): number {
    // console.log(data);
    let childrenId: number;
    if ( setInfo ) {
      childrenId = data.CategoriaId;
    } else {
      childrenId = data.PadreCategoriaId;
    }
    return childrenId;
  }


  getModelComponentUx(obj: any, iTNotCategory: boolean): object {
    const fixModel = { parentId: '', title: ''};
    if ( iTNotCategory ) {
      fixModel.parentId = obj.CategoriaId;
      fixModel.title = obj.Titulo;
    } else {
      fixModel.parentId = obj.PadreCategoriaId;
      fixModel.title = obj.Nombre;
    }
    return {
      name: fixModel.title,
      parentId: fixModel.parentId,
      children: [],
      hasChildren: !iTNotCategory,
      info: iTNotCategory,
      id: obj.Id
    };
  }

  getAll(): Information[] {
    return this.tree;
  }

  hasInformations(): boolean {
    return ( !isNullOrUndefined(this.informations) && this.informations.length > 0);
  }
}
