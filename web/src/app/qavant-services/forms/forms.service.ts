import { Observable } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';
import {SettingsService} from '@services/settings/settings.service';
import {Injectable} from '@angular/core';
import {Form} from '@interface/form';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FormsService {
  private api_url: string;
  formType: Array<any>;
  constructor(
    private http: HttpClient,
    private settingsService: SettingsService,
  ) {
    this.formType = [
      {
        url: '/encuestas',
        param: 1,
        title: 'ENCUESTAS',
      },
      {
        url: '/formularios',
        param: 2,
        title: 'FORMULARIOS',
      },
      {
        url: '/evaluacion',
        param: 3,
        title: 'EVALUACIÓN',
      },
      {
        url: '/evaluacion-des',
        param: 4,
        title: 'EVALUACIÓN DE DESEMPEÑ0',
      },
    ];
  }

  getForms(url): Observable<Form[]> {
    return this.http.get<Form[]>(
      `${this.settingsService.getApiUrl()}forms/getForms/${this.getParam(url)}`,
      {headers: this.settingsService.getAppHeaders()}
    );
  }

  getFormById(id: number): Observable<Form> {
    return this.http.get<Form>(
      `${this.settingsService.getApiUrl()}forms/getQuestionsById/${id}`,
      {headers: this.settingsService.getAppHeaders()}
    );
  }

  getFormsTrainings(): Observable<Form[]> {
    return this.http.get<Form[]>(
      `${this.settingsService.getApiUrl()}courses/getCourses`,
      {headers: this.settingsService.getAppHeaders()}
    );
  }

  saveFormQuestions(form: Form): any {
    return this.http.post(
      `${this.settingsService.getApiUrl()}form/SaveForm`, form,
      {headers: this.settingsService.getAppHeaders()}
    );
  }

  getTitle(url: string): string {
    for ( let i = 0; i < this.formType.length; i++) {
      if ( url === this.formType[i].url) {
        return this.formType[i].title;
      }
    }
  }

  private getParam(url: string): string {
    for ( let i = 0; i < this.formType.length; i++) {
      if ( url === this.formType[i].url) {
        return this.formType[i].param;
      }
    }
  }
}
