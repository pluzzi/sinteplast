import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import {Form } from '@interface/form';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { SettingsService } from '@services/settings/settings.service';
import { FormsService } from '@services/forms/forms.service';
import { isNullOrUndefined } from 'util';
@Component({
  selector: 'app-forms-polls',
  templateUrl: './forms-polls.component.html',
  styleUrls: ['./forms-polls.component.scss'],
  animations: [
    trigger('enterAnimation', [
      state('loading', style({
        opacity: '0',
        transform: 'translateY(8%)'
      })),
      state('ready', style({
        opacity: '1',
        transform: 'translateY(0)'
      })),
      transition('loading => ready', animate('300ms cubic-bezier(0.1, 0.0, 0.2, 1)'))
    ])
  ],
})
export class FormsPollsComponent implements OnInit, OnDestroy {
  private pollsRequest: any;
  isForm: boolean;
  title: string;
  forms: Form [];
  constructor(
    private router: Router,
    private formsService: FormsService,
    public settingService: SettingsService,
  ) {}

  ngOnInit() {
    if (this.router.url === '/formularios') {
      this.isForm = true;
    }
    this.title = this.formsService.getTitle(this.router.url);
    this.pollsRequest = this.formsService.getForms(this.router.url)
    .subscribe(
      response => {
        this.forms = response;
      }
    );
  }

  ngOnDestroy() {
    this.pollsRequest.unsubscribe();
  }

  hasForm(): boolean {
    return ( !isNullOrUndefined(this.forms) );
  }

  goToForm(form): void {
    if (form.AlreadyAnswered && form.UniqueReply) {
      form.message = true;
    }else {
      this.router.navigate([this.router.url, form.Id]);
    }
  }

  getDescription(form: Form): string {
    if ( isNullOrUndefined(form.Description) || form.Description === '' ) {
      return 'Descripción no ingresada';
    } else {
      return form.Description;
    }
  }

  isFormType(): boolean {
    return this.isForm;
  }

  answerAgain(form: Form): boolean {
    if ( this.isFormType() ) {
      return false;
    }
    return ( form.AlreadyAnswered && !form.UniqueReply );
  }

  formMessage(form: Form): string {
    if ( form.AlreadyAnswered && form.UniqueReply ) {
      if ( this.isForm ) {
        return 'Este es un formulario que se puede completar una sola vez y ya lo has hecho.';
      } else {
        return 'Esta es una encuesta que se puede responder una sola vez y ya los has hecho.';
      }
    }
    if ( form.AlreadyAnswered && !form.UniqueReply ) {
      if ( !this.isForm ) {
        return 'Podrás volver a responder esta encuesta aunque ya la hayas respondido.';
      }
    }
    return '';
  }
}
