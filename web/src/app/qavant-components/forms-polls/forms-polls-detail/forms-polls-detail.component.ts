import {Component, OnInit, OnDestroy } from '@angular/core';
import {FormsService } from '@services/forms/forms.service';
import {ActivatedRoute, Router } from '@angular/router';
import {SettingsService } from '@services/settings/settings.service';
import {isNullOrUndefined } from '@swimlane/ngx-datatable/release/utils';
import {Question } from '@interface/question';
import {Form } from '@interface/form';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';

@Component({
  selector: 'app-forms-polls-detail',
  templateUrl: './forms-polls-detail.component.html',
  styleUrls: ['./forms-polls-detail.component.scss'],
  animations: [
    trigger('enterAnimation', [
      state('loading', style({
        opacity: '0',
        transform: 'translateY(8%)'
      })),
      state('ready', style({
        opacity: '1',
        transform: 'translateY(0)'
      })),
      transition('loading => ready', animate('300ms cubic-bezier(0.1, 0.0, 0.2, 1)'))
    ])
  ],
})
export class FormsPollsDetailComponent implements OnInit, OnDestroy {
  private formRequest: any;
  private form: Form;
  send: boolean;
  formatDate: any;
  pollForm: any;
  serverError: boolean;
  answers: number;
  msn: string;
  constructor(
    private formsService:  FormsService,
    private route: ActivatedRoute,
    private router: Router,
    public settingsService: SettingsService
  ) {}
  ngOnInit() {
    console.log('entro en form -poll');
    const pattern = new RegExp('formularios');
    if (pattern.test(this.router.url) ) {
      this.msn = `Tu solicitud fue enviada con éxito y el
      área correspondiente estará dando respuesta a la brevedad. Muchas gracias.`;
    } else {
      this.msn = 'Tu encuesta a sido enviada. Muchas gracias.';
    }

    this.formRequest = this.route.params.subscribe(
      params => {
        this.formsService.getFormById(params.id).subscribe(
        response => {
          this.form = response;
        }
      );
    });
  }

  ngOnDestroy() {
    this.formRequest.unsubscribe();
  }

  hasForm(): boolean {
    return ( !isNullOrUndefined(this.form) );
  }

  getPollTitle(): string {
    return this.form.Title;
  }

  getQuestions(): Array<any> {
    return this.form.Questions;
  }

  setDropdwn(question: Question): boolean {
    return (question.QuestionTypeId === 'DROPDOWN');
  }

  setOption(question: Question): boolean {
    return (question.QuestionTypeId === 'OPTION');
  }

  setOptionRadio(selected: any, questionId: number): void {
    for ( const check of this.form.Questions ) {
      if ( check.QuestionTypeId === 'OPTION' && questionId === check.Id) {
        for ( const option of check.Options ) {
          if (option.Id === selected.Id) {
            option.AnswerSelected = true;
            check.AnswerOptionId = selected.Id;
          }else {
            option.AnswerSelected = false;
          }
        }
      }
    }
  }

  setOptionSelected(question: Question, id ): void {
    for (const setOption of question.Options) {
      if ( id === setOption.Id ) {
        setOption.AnswerSelected = true;
      } else {
        setOption.AnswerSelected = false;
      }
    }
    // question.AnswerOptionId = 0;
  }

  setMultipleOption(question: Question): boolean {
    return (question.QuestionTypeId === 'MULTIPLE');
  }

  setOptionMultiple(question: Question, data ): void {
    for (const setOption of question.Options) {
      if ( data.indexOf(setOption.Id) !== -1 ) {
        setOption.AnswerSelected = true;
      } else {
        setOption.AnswerSelected = false;
      }
    }
    // question.AnswerOptionId = 0;
  }

  setDate(question: Question): boolean {
    return (question.QuestionTypeId === 'DATE');
  }

  setDateFormat(event: any, question: any): void {
    this.formatDate = new Date(question.Answer);
    this.formatDate  = this.formatDate.getFullYear()
    + '-' + ('0' + (this.formatDate.getMonth() + 1)).slice(-2)
    + '-' + ('0' + (this.formatDate.getDate())).slice(-2) ;
    question.Answer = this.formatDate;
  }

  setInputText(question: Question): boolean {
    return (question.QuestionTypeId === 'TEXT');
  }

  setTextArea(question: Question): boolean {
    return (question.QuestionTypeId === 'TEXTAREA');
  }

  hasQuestions() {
    return (!isNullOrUndefined(this.form.Questions));
  }

  savePolls(event: Event): void {
    this.answers = 0;
    if ( !isNullOrUndefined(event) ) {
      event.preventDefault();

      for ( const question of this.form.Questions){
        // campo verifica validación
        if ( question.Required ) {
          question.validate = true;
        }
        // Array de multiples respuestas o respuesta simple
        if ( question.Options.length > 0 ) {
          for ( const option of question.Options ) {
            if ( question.Required && option.AnswerSelected ) {
              question.validate = false;
            }
            // verifico por lo menos una respuesta
            if ( option.AnswerSelected ) {
              this.answers = this.answers + 1;
            }
          }
        } else {
          // Respuestas campo simple text/texarea
          if ( !isNullOrUndefined(question.Answer) ) {
            question.validate = false;
            this.answers = this.answers + 1;
          }
          // Verifico en los campos input y textarea que no hayan sido borrados
          if ( question.Answer === '' ) {
            question.Answer = null;
            question.validate = true;
            this.answers = this.answers - 1;
          }
        }
      }
      // Verifico validación de campos
      let check = true;
      for ( const validate of this.form.Questions) {
        if ( validate.validate ) {
          check = false;
        }
      }

      if ( check && this.answers > 0) {
        this.formsService.saveFormQuestions(this.form)
        .subscribe(
          response => {
            this.send = true;
          },
          error => {
            this.send = true;
            this.serverError = true;
            if ( !isNullOrUndefined(error.error) ) {
              this.msn = error.error.title;
            }
          }
        );
      }
    }
  }
}
