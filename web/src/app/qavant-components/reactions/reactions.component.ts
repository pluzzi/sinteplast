import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import {SettingsService} from '@services/settings/settings.service';
import {ReactionsService} from '@services/reactions/reactions.service';
import {Reaction} from '@interface/reaction';
import { isNullOrUndefined } from 'util';
@Component({
  selector: 'app-reactions',
  templateUrl: './reactions.component.html',
  styleUrls: ['./reactions.component.scss']
})
export class ReactionsComponent implements OnInit, OnDestroy {
  @Input() data: any;
  reactions: any = [];
  iconDefaultId: number;
  iconDefault: any;
  showIcons: boolean;
  private request: any;
  private timer: any;
  constructor(
    private settingsService: SettingsService,
    private reactionService: ReactionsService
  ) {}

  ngOnInit() {
    this.iconDefaultId = 2;
    this.iconDefault = {};
    this.showIcons = false;
    this.reactions = this.settingsService.getReactions();
    this.setIconDefault();
  }

  ngOnDestroy() {
  }

  saveReaction(action: any): void {
    // correcion error de al api. No llega este dato en sec multimedia
    if ( isNullOrUndefined(this.data.Tipo) ) {
      this.data.Tipo = 'GALER';
    }
    const reaction = {
      ReaccionId : action.id,
      EntidadId : this.data.Id,
      Modulo: this.data.Tipo
    };
    this.iconDefault = action;
    this.request = this.reactionService.saveReaction(reaction)
      .subscribe(
        response => {
          if ( this.data.ReaccionId === 0) {
            this.data.ReaccionId = action.id;
            this.data.TotalReacciones += 1;
          }
        }, error => {
          console.log(error);
        }
      );
  }

  showReactions(): void {
    if ( this.showIcons ) {
      clearTimeout(this.timer);
    } else {
      this.showIcons = true;
    }
  }

  userHasReaction(): boolean {
    return (this.data.ReaccionId !== 0);
  }

  hideReactions(): void {
    this.timer = setTimeout(() => {
      this.showIcons = false;
    }, 1000);
    if (!this.showIcons) {
      clearTimeout(this.timer);
    }
  }

  setIconDefault(): void {
    for (const icon of this.reactions) {
      icon.id = +icon.id;
      if ( this.data.ReaccionId === 0 ) {
        if ( this.iconDefaultId === icon.id) {
          this.iconDefault = icon;
        }
      } else {
        if (this.data.ReaccionId === icon.id ) {
          this.iconDefault = icon;
        }
      }
    }
  }

  getIconoDefault(): string {
    return this.iconDefault.icono;
  }

  getNameDefault(): string {
    if ( this.userHasReaction()) {
      return this.iconDefault.nombre;
    } else {
      return '';
    }
  }

  getTotal(): number {
    return this.data.TotalReacciones;
  }
}
