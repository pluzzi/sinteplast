import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../@pages/components/shared.module';
import {QavantSharedModule} from '@shared';
import {TermComponent} from '@qavant/term/term.component';
const routes: Routes =
  [
    { path: '', component: TermComponent }
  ];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    QavantSharedModule,
  ],
  declarations: [TermComponent]
})
export class TermModule { }
