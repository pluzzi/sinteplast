import {Router } from '@angular/router';
import {Information } from '@interface/information';
import {Component, OnInit, OnDestroy } from '@angular/core';
import {InformationService } from '@services/information/information.service';
import {SettingsService } from '@services/settings/settings.service';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.scss'],
  animations: [
    trigger('enterAnimation', [
      state('loading', style({
        opacity: '0',
        transform: 'translateY(8%)'
      })),
      state('ready', style({
        opacity: '1',
        transform: 'translateY(0)'
      })),
      transition('loading => ready', animate('300ms cubic-bezier(0.1, 0.0, 0.2, 1)'))
    ])
  ],
})
export class InformationComponent implements OnInit, OnDestroy {
  private informationRequest: any;
  _isLoading = true;
  information: Information[];

  constructor(
    private informationService: InformationService,
    private router: Router,
    public settingService: SettingsService,
  ) {  }

  ngOnInit() {
    this.informationRequest = this.informationService.getInformationRequest()
    .subscribe(
      response => {
        this.information = response;
        this.informationService.setAllInformation(response);
        if ( response.length > 0 ) {
          // this.router.navigate(['/informacion', response[0].Id]);
        }
        this._isLoading = false;
      }
    );
  }

  ngOnDestroy() {
    this.informationRequest.unsubscribe();
  }

  goToInformation(id: number): void {
    this.router.navigate(['informacion', id]);
  }

  hasInformation(): boolean {
    if ( !isNullOrUndefined(this.information) ) {
      return (this.information.length > 0 );
    }
  }

}
