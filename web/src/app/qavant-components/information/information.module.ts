import {NgModule } from '@angular/core';
import {CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../@pages/components/shared.module';
import {QavantSharedModule} from '@shared';
import {InformationComponent} from '@qavant/information/information.component';
import {InformationDetailComponent } from '@qavant/information/information-detail/information-detail.component';
import {TreeviewpageComponent } from '@qavant/treeviewpage/treeviewpage.component';
import {TreeModule} from 'angular-tree-component';
import {ProgressModule} from 'app/@pages/components/progress/progress.module';
const routes: Routes =
  [
    { path: '', component: TreeviewpageComponent },
    { path: ':id', component: TreeviewpageComponent }
  ];
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    QavantSharedModule,
    TreeModule.forRoot(),
    RouterModule.forChild(routes),
    ProgressModule.forRoot(),
  ],
  declarations: [
    InformationComponent,
    InformationDetailComponent,
    TreeviewpageComponent
  ]
})
export class InformationModule { }
