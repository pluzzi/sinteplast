import { isNullOrUndefined } from 'util';
import { ActivatedRoute, Router } from '@angular/router';
import { InformationService } from '@services/information/information.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Information } from '@interface/information';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { SettingsService } from '@services/settings/settings.service';
@Component({
  selector: 'app-information-detail',
  templateUrl: './information-detail.component.html',
  styleUrls: ['./information-detail.component.scss'],
  animations: [
    trigger('enterAnimation', [
      state('loading', style({
        opacity: '0',
        transform: 'translateY(8%)'
      })),
      state('ready', style({
        opacity: '1',
        transform: 'translateY(0)'
      })),
      transition('loading => ready', animate('300ms cubic-bezier(0.1, 0.0, 0.2, 1)'))
    ])
  ],
})
export class InformationDetailComponent implements OnInit, OnDestroy {
  _limit = 5;
  _isLoading = true;
  information: Information;
  allInformations: Information[];
  request: any;
  id: number;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private informationService: InformationService,
    public settingsService: SettingsService
  ) { }

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.id = +params.id;
      if ( !isNullOrUndefined(params.id) ) {
        this.getById();
      }
    });

    if ( this.informationService.hasInformations() ) {
      this.allInformations = this.informationService.getAll();
    } else {
      this.informationService.getInformationRequest()
        .subscribe(
          response => {
            this.allInformations = response;
            this.informationService.setAllInformation(response);
          }
        );
    }
  }

  ngOnDestroy() {
    this.request.unsubscribe();
  }

  getById(): void {
    this.request = this.informationService.getById(this.id)
      .subscribe(
        response => {
          this.information = response;
          this._isLoading = false;
        }
      );
  }

  hasImage(): boolean {
    return ( !isNullOrUndefined(this.information.Imagen) );
  }

  goToInformation(id: number) {
    this.router.navigate(['informacion', id]);
  }

  validate(index: number, id: number): boolean {
    // return ( index < this._limit && id !== this.id );
    return ( id !== this.id );
  }

}
