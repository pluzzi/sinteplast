import {
  Component,
  OnInit,
  ViewChild,
  OnDestroy,
  QueryList,
  ViewChildren,
  AfterViewChecked,
  AfterViewInit} from '@angular/core';
import {GalleryService} from '@services/gallery/gallery.service';
import {isNullOrUndefined} from 'util';
// import {SwiperDirective, SwiperComponent} from 'ngx-swiper-wrapper';
@Component({
  selector: 'app-modal-gallery',
  templateUrl: './modal-gallery.component.html',
  styleUrls: ['./modal-gallery.component.scss']
})
export class ModalGalleryComponent implements OnInit, OnDestroy, AfterViewChecked, AfterViewInit {
  @ViewChild('slider', {read: false, static: true}) public _slider: any;
  @ViewChild('fadInModal', {read: false, static: false}) _modal: any;
  // @ViewChildren(SwiperDirective) swiperViewes: QueryList<SwiperDirective>;
  private observable: any;
  gallery: any = [];
  images: any = [];
  rangeValue;
  activeSlideIndex = 0;
  _isLoading = true;
  constructor(private galleryService: GalleryService) { }

  ngOnInit() {
    this.observable = this.galleryService.modalObserver()
    .subscribe(
      response => {
        if (response) {
          this.getGallery();
        }
      }
      );
  }

  ngAfterViewChecked() {
    // console.log(this.configModal);
  }

  ngOnDestroy() {
    if ( !isNullOrUndefined(this.observable) ) {
      this.observable.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    // setTimeout(() => {
    //   this.swiperViewes.forEach(dir =>  {
    //     dir.init();
    //   });
    //   if ( !isNullOrUndefined (this.swiperViewes.first) ) {
    //     this.swiperViewes.first.startAutoplay();
    //   }
    // }, 1);
  }

  getGallery(): void {
    this.galleryService.getGalleryById()
      .subscribe(
        response => {
          this.images = [];
          this.gallery = response;
          this.gallery.Tipo = 'GALER';
          this.images = this.gallery.ElementosMultimedia;
          this._modal.show();
          this._isLoading = false;
        }
      );
  }

  isVideo(data: any): boolean {
    return (data.Tipo === 'VID');
  }

  hasImages(): boolean {
    if ( !isNullOrUndefined(this.images) ) {
      return ( this.images.length > 0 );
    }
  }

  // nextSlide() {
  //   this.swiperViewes.last.nextSlide();
  // }

  // prevSlide() {
  //   this.swiperViewes.last.prevSlide();
  // }

  // toggleModal() {
  //   this._modal.show();
  //   setTimeout( () => {
  //     this.swiperViewes.last.update();
  //   }, 500);
  // }

  close(): void {
    this.galleryService.openModal(false);
    this.images = [];
    this._modal.hide();
  }

  // onSlideClicked(value: any) {
  //   console.log(value);
  //   console.log(value.activeId);
  // }

  style(): any {
    const styles: any = {
      display: 'block',
      width: '100%',
    };
    return styles;
  }
}
