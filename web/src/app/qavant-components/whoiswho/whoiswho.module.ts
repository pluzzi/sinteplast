import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WhoiswhoComponent} from '@qavant/whoiswho/whoiswho.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../@pages/components/shared.module';
import {QavantSharedModule} from '@shared';
import {NgxPaginationModule} from 'ngx-pagination';
import {FormsModule} from '@angular/forms';
import {Ng2CompleterModule} from 'ng2-completer';
const routes: Routes =
  [
    { path: '', component: WhoiswhoComponent }
  ];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    Ng2CompleterModule,
    RouterModule.forChild(routes),
    SharedModule,
    QavantSharedModule,
    NgxPaginationModule
  ],
  declarations: [WhoiswhoComponent]
})
export class WhoiswhoModule { }
