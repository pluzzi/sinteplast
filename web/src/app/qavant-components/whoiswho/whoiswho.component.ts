import {Component, OnInit} from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { UserService } from '@services/users/user.service';
import { SettingsService } from '@services/settings/settings.service';
import { User } from '@interface/user';
import { isNullOrUndefined } from '@swimlane/ngx-datatable/release/utils';
import { CompleterService, CompleterData, CompleterItem } from 'ng2-completer';
import { ViewEncapsulation } from '@angular/compiler/src/core';

@Component({
  selector: 'app-whoiswho',
  templateUrl: './whoiswho.component.html',
  styleUrls: ['./whoiswho.component.scss'],
  animations: [
    trigger('enterAnimation', [
      state('loading', style({
        opacity: '0',
        transform: 'translateY(8%)'
      })),
      state('ready', style({
        opacity: '1',
        transform: 'translateY(0)'
      })),
      transition('loading => ready', animate('300ms cubic-bezier(0.1, 0.0, 0.2, 1)'))
    ])
  ],
})
export class WhoiswhoComponent implements OnInit {
  protected dataService: CompleterData;
  protected searchStr: string;
  public captain: string;
  public suggestion: any;
  private usertext: string;
  request: any;
  searchText: any;
  p = 1;
  users: User[];
  constructor(
    private completerService: CompleterService,
    private userService: UserService,
    public settingsService: SettingsService
    ) { }

  ngOnInit() {
    this.request = this.userService.getUsers()
    .subscribe(
      response => {
        this.users = response;
        this.userService.setAllUser(response);
        this.confAutocomplete();
      }
    );
  }

  hasUsers(): boolean {
    return ( !isNullOrUndefined(this.users) );
  }

  private confAutocomplete(): void {
    // users/search/{searchTerm}/page/{page}
    // this.suggestion = this.completerService.remote(
    //   `${this.settingsService.getApiUrl()}users/search/${this.captain}/page/1&` ,
    //   this.captain, null
    //   );
    this.suggestion = this.completerService.local
    (
      this.userService.getAllUsers(),
      'FullName,LastName', 'Name,Surname'
    );
  }

  onSelected(selected: CompleterItem): void {
    if ( !isNullOrUndefined(selected) ) {
      this.users = [];
      this.users.push(selected.originalObject);
    } else {
      this.users = this.userService.getAllUsers();
    }
  }

  open(): void {
    const dropdown = document.getElementsByClassName('completer-dropdown');
  }

  sendParameter(): void {
    document.getElementById('autocomplete').blur();
    // this.router.navigate(['/buscar/', this.appService.prettyUrl(this.captain)]);
  }

  getUserName(user: User): string {
    return user.Name.toUpperCase() + ' ' + user.Surname.toUpperCase();
  }

  getUserEmail(user: User): string {
    if ( isNullOrUndefined(user.Email) ) {
      return 'No ingresado';
    } else {
      return user.Email;
    }
  }

  getUserIntern(user: User): string {
    let phone;
    let int;
    if ( isNullOrUndefined(user.Phone)) {
      phone = '';
    } else {
      phone = user.Phone;
    }

    if ( isNullOrUndefined(user.Intern) ) {
      int = '';
    } else {
      int = `Int. ${user.Intern}`;
    }

    if ( phone === '' && int === '') {
      return 'No ingresado';
    }
    return `${phone}  ${int}`;
  }

  getUserPhone(user: User): string {
    if ( isNullOrUndefined(user.Mobile) || user.Mobile === '' ) {
      return 'No ingresado';
    } else {
      return user.Mobile;
    }
  }

  getUserMail(user: User): string {
    if ( isNullOrUndefined(user.Email) ) {
      return 'No ingresado';
    } else {
      return user.Email;
    }
  }

  getUserArea(user: User): string {
    if ( isNullOrUndefined(user.Area) ) {
      return 'No ingresado';
    }else {
      return user.Area.toLowerCase();
    }
  }

  getAllUser(): void {
    this.captain = '';
    this.users = this.userService.getAllUsers();
  }

}
