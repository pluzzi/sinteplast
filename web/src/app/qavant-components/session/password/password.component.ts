import { Component, OnInit } from '@angular/core';
import { SettingsService } from '@services/settings/settings.service';
import { UserService } from '@services/users/user.service';
import { Router } from '@angular/router';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss']
})
export class PasswordComponent implements OnInit {
  validate: any;
  passwordUser: any;
  confirmUser: any;
  constructor(
    public settingsService: SettingsService,
    private userService: UserService,
    private router: Router,
    ) {
    this.validate = {
      'message' : '',
      'status': false,
      'type': '',
      'redirect': false,
      'form': true
    };
  }

  ngOnInit() {
    if ( !this.hasSettings() ) {
      this.router.navigate(['/session/login']);
    }
  }

  hasSettings(): boolean {
    return this.settingsService.hasSettings();
  }

  resetPassword(): void {
    if ( !isNullOrUndefined(this.confirmUser) && !isNullOrUndefined(this.passwordUser) ) {
      if ( this.confirmUser === this.passwordUser) {
        this.userService.resetPassword(this.passwordUser, this.confirmUser)
          .subscribe(
            response => {
              this.router.navigate(['/muro']);
            },
            error => {
              if ( !isNullOrUndefined(error) ) {
                this.validate.message = error.error.title;
                this.validate.type = 'danger';
                this.validate.status = true;
              }
            }
          );
      } else {
          this.validate.message = 'Los campos no coinciden';
          this.validate.type = 'danger';
          this.validate.status = true;
      }
      // this.userService.forgotPassword(this.username).subscribe(
      //   response => {
      //     console.log(response);
      //     if ( isNullOrUndefined(response) ) {
      //       this.validate.message = 'Bien! ... ahora ingresa con tu usuario en los campos "usuario" y "contraseña".';
      //       this.validate.type = 'success';
      //       this.validate.status = true;
      //       this.validate.form = false;
      //       this.validate.redirect = true;
      //     }
      //   },
      //   error => {
      //     console.log(error);
      //     if ( error.status === 401 ) {
      //       this.validate.message = 'El usuario no existe';
      //       this.validate.type = 'danger';
      //       this.validate.status = true;
      //       this.username = null;
      //     }
      //   }
      // );
    }
  }

}
