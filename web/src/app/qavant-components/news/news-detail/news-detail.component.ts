import {News} from '@interface/news';
import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router, NavigationEnd} from '@angular/router';
import {NewsService} from '@services/news/news.service';
import {IsotopeOptions} from 'ngx-isotope';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {SettingsService} from '@services/settings/settings.service';
import {isNullOrUndefined} from 'util';
import {AppService} from '@services/app/app.service';
import {WallService} from '@services/wall/wall.service';
declare var stepsForm: any;
declare var pg: any;
@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.component.html',
  styleUrls: ['./news-detail.component.scss'],
  animations: [
    trigger('enterAnimation', [
      state('loading', style({
        opacity: '0',
        transform: 'translateY(8%)'
      })),
      state('ready', style({
        opacity: '1',
        transform: 'translateY(0)'
      })),
      transition('loading => ready', animate('300ms cubic-bezier(0.1, 0.0, 0.2, 1)'))
    ])
  ],
})
export class NewsDetailComponent implements OnInit, OnDestroy {
  private request: any;
  private id: number;
  news: News;
  suggestions: any;
  limitSomeNews = 8;
  previousUrl: string;
  constructor(
    private route: ActivatedRoute,
    private newsService: NewsService,
    private wallService: WallService,
    private router: Router,
    public settingService: SettingsService,
    public appService: AppService
  ) {
   }

  ngOnInit() {
    const pattern = new RegExp('muro');
    if (pattern.test(this.router.url) ) {
      this.getWallSuggestions();
    } else {
      this.getNewsSuggestions();
    }

    this.route.params.subscribe(
      params => {
        this.news = null;
        this.id = +params.id;
        this.getNewsDetail();
    });
  }

  ngOnDestroy() {
    this.request.unsubscribe();
  }

  private getNewsDetail(): void {
    this.request = this.newsService.getNewsById(this.id)
    .subscribe(
      response => {
        this.news = response;
        this.news.Type = 'Noticias';
      }
    );
  }

  private getNewsSuggestions(): void {
    if (this.newsService.getBackupNews().length > 0 ) {
      this.suggestions = this.newsService.getBackupNews();
    } else {
      this.newsService.getNews()
      .subscribe(
          response => {
            this.suggestions = response;
          }
        );
    }
  }

  private getWallSuggestions(): void {
    if ( !isNullOrUndefined(this.wallService.allNews) ) {
      this.suggestions = this.wallService.getAllNews();
    } else {
      this.wallService.getWallNews()
      .subscribe(
        response => {
          this.suggestions = response;
        }
      );
    }
  }

  hasNews(): boolean {
    return ( !isNullOrUndefined(this.news) );
  }

  goToNewDetail(id: number): void {
    this.router.navigate(['noticias', id]);
  }
}
