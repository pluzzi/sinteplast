import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {WallComponent} from '@qavant/wall/wall.component';
import {SharedModule} from '../../@pages/components/shared.module';
import {QavantSharedModule} from '@shared';

const routes: Routes =
  [
    { path: '', component: WallComponent },
  ];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    QavantSharedModule,
  ],
  declarations: [WallComponent],
})

export class WallModule { }
