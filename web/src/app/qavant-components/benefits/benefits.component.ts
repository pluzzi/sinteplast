import {BenefitsService} from '@services/benefits/benefits.service';
import { Router } from '@angular/router';
import { Benefits } from '@interface/benefits';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { isNullOrUndefined } from 'util';
import { IsotopeOptions } from 'ngx-isotope';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { BenefitsCategory } from '@interface/benefitsCategory';
import { ModalDirective } from 'ngx-bootstrap';
import { SettingsService } from '@services/settings/settings.service';
import { CardData } from '@interface/cardData';
@Component({
  selector: 'app-benefits',
  templateUrl: './benefits.component.html',
  styleUrls: ['./benefits.component.scss'],
  animations: [
    trigger('enterAnimation', [
      state('loading', style({
        opacity: '0',
        transform: 'translateY(8%)'
      })),
      state('ready', style({
        opacity: '1',
        transform: 'translateY(0)'
      })),
      transition('loading => ready', animate('300ms cubic-bezier(0.1, 0.0, 0.2, 1)'))
    ])
  ],
})
export class BenefitsComponent implements OnInit, OnDestroy {
  @ViewChild(ModalDirective, {static: false}) modal: ModalDirective;
  private benefitsRequest: any;
  showAll: boolean;
  _isLoading = true;
  cardData: CardData[] = [];
  allBenefist: Benefits[];
  categories: BenefitsCategory[] = [];
  rangeValue: any;
  public galleryOptions: IsotopeOptions = {
      itemSelector: '.card',
      masonry: {
          columnWidth: 290,
          gutter: 20,
          fitWidth: true
      }
  };
  constructor(
    private benefitsService: BenefitsService,
    private router: Router,
    public settingsService: SettingsService,
  ) { }

  ngOnInit() {
    this.getAll();
    this.benefitsService.getCategories()
      .subscribe(
        response => {
          this.categories = response;
        }
      );
  }

  ngOnDestroy() {
    this.benefitsRequest.unsubscribe();
  }

  hasCategories(): boolean {
    return (this.categories.length > 0);
  }

  goToDetail(id: string): void {
    this.router.navigate(['beneficios', id]);
  }

  hasBenefist(): boolean {
    return ( this.cardData.length > 0 );
  }

  getAll(): void {
    this.benefitsRequest = this.benefitsService.getAllBenefits()
    .subscribe(
      response => {
        this.cardData = [];
        this.allBenefist = response;
        this.fixModelCard(response);
        this._isLoading = false;
      }
    );
  }

  fixModelCard(benef: Benefits[]): void {
    for ( const _news of benef) {
      const data: CardData = {
        Id: _news.Id,
        Contenido: _news.Content,
        FechaAlta: _news.To,
        Imagen: _news.Image,
        Tipo: 'BENEF',
        Titulo: _news.Title,
        Copete: _news.Subtitle,
        Destacada: false,
        DescRegistro: 'BENEFICIOS',
        ReaccionId: _news.ReaccionId,
        TotalReacciones: _news.TotalReacciones
      };
      this.cardData.push(data);
    }
    this._isLoading = false;
    this.showAll = true;
  }

  like(ben: Benefits, value: boolean): void {
    ben.Liked = value;
    this.benefitsService.like(ben.Id)
      .subscribe(
        response => {
          console.log(response);
        }
      );
  }

  filterBy(cat: BenefitsCategory): void {
    this._isLoading = true;
    this.modal.hide();
    this.benefitsService.getBenefitByCategory(cat.Id)
      .subscribe(
        response => {
          this.allBenefist = response;
          this.cardData = [];
          this.fixModelCard(response);
          this._isLoading = false;
          this.showAll = false;
        }
      );
  }
}
