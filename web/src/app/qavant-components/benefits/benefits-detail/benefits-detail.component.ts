import {ActivatedRoute, Router} from '@angular/router';
import {Component, OnInit, OnDestroy} from '@angular/core';
import {BenefitsService} from '@services/benefits/benefits.service';
import {Benefits} from '@interface/benefits';
import {SettingsService} from '@services/settings/settings.service';
import {isNullOrUndefined} from 'util';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';

@Component({
  selector: 'app-benefits-detail',
  templateUrl: './benefits-detail.component.html',
  styleUrls: ['./benefits-detail.component.scss'],
  animations: [
    trigger('enterAnimation', [
      state('loading', style({
        opacity: '0',
        transform: 'translateY(8%)'
      })),
      state('ready', style({
        opacity: '1',
        transform: 'translateY(0)'
      })),
      transition('loading => ready', animate('300ms cubic-bezier(0.1, 0.0, 0.2, 1)'))
    ])
  ],
})
export class BenefitsDetailComponent implements OnInit, OnDestroy {
  private id: number;
  private request: any;
  limitBenefits = 8;
  someBenefits: Benefits[];
  benefit: Benefits;
  constructor(
    private route: ActivatedRoute,
    private benefitsService: BenefitsService,
    private router: Router,
    public settingsService: SettingsService
  ) { }

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.benefit = null;
      this.id = +params.id;
      this.getBenefitsDetail();
      this.getSuggestions();
    });
  }

  ngOnDestroy() {
    this.request.unsubscribe();
  }

  private getBenefitsDetail(): void {
    this.request =  this.benefitsService.getBenefitsById(this.id)
    .subscribe(
      response => {
        this.benefit = response;
      }
    );
  }

  hasBenefits(): boolean {
    return ( !isNullOrUndefined(this.benefit) );
  }

  getSuggestions(): void {
    if ( isNullOrUndefined(this.benefitsService.allBenefits) ) {
      this.benefitsService.getAllBenefits().subscribe(
        response => {
          this.someBenefits = response;
        }
      );
    } else {
      this.someBenefits = this.benefitsService.getStorageBenefits();
    }
  }

  goToBenefits(id: number): void {
    this.router.navigate(['beneficios', id]);
  }
}
