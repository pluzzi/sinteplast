import {Component, OnInit, ViewChild, OnDestroy} from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { DocumentsService } from '@services/documents/documents.service';
import { SettingsService } from '@services/settings/settings.service';
import { ModalDirective } from 'ngx-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss'],
  animations: [
    trigger('enterAnimation', [
      state('loading', style({
        opacity: '0',
        transform: 'translateY(8%)'
      })),
      state('ready', style({
        opacity: '1',
        transform: 'translateY(0)'
      })),
      transition('loading => ready', animate('300ms cubic-bezier(0.1, 0.0, 0.2, 1)'))
    ])
  ],
})
export class DocumentsComponent implements OnInit, OnDestroy {
  @ViewChild(ModalDirective, {static: false}) modal: ModalDirective;
  private id: number;
  basic_table_data;
  selected = [];
  showAll: boolean;
  documents: any;
  categories: any;
  request: any;
  loading = true;
    // Expandable Table Code;
  @ViewChild('expTable', {static: false}) expTable: any;
  expanded: any = {};

  // No Option YET
  // https://github.com/swimlane/ngx-datatable/issues/423
  scrollBarHorizontal = (window.innerWidth < 960);
  columnModeSetting = (window.innerWidth < 960) ? 'standard' : 'force';
  columnModeSettingSmall = (window.innerWidth < 560) ? 'standard' : 'force';
  constructor(
    public documentService: DocumentsService,
    public settingsService: SettingsService,
    private route: ActivatedRoute,
    private router: Router
    ) {
    // this.fetch((data) => {
    //  this.basic_table_data = data;
    // });
    window.onresize = () => {
      this.scrollBarHorizontal = (window.innerWidth < 960);
      this.columnModeSetting = (window.innerWidth < 960) ? 'standard' : 'force';
      this.columnModeSettingSmall = (window.innerWidth < 560) ? 'standard' : 'force';
    };
  }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.id = +params.id;
        if ( isNaN(this.id) ) {
          this.getAll();
        } else {
          const pattern = new RegExp('categoria');
          if (pattern.test(this.router.url) ) {
            this.filterBy(this.id);
          } else {
            this.getDocumentById();
          }
        }
    });
  }

  ngOnDestroy() {
    if ( this.request !== undefined ) {
      this.request.unsubscribe();
    }
  }

  filterBy(id: number): void {
    if ( this.modal !== undefined ) {
      this.modal.hide();
    }
    this.documentService.getDocumentsByCategory(id)
      .subscribe(
      response => {
        this.documents = response;
        this.showAll = false;
        this.loading = false;
      }
    );
  }

  getDocumentById(): void {
    this.request = this.documentService.getById(this.id)
      .subscribe(
        response => {
          this.documents = response;
          this.loading = false;
          this.showAll = false;
        }, error => {
          if ( error.status === 400 ) {
            this.documents = [];
          }
          this.loading = false;
        }
      );
  }

  hasDocuments(): boolean {
    return ( this.documents !== undefined );
  }

  getAll(): void {
    this.documentService.getDocuments()
    .subscribe(
      response => {
        this.documents = response;
        this.showAll = true;
      }
    );
    this.documentService.getCategories()
      .subscribe(
        response => {
          this.categories = response;
        }
      );
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  onActivate(event) {
  }

  // fetch(cb) {
  //   const req = new XMLHttpRequest();
  //   req.open('GET', `assets/data/table.json`);
  //   req.onload = () => {
  //     cb(JSON.parse(req.response));
  //   };
  //   req.send();
  // }



  toggleExpandRow(row) {
    this.expTable.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle(event) {
  }

}
