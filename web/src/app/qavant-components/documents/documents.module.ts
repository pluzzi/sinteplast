import {NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {QavantSharedModule} from '@shared';
import {DocumentsComponent} from '@qavant/documents/documents.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
const routes: Routes =
  [
    { path: '', component: DocumentsComponent },
    { path: ':id', component: DocumentsComponent },
    { path: 'categoria/:id', component: DocumentsComponent }
  ];
@NgModule({
  imports: [
    NgxDatatableModule,
    RouterModule.forChild(routes),
    QavantSharedModule,
  ],
  declarations: [DocumentsComponent]
})
export class DocumentsModule { }
