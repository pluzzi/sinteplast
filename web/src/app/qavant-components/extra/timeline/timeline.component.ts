import {Birthday } from './../../../qavant-interfaces/birthday';
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NgsRevealConfig} from 'ng-scrollreveal';
import {BirthdayService} from '@services/birthday/birthday.service';
import {SettingsService} from '@services/settings/settings.service';
import {User} from '@interface/user';
import {isNullOrUndefined } from 'util';
declare var pg: any;
@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit {
  // Config Docs
  // https://tinesoft.github.io/ng-scrollreveal/doc/injectables/NgsRevealConfig.html
  birthdays: Birthday[];
  today = [];
  users: any;
constructor(
  private config: NgsRevealConfig,
  private route: ActivatedRoute,
  private birthdayService: BirthdayService,
  private router: Router,
  public settingService: SettingsService,
  ) {
    // Only for demo - Casual and Executive layout
    this.route.params
      .subscribe(params => {
        if (params['type'] === 'container') {
          config.container = document.querySelector('.page-container');
        }
        config.distance = '0';
    });
  }

  ngOnInit() {
    this.birthdayService.getBirthdays()
      .subscribe(
        response => {
          this.birthdays = response;
        }
    );
  }

  hasBirthdays(): boolean {
    if ( !isNullOrUndefined(this.birthdays) ) {
      return (this.birthdays.length > 0);
    }
  }

  getUserName(user: Birthday): string {
    return `${user.Name} ${user.Surname}`;
  }

  goToProfile(id: number): void {
    this.router.navigate(['perfil', id]);
  }

  hasUserToday(): boolean {
    return (this.today.length > 0);
  }

  isToday(date: any): boolean {
    const user = new Date(date);
    const userFormat = user.getDate() + '-' + user.getMonth();
    const today = new Date();
    const todayformat = today.getDate() + '-' + today.getMonth();
    return ( userFormat === todayformat );
  }

  getUserArea(user: User): any {
    if ( isNullOrUndefined(user.Area) ) {
    return 'No ingresado';
    } else {
      return user.Area;
    }
  }

  getUserDateBirthDay(date): string {
    const date2 = new Date(date);
    return date2.toLocaleString('es-ar', {day: 'numeric', month: 'long'});
  }
}
