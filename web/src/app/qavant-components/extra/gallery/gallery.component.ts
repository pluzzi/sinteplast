import {Component, OnInit, ViewChild, ViewChildren, QueryList, AfterContentInit, AfterViewInit} from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {IsotopeOptions} from 'ngx-isotope';
import {GalleryService} from '@services/gallery/gallery.service';
import {Gallery} from '@interface/gallery';
import {SettingsService} from '@services/settings/settings.service';
import {ModalDirective} from 'ngx-bootstrap';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss'],
  animations: [
    trigger('enterAnimation', [
      state('loading', style({
        opacity: '0',
        transform: 'translateY(8%)'
      })),
      state('ready', style({
        opacity: '1',
        transform: 'translateY(0)'
      })),
      transition('loading => ready', animate('300ms cubic-bezier(0.1, 0.0, 0.2, 1)'))
    ])
  ],
})
export class GalleryComponent implements OnInit, AfterViewInit {
  @ViewChild(ModalDirective, {read: false, static: false}) modal: ModalDirective;
  galleries: Gallery[] = [];
  categories: any;
  config;
  configModal;
  index = 0;
  index2 = 0;
  _isLoading = true;
  rangeValue;
  showAll: boolean;
  public galleryOptions: IsotopeOptions = {
    itemSelector: '.gallery-item',
    masonry: {
      columnWidth: 280,
      gutter: 10,
      fitWidth: true
    }
  };

  constructor(
    private galleryService: GalleryService,
    public settingsService: SettingsService
  ) { }

  ngOnInit() {
    this.getCategories();
    this.getGalleries();

    this.config = {
      init: false,
      observer: true,
      direction: 'vertical',
      autoplay: {
        delay: 2000,
      }
    };

    // this.configModal = {
    //   init: false,
    //   observer: true,
    //   direction: 'horizontal',
    //   pagination: true
    // };
  }

  getGalleries(): void {
    this._isLoading = true;
    this.galleryService.getGallery()
      .subscribe(
        response => {
          this.galleries = response;
          this._isLoading = false;
          this.showAll = false;
      });
  }

  ngAfterViewInit(): void {
    // setTimeout(() => {
    //   this.swiperViewes.forEach(dir =>  {
    //     dir.init();
    //   });
    //   this.swiperViewes.first.startAutoplay();
    // }, 1000);
  }


  // showAll(): boolean {
  //   // this.getGalleries();
  //   return true;
  // }

  getCategories(): void {
    this.galleryService.getCategories()
    . subscribe(
      response => {
        this.categories = response;
      }
    );
  }

  hasGalleries(): boolean {
    return (this.galleries.length > 0);
  }

  filterBy(cat: any): void {
    this._isLoading = true;
    this.modal.hide();
    this.galleryService.filterByCategory(cat.Id)
    .subscribe(
      response => {
        this.galleries = response;
        this._isLoading = false;
        this.showAll = true;
      }
    );
  }
}