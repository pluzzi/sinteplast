import {NgModule } from '@angular/core';
import {RouterModule } from '@angular/router';

// Demo Page
import {BlankpageComponent} from './blankpage/blankpage.component';
import {GalleryComponent} from './gallery/gallery.component';
import {TimelineComponent} from './timeline/timeline.component';
import {InvoiceComponent } from './invoice/invoice.component';
import {ExtraRouts} from './extra.routing';
import {QavantSharedModule} from '@shared';
@NgModule({
  imports: [
    QavantSharedModule,
    RouterModule.forChild(ExtraRouts),
  ],
  declarations: [
    BlankpageComponent,
    GalleryComponent,
    TimelineComponent,
    InvoiceComponent,
  ]
})
export class ExtraModule { }
