import {Routes, RouterModule} from '@angular/router';
import {ProfileComponent} from './profile.component';
import {QavantSharedModule} from './../../qavant-shared/qavant-shared.module';
import {SharedModule} from './../../@pages/components/shared.module';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { FormsModule } from '@angular/forms';
const routes: Routes =
  [
    { path: '', component: ProfileComponent },
    { path: ':id', component: ProfileComponent }
  ];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    SharedModule,
    QavantSharedModule,
  ],
  declarations: [
    ProfileComponent
  ]
})
export class ProfileModule { }
