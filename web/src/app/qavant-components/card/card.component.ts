import {Component, OnInit, Input} from '@angular/core';
import {CardData} from '@interface/cardData';
import {SettingsService} from '@services/settings/settings.service';
import {isNullOrUndefined} from 'util';
import {Router} from '@angular/router';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() data: CardData;
  @Input() additionalClasses: string;
  constructor(
    public settingsService: SettingsService,
    private router: Router) { }
  ngOnInit() {
  }

  getId(): number {
    return this.data.Id;
  }

  hasDetail(): boolean {
    return (this.data.Tipo !== 'DOCUM');
  }

  getCategory(): string {
    return this.data.DescRegistro;
  }

  getType(): string {
    return this.data.Tipo;
  }

  isBirthdayCard(): boolean {
    return (this.data.Tipo === 'CUMPL');
  }

  isInfo(): boolean {
    return (this.data.Tipo === 'INFUT');
  }

  getTitle(): string {
    return this.data.Titulo;
  }

  getSubTitle(): string {
    return this.data.Copete;
  }

  getDate(): string {
    return this.data.FechaAlta;
  }

  getImage(): string {
    return this.data.Imagen;
  }

  hasImage(): boolean {
    return ( !isNullOrUndefined(this.data.Imagen) && this.data.Imagen !== '' );
  }

  getContent(): string {
    return this.data.Contenido;
  }

  outStanding(): boolean {
    return this.data.Destacada;
  }

  getReactionId(): number {
    return this.data.ReaccionId;
  }

  getTotalReactions(): number {
    return this.data.TotalReacciones;
  }

  verticalStyle(): boolean {
    return ( this.data.Tipo !== 'NOTIC' && this.data.Tipo !== 'BENEF' );
  }

  getRouter(): string {
    let route;
    switch (this.data.Tipo) {
      case 'NOTIC':
        route = '/noticias';
        break;
      case 'BENEF':
        route = '/beneficios';
        break;
      case 'ENCUE':
        route = '/encuestas';
        break;
      case 'DOCUM':
        route = '/documentos';
        break;
      case 'CUMPL':
        route = '/perfil';
        break;
      case 'CAPAC':
        route = '/capacitaciones';
        break;
      case 'INFUT':
        route = '/informacion';
        break;
      default:
        break;
    }
    return route;
  }
}
