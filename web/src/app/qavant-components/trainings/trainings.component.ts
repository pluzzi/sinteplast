import { Component, OnInit, OnDestroy } from '@angular/core';
import { TrainingsService } from '@services/trainings/trainings.service';
import { Training } from '@interface/training';
import { SettingsService } from '@services/settings/settings.service';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Router } from '@angular/router';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-trainings',
  templateUrl: './trainings.component.html',
  styleUrls: ['./trainings.component.scss'],
  animations: [
    trigger('enterAnimation', [
      state('loading', style({
        opacity: '0',
        transform: 'translateY(8%)'
      })),
      state('ready', style({
        opacity: '1',
        transform: 'translateY(0)'
      })),
      transition('loading => ready', animate('300ms cubic-bezier(0.1, 0.0, 0.2, 1)'))
    ])
  ],
})
export class TrainingsComponent implements OnInit, OnDestroy {
  trainingRequest: any;
  trainings: Training[];
  constructor(
    private router: Router,
    private trainingsService: TrainingsService,
    public settingsService: SettingsService,
  ) { }

  ngOnInit() {
    this.trainingRequest = this.trainingsService.getTrainings()
      .subscribe(
        response => {
          this.trainings = response;
        }
      );
  }

  ngOnDestroy() {
    this.trainingRequest.unsubscribe();
  }

  hasTraining(): boolean {
    return ( !isNullOrUndefined(this.trainings) );
  }

  goToDetail(id: number): void {
    this.router.navigate(['capacitaciones', id]);
  }

  hasVideo(training: Training): boolean {
    return ( training.CourseType === 'VIDEO' );
  }

  hasDocument(training: Training): boolean {
    return ( training.CourseType === 'DOCUMENTO' );
  }

  hasLink(training: Training): boolean {
    return ( training.CourseType === 'URL' );
  }
}
