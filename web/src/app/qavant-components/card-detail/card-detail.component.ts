import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {SettingsService} from '@services/settings/settings.service';
import {CardData} from '@interface/cardData';
import {isNullOrUndefined} from 'util';
@Component({
  selector: 'app-card-detail',
  templateUrl: './card-detail.component.html',
  styleUrls: ['./card-detail.component.scss'],
})
export class CardDetailComponent implements OnInit {
  @Input() model: any;
  @Input() type: string;
  @Input() suggestions: CardData[];
  @Output() goToDetail: EventEmitter<any> = new EventEmitter<any>();
  data: CardData;
  limitSomeNews = 8;
  id: number;
  constructor(
    public settingsService: SettingsService,
    ) { }

  ngOnInit() {
    if ( !isNullOrUndefined(this.model) ) {
      this.fixModel();
      this.getTitleSuggestons();
    }
  }

  fixModel(): void {
    this.data = {
      'Id': this.model.Id,
      'Titulo' : this.model.Title,
      'Contenido': this.model.Content,
      'FechaAlta': this.model.To,
      'Imagen': this.model.Image,
      'Tipo': this.type,
      'Copete': this.model.Subtitle,
      'ReaccionId': this.model.ReaccionId,
      'DescRegistro': '',
      'Destacada': false,
      'TotalReacciones': this.model.TotalReacciones
    };
  }

  hasData(): boolean {
    return ( !isNullOrUndefined(this.data) );
  }

  hasImage(): boolean {
    return ( !isNullOrUndefined(this.data.Imagen) );
  }

  getTitle(): string {
    return this.data.Titulo;
  }

  getImage(): string {
    return this.data.Imagen;
  }

  getType(): string {
    return this.data.DescRegistro;
  }

  getDate(): string {
    if ( !isNullOrUndefined(this.data.FechaAlta) ) {
      return this.data.FechaAlta;
    } else {
      return this.model.Date;
    }
  }

  getContent(): any {
    return this.data.Contenido;
  }

  getTitleSuggestons(): string {
    if ( this.data.Tipo === 'NOTIC') {
      this.data.DescRegistro = 'Noticias';
      return 'Otras Noticias';
    } else {
      this.data.DescRegistro = 'Beneficios';
      return 'Otros Beneficios';
    }
  }

  getSection(): string {
    if ( this.data.Tipo === 'NOTIC') {
      return 'noticias';
    } else {
      return 'beneficios';
    }
  }

  goToNewDetail(id: number): void {
    this.goToDetail.emit(id);
  }

  checkRepeatData(id: number): boolean {
    return (this.data.Id !== id);
  }
}
