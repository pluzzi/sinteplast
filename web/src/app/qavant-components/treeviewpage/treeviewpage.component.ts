import {Component, OnInit, ViewChild, OnDestroy, ViewEncapsulation } from '@angular/core';
import {
  TreeModel,
  TreeNode,
  TreeComponent} from 'angular-tree-component';
import {SettingsService} from '@services/settings/settings.service';
import {InformationService} from '@services/information/information.service';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
// import { ITreeNode, ITreeState } from 'angular-tree-component/dist/defs/api';
import { isNullOrUndefined } from '@swimlane/ngx-datatable/release/utils';
import { ActivatedRoute } from '@angular/router';
import { ITreeNode } from 'angular-tree-component/dist/defs/api';
@Component({
  selector: 'app-treeviewpage',
  templateUrl: './treeviewpage.component.html',
  styleUrls: ['./treeviewpage.component.scss'],
  animations: [
    trigger('enterAnimation', [
      state('loading', style({
        opacity: '0',
        transform: 'translateY(8%)'
      })),
      state('ready', style({
        opacity: '1',
        transform: 'translateY(0)'
      })),
      transition('loading => ready', animate('300ms cubic-bezier(0.1, 0.0, 0.2, 1)'))
    ])
  ],
  encapsulation: ViewEncapsulation.None,
})
export class TreeviewpageComponent implements OnInit, OnDestroy {
  @ViewChild(TreeComponent, {read: false, static: false})
  private tree: TreeComponent;
  content = [];
  id: number;
  detail: any;
  nodes: any;
  searchnodes: any;
  options: any;
  informationRequest: any;
  _loading = true;
  // public state: ITreeState;
  // simpleNodes = [];
  // optionsdng: any;
  // ITreeOptions: any;
  // searchnodes: ITreeOptions = {
  //   getChildren: this.getChildren.bind(this)
  // };
  constructor(
    public settingsService: SettingsService,
    public informationService: InformationService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.options = {
      animateExpand: true,
    };
    this.route.params.subscribe( params => {
      this.id = +params.id;
    });
    this.informationService.getCategories()
      .subscribe(
        response => {
          this.getAllInformation();
        }
      );
    // this.searchnodes = [
    //   {
    //     name: 'North America',
    //     children: [
    //       { name: 'United States',
    //         children: [
    //           {name: 'New York', children: [{name : 'Barrio', children: [{name : 'Barrio2', children: [{name: 'otro'}]}]}]},
    //           {name: 'California'},
    //           {name: 'Florida'}
    //         ]
    //       },
    //       { name: 'Canada' },
    //       { name: 'Santi'},
    //       { name: 'Carpeta', children: [{name: ''}]}
    //     ]
    //   },
    //   {
    //     name: 'South America',
    //     children: [
    //       { name: 'Argentina', children: [] },
    //       { name: 'Brazil' }
    //     ]
    //   },
    //   {
    //     name: 'Europe',
    //     children: [
    //       { name: 'England' },
    //       { name: 'Germany' },
    //       { name: 'France' },
    //       { name: 'Italy' },
    //       { name: 'Spain' }
    //     ]
    //   }
    // ];
    // this.options = {
    //   animateExpand: false,
    // };
    // this.optionsdng = {
    //   animateExpand: true,
    //   allowDrag: true,
    //   allowDrop: true
    // };
    // options_checkbox: this.ITreeOptions = {
    //   useCheckbox: true
    // };
  }


  ngOnDestroy() {
    this.informationRequest.unsubscribe();
  }

  hasTreeCategories(): boolean {
    return (this.informationService.tree.length > 0);
  }

  hasImage(): boolean {
    return ( isNullOrUndefined(this.detail.Image) || this.detail.Image !== '' );
  }

  getAllInformation(): void {
    this.informationRequest = this.informationService.getInformationRequest()
    .subscribe(
      response => {
        this.content = response;
        if ( this.hasTreeCategories() && this.content.length > 0) {
          this.informationService.setAllInformation(response);
          this.tree.treeModel.update();
          if ( !isNaN (this.id) ) {
            this.getInfo(this.id);
            const someNode = this.tree.treeModel.getNodeById(this.id);
            someNode.expand();
            this.tree.treeModel.expandAll();
            // const firstRoot = this.tree.treeModel.roots[0];
            // firstRoot.setActiveAndVisible();
          }
        }
        this._loading = false;
      }
    );
  }

  onEvent = ($event) => {
    if ($event.node.parent.data.children[0].children.length < 1) {
      console.log($event);
    }
  }

  filterFn(value: string, treeModel: TreeModel) {
    treeModel.filterNodes((node: TreeNode) => this.fuzzysearch(value, node.data.name));
  }

  hasInfo(): boolean {
    return isNullOrUndefined(this.detail);
  }

  hasSubtitle(): boolean {
    return ( !isNullOrUndefined(this.detail.Copete) && this.detail.Copete !== '' );
  }

  getChildren(event: any): void {
    if (event.node.data.info) {
      this.getInfo(event.node.id);
    }
  }

  getInfo(id: any): any {
    if ( !isNullOrUndefined(id) ) {
      const index = this.content.findIndex(i => i.Id === id);
      if ( !isNullOrUndefined(index) && index !== -1 ) {
        if ( isNullOrUndefined(this.detail) ) {
          this.detail = this.content[index];
        } else {
          if ( this.detail.Id !== this.content[index].id) {
            this.detail = this.content[index];
          }
        }
      }
    }
  }

  fuzzysearch (needle: string, haystack: string) {
    const haystackLC = haystack.toLowerCase();
    const needleLC = needle.toLowerCase();
    const hlen = haystack.length;
    const nlen = needleLC.length;
    if (nlen > hlen) {
      return false;
    }
    if (nlen === hlen) {
      return needleLC === haystackLC;
    }
    outer: for (let i = 0, j = 0; i < nlen; i++) {
      const nch = needleLC.charCodeAt(i);
      while (j < hlen) {
        if (haystackLC.charCodeAt(j++) === nch) {
          continue outer;
        }
      }
      return false;
    }
    return true;
  }

  // getChildren(node: any) {
  //   const newNodes = this.asyncChildren.map((c) => Object.assign({}, c));
  //   return new Promise((resolve, reject) => {
  //     setTimeout(() => resolve(newNodes), 1000);
  //   });
  // }

}
