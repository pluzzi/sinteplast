﻿using QavantWeb.Models;

namespace QavantWeb.ViewModels
{
    public class DocumentoViewModel
    {
        public DocumentoViewModel()
        {
            FiltroConfig = new FiltroConfigViewModel();
        }

        public Document Documento { get; set; }

        public FiltroConfigViewModel FiltroConfig { get; set; }
    }
}