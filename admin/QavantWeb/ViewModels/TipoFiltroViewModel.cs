﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantWeb.ViewModels
{
    public class TipoFiltroViewModel
    {
        public int? Id { get; set; }
        public string Nombre { get; set; }
    }
}