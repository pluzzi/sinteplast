﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantWeb.ViewModels
{
    public class SegmentacionViewModel
    {
        public SegmentacionViewModel()
        {
            SegmentoCollection = new List<SegmentoViewModel>();
        }

        public List<SegmentoViewModel> SegmentoCollection { get; set; }

    }
}