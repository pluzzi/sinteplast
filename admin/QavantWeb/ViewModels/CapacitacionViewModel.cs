﻿using QavantWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantWeb.ViewModels
{
    public class CapacitacionViewModel
    {
        public CapacitacionViewModel()
        {
            FiltroConfig = new FiltroConfigViewModel();
        }

        public Cours Capacitacion { get; set; }
        public FiltroConfigViewModel FiltroConfig { get; set; }
    }
}