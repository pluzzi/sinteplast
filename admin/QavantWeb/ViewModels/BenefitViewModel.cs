﻿using QavantWeb.Models;

namespace QavantWeb.ViewModels
{
    public class BenefitViewModel
    {
        public BenefitViewModel()
        {
            FiltroConfig = new FiltroConfigViewModel();
        }

        public Benefit Benefit { get; set; }

        public FiltroConfigViewModel FiltroConfig { get; set; }
    }
}