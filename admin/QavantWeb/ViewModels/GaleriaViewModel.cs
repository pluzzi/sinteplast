﻿using QavantWeb.Models;

namespace QavantWeb.ViewModels
{
    public class GaleriaViewModel
    {
        public GaleriaViewModel()
        {
            FiltroConfig = new FiltroConfigViewModel();
        }

        public Galeria Galeria { get; set; }

        public FiltroConfigViewModel FiltroConfig { get; set; }
    }
}