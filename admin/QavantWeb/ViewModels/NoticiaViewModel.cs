﻿using QavantWeb.Models;

namespace QavantWeb.ViewModels
{
    public class NoticiaViewModel
    {
        public NoticiaViewModel()
        {
            FiltroConfig = new FiltroConfigViewModel();
        }

        public Noticia Noticia { get; set; }

        public FiltroConfigViewModel FiltroConfig { get; set; }
    }
}