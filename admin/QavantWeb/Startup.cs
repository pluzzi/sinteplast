﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(QavantWeb.Startup))]
namespace QavantWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
