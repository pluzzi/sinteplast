﻿$("#btn_guardar").on('click', function () {
    var isValid = true;

    if ($('#Image').val() == "") {
        addToValidationSummary("Seleccione una imagen.");
        isValid = false;
    }

    return isValid;
});