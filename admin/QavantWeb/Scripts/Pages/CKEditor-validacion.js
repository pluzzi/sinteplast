﻿// Tiene dependencias con validation-summary.js
// Tiene dependencias con CKEditor-is-there-content.js
$("#btn_guardar").on('click', function () {
    var isValid = isCKEditorValid();

    if (!isValid) {
        addToValidationSummary("Ingrese contenido.");
    }

    return isValid;
});