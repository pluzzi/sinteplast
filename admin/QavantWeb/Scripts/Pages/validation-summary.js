﻿$("#myAlert").ready(function () {
    showOrHideErrorAlert();
});

function showOrHideErrorAlert() {
    var errorsCount = $('#myAlert > ul > li:not([style*="display:none"])').length;
    console.log("hay " + errorsCount + " errores.");
    if (errorsCount > 0) {
        $("#myAlert").alert().show();
        window.scrollTo(0, 0);
    }
    else {
        $("#myAlert").alert().hide();
    }
}



$('#myAlert ul').bind('DOMNodeInserted', function () {
    showOrHideErrorAlert();
});

function addToValidationSummary(message) {
    if ($('#myAlert > ul > li:contains("' + message + '")').length > 0)
        return;
    $('#myAlert > ul').append('<li>' + message + '</li>');
}
