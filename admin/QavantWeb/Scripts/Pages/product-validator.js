﻿$("#btn_guardar").on('click', function () {
    var isValid = true;

    if ($("#Benefit_Puntos").val()=="") {
        addToValidationSummary("Ingrese un valor para los puntos.");
        isValid = false;
    }

    if (isNaN($("#Benefit_Puntos").val())) {
        addToValidationSummary("Debe ingresar un número entero.");
        isValid = false;
    } else {
        if ($("#Benefit_Puntos").val() % 1 != 0) {
            addToValidationSummary("Debe ingresar un número entero.");
            isValid = false;
        }
    }

    if ($("#Benefit_TipoCatalogoProductosId option:selected").val() == "") {
        addToValidationSummary("Seleccione un catálogo.");
        isValid = false;
    }

    if ($("#Benefit_EsNuevo option:selected").val() == "") {
        addToValidationSummary("Seleccione si es nuevo o no.");
        isValid = false;
    }



    return isValid;
});