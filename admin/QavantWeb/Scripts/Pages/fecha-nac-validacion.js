﻿$(".close").click(function () {
    $("#myAlert").alert().hide();
});

$("#btn_guardar").on('click', function () {
    $("#myAlert").alert().hide();
    Mensaje = "";
    var anioMin = "1900-01-01";
    var isValid = true;
    var hoy = new Date();
    var dd = hoy.getDate();
    if (dd < 10) {
        dd = '0' + dd
    }
    var mm = hoy.getMonth() + 1;
    if (mm < 10) {
        mm = '0' + mm
    }
    var yyyy = hoy.getFullYear();
    var fechaActual = yyyy + "-" + mm + "-" + dd;
    var fechaActualFormateada = dd + "/" + mm + "/" + yyyy;

    //validar fechas
    if ($("#fecha_nac").val() != "") {
        if ($("#fecha_nac").val() < anioMin) {
            addToValidationSummary("- Por favor, ingrese una fecha mayor o igual a 01/01/1900. <br>");
            isValid = false;
        }
        if ($("#fecha_nac").val() > fechaActual) {
            addToValidationSummary("- Por favor, ingrese una fecha menor o igual a " + fechaActualFormateada + ". <br>");
            isValid = false;
        }
    }
    if ($("#fecha_nac").val() == "") {
        addToValidationSummary("- Debe ingresar una fecha de nacimiento. <br>");
        isValid = false;
    }
    return isValid;

})