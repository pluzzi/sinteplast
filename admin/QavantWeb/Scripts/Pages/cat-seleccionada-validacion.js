﻿// Tiene dependencias con validation-summary.js
$("#btn_guardar").on('click', function () {
    var isValid = true;

    // validar si hay al menos una categoría seleccionada.
    if ($("#lista:checked").length == 0) {
        addToValidationSummary("Ingrese al menos una categoría.");
        isValid = false;
    }

    return isValid;
});