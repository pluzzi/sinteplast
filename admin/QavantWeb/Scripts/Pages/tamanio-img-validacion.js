﻿function validateImageSize(fileInput) {
    var tam = fileInput.files[0].size;
    if (tam > 1 * 1204 * 1024) {
        alert("La imagen seleccionada no debe superar 1MB de tamaño.");
        fileInput.value = "";
    };
}