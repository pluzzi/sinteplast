﻿function validateFileSize(fileInput) {
    var tam = fileInput.files[0].size;

    if (tam > 8 * 1204 * 1024) {
        alert("El archivo seleccionado no debe superar 8MB de tamaño.");
        fileInput.value = "";
    }
}