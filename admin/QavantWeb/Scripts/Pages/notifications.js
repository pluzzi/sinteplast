﻿$(document).ready(function () {
    $("#myAlert").alert().hide();
    $("#BeneficioGroup").hide();
    $("#NoticiaGroup").hide();
    $("#CursoGroup").hide();
    $("#EncuestaGroup").hide();
    $("#FormularioGroup").hide();
    $("#InfoUtilGroup").hide();
    $("#GaleriaGroup").hide();

    $("#SectionId").on('change', function () {
        $("#BeneficioGroup").hide();
        $("#NoticiaGroup").hide();
        $("#CursoGroup").hide();
        $("#EncuestaGroup").hide();
        $("#FormularioGroup").hide();
        $("#InfoUtilGroup").hide();
        $("#GaleriaGroup").hide();
        var foo = $("#SectionId").val();
        switch ($("#SectionId").val()) {
            case "1":
                $("#BeneficioGroup").show();
                break;
            case "2":
                $("#NoticiaGroup").show();
                break;
            case "17":
                $("#EncuestaGroup").show();
                break;
            case "19":
                $("#CursoGroup").show();
                break;
            case "21":
                $("#GaleriaGroup").show();
                break;
            case "25":
                $("#FormularioGroup").show();
                break;
            case "26":
                $("#InfoUtilGroup").show();
                break;

        }
    });


    setFiltersOptions();
    bindDeletes();

    $("#FilterType").on('change', function () {
        setFiltersOptions();
    });

    $("#btn_FilterAdd").on('click', function () {

        itemNro = $("#tbl_Filters tbody tr").length;
        description = "";
        value = "";
        tipoDesc = "";
        switch ($("#FilterType").val()) {
            case "GROUP":
                description = $("#FilterGroups option:selected").text();
                value = $("#FilterGroups").val();
                tipoDesc = "Grupo";
                break;
            case "SEX":
                description = $("#FilterGenders option:selected").text();
                value = $("#FilterGenders").val();
                tipoDesc = "Género";
                break;
            case "AREA":
                description = $("#FilterAreas option:selected").text();
                value = $("#FilterAreas").val();
                tipoDesc = "Área";
                break;
        }

        if (value == "") {
            alert("Debe seleccionar una opción");
            return;
        }

        tipo = $("#FilterType").val();
        notificationId = $("#Id").val();

        if (notificationId == undefined) {
            notificationId = '0';
        }

        var newFilter = "";
        newFilter = "<tr id=\"tr_fil_" + itemNro + "\">";
        newFilter += "<td>" + tipoDesc + "</td>";
        newFilter += "<td>" + description + "</td>";

        newFilter += "<td>";
        newFilter += "<button id=\"btn_optDelete\" actionid=\"" + itemNro + "\" type=\"button\" class=\"btn btnDelete btn-default\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></button>";
        newFilter += "<input type=\"hidden\" id=\"hdn_NotificationsFiltersAction_" + itemNro + "\" name=\"NotificationsFilters[" + itemNro + "].Action\" value=\"A\" />";

        newFilter += "<input type=\"hidden\" id=\"hdn_NotificationsFiltersId_" + itemNro + "\" name=\"NotificationsFilters[" + itemNro + "].Id\" value=\"0\" />";
        newFilter += "<input type=\"hidden\" id=\"hdn_NotificationsFiltersNotificationId_" + itemNro + "\" name=\"NotificationsFilters[" + itemNro + "].NotificationId\" value=\"" + notificationId + "\" />";
        newFilter += "<input type=\"hidden\" id=\"hdn_NotificationsFiltersFilteType_" + itemNro + "\" name=\"NotificationsFilters[" + itemNro + "].FilteType\" value=\"" + tipo + "\" />";
        newFilter += "<input type=\"hidden\" id=\"hdn_NotificationsFiltersFilterValue_" + itemNro + "\" name=\"NotificationsFilters[" + itemNro + "].FilterValue\" value=\"" + value + "\" />";

        newFilter += "</td></tr>";

        $("#tbl_Filters tbody").append(newFilter);

        bindDeletes();
    });

});

function bindDeletes() {
    $(".btnDelete").unbind("click");

    $(".btnDelete").click(function () {
        if (!confirm("Seguro desea eliminar este filtro?")) {
            return;
        }

        var index = $(this).attr('actionid');
        $("#hdn_NotificationsFiltersAction_" + index).val('B');
        $(this).parents("tr").remove();
        //$("#tr_fil_" + index).hide();
 
    });

}


function setFiltersOptions() {
    $(".filterOptions").hide();
    switch ($("#FilterType").val()) {
        case "SEX":
            $("#FilterGender_group").show();
            break;
        case 'AREA':
            $("#FilterAreas_group").show();
            break;
        case 'GROUP':
            $("#FilterGroups_group").show();
            break;
    }
}

function setFormRelated() {
    $(".formRelated").hide();
    if ($('#RequiresEvaluation').is(':checked')) {
        $(".formRelated").show();
    }
}

$(".close").click(function () {
    $("#myAlert").alert().hide();
});

function confirmMessages() {
    $("#myAlert").alert().hide();
    valor = undefined;
    Mensaje = "";
   
    zfitro = $("#tbl_Filters tbody tr").length;

    switch ($("#SectionId").val()) {
        case "1":
            valor = $("#BenefitId option:selected").val();
            break;
        case "2":
            valor = $("#NewsId option:selected").val();
            break;
        case "17":
            valor = $("#EncuestaGroup option:selected").val();
            break;
        case "19":
            valor = $("#CursoGroup option:selected").val();
            break;
        case "21":
            valor = $("#GaleriaGroup option:selected").val();
            break;
        case "25":
            valor = $("#FormularioGroup option:selected").val();
            break;
        case "26":
            valor = $("#InfoUtilGroup option:selected").val();
            break;
    }

    if ($("#Title").val() == "") {
        Mensaje = Mensaje + "- Debe ingresar un título. <br>";
    }
    if ($("#Message").val() == "") {
        Mensaje = Mensaje + "- Debe ingresar un mensaje. <br>";
    }
    if ($("#SectionId").val() == "") {
        Mensaje = Mensaje + "- Debe ingresar una sección. <br>";
    }
    if (valor == "") {
        Mensaje = Mensaje + "- Seleccione un detalle para la sección: " + $("#SectionId option:selected").text();
    }

    if (Mensaje != "") {
        $("#msgAlert").html("<strong>Controlar los siguientes errores:</strong><br >" + Mensaje);
        $("#myAlert").alert().show();
        return false;
    }
    else {
        if (zfitro == 0) {
            $("#myModal").modal();
            return false;
        }

    }

}



