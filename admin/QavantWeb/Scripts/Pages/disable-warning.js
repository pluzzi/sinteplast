﻿$("#chbHabilitado").on('click', function () {
    if (!$('#chbHabilitado').is(":checked")) {
        if ($("#checkId").val() > 0) {
            $("#myModal").modal();
            return false;
        }
        else {
            return true;
        }
    }
    else {
        return true;
    }
});

$("#btn-diseable-warning-yes").on('click', function () {
    $('#chbHabilitado').prop('checked', false);
});