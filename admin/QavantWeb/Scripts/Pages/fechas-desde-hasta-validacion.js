﻿// Tiene dependencias con validation-summary.js
$("#btn_guardar").on('click', function () {
    // inicializaciones
    var anioMin = "1900-01-01";
    var isValid = true;

    // validar fechas
    if ($("#from_Id").length == 1 && $("#from_Id").val() != "") {
        if ($("#from_Id").val() < anioMin) { 
            addToValidationSummary("Por favor, ingrese una fecha 'desde' mayor o igual a 01/01/1900.");
            isValid = false;
        }
    }
    if ($("#to_Id").length == 1 && $("#to_Id").val() != "") {
        if ($("#to_Id").val() < anioMin) { 
            addToValidationSummary("Por favor, ingrese una fecha 'hasta' mayor o igual a 01/01/1900.");
            isValid = false;
        }
    }
    if ($("#from_Id").length == 1 && $("#to_Id").length == 1 && $("#from_Id").val() != "" && $("#to_Id").val() != "") {
        if ($("#from_Id").val() > $("#to_Id").val()) { 
            addToValidationSummary("La fecha de fin no puede ser menor a la fecha de inicio.");
            isValid = false;
        }
    }
    if ($("#from_Id").length == 1 && $("#from_Id").val() == "") { 
        addToValidationSummary("Debe ingresar una fecha para el campo 'desde'.");
        isValid = false;
    }
    if ($("#to_Id").length == 1 && $("#to_Id").val() == "") { 
        addToValidationSummary("Debe ingresar una fecha para el campo 'hasta'.");
        isValid = false;
    }

    return isValid;
});