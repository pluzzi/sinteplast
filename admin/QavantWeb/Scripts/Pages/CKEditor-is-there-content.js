﻿function isCKEditorValid() {
    var isValid = true;
    // validar si se ha ingresado algo en el ckEditor.
    var editor_val = CKEDITOR.instances.__Content.document.getBody().getChild(0).getText();
    if ($.trim(editor_val) == '') {
        isValid = false;
    }

    return isValid;
}