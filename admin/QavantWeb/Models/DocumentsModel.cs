﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QavantWeb.Models
{    
    [MetadataType(typeof(DocumentsModel))]
    public partial class Document
    {
    }

    public class DocumentsModel
    {
        [Required(ErrorMessage = "Ingrese una categoría. Si no existen categorías, deberá crear al menos una.")]
        public int CategoriaId { get; set; }

        [Required(ErrorMessage = "Ingrese un título.")]
        [StringLength((150), ErrorMessage = "El título no debe superar los 150 caracteres")]
        [Display(Name = "Título")]
        public string Title { get; set; }

        [DataType(DataType.MultilineText)]
        [StringLength((300), ErrorMessage = "El subtítulo no debe superar los 300 caracteres")]
        [Display(Name = "Subtítulo")]
        public string Description { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}")]
        public System.DateTime CreatedDate { get; set; }
    }
}