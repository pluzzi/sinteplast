//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QavantWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Form
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Form()
        {
            this.Courses = new HashSet<Cours>();
            this.FormsRespCabeceras = new HashSet<FormsRespCabecera>();
            this.Notifications = new HashSet<Notification>();
            this.FormsQuestions = new HashSet<FormsQuestion>();
        }
    
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public System.DateTime CreationDate { get; set; }
        public System.DateTime DateFrom { get; set; }
        public System.DateTime DateTo { get; set; }
        public string CreationUserId { get; set; }
        public bool Enabled { get; set; }
        public bool UniqueReply { get; set; }
        public bool SendMails { get; set; }
        public string MailsTo { get; set; }
        public string MailSubject { get; set; }
        public string MailTitle { get; set; }
        public string MailIntro { get; set; }
        public string Code { get; set; }
        public byte FormTypeId { get; set; }
        public bool Deleted { get; set; }
        public bool CanTheySelfEvaluate { get; set; }
        public int EmpresaId { get; set; }
        public Nullable<int> CategoriaId { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual Categoria Categoria { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cours> Courses { get; set; }
        public virtual Empresa Empresa { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FormsRespCabecera> FormsRespCabeceras { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Notification> Notifications { get; set; }
        public virtual FormsType FormsType { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FormsQuestion> FormsQuestions { get; set; }
    }
}
