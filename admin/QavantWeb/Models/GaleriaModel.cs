﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QavantWeb.Models
{
    [MetadataType(typeof(GaleriaModel))]
    public partial class Galeria
    {

    }

    public class GaleriaModel
    { 
        [Required(ErrorMessage = "Ingrese un título.")]
        [StringLength((150), ErrorMessage = "El título no debe superar los 150 caracteres")]
        [Display(Name = "Título")]
        public string Titulo { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Ingrese un subtítulo.")]
        [StringLength((300), ErrorMessage = "El subtítulo no debe superar los 300 caracteres")]
        [Display(Name = "Subtítulo")]
        public string Copete { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Ingrese fecha desde.")]
        [Display(Name = "Fecha desde")]
        public System.DateTime FechaDesde { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Ingrese fecha hasta.")]
        [Display(Name = "Fecha hasta")]
        public System.DateTime FechaHasta { get; set; }

        //[Required(ErrorMessage = "Ingrese una imagen de portada.")]
        [Display(Name = "Imagen Portada")]
        public string ImagenPortada { get; set; }

        [Required(ErrorMessage = "Ingrese una categoría.")]
        [Display(Name = "Categoría")]
        public int CategoriaId { get; set; }
    }
}