﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QavantWeb.Models
{
    [MetadataType(typeof(FormsQuestionGroupMetaData))]
    public partial class FormsQuestionGroup
    {

    }
    public class FormsQuestionGroupMetaData
    {
        [Required(ErrorMessage = "Ingrese la Descripcion. Campo obligatorio.")]
        [Display(Name = "Descripcion")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Ingrese el orden. Campo obligatorio.")]
        [Display(Name = "Orden")]
        public short Order { get; set; }
        [Display(Name = "Habilitada")]
        public bool Enabled { get; set; }
        [Display(Name = "Mostrar Promedio")]
        public bool ShowsAverage { get; set; }
    }
}