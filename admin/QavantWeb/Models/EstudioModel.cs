﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QavantWeb.Models
{
    [MetadataType(typeof(EstudioMetaData))]
    public partial class Estudio
    {

    }

    public class EstudioMetaData
    {
        [Display(Name = "Estudios")]
        [Required(ErrorMessage = "Ingrese estudios.")]
        public int TipoEstudioCatalogoId { get; set; }

        [Display(Name = "Título")]
        [Required(ErrorMessage = "Ingrese título.")]
        public Nullable<int> TituloId { get; set; }

        [Display(Name = "Establecimiento")]
        [Required(ErrorMessage = "Ingrese Establecimiento.")]
        public Nullable<int> EstablecimientoId { get; set; }

        [Display(Name = "Fecha desde")]
        [Required(ErrorMessage = "Ingrese fecha desde.")]
        public Nullable<System.DateTime> FechaDesde { get; set; }

        [Display(Name = "Fecha hasta")]
        [Required(ErrorMessage = "Ingrese fecha hasta.")]
        public Nullable<System.DateTime> FechaHasta { get; set; }

        [Display(Name = "Estudios completos")]
        [Required(ErrorMessage = "Ingrese opción.")]
        public Nullable<bool> EstudioCompleto { get; set; }

    }
}