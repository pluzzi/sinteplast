﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantWeb.Models
{
    public abstract class Enums
    {


        public enum SectionsIds { Beneficios = 1, Noticias = 2, AppGeneral = 11, Encuestas = 17, Cursos = 19, Galeria = 21, Formularios = 25, InfoUtil = 26 };

        //public enum ActionsLog { Visitas = 1, MeGusta = 2, NoMeGusta = 3 };

        public enum FormsTypes { Encuesta = 1, Formulario = 2, Evaluacion = 3, EvaluacionDesempenio = 4 };

        public enum FormsQuestionsTypes { TEXT, OPTION, DROPDOWN, TEXTAREA, DATE, MULTIPLE };

        public enum CoursesTypes { Video = 1, Documento = 2, Link = 3 };

        public enum CoursesFilterTypes { SEX, GROUP, AREA };

        public enum NotificationsFilterTypes { SEX, GROUP, AREA };

        public enum DocumentLibrary { Planillas = 1, Cursos = 2 };

        public enum DocumentTypes { Documento = 1, Video = 2, Archivo = 3 };


        //Nuevos Enums:

        public enum Canales { App = 1, Web = 2, Admin = 3};

        public enum Modulos {
            Documentos = 1, Capacitaciones = 2, Muro = 3,
            MiPerfil = 4, Noticias = 5, QuienEsQuien = 6,
            Cumpleanos = 7, Beneficios = 8, Encuestas = 9,
            InfoUtil = 10, TerminosYCondiciones = 11,
            Login = 13, Activacion = 14, OlvidoPass = 15,
            FormulariosGenerales = 16,
            Params = 24,
            Galerias = 26
        }

        public enum LogTipos {VisitaLista = 1, VisitaDetalle = 2, VisitaXCategorias = 3, AccionExitosa =4 , AccionFallida =5, Descarga=6,Respuesta=7}

        public enum EmpresasLayoutCodigos { DEFAULT, SERIE1, SERIE2, SERIE3, SERIE4, SERIE5 };

        public enum Filtros {
            Genero = 1,
            Grupos = 2,
            Canal = 3,
            Areas = 4,
            Region = 5,
            Sucursal = 6
        }
        
    }

}