//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QavantWeb.Models
{
    using System;
    
    public partial class log_GetFactorValidacion_Result
    {
        public Nullable<int> id { get; set; }
        public Nullable<bool> RegistroOk { get; set; }
        public string Factor { get; set; }
        public string Descripcion { get; set; }
    }
}
