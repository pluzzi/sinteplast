﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;

namespace QavantWeb.Models
{
    
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    //Puede agregar datos de perfil para el usuario agregando más propiedades a su clase ApplicationUser
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
        // agrego los campos para pasar por parametro a new ApplicationUser 
        public string Name { get; set; }
        public string Surname { get; set; }

        public int EmpresaId { get; set; }
        public bool Enable { get; set; }

    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        //public System.Data.Entity.DbSet<QavantWeb.Models.Notification> Notifications { get; set; }

        //public System.Data.Entity.DbSet<QavantWeb.Models.Benefit> Benefits { get; set; }

        //public System.Data.Entity.DbSet<QavantWeb.Models.Noticias> Noticias { get; set; }

        //public System.Data.Entity.DbSet<QavantWeb.Models.Section> Sections { get; set; }

        //public System.Data.Entity.DbSet<QavantWeb.Models.ActionLogType> ActionLogTypes { get; set; }

        //public System.Data.Entity.DbSet<QavantWeb.Models.ActionLog> ActionLogs { get; set; }

        //public System.Data.Entity.DbSet<QavantWeb.Models.User> Users1 { get; set; }
    }
}