//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QavantWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class InfoUtil
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InfoUtil()
        {
            this.Notifications = new HashSet<Notification>();
        }
    
        public int Id { get; set; }
        public int EmpresaId { get; set; }
        public Nullable<int> CategoriaId { get; set; }
        public string Titulo { get; set; }
        public string Copete { get; set; }
        public string Html { get; set; }
        public string Icono { get; set; }
        public Nullable<short> Orden { get; set; }
        public bool Destacada { get; set; }
        public System.DateTime FechaAlta { get; set; }
        public string AspNetUsersIdAlta { get; set; }
        public Nullable<System.DateTime> FechaBaja { get; set; }
        public string AspNetUsersIdBaja { get; set; }
        public string Imagen { get; set; }
        public Nullable<bool> Habilitado { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual AspNetUser AspNetUser1 { get; set; }
        public virtual Categoria Categoria { get; set; }
        public virtual Empresa Empresa { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Notification> Notifications { get; set; }
    }
}
