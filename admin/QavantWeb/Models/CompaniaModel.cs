﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace QavantWeb.Models
{
    [MetadataType(typeof(CompaniaMetaData))]
    public partial class Compania
    {
    }

    public class CompaniaMetaData
    {
        [Required(ErrorMessage = "Ingrese Descripción")]
        [StringLength((100), ErrorMessage = "La descripción no debe superar los 100 caracteres.")]
        [Display(Name = "Descripción")]
        public string Descripcion { get; set; }

    }
}