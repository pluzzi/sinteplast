﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantWeb.Models
{
    public class FormReportingGrouppedViewModel
    {
        public string Pregunta { get; set; }
        public string Respuesta { get; set; }
        public int Cantidad { get; set; }
    }
    public class FormReportingDetailedViewModel
    {
        public int UsuarioId { get; set; }
        public string Usuario { get; set; }
        public IEnumerable<FormAnswersViewModel> Respuestas { get; set; }
    }

    public class FormReportingDetailedHeaderViewModel
    {
        public int PreguntaId { get; set; }
        public string Pregunta { get; set; }
    }

    public class FormAnswersViewModel
    {
        public int PreguntaId {get; set;}
        public DateTime Fecha { get; set; }
        public string Respuesta { get; set; }
    }
}