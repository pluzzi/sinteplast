﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QavantWeb.Models
{
    [MetadataType(typeof(InfoUtilModel))]
    public partial class InfoUtil
    {
    }

    public class InfoUtilModel
    {
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Ingrese título.")]
        [StringLength((150), ErrorMessage = "El título no debe superar los 150 caracteres")]
        [Display(Name = "Título")]
        public string Titulo { get; set; }

        [DataType(DataType.MultilineText)]
        [StringLength((300), ErrorMessage = "El subtítulo no debe superar los 300 caracteres")]
        [Display(Name = "Subtítulo")]
        public string Copete { get; set; }

        [Required(ErrorMessage = "Seleccione una categoría.")]
        public int CategoriaId { get; set; }
    }
}