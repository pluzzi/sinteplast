﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantWeb.Models
{
    public class PuntoExcelRow
    {
        public string User { get; set; }
        public string Catalogo { get; set; }
        public int Puntos { get; set; }
        public string Mensaje { get; set; }
    }
}