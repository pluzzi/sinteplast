﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QavantWeb.Models
{
    [MetadataType(typeof(NotificationsModel))]
    public partial class Notification  
    {
        public Nullable<int> SurveysId { get; set; }
        public Nullable<int> GaleriaId { get; set; }
    }

    public class NotificationsModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Ingrese título")]
        [StringLength((150), ErrorMessage = "El título no debe superar los 150 caracteres")]
        [Display(Name = "Título")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Ingrese mensaje")]
        [DataType(DataType.MultilineText)]
        [StringLength((500), ErrorMessage = "El mesaje no debe superar los 500 caracteres")]
        [Display(Name = "Mensaje")]
        public string Message { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public System.DateTime CreationDate { get; set; }

        [Required(ErrorMessage = "Ingrese sección")]
        public byte SectionId { get; set; }

        public Nullable<int> BenefitId { get; set; }

        public Nullable<int> NewsId { get; set; }

        public string Status { get; set; }
        


    }
 }

