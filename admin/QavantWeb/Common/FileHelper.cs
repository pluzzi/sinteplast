﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace QavantWeb.Common
{
    public static class FileHelper
    {
        /// <summary>
        /// Devuelve true si la extensión del archivo es una de las siguientes:
        /// .doc .docx .xls .xlsx .ppt .pps .pptx .ppsx .pdf .jpg .jpeg .png
        /// </summary>
        /// <param name="file">Archivo cuya extensión se desea evaluar.</param>
        /// <returns>True si la extensión de archivo es válida, false en caso contrario.</returns>
        public static bool ValidateFileExtension(HttpPostedFileBase file)
        {
            string ext = Path.GetExtension(file.FileName);
            bool isValid = !String.IsNullOrEmpty(ext) && (
                    ext.Equals(".pdf", StringComparison.OrdinalIgnoreCase)
                    ||
                    ext.Equals(".jpg", StringComparison.OrdinalIgnoreCase)
                    ||
                    ext.Equals(".jpeg", StringComparison.OrdinalIgnoreCase)
                    ||
                    ext.Equals(".png", StringComparison.OrdinalIgnoreCase)
                    ||
                    ext.Equals(".doc", StringComparison.OrdinalIgnoreCase)
                    ||
                    ext.Equals(".docx", StringComparison.OrdinalIgnoreCase)
                    ||
                    ext.Equals(".xls", StringComparison.OrdinalIgnoreCase)
                    ||
                    ext.Equals(".xlsx", StringComparison.OrdinalIgnoreCase)
                    ||
                    ext.Equals(".ppt", StringComparison.OrdinalIgnoreCase)
                    ||
                    ext.Equals(".pps", StringComparison.OrdinalIgnoreCase)
                    ||
                    ext.Equals(".pptx", StringComparison.OrdinalIgnoreCase)
                    ||
                    ext.Equals(".ppsx", StringComparison.OrdinalIgnoreCase)
                    ||
                    ext.Equals(".ai", StringComparison.OrdinalIgnoreCase)
                    ||
                    ext.Equals(".psd", StringComparison.OrdinalIgnoreCase)
                    ||
                    ext.Equals(".xlsm", StringComparison.OrdinalIgnoreCase)
                    );
            return isValid;
        }

        public static bool ValidateFileSize(HttpPostedFileBase file)
        {
            var isValid = file != null && file.FileName != String.Empty && file.ContentLength != 0;
            return isValid;
        }

        public static bool ValidateFileExistence(HttpPostedFileBase file) {
            var isValid = file != null && file.FileName != String.Empty;
            return isValid;
        }

    }
}