﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using QavantWeb.Common;
using Microsoft.AspNet.Identity.Owin;


namespace QavantWeb.Common
{
    public static class SecurityHelpers
    {
        public static int getEmpresaId()
        {
            try
            {
                return HttpContext.Current.GetOwinContext()
                                        .GetUserManager<ApplicationUserManager>()
                                        .FindById(HttpContext.Current.User.Identity.GetUserId()).EmpresaId;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public static bool IsAdminEnabled(string username) {
            bool result;
            var user =   HttpContext.Current.GetOwinContext()
                                        .GetUserManager<ApplicationUserManager>()
                                        .FindByName(username);
            if (user == null)
            {
                result = false;
                return result;
            }

            result = user.Enable;
            return result;
        }
    }
}