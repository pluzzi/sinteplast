﻿using System;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace QavantWeb.Common
{

    public static class StringHelpers
    {
        /// <summary>
        /// Elimina todo caracter que no sea número y letra.
        /// </summary>
        /// <param name="str">string al que se le eliminarán caracteres</param>
        /// <returns>string inicial pero sólo con números y letras</returns>
        public static string SoloLetrasYNumeros(this string str)
        {
            var test1 = Regex.Replace(str, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
            var strOutput = Regex.Replace(str, "[^0-9A-Za-z .]", "", RegexOptions.Compiled);
            return strOutput;
        }

        /// <summary>
        /// Elimina acentos de un string dado.
        /// </summary>
        /// <param name="text">string al que se le eliminarán los acentos</param>
        /// <returns>string inicial sin acentos</returns>
        public static string RemueveAcentos(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return text;

            text = text.Normalize(NormalizationForm.FormD);
            var chars = text.Where(c => CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark).ToArray();
            return new string(chars).Normalize(NormalizationForm.FormC);
        }

        public static string ReemplazaCaracteresEspeciales(this string texto)
        {
            var txtOutput = texto.Replace("'", "");
            return txtOutput;


        }
    }
}