﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantWeb.Functions
{
    public class QueueItem
    {
        public QueueItem(string to, string htmlMessage, string subject)
        {
            To = to;
            HtmlMessage = htmlMessage;
            Subject = subject;
        }

        public string To { get; set; }
        public string HtmlMessage { get; set; }
        public string Subject { get; set; }
    }
}