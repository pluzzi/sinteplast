﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QavantWeb.Models;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using QavantWeb.Common;
using QavantWeb.Services;
using QavantWeb.ViewModels;
using QavantWeb.Services.Settings;
using QavantWeb.Infrastructure.FileStorage;
using Ganss.XSS;

namespace QavantWeb.Controllers
{
    public class BenefitsController : Controller
    {
        private const string DELETE_MARK = "B";
        private QavantEntities db = new QavantEntities();
        private int empresaId = SecurityHelpers.getEmpresaId();
        private int moduloId = (int)Enums.Modulos.Beneficios;
        private HtmlSanitizer _sanitizer = new HtmlSanitizer();

        private readonly IFiltroService _filterService;
        private readonly ISettingsService _settingsService;
        private readonly IFileStorageFactory _fileStorageFactory;

        public BenefitsController() : this(new SettingsService(Enums.Modulos.Beneficios), new FiltroService(), new FileStorageFactory()) { }

        public BenefitsController(ISettingsService settingsService, IFiltroService filterService, IFileStorageFactory fileStorageFactory)
        {
            _filterService = filterService;
            _settingsService = settingsService;
            _fileStorageFactory = fileStorageFactory;
        }

        // GET: Benefits
        [Authorize(Roles = "Administrador")]
        public ActionResult Index()
        {
            var cat = db.Categorias
                                .Where(w => w.ModuloId == (int)Enums.Modulos.Beneficios)
                                .Where(w => w.EmpresaId == empresaId)
                                .Where(w => w.Habilitada)
                                .ToList().OrderBy(x => x.Nombre);
            ViewBag.Categories = db.Categorias
                                        .Where(w => w.ModuloId == (int)Enums.Modulos.Beneficios)
                                        .Where(w => w.EmpresaId == empresaId)
                                        .Where(w => w.Habilitada)
                                        .ToList().OrderBy(x => x.Nombre);

            List<TipoCatalogoProducto> list = db.TipoCatalogoProductos.ToList();
            SelectList tipoCatalogoProducto = new SelectList(list, "Id", "Descripcion");
            ViewBag.TipoCatalogoProducto = tipoCatalogoProducto;

            return View(db.Benefits.Where(w => w.EmpresaId == empresaId).Where(x => x.Enabled).ToList().OrderByDescending(x => x.Date));
        }

        // GET: Benefits/Details/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Benefit benefit = db.Benefits.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            if (benefit == null)
            {
                return HttpNotFound();
            }
            return View(benefit);
        }

        // GET: Benefits/Create
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            List<Categoria> lista = new List<Categoria>();
            List<Categoria> list = db.Categorias
                                        .Where(w => w.EmpresaId == empresaId)
                                        .Where(w => w.ModuloId == (int)Enums.Modulos.Beneficios)
                                        .Where(w => w.Habilitada).ToList();
            Common.FormatterHelpers.CategoriasToTree(list, ref lista, null, "", true);
            ViewBag.lista = lista;

            var viewModel = new BenefitViewModel();
            var beneficio = new Benefit();
            viewModel.Benefit = beneficio;
            viewModel.FiltroConfig.Filtros = _filterService.GetFiltros(empresaId);

            List<TipoCatalogoProducto> listCatalogo = db.TipoCatalogoProductos.ToList();
            SelectList tipoCatalogoProducto = new SelectList(listCatalogo, "Id", "Descripcion");
            ViewBag.TipoCatalogoProducto = tipoCatalogoProducto;

            return View(viewModel);
        }

        // POST: Benefits/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult Create(BenefitViewModel model, List<int> lista)
        {
            try
            {
                model.Benefit.Likes = 0;  //like not null
                model.Benefit.Enabled = true;

                if (lista == null || lista.Count == 0)
                    ModelState.AddModelError("", "Ingrese al menos una categoría.");

                model.Benefit.Content = _sanitizer.Sanitize(Request.Form["__Content"]);
                if (model.Benefit.Content == null || model.Benefit.Content.Length == 0)
                    ModelState.AddModelError("", "Ingrese contenido.");

                if (!ModelState.IsValid)
                {
                    model.Benefit.BenefitCategories = new List<BenefitCategory>();
                    //agrego las nuevas categorias
                    if (lista != null)
                    {
                        foreach (int item in lista)
                        {
                            Categoria a = db.Categorias
                                            .Where(w => w.ModuloId == (int)Enums.Modulos.Beneficios)
                                            .Where(w => w.EmpresaId == empresaId)
                                            .Where(w => w.Habilitada).FirstOrDefault(f => f.Id == item);
                            BenefitCategory b = new BenefitCategory();
                            b.Categoria = a;
                            b.CategoriaId = a.Id;
                            model.Benefit.BenefitCategories.Add(b);
                        }
                    }
                    List<Categoria> nlista = db.Categorias
                                            .Where(w => w.ModuloId == (int)Enums.Modulos.Beneficios)
                                            .Where(w => w.EmpresaId == empresaId)
                                            .Where(w => w.Habilitada).ToList();//
                    ViewBag.lista = nlista;
                    return View(model);
                }
                SaveModel(model, lista);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error en el alta.  \n " + ex.Message);
                model.FiltroConfig.Filtros = _filterService.GetFiltros(empresaId);

                model.Benefit.BenefitCategories = new List<BenefitCategory>();
                //agrego las nuevas categorias
                if (lista != null)
                {
                    foreach (int item in lista)
                    {
                        Categoria a = db.Categorias
                                        .Where(w => w.ModuloId == (int)Enums.Modulos.Beneficios)
                                        .Where(w => w.EmpresaId == empresaId)
                                        .Where(w => w.Habilitada).FirstOrDefault(f => f.Id == item);
                        BenefitCategory b = new BenefitCategory();
                        b.Categoria = a;
                        b.CategoriaId = a.Id;
                        model.Benefit.BenefitCategories.Add(b);
                    }
                }
                List<Categoria> nlista = db.Categorias
                                        .Where(w => w.ModuloId == (int)Enums.Modulos.Beneficios)
                                        .Where(w => w.EmpresaId == empresaId)
                                        .Where(w => w.Habilitada).ToList();//
                ViewBag.lista = nlista;
                return View(model);
            }


            return RedirectToAction("Index");
        }

        private void SaveModel(BenefitViewModel model, List<int> lista)
        {
            using (DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    foreach (int item in lista)
                    {
                        Categoria catModuloBenef = db.Categorias
                                            .Where(w => w.ModuloId == (int)Enums.Modulos.Beneficios)
                                            .Where(w => w.EmpresaId == empresaId)
                                            .Where(w => w.Habilitada).FirstOrDefault(f => f.Id == item);
                        BenefitCategory benefCat = new BenefitCategory();
                        benefCat.Categoria = catModuloBenef;
                        benefCat.CategoriaId = catModuloBenef.Id;
                        model.Benefit.BenefitCategories.Add(benefCat);
                    }

                    HttpPostedFileBase file = Request.Files["Image"];
                    if (file != null && file.ContentLength != 0)
                    {
                        model.Benefit.Image = SaveFile(file);
                    }
                    model.Benefit.CreationUserId = User.Identity.GetUserId();
                    model.Benefit.Date = DateTime.Now;
                    model.Benefit.EmpresaId = empresaId;
                    db.Benefits.Add(model.Benefit);
                    db.SaveChanges();

                    //Segmentación
                    foreach (var item in model.FiltroConfig.Segmentacion.SegmentoCollection)
                    {
                        if (item.Action == DELETE_MARK)
                            continue;
                        var segmentoDb = new Segmentacion
                        {
                            EmpresaId = empresaId,
                            ModuloId = moduloId,
                            EntidadId = model.Benefit.Id,
                            FiltroId = item.TipoFiltroId,
                            FiltroValor = item.ValorFiltroId
                        };
                        db.Segmentacions.Add(segmentoDb);
                    }
                    db.SaveChanges();
                    dbTran.Commit();
                }
                catch (Exception ex)
                {
                    dbTran.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }
        // GET: Benefits/Edit/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id)
        {
            //agrego la lista de categorias
            // beneficios = db.Benefits.Where(w => w.EmpresaId == empresaId).Include(a => a.BenefitCategories).ToList().Find(c => c.Id == id);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Benefit benefit = db.Benefits.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            if (benefit == null)
            {
                return HttpNotFound();
            }

            List<Categoria> lista = new List<Categoria>();
            List<Categoria> list = db.Categorias
                                        .Where(w => w.EmpresaId == empresaId)
                                        .Where(w => w.ModuloId == (int)Enums.Modulos.Beneficios)
                                        .Where(w => w.Habilitada).ToList();
            Common.FormatterHelpers.CategoriasToTree(list, ref lista, null, "", true);
            ViewBag.lista = lista;

            var viewModel = new BenefitViewModel();
            viewModel.Benefit = benefit;
            viewModel.FiltroConfig.Filtros = _filterService.GetFiltros(empresaId);
            viewModel.FiltroConfig.Segmentacion = _filterService.GetSegmentacion(empresaId, moduloId, benefit.Id);

            List<TipoCatalogoProducto> listCatalogo = db.TipoCatalogoProductos.ToList();
            SelectList tipoCatalogoProducto = new SelectList(listCatalogo, "Id", "Descripcion");
            ViewBag.TipoCatalogoProducto = tipoCatalogoProducto;

            return View(viewModel);
        }

        // POST: Benefits/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(BenefitViewModel model, string foto, List<int> lista)
        {
            var fDesde = model.Benefit.From.Value;
            var fHasta = model.Benefit.To.Value;
            if (fDesde > fHasta)
                ModelState.AddModelError("", "La fecha de fin no puede ser menor a la fecha de inicio.");

            if (model.Benefit.EmpresaId != empresaId)
                ModelState.AddModelError("", "Error al actualizar el registro.");

            model.Benefit.Image = (string.IsNullOrEmpty(foto)) ? string.Empty : foto;
            if (model.Benefit.Image == "")
                model.Benefit.Image = null;

            //me traigo el beneficio de la bd y le vuelvo a poner los likes que tenía
            //var bl = db.Benefits.Find(benefit.Id);
            var benefitLikes = db.Benefits.Where(w => w.EmpresaId == empresaId).Where(w => w.Id == model.Benefit.Id).FirstOrDefault();
            model.Benefit.Likes = benefitLikes.Likes;
            db.Entry(benefitLikes).State = EntityState.Detached; //con Detached:la entidad no será tenida en cuenta a la hora de aplicar cambios. Lo agrego xq sino lanza una excepción

            model.Benefit.Enabled = true;

            model.Benefit.Content = _sanitizer.Sanitize(Request.Form["__Content"]);
            //Asigno primero la imagen anterior para saltear la validacion
            //benefit.Image = db.Benefits.First(x => x.Id == benefit.Id).Image;

            if (lista == null || lista.Count == 0)
                ModelState.AddModelError("", "Ingrese al menos una categoría.");

            if (model.Benefit.Content == null || model.Benefit.Content.Length == 0)
                ModelState.AddModelError("", "Ingrese contenido.");

            if (!ModelState.IsValid)
            {
                model.Benefit.BenefitCategories.Clear();
                if (lista != null)
                {
                    foreach (int item in lista)
                    {
                        Categoria a = db.Categorias
                                        .Where(w => w.ModuloId == (int)Enums.Modulos.Beneficios)
                                        .Where(w => w.EmpresaId == empresaId)
                                        .Where(w => w.Habilitada)
                                        .OrderBy(o => o.Nombre).FirstOrDefault(f => f.Id == item);
                        BenefitCategory b = new BenefitCategory();
                        b.Categoria = a;
                        b.CategoriaId = a.Id;
                        model.Benefit.BenefitCategories.Add(b);
                    }
                }

                List<Categoria> catLista = new List<Categoria>();
                List<Categoria> list = db.Categorias
                                            .Where(w => w.EmpresaId == empresaId)
                                            .Where(w => w.ModuloId == (int)Enums.Modulos.Beneficios)
                                            .Where(w => w.Habilitada).ToList();
                Common.FormatterHelpers.CategoriasToTree(list, ref catLista, null, "", true);
                ViewBag.lista = catLista;
                
                model.FiltroConfig.Filtros = _filterService.GetFiltros(empresaId);
                model.FiltroConfig.Segmentacion = _filterService.GetSegmentacion(empresaId, moduloId, model.Benefit.Id);
                return View(model);
            }

            UpdateModel(model, lista);
            return RedirectToAction("Index");
        }

        private void UpdateModel(BenefitViewModel model, List<int> lista)
        {
            using(DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    HttpPostedFileBase file = Request.Files["ImageFile"];
                    if (file != null && file.ContentLength != 0)
                    {
                        model.Benefit.Image = SaveFile(file);
                    }

                    //1 guardo los datos de beneficio
                    model.Benefit.CreationUserId = User.Identity.GetUserId();
                    model.Benefit.Date = DateTime.Now;
                    db.Entry(model.Benefit).State = EntityState.Modified;
                    db.SaveChanges();

                    //2 recupero el benef y sus categ
                    Benefit c = db.Benefits.Where(w => w.EmpresaId == empresaId).Include(a => a.BenefitCategories).ToList().Find(ca => ca.Id == model.Benefit.Id);

                    //3 borro las antiguas categ
                    //c.BenefitCategories.Clear();
                    if (c.BenefitCategories.Count > 0)
                        db.BenefitCategories.RemoveRange(c.BenefitCategories);
                    //c.BenefitAreas.Clear();
                    db.SaveChanges();

                    //4 agrego las nuevas categorias
                    foreach (int item in lista)
                    {
                        Categoria a = db.Categorias
                                            .Where(w => w.ModuloId == (int)Enums.Modulos.Beneficios)
                                            .Where(w => w.EmpresaId == empresaId)
                                            .Where(w => w.Habilitada)
                                            .OrderBy(o => o.Nombre).FirstOrDefault(f => f.Id == item);
                        BenefitCategory b = new BenefitCategory();
                        b.Categoria = a;
                        b.CategoriaId = a.Id;
                        c.BenefitCategories.Add(b);
                    }

                    //5 vuelvo a guardar 
                    db.SaveChanges();

                    // Segmentación
                    db.Segmentacions.RemoveRange(db.Segmentacions.Where(x => x.EmpresaId == empresaId && x.ModuloId == moduloId && x.EntidadId == model.Benefit.Id));
                    foreach (var segmento in model.FiltroConfig.Segmentacion.SegmentoCollection)
                    {
                        if (segmento.Action != DELETE_MARK)
                        {
                            var segmentoDb = new Segmentacion
                            {
                                EmpresaId = empresaId,
                                ModuloId = moduloId,
                                EntidadId = model.Benefit.Id,
                                FiltroId = segmento.TipoFiltroId,
                                FiltroValor = segmento.ValorFiltroId
                            };
                            db.Segmentacions.Add(segmentoDb);
                        }
                    }
                    db.SaveChanges();
                    dbTran.Commit();

                }
                catch (Exception ex)
                {
                    dbTran.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        // GET: Benefits/Delete/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Benefit benefit = db.Benefits.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            if (benefit == null)
            {
                return HttpNotFound();
            }
            return View(benefit);
        }

        // POST: Benefits/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult DeleteConfirmed(int id)
        {
            Benefit benefit = db.Benefits.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            benefit.CreationUserId = User.Identity.GetUserId();
            benefit.Enabled = false;
            //db.Benefits.Remove(benefit);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Benefits        
        [Authorize(Roles = "Administrador")]
        public ActionResult IndexDeleted()
        {
            return View(db.Benefits.Where(w => w.EmpresaId == empresaId).Where(x => x.Enabled == false).ToList().OrderByDescending(x => x.Date));
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult Enable(int id)
        {
            Benefit benefit = db.Benefits.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            benefit.CreationUserId = User.Identity.GetUserId();
            benefit.Enabled = true;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Internal members
        private string SaveFile(HttpPostedFileBase file)
        {
            var parametros = _settingsService.GetParametrosGlobales();
            var fileStorage = _fileStorageFactory.CreateFileStorage(parametros);
            var filename = FileStorageHelper.FormatFilename(file, parametros);
            var container = parametros["ContainerBeneficios"];
            return fileStorage.Save(file, filename, container);
        }
        #endregion
    }
}
