﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QavantWeb.Models;
/**/
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Threading.Tasks;
using System.Globalization;
using System.Security.Claims;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web.Security;
using QavantWeb.Common;

namespace QavantWeb.Controllers
{
    
    public class AspNetUsersController : Controller
    {       
        private QavantEntities db = new QavantEntities();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private int empresaId = SecurityHelpers.getEmpresaId();

        public AspNetUsersController()
        {
        }

        public AspNetUsersController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
   

        // GET: AspNetUsers
        [Authorize(Roles = "Administrador")]
        public ActionResult Index()
        {
            var users = db.AspNetUsers.Where(w => w.EmpresaId == empresaId).Include(u => u.AspNetRoles).Where(x => x.Enable == true);
            var aux = users.Count();
            ViewBag.actualUserId = User.Identity.GetUserId();
            
            return View(users.ToList());
        }


        // GET: AspNetUsers/IndexDeleted
        [Authorize(Roles = "Administrador")]
        public ActionResult IndexDeleted()
        {
            var users = db.AspNetUsers.Where(w => w.EmpresaId == empresaId).Include(u => u.AspNetRoles).Where(x => !x.Enable);
            return View(users.ToList());
        }

        // GET: AspNetUsers/Details/5       
        [Authorize(Roles = "Administrador")]
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUser);
        }


        private static string ComposeUsernamePostfix()
        {
            var result = "." + EmpresayHelpers.getEmpresaNemonico();
            return result;
        }


        // GET: AspNetUsers/Create
        //[AllowAnonymous]
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            List<AspNetRole> lista = db.AspNetRoles.ToList();
            ViewBag.lista = lista;

            AspNetUserCreate aspNetUser = new AspNetUserCreate();
            aspNetUser.UsernamePostfix = ComposeUsernamePostfix();

            return View(aspNetUser);
        }


        private bool IsThereAnotherAdminWithSameUsername(String username, int empresaId, string userId)
        {
            int count = db.AspNetUsers.Count(us => us.UserName.ToLower().Trim() == username.ToLower().Trim()
                                              &&
                                              us.EmpresaId == empresaId
                                              &&
                                              us.Id != userId);
            return count > 0; 
        }


        // POST: AspNetUsers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        // [AllowAnonymous] que no entren por fuera
        [ValidateAntiForgeryToken]
        //public ActionResult Create(AspNetUser aspNetUser)       
        [Authorize(Roles = "Administrador")]       
        public async Task<ActionResult> Create(CreateViewModel model, AspNetUserCreate aspNetUser, List<int> lista)
        {
            AspNetRole a = db.AspNetRoles.Find(lista[0].ToString());
            if (ModelState.IsValid)
            {               
                var user = new ApplicationUser { Name = model.Name,
                                                Surname = model.Surname,
                                                UserName = model.UserName + aspNetUser.UsernamePostfix,
                                                Email = model.Email,
                                                PhoneNumber = model.PhoneNumber };
                user.EmpresaId = empresaId;
                user.Enable = true;
                IdentityResult result = null;
                if (IsThereAnotherAdminWithSameUsername(user.UserName, user.EmpresaId, user.Id))
                {
                    ModelState.AddModelError("", "Su empresa ya tiene un administrador con el nombre de usuario: " + user.UserName + ".");   
                }
                else
                {
                    result = await UserManager.CreateAsync(user, model.Password);//se fija si lo datos estan ok
                }
                

                if (ModelState.IsValid && result != null && result.Succeeded)
                {
                    await this.UserManager.AddToRoleAsync(user.Id, a.Name);
                }

                //retorno la lista de roles 
                List<AspNetRole> nlista = db.AspNetRoles.ToList();
                ViewBag.lista = nlista;

                if(result != null && !result.Succeeded)
                {
                    AddIdentityErrorsToModelState(model, result);
                    return View(aspNetUser);
                }

                if (!ModelState.IsValid)
                {
                    return View(aspNetUser);
                }

                return RedirectToAction("Index", "AspNetUsers");
            }
            else
            {
                AspNetRole aux;
                List<AspNetRole> nnlista = db.AspNetRoles.ToList();
                List<AspNetRole> auxList = nnlista;
                //que quede seleccionado el rol que eligió
                for (int i = 0; i < nnlista.Count; i++)
                {
                    for (int j = 0; j < auxList.Count; j++)
                    {
                        if(nnlista[i].Id == a.Id)
                        {
                            aux = auxList[i];
                            auxList[i] = auxList[j];
                            auxList[j] = aux;
                        }
                    }
                }


                ViewBag.lista = nnlista;//? o auxList
                return View(aspNetUser);
            }
         
        }

        private void AddIdentityErrorsToModelState(CreateViewModel model, IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                if (error.Substring(0, error.IndexOf(" ")) == "Name")
                {
                    ModelState.AddModelError("", "El Usuario " + model.UserName + " ya existe.");
                }
                if (error.Substring(0, error.IndexOf(" ")) == "Email")
                {
                    ModelState.AddModelError("", "El Email " + model.Email + " ya existe.");
                }
                if (error.Substring(0, error.IndexOf(" ")) == "Passwords")
                {
                    ModelState.AddModelError("", "La contraseña debe contener al menos 6 caracteres, incluyendo como mínimo una letra y un número. ");
                }
            }
        }


        // GET: AspNetUsers/Edit/5       
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(string id)
        {
            //agrego la lista de roles
            AspNetUser user = db.AspNetUsers.Where(w => w.EmpresaId == empresaId).Include(a => a.AspNetRoles).ToList().Find(c => c.Id == id);

            if (user.EmpresaId != empresaId)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }

            AspNetUserEdit userViewModel = new AspNetUserEdit();
            userViewModel.UsernamePostfix = ComposeUsernamePostfix();
            userViewModel.UserName = aspNetUser.UserName.Replace(userViewModel.UsernamePostfix, "");
            userViewModel.Email = aspNetUser.Email;
            userViewModel.Name = aspNetUser.Name;
            userViewModel.Surname = aspNetUser.Surname;
            userViewModel.EmpresaId = aspNetUser.EmpresaId;

            List<AspNetRole> lista = db.AspNetRoles.ToList();
            ViewBag.lista = lista;

            return View(userViewModel);
        }

        // POST: AspNetUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]       
        [Authorize(Roles = "Administrador")]
        public async Task<ActionResult> Edit(EditViewModel model, AspNetUserEdit aspNetUser, List<int> lista)
        {
            //valido el nuevo password ingresado
            await validatePassword(model);

            //guardo los datos viejos en user
            AspNetUser userOld = db.AspNetUsers.Find(aspNetUser.Id);
            //le asigno la que ya tenía
            if (aspNetUser.PasswordHash == null)
            {
                aspNetUser.PasswordHash = userOld.PasswordHash;
                aspNetUser.SecurityStamp = userOld.SecurityStamp;
            }
            if (aspNetUser.EmpresaId != empresaId)
                ModelState.AddModelError("", "Error al actualizar el registro.");

            AspNetRole rol = db.AspNetRoles.Find(lista[0].ToString());//mi rol

            if (ModelState.IsValid)
            {
                if (IsThereAnotherAdminWithSameUsername(aspNetUser.UserName + aspNetUser.UsernamePostfix, aspNetUser.EmpresaId, aspNetUser.Id))
                {
                    ModelState.AddModelError("", "Su empresa ya tiene un administrador con el nombre de usuario: " + aspNetUser.UserName + aspNetUser.UsernamePostfix + ".");
                    List<AspNetRole> nnlista = db.AspNetRoles.ToList();
                    ViewBag.lista = nnlista;
                    return View(aspNetUser);
                }
                else //si no existe un usuario con ese nombre           
                {
                    ApplicationUser u = UserManager.FindById(aspNetUser.Id);

                    // borro las antiguas categ   
                    u.Roles.Clear();
                    UserManager.AddToRole(u.Id, rol.Name);
                    u.UserName = aspNetUser.UserName + aspNetUser.UsernamePostfix;
                    u.Email = aspNetUser.Email;
                    u.Name = aspNetUser.Name;
                    u.Surname = aspNetUser.Surname;

                    if (aspNetUser.Password != null) //si ingresa un nuevo password
                    {
                        u.PasswordHash = UserManager.PasswordHasher.HashPassword(aspNetUser.Password);
                    }

                    UserManager.Update(u);
                }

                return RedirectToAction("Index", "AspNetUsers");
            }
            else
            {
                AspNetRole aux;
                List<AspNetRole> nlista = db.AspNetRoles.ToList();
                List<AspNetRole> auxList = nlista;

                for (int i = 0; i < nlista.Count; i++)
                {
                    for (int j = 0; j < auxList.Count; j++)
                    {
                        if (nlista[i].Id == rol.Id)
                        {
                            aux = auxList[i];
                            auxList[i] = auxList[j];
                            auxList[j] = aux;
                        }
                    }
                }

                ViewBag.lista = nlista;
                return View(aspNetUser);
            }
        }

        private async Task validatePassword(EditViewModel model)
        {
            if (model.Password != null)
            {
                PasswordValidator ser = new PasswordValidator();
                ser.RequiredLength = 6;
                ser.RequireNonLetterOrDigit = false;
                ser.RequireDigit = true;
                ser.RequireLowercase = true;
                ser.RequireUppercase = false;

                IdentityResult res = await ser.ValidateAsync(model.Password);

                if (!res.Succeeded)
                {
                    ModelState.AddModelError("", "La contraseña debe contener al menos 6 caracteres, incluyendo como mínimo una letra y un número. ");
                }
            }
        }

        // GET: AspNetUsers/Delete/5       
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(string id)
        {
            //agrego la lista de roles
            AspNetUser user = db.AspNetUsers.Include(a => a.AspNetRoles).Where(w=>w.EmpresaId==empresaId).ToList().Find(c => c.Id == id);

            /**/
            List<AspNetRole> lista = db.AspNetRoles.ToList();
            ViewBag.lista = lista;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUser);
        }

        // POST: AspNetUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]        
        [Authorize(Roles = "Administrador")]
        public ActionResult DeleteConfirmed(string id)
        {
            AspNetUser user = db.AspNetUsers.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f=>f.Id == id);
            user.Enable = false;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: AspNetUsers/Enable/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Enable(string id)
        {
            AspNetUser user = db.AspNetUsers.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            user.Enable = true;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        
    }
    
}
