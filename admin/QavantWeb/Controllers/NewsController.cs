﻿using Ganss.XSS;
using QavantWeb.Common;
using QavantWeb.Infrastructure.FileStorage;
using QavantWeb.Models;
using QavantWeb.Services;
using QavantWeb.Services.Settings;
using QavantWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace QavantWeb.Controllers
{
    public class NewsController : Controller
    {
        private const string DELETE_MARK = "B";
        private QavantEntities db = new QavantEntities();
        private int empresaId = SecurityHelpers.getEmpresaId();
        private HtmlSanitizer _sanitizer = new HtmlSanitizer();
        private int moduloId = (int)Enums.Modulos.Noticias;

        private readonly IFiltroService _filtroService;
        private readonly ISettingsService _settingsService;
        private readonly IFileStorageFactory _fileStorageFactory;

        public NewsController() : this(new SettingsService(Enums.Modulos.Noticias), new FiltroService(), new FileStorageFactory()) { }

        public NewsController(ISettingsService settingsService, IFiltroService filtroService, IFileStorageFactory fileStorageFactory)
        {
            _settingsService = settingsService;
            _fileStorageFactory = fileStorageFactory;
            _filtroService = filtroService;
        }

        // GET: Noticias        
        [Authorize(Roles = "Administrador")]
        public ActionResult Index()
        {
            return View(db.Noticias.Where(w => w.EmpresaId == empresaId).Where(x => x.Habilitada).ToList().OrderByDescending(x => x.FechaDesde));
        }

        // GET: Noticias/Details/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Noticia news = db.Noticias.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        // GET: Noticias/Create
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            var viewModel = new NoticiaViewModel();
            var noticia = new Noticia();
            noticia.FechaDesde = DateTime.Today;
            viewModel.Noticia = noticia;
            viewModel.FiltroConfig.Filtros = _filtroService.GetFiltros(empresaId);
            
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult Create(NoticiaViewModel model)
        {
            model.Noticia.Contenido = _sanitizer.Sanitize(Request.Form["__Content"]);
            if (model.Noticia.Contenido == null || model.Noticia.Contenido.Length == 0)
                ModelState.AddModelError("", "El campo 'Contenido' es obligatorio.");

            if (!ModelState.IsValid) {
                model.FiltroConfig.Filtros = _filtroService.GetFiltros(empresaId);
                return View(model);
            }

            Save(model);
            return RedirectToAction("Index");
        }

        // GET: Noticias/Edit/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Noticia noticia = db.Noticias.Where(w => w.EmpresaId == empresaId).FirstOrDefault(w=>w.Id==id);
            if (noticia == null)
                return HttpNotFound();

            var viewModel = new NoticiaViewModel();
            viewModel.Noticia = noticia;
            viewModel.FiltroConfig.Filtros = _filtroService.GetFiltros(empresaId);
            viewModel.FiltroConfig.Segmentacion = _filtroService.GetSegmentacion(empresaId, moduloId, noticia.Id);

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(NoticiaViewModel model, string foto)
        {
            if (model.Noticia.EmpresaId != empresaId)
                ModelState.AddModelError("", "Error al actualizar el registro.");

            model.Noticia.Contenido = _sanitizer.Sanitize(Request.Form["__Content"]);
            if (model.Noticia.Contenido == null || model.Noticia.Contenido.Length == 0)
                ModelState.AddModelError("", "El campo 'Contenido' es obligatorio.");

            model.Noticia.Image = (string.IsNullOrEmpty(foto)) ? string.Empty : foto;
            if (model.Noticia.Image == "")
                model.Noticia.Image = null;

            if (!ModelState.IsValid)
            {
                model.FiltroConfig.Filtros = _filtroService.GetFiltros(empresaId);
                model.FiltroConfig.Segmentacion = _filtroService.GetSegmentacion(empresaId, moduloId, model.Noticia.Id);
                return View(model);
            }

            Update(model);
            return RedirectToAction("Index");
        }

        // GET: Noticias/Delete/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {
            if (id != 0)
            {
                if (db.Noticias.FirstOrDefault(f => f.Id == id).EmpresaId != empresaId)
                    //Intenton hackear por empresaId
                    return RedirectToAction("Index");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Noticia news = db.Noticias.Where(w => w.EmpresaId == empresaId).FirstOrDefault(w=>w.Id== id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        // POST: Noticias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult DeleteConfirmed(int id)
        {
            Noticia news = db.Noticias.Where(w => w.EmpresaId == empresaId).FirstOrDefault ( f=>f.Id== id);
            news.Habilitada = false;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Noticias        
        [Authorize(Roles = "Administrador")]
        public ActionResult IndexDeleted()
        {
            return View(db.Noticias.Where(w => w.EmpresaId == empresaId).Where(x => x.Habilitada == false).ToList().OrderByDescending(x => x.FechaDesde));
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult Enable(int id)
        {
            Noticia news = db.Noticias.Where(w => w.EmpresaId == empresaId).FirstOrDefault (f=>f.Id== id);
            news.Habilitada = true;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Private members
        private void Update(NoticiaViewModel model)
        {
            using (DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    // Noticia
                    HttpPostedFileBase file = Request.Files["ImageFile"];
                    if (file != null && file.ContentLength != 0)
                    {
                        model.Noticia.Image = SaveFile(file);
                    }
                    model.Noticia.Habilitada = true;
                    model.Noticia.EmpresaId = empresaId;
                    db.Entry(model.Noticia).State = EntityState.Modified;
                    db.SaveChanges();

                    // Segmentación
                    db.Segmentacions.RemoveRange(db.Segmentacions.Where(x => x.EmpresaId == empresaId && x.ModuloId == moduloId && x.EntidadId == model.Noticia.Id));
                    foreach (var segmento in model.FiltroConfig.Segmentacion.SegmentoCollection)
                    {
                        if (segmento.Action != DELETE_MARK)
                        {
                            var segmentoDb = new Segmentacion
                            {
                                EmpresaId = empresaId,
                                ModuloId = moduloId,
                                EntidadId = model.Noticia.Id,
                                FiltroId = segmento.TipoFiltroId,
                                FiltroValor = segmento.ValorFiltroId
                            };
                            db.Segmentacions.Add(segmentoDb);
                        }
                    }
                    db.SaveChanges();

                    dbTran.Commit();
                }
                catch (Exception ex)
                {
                    dbTran.Rollback();
                    throw;
                }
            }
        }

        private void Save(NoticiaViewModel model)
        {
            using (DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    // Noticia
                    HttpPostedFileBase file = Request.Files["Image"];
                    if (file != null && file.ContentLength != 0)
                    {
                        model.Noticia.Image = SaveFile(file);
                    }
                    model.Noticia.FechaAlta = DateTime.Now;
                    model.Noticia.Habilitada = true;
                    model.Noticia.EmpresaId = empresaId;
                    db.Noticias.Add(model.Noticia);
                    db.SaveChanges();

                    // Segmentación
                    foreach (var segmento in model.FiltroConfig.Segmentacion.SegmentoCollection)
                    {
                        if (segmento.Action == DELETE_MARK)
                            continue;
                        var segmentoDb = new Segmentacion
                        {
                            EmpresaId = empresaId,
                            ModuloId = moduloId,
                            EntidadId = model.Noticia.Id,
                            FiltroId = segmento.TipoFiltroId,
                            FiltroValor = segmento.ValorFiltroId
                        };
                        db.Segmentacions.Add(segmentoDb);
                    }
                    db.SaveChanges();

                    dbTran.Commit();
                }
                catch (Exception ex)
                {
                    dbTran.Rollback();
                    throw;
                }
            }
        }

        private string SaveFile(HttpPostedFileBase file)
        {
            var parametros = _settingsService.GetParametrosGlobales();
            var fileStorage = _fileStorageFactory.CreateFileStorage(parametros);
            var filename = FileStorageHelper.FormatFilename(file, parametros);
            var container = parametros["ContainerNoticias"];
            return fileStorage.Save(file, filename, container);
        }
        #endregion
    }
}