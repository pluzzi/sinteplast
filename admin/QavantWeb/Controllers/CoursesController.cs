﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QavantWeb.Models;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using QavantWeb.Common;
using QavantWeb.ViewModels;
using QavantWeb.Services;
using QavantWeb.Services.Settings;
using QavantWeb.Infrastructure.FileStorage;

namespace QavantWeb.Controllers
{
    public class CoursesController : Controller
    {
        private const string DELETE_MARK = "B";
        private QavantEntities db = new QavantEntities();
        private int empresaId = SecurityHelpers.getEmpresaId();
        private int moduloId = (int)Enums.Modulos.Capacitaciones;

        private readonly IFiltroService _filtroService;
        private readonly ISettingsService _settingsService;
        private readonly IFileStorageFactory _fileStorageFactory;

        public CoursesController() : this(new SettingsService(Enums.Modulos.Capacitaciones), new FileStorageFactory(), new FiltroService()) { }

        public CoursesController(ISettingsService settingsService, IFileStorageFactory fileStorageFactory, IFiltroService filtroService)
        {
            _filtroService = filtroService;
            _settingsService = settingsService;
            _fileStorageFactory = fileStorageFactory;
        }

        // GET: Courses
        [Authorize(Roles = "Administrador")]
        public ActionResult Index()
        {
            var courses = db.Courses.Where(w=>w.EmpresaId==empresaId).Where(w => w.Enabled).OrderByDescending(o => o.CreationDate);
            return View(courses.ToList());
        }

        // GET: Courses/Create
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            ViewBag.Form_Id = new SelectList(db.Forms.Where(w => w.EmpresaId == empresaId).Where(w => w.Enabled && !w.Deleted).Where(w=>w.FormTypeId==(byte)Enums.FormsTypes.Evaluacion).OrderBy(o => o.Title), "Id", "Title");
            ViewBag.CategoriaId = new SelectList(db.Categorias
                                                            .Where(w=>w.ModuloId==(short)Enums.Modulos.Capacitaciones)
                                                            .Where(w => w.EmpresaId == empresaId)
                                                            .Where(w => w.Habilitada)
                                                            .OrderBy(o => o.Nombre), "Id", "Nombre", 1);

            ViewBag.CourseType_Id = new SelectList(db.CoursesTypes.OrderBy(o => o.Name), "Id", "Name");

            CapacitacionViewModel viewModel = new CapacitacionViewModel();
            Cours capacitacion = new Cours();
            capacitacion.Enabled = true;
            viewModel.Capacitacion = capacitacion;
            viewModel.FiltroConfig.Filtros = _filtroService.GetFiltros(empresaId);

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult Create(CapacitacionViewModel model)
        {
            CheckConditionalValidations(model.Capacitacion, CoursesValidationModes.CREATE);
            if (!ModelState.IsValid)
            {
                ViewBag.Form_Id = new SelectList(db.Forms.Where(w => w.EmpresaId == empresaId).Where(w => w.Enabled).Where(w => (w.Enabled && !w.Deleted) || (model.Capacitacion.Form_Id != null && w.Id == model.Capacitacion.Form_Id)).Where(w => w.FormTypeId == (byte)Enums.FormsTypes.Evaluacion).OrderBy(o => o.Title), "Id", "Title", model.Capacitacion.Form_Id);
                ViewBag.CategoriaId = new SelectList(db.Categorias
                                                                .Where(w => w.ModuloId == (short)Enums.Modulos.Capacitaciones)
                                                                .Where(w => w.EmpresaId == empresaId)
                                                                .Where(w => w.Habilitada || w.Id == model.Capacitacion.CategoriaId)
                                                                .OrderBy(o => o.Nombre), "Id", "Nombre", model.Capacitacion.CategoriaId);

                ViewBag.CourseType_Id = new SelectList(db.CoursesTypes.OrderBy(o => o.Name), "Id", "Name", model.Capacitacion.CourseType_Id);
                model.FiltroConfig.Filtros = _filtroService.GetFiltros(empresaId);
                return View(model);
            }

            Save(model);
            return RedirectToAction("Index");
        }

        // GET: Courses/Edit/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Cours capacitacion = db.Courses.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            if (capacitacion == null)
                return HttpNotFound();

            ViewBag.Form_Id = new SelectList(db.Forms.Where(w => w.EmpresaId == empresaId).Where(w => (w.Enabled && !w.Deleted) || (capacitacion.Form_Id != null && w.Id == capacitacion.Form_Id)).Where(w => w.FormTypeId == (byte)Enums.FormsTypes.Evaluacion).OrderBy(o => o.Title), "Id", "Title", capacitacion.Form_Id);
            ViewBag.CourseType_Id = new SelectList(db.CoursesTypes.OrderBy(o => o.Name), "Id", "Name", capacitacion.CourseType_Id);
            ViewBag.CategoriaId = new SelectList(db.Categorias
                                                            .Where(w => w.ModuloId == (short)Enums.Modulos.Capacitaciones)
                                                            .Where(w => w.EmpresaId == empresaId)
                                                            .Where(w => w.Habilitada || w.Id == capacitacion.CategoriaId)
                                                            .OrderBy(o => o.Nombre), "Id", "Nombre", capacitacion.CategoriaId);

            CapacitacionViewModel viewModel = new CapacitacionViewModel();
            viewModel.Capacitacion = capacitacion;
            viewModel.FiltroConfig.Filtros = _filtroService.GetFiltros(empresaId);
            viewModel.FiltroConfig.Segmentacion = _filtroService.GetSegmentacion(empresaId, moduloId, capacitacion.Id);

            return View(viewModel);
        }

        // POST: Courses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(CapacitacionViewModel model)
        {
            model.Capacitacion.DocumentURL = Request.Form["hiFile"];

            if (model.Capacitacion.EmpresaId != empresaId)
                ModelState.AddModelError("", "Error al actualizar el registro.");
            
            CheckConditionalValidations(model.Capacitacion, CoursesValidationModes.EDIT);
            if (!ModelState.IsValid)
            {
                ViewBag.Form_Id = new SelectList(db.Forms.Where(w => w.EmpresaId == empresaId).Where(w => (w.Enabled && !w.Deleted) || (model.Capacitacion.Form_Id != null && w.Id == model.Capacitacion.Form_Id)).Where(w => w.FormTypeId == (byte)Enums.FormsTypes.Evaluacion).OrderBy(o => o.Title), "Id", "Title", model.Capacitacion.Form_Id);
                ViewBag.CourseType_Id = new SelectList(db.CoursesTypes.OrderBy(o => o.Name), "Id", "Name", model.Capacitacion.CourseType_Id);
                ViewBag.CategoriaId = new SelectList(db.Categorias.Where(w => w.ModuloId == (short)Enums.Modulos.Capacitaciones)
                                                                .Where(w => w.EmpresaId == empresaId)
                                                                .Where(w => w.Habilitada || w.Id == model.Capacitacion.CategoriaId)
                                                                .OrderBy(o => o.Nombre), "Id", "Nombre", model.Capacitacion.CategoriaId);

                model.FiltroConfig.Filtros = _filtroService.GetFiltros(empresaId);
                model.FiltroConfig.Segmentacion = _filtroService.GetSegmentacion(empresaId, moduloId, model.Capacitacion.Id);
                return View(model);
            }

            Update(model);
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cours course = db.Courses.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            if (course == null)
            {
                return HttpNotFound();
            }
            return View(course);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult DeleteConfirmed(int id)
        {
            Cours course = db.Courses.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            course.Author_Id = User.Identity.GetUserId();
            course.Enabled = false;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult IndexDeleted()
        {
            return View(db.Courses.Where(w => w.EmpresaId == empresaId).Where(x => x.Enabled == false).ToList().OrderByDescending(x => x.CreationDate));
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult Enable(int id)
        {
            Cours course = db.Courses.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            course.Author_Id = User.Identity.GetUserId();
            course.Enabled = true;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        private enum CoursesValidationModes {
            CREATE=1, EDIT=2
        }

        #region Private members
        private void CheckConditionalValidations(Cours course, CoursesValidationModes mode)
        {
            if (course.CourseType_Id != null)
            {
                switch (course.CourseType_Id)
                {
                    case (int)Enums.CoursesTypes.Documento:
                        HttpPostedFileBase file = Request.Files["File"];
                        //Está creando una capacitación de tipo documento y no ingresó archivo.
                        if (mode == CoursesValidationModes.CREATE && file.FileName == String.Empty)
                            ModelState.AddModelError("Capacitacion.DocumentURL", "Debe ingresar el documento.");
                        //Venía siendo una capacitación de otro tipo y ahora se seleccionó tipo "documento", pero no se ingresó archivo
                        if (mode == CoursesValidationModes.EDIT && course.DocumentURL == String.Empty && file.FileName == String.Empty) 
                            ModelState.AddModelError("Capacitacion.DocumentURL", "Debe ingresar el documento.");
                        //El archivo fue ingresado, pero no tiene contenido.
                        if (file.FileName != String.Empty && !FileHelper.ValidateFileSize(file))
                            ModelState.AddModelError("Capacitacion.DocumentURL", "El documento debe tener contenido.");
                        break;
                    case (int)Enums.CoursesTypes.Link:
                        if (course.LinkURL == null)
                            ModelState.AddModelError("Capacitacion.LinkURL", "Debe ingresar una URL del link.");
                        break;
                    case (int)Enums.CoursesTypes.Video:
                        if (course.VideoURL == null)
                            ModelState.AddModelError("Capacitacion.VideoURL", "Ingrese ID del video de YouTube.");
                        break;
                    default:
                        break;
                }
            }
            if (course.RequiresEvaluation)
            {
                if (course.Form_Id == null)
                    ModelState.AddModelError("Form_Id", "Debe ingresar el formulario de evaluación");
                if (course.ApprovedPerc == null || course.ApprovedPerc < 0 || course.ApprovedPerc > 100)
                    ModelState.AddModelError("ApprovedPerc", "El porcentaje de aprobación debe ser un número entre 0 y 100%");
            }
        }

        private void Update(CapacitacionViewModel model)
        {
            using (DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    // Capacitacion
                    HttpPostedFileBase file = Request.Files["File"];
                    if (file != null && file.ContentLength != 0)
                        model.Capacitacion.DocumentURL = SaveFile(file);

                    model.Capacitacion.Author_Id = User.Identity.GetUserId();
                    model.Capacitacion.CreationDate = DateTime.Now;
                    model.Capacitacion.Enabled = true;
                    db.Entry(model.Capacitacion).State = EntityState.Modified;
                    db.SaveChanges();

                    // Segmentación
                    db.Segmentacions.RemoveRange(db.Segmentacions.Where(x => x.EmpresaId == empresaId && x.ModuloId == moduloId && x.EntidadId == model.Capacitacion.Id));
                    foreach (var segmento in model.FiltroConfig.Segmentacion.SegmentoCollection)
                    {
                        if (segmento.Action != DELETE_MARK)
                        {
                            var segmentoDb = new Segmentacion
                            {
                                EmpresaId = empresaId,
                                ModuloId = moduloId,
                                EntidadId = model.Capacitacion.Id,
                                FiltroId = segmento.TipoFiltroId,
                                FiltroValor = segmento.ValorFiltroId
                            };
                            db.Segmentacions.Add(segmentoDb);
                        }
                    }
                    db.SaveChanges();

                    dbTran.Commit();
                }
                catch (Exception ex)
                {
                    dbTran.Rollback();
                    throw;
                }
            }
        }

        private void Save(CapacitacionViewModel model)
        {
            using (DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    // Capacitación
                    HttpPostedFileBase file = Request.Files["File"];
                    if (file != null && file.ContentLength != 0)
                        model.Capacitacion.DocumentURL = SaveFile(file);

                    model.Capacitacion.EmpresaId = empresaId;
                    model.Capacitacion.Author_Id = User.Identity.GetUserId();
                    model.Capacitacion.CreationDate = DateTime.Now;
                    model.Capacitacion.Enabled = true;
                    db.Courses.Add(model.Capacitacion);
                    db.SaveChanges();

                    // Segmentación
                    foreach (var segmento in model.FiltroConfig.Segmentacion.SegmentoCollection)
                    {
                        if (segmento.Action == DELETE_MARK)
                            continue;
                        var segmentoDb = new Segmentacion
                        {
                            EmpresaId = empresaId,
                            ModuloId = moduloId,
                            EntidadId = model.Capacitacion.Id,
                            FiltroId = segmento.TipoFiltroId,
                            FiltroValor = segmento.ValorFiltroId
                        };
                        db.Segmentacions.Add(segmentoDb);
                    }
                    db.SaveChanges();

                    dbTran.Commit();
                }
                catch (Exception ex)
                {
                    dbTran.Rollback();
                    throw;
                }
            }
        }

        private string SaveFile(HttpPostedFileBase file)
        {
            var parametros = _settingsService.GetParametrosGlobales();
            var fileStorage = _fileStorageFactory.CreateFileStorage(parametros);
            var filename = FileStorageHelper.FormatFilename(file, parametros);
            var container = parametros["ContainerCapacitaciones"];
            return fileStorage.Save(file, filename, container);
        }
        #endregion
    }
}
