﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QavantWeb.Models;
using QavantWeb.Common;
using QavantWeb.Services;

namespace QavantWeb.Controllers
{
    public class GroupsController : Controller
    {
        private QavantEntities db = new QavantEntities();
        private int empresaId = SecurityHelpers.getEmpresaId();
        private readonly IFiltroService _filtroService;


        public GroupsController() : this(new FiltroService()) { }

        public GroupsController(IFiltroService filtroService)
        {
            _filtroService = filtroService;
        }



        // GET: Groups

        [Authorize(Roles = "Administrador")]
        public ActionResult Index()
        {
            return View(db.Groups.Where(w => w.EmpresaId == empresaId).ToList());
        }

        // GET: Groups/Details/5

        [Authorize(Roles = "Administrador")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group group = db.Groups.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f=>f.Id == id);
            if (group == null)
            {
                return HttpNotFound();
            }
            return View(group);
        }


        private void SetUsersInViewBag()
        {
            ViewBag.Users = db.Users.Where(user => user.EmpresaId == empresaId).Where(us => us.Enable).OrderBy(us => us.Surname).ToList();
        }



        // GET: Groups/Create
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            SetUsersInViewBag();
            Group og = new Group();
            og.UsersGroups = new List<UsersGroup>();
            og.Enabled = true;
            return View(og);
        }

       

        // POST: Groups/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult Create(Group group, string[] Users)
        {
            if (Users == null || Users.Length == 0)
                ModelState.AddModelError("", "Debe seleccionar al menos un usuario");

            if (ModelState.IsValid)
            {
                group.EmpresaId = empresaId;
                db.Groups.Add(group);
                db.SaveChanges();

                for (int i = 0; i < Users.Length; i++)
                {
                    UsersGroup u = new Models.UsersGroup();
                    u.UserId = int.Parse(Users[i]);
                    u.GroupId = group.Id;
                    db.UsersGroups.Add(u);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            SetUsersInViewBag();
            return View(group);
        }

        // GET: Groups/Edit/5

        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group group = db.Groups.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f=>f.Id == id);
            if (group == null)
            {
                return HttpNotFound();
            }
            SetUsersInViewBag();
            return View(group);
        }

        // POST: Groups/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]

        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(Group group, string[] Users)
        {
            if (Users == null || Users.Length == 0)
                ModelState.AddModelError("", "Debe seleccionar al menos un usuario");

            if (group.EmpresaId != empresaId)
                ModelState.AddModelError("", "Error al actualizar el registro.");

            if (ModelState.IsValid)
            {
                db.Entry(group).State = EntityState.Modified;
                db.UsersGroups.RemoveRange( db.UsersGroups.Where(w => w.GroupId == group.Id).ToList());
                db.SaveChanges();

                for (int i = 0; i < Users.Length; i++)
                {
                    UsersGroup u = new Models.UsersGroup();
                    u.UserId = int.Parse(Users[i]);
                    u.GroupId = group.Id;
                    db.UsersGroups.Add(u);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            SetUsersInViewBag();
            return View(group);
        }

        // GET: Groups/Delete/5

        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group group = db.Groups.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f=>f.Id == id);
            if (group == null)
            {
                return HttpNotFound();
            }
            return View(group);
        }

        // POST: Groups/Delete/5

        [Authorize(Roles = "Administrador")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var group = db.Groups.Include("UsersGroups").FirstOrDefault(g => g.Id == id && g.EmpresaId == empresaId);
            if (group == null)
            {
                return HttpNotFound();
            }

            // valida filtros asociados.
            var cantFiltros = _filtroService.GetSegmentationCountByFilterValue(empresaId, Enums.Filtros.Grupos, id);
            if (cantFiltros > 0)
            {
                ModelState.AddModelError("", "No es posible eliminar el grupo. El grupo posee filtros asociados.");
            }


            if (ModelState.IsValid)
            {
                db.UsersGroups.RemoveRange(group.UsersGroups);
                db.Groups.Remove(group);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(group);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
