﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QavantWeb.Models;
using Microsoft.AspNet.Identity;
using QavantWeb.Common;

namespace QavantWeb.Controllers
{
    public class GrafComportamientoController : Controller
    {
        private QavantEntities db = new QavantEntities();
        private int empresaId = SecurityHelpers.getEmpresaId();
        private string fechaDesde = DateTime.Now.Date.AddDays(-30).ToShortDateString();
        private string fechaHasta = DateTime.Now.Date.ToShortDateString();

        // GET: Dashboard
        [Authorize(Roles = "Administrador")]
        public ActionResult Index()
        {
            DateTime desde = DateTime.Parse(fechaDesde);
            DateTime hasta = DateTime.Parse(fechaHasta);

            #region COMPARACION REACCIONES
            var listaDeComportamientos = db.est_Comportamiento_ComparacionReacciones(empresaId,desde,hasta);

            var lstNombres = "";
            List<est_Comportamiento_ComparacionReacciones_Result> lstComportamientos = new List<est_Comportamiento_ComparacionReacciones_Result>();
            try
            {
                foreach (var item in listaDeComportamientos)
                {
                    lstNombres += "['" + item.Nombre.SoloLetrasYNumeros().ReemplazaCaracteresEspeciales() + "'," + item.porcentaje + "],";
                    lstComportamientos.Add(item);
                }
            }
            catch (Exception )
            {}
            finally
            {
                ViewBag.listaComportamiento = lstComportamientos;
                if (lstNombres != "")
                    lstNombres = lstNombres.Substring(0, lstNombres.Length - 1);
                ViewBag.comparacionReaccionesChart = lstNombres;
            }

            #endregion

            #region CONTENIDO MÁS REACCIONES
            var listaContenidoReacciones = db.est_Comportamiento_ContenidoMasReacciones(empresaId, desde, hasta,"");

            var catContenidosReaccionesTitulos = "";
            var meEncantaCant = "";
            var meGustaCant = "";
            var meSorprendeCant = "";
            var noMeGustaCant = "";
            var meEnojaCant = "";

            foreach (var item in listaContenidoReacciones)
            {                
                catContenidosReaccionesTitulos += "\'" + item.Titulo.ReemplazaCaracteresEspeciales() + "\'" + ",";
                meEncantaCant += item.MeEncanta + ",";
                meGustaCant += item.MeGusta + ",";
                meSorprendeCant += item.MeSorprende + ",";
                noMeGustaCant += item.NoMeGusta + ",";
                meEnojaCant += item.MeEnfurece + ",";

            }
            if (meEncantaCant != "")
                meEncantaCant = meEncantaCant.Substring(0, meEncantaCant.Length - 1);
            ViewBag.meEncantaCantidad = meEncantaCant;

            if (meGustaCant != "")
                meGustaCant = meGustaCant.Substring(0, meGustaCant.Length - 1);
            ViewBag.meGustaCantidad = meGustaCant;

            if (meSorprendeCant != "")
                meSorprendeCant = meSorprendeCant.Substring(0, meSorprendeCant.Length - 1);
            ViewBag.meSorprendeCantidad = meSorprendeCant;

            if (noMeGustaCant != "")
                noMeGustaCant = noMeGustaCant.Substring(0, noMeGustaCant.Length - 1);
            ViewBag.noMeGustaCantidad = noMeGustaCant;

            if (meEnojaCant != "")
                meEnojaCant = meEnojaCant.Substring(0, meEnojaCant.Length - 1);
            ViewBag.meEnojaCantidad = meEnojaCant;

            if (catContenidosReaccionesTitulos != "")
                catContenidosReaccionesTitulos = catContenidosReaccionesTitulos.Substring(0, catContenidosReaccionesTitulos.Length - 1);
            ViewBag.contenidosReaccionesTitulos = catContenidosReaccionesTitulos;
            #endregion

            return View();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
