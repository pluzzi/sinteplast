﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QavantWeb.Models;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using Microsoft.Office.Interop.Excel;
using QavantWeb.Common;
using QavantWeb.Services;
using QavantWeb.ViewModels;
using QavantWeb.Infrastructure.FileStorage;
using QavantWeb.Services.Settings;
using System.IO;

namespace QavantWeb.Controllers
{
    public class DocumentsController : Controller
    {
        private const string DELETE_MARK = "B";
        private QavantEntities db = new QavantEntities();
        private int empresaId = SecurityHelpers.getEmpresaId();
        private int moduloId = (int)Enums.Modulos.Documentos;

        private readonly IFiltroService _filtroService;
        private readonly ISettingsService _settingsService;
        private readonly IFileStorageFactory _fileStorageFactory;


        public DocumentsController() : this(new SettingsService(Enums.Modulos.Documentos), new FileStorageFactory(), new FiltroService()) { }

        public DocumentsController(ISettingsService settingsService, IFileStorageFactory fileStorageFactory, IFiltroService filtroService)
        {
            _settingsService = settingsService;
            _fileStorageFactory = fileStorageFactory;
            _filtroService = filtroService;
        }

        // GET: Documents
        [Authorize(Roles = "Administrador")]
        public ActionResult Index(int iDocumentLibrary)
        {
            ViewBag.SubTitle = getSubTitle(iDocumentLibrary);
            ViewBag.iDocumentLibrary = iDocumentLibrary;
            return View(db.Documents
                            .Where(w => w.EmpresaId == empresaId)
                            .Where(x => x.Enabled == true)
                            .Where(w => w.DocumentLibraryId == iDocumentLibrary)
                            .OrderByDescending(x => x.CreatedDate)
                            .ToList());
        }

        public string getSubTitle(int iDocumentLibrary)
        {
            string ret = "";
            switch (iDocumentLibrary)
            {
                case (int)Enums.DocumentLibrary.Cursos:
                    ret = " - Cursos";
                    break;
                case (int)Enums.DocumentLibrary.Planillas:
                    ret = " - Planillas";
                    break;
            }
            return ret;
        }

        // GET: Documents/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Document document = db.Documents.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            if (document == null)
            {
                return HttpNotFound();
            }
            ViewBag.SubTitle = getSubTitle(document.DocumentLibraryId);
            return View(document);
        }

        // GET: Documents/Create
        public ActionResult Create(int iDocumentLibrary)
        {

            ViewBag.DocumentTypeId = new SelectList(db.DocumentTypes, "Id", "Name");
            Document oDoc = new Document();
            oDoc.DocumentLibraryId = iDocumentLibrary;
            ViewBag.SubTitle = getSubTitle(iDocumentLibrary);

            List<Categoria> lsCat = new List<Categoria>();
            List<Categoria> list = db.Categorias
                                        .Where(w => w.EmpresaId == empresaId)
                                        .Where(w => w.ModuloId == (int)Enums.Modulos.Documentos)
                                        .Where(w => w.Habilitada).ToList();
            Common.FormatterHelpers.CategoriasToTree(list, ref lsCat, null, "", true);
            ViewBag.CategoriaId = new SelectList(lsCat, "Id", "Nombre");

            var viewModel = new DocumentoViewModel();
            var documento = new Document();
            viewModel.Documento = documento;
            viewModel.FiltroConfig.Filtros = _filtroService.GetFiltros(empresaId);

            viewModel.Documento.DocumentLibraryId = oDoc.DocumentLibraryId;
            return View(viewModel);
        }

        // POST: Documents/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DocumentoViewModel model)
        {
            try
            {
                var file = Request.Files["File"];

                if (!FileHelper.ValidateFileExistence(file))
                {
                    ModelState.AddModelError("", "Debe seleccionar un archivo.");

                }
                if (!FileHelper.ValidateFileSize(file))
                {
                    ModelState.AddModelError("", "El archivo debe tener contenido.");
                }
                if (!FileHelper.ValidateFileExtension(file))
                {
                    ModelState.AddModelError("", "El formato del archivo que cargue debe encontrarse entre los siguientes: .doc, .docx, .xls, .xlsx, .ppt, .pps, .pptx, .ppsx, .pdf, .jpg, .jpeg, .png, .ai, .psd, .xlsm");
                }
                if (!ModelState.IsValid)
                {
                    List<Categoria> lsCat = new List<Categoria>();
                    List<Categoria> list = db.Categorias
                                                .Where(w => w.EmpresaId == empresaId)
                                                .Where(w => w.ModuloId == (int)Enums.Modulos.Documentos)
                                                .Where(w => w.Habilitada).ToList();
                    Common.FormatterHelpers.CategoriasToTree(list, ref lsCat, null, "", true);
                    ViewBag.CategoriaId = new SelectList(lsCat, "Id", "Nombre");

                    ViewBag.SubTitle = getSubTitle(model.Documento.DocumentLibraryId);
                    ViewBag.DocumentTypeId = new SelectList(db.DocumentTypes, "Id", "Name", model.Documento.DocumentTypeId);
                    ViewBag.iDocumentLibrary = model.Documento.DocumentLibraryId;

                    model.FiltroConfig.Filtros = _filtroService.GetFiltros(empresaId);
                    ViewBag.DocumentTypeId = new SelectList(db.DocumentTypes, "Id", "Name", model.Documento.DocumentTypeId);
                    return View(model);
                }
                SaveModel(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error en el alta. \n " + ex.Message);
                List<Categoria> lstCat = new List<Categoria>();
                List<Categoria> lista = db.Categorias
                                            .Where(w => w.EmpresaId == empresaId)
                                            .Where(w => w.ModuloId == (int)Enums.Modulos.Documentos)
                                            .Where(w => w.Habilitada).ToList();
                Common.FormatterHelpers.CategoriasToTree(lista, ref lstCat, null, "", true);
                ViewBag.CategoriaId = new SelectList(lstCat, "Id", "Nombre");

                ViewBag.SubTitle = getSubTitle(model.Documento.DocumentLibraryId);
                ViewBag.DocumentTypeId = new SelectList(db.DocumentTypes, "Id", "Name", model.Documento.DocumentTypeId);
                ViewBag.iDocumentLibrary = model.Documento.DocumentLibraryId;
                model.FiltroConfig.Filtros = _filtroService.GetFiltros(empresaId);
                ViewBag.DocumentTypeId = new SelectList(db.DocumentTypes, "Id", "Name", model.Documento.DocumentTypeId);
                return View(model);
            }

            return RedirectToAction("Index", new { iDocumentLibrary = model.Documento.DocumentLibraryId });
        }

        

        private void SaveModel(DocumentoViewModel model)
        {
            using (DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    switch (model.Documento.DocumentTypeId)
                    {
                        case (int)Enums.DocumentTypes.Documento:
                        case (int)Enums.DocumentTypes.Archivo:
                            HttpPostedFileBase file = Request.Files["File"];

                            model.Documento.Filepath = SaveFile(file);
                            model.Documento.EmpresaId = empresaId;
                            model.Documento.CreatedDate = DateTime.Now;
                            model.Documento.Enabled = true;
                            model.Documento.CreationUserId = User.Identity.GetUserId();

                            db.Documents.Add(model.Documento);
                            db.SaveChanges();
                            //return RedirectToAction("Index", new { iDocumentLibrary = model.Documento.DocumentLibraryId });

                            break;

                        case (int)Enums.DocumentTypes.Video:

                            model.Documento.EmpresaId = empresaId;
                            model.Documento.CreatedDate = DateTime.Now;
                            model.Documento.Enabled = true;
                            model.Documento.CreationUserId = User.Identity.GetUserId();

                            db.Documents.Add(model.Documento);
                            db.SaveChanges();
                            break;
                            //return RedirectToAction("Index", new { iDocumentLibrary = model.Documento.DocumentLibraryId });
                    }

                    //Segmentación
                    foreach (var item in model.FiltroConfig.Segmentacion.SegmentoCollection)
                    {
                        if (item.Action == DELETE_MARK)
                            continue;
                        var segmentoDb = new Segmentacion
                        {
                            EmpresaId = empresaId,
                            ModuloId = moduloId,
                            EntidadId = model.Documento.Id,
                            FiltroId = item.TipoFiltroId,
                            FiltroValor = item.ValorFiltroId
                        };
                        db.Segmentacions.Add(segmentoDb);
                    }
                    db.SaveChanges();
                    dbTran.Commit();
                }
                catch (Exception ex )
                {
                    dbTran.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        // GET: Documents/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Document document = db.Documents.Where(w => w.EmpresaId == empresaId).Single(f => f.Id == id);
            if (document == null)
            {
                return HttpNotFound();
            }

            List<Categoria> lsCat = new List<Categoria>();
            List<Categoria> list = db.Categorias
                                        .Where(w => w.EmpresaId == empresaId)
                                        .Where(w => w.ModuloId == (int)Enums.Modulos.Documentos)
                                        .Where(w => w.Habilitada).ToList();
            Common.FormatterHelpers.CategoriasToTree(list, ref lsCat, null, "", true);

            SelectList lsCategoriaPadre = new SelectList(lsCat, "Id", "Nombre");
            foreach (var item in lsCategoriaPadre)
            {
                if (document.CategoriaId == Convert.ToInt32(item.Value))
                {
                    item.Selected = true;
                }
            }
            ViewBag.CategoriaId = lsCategoriaPadre;

            ViewBag.SubTitle = getSubTitle(document.DocumentLibraryId);
            ViewBag.DocumentTypeId = new SelectList(db.DocumentTypes, "Id", "Name", document.DocumentTypeId);

            var viewModel = new DocumentoViewModel();
            viewModel.Documento = document;
            viewModel.FiltroConfig.Filtros = _filtroService.GetFiltros(empresaId);
            viewModel.FiltroConfig.Segmentacion = _filtroService.GetSegmentacion(empresaId, moduloId, document.Id);

            return View(viewModel);
        }

        // POST: Documents/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DocumentoViewModel model)
        {
            if (model.Documento.EmpresaId != empresaId)
            { 
                ModelState.AddModelError("", "Error al actualizar el registro.");
            }

            var file = Request.Files["File"];

            if (file.FileName != String.Empty && !FileHelper.ValidateFileSize(file))
            {
                ModelState.AddModelError("", "El archivo debe tener contenido.");
            }
            if (file.FileName != String.Empty && !FileHelper.ValidateFileExtension(file))
            {
                ModelState.AddModelError("", "El formato del archivo que cargue debe encontrarse entre los siguientes: .doc, .docx, .xls, .xlsx, .ppt, .pps, .pptx, .ppsx, .pdf, .jpg, .jpeg, .png, .ai, .psd, .xlsm");
            }


            try
            {
                if (!ModelState.IsValid)
                {
                    List<Categoria> lsCat = new List<Categoria>();
                    List<Categoria> list = db.Categorias
                                                .Where(w => w.EmpresaId == empresaId)
                                                .Where(w => w.ModuloId == (int)Enums.Modulos.Documentos)
                                                .Where(w => w.Habilitada).ToList();
                    Common.FormatterHelpers.CategoriasToTree(list, ref lsCat, null, "", true);
                    SelectList lsCategoriaPadre = new SelectList(lsCat, "Id", "Nombre");
                    foreach (var item in lsCategoriaPadre)
                    {
                        if (model.Documento.CategoriaId == Convert.ToInt32(item.Value))
                        {
                            item.Selected = true;
                        }
                    }
                    ViewBag.CategoriaId = lsCategoriaPadre;
                    model.FiltroConfig.Filtros = _filtroService.GetFiltros(empresaId);
                    model.FiltroConfig.Segmentacion = _filtroService.GetSegmentacion(empresaId, moduloId, model.Documento.Id);
                    return View(model);
                }

                UpdateModel(model);
                return RedirectToAction("Index", new { iDocumentLibrary = model.Documento.DocumentLibraryId });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error en el alta.  \n " + ex.Message);
                List<Categoria> lsCat = new List<Categoria>();
                List<Categoria> list = db.Categorias
                                            .Where(w => w.EmpresaId == empresaId)
                                            .Where(w => w.ModuloId == (int)Enums.Modulos.Documentos)
                                            .Where(w => w.Habilitada).ToList();
                Common.FormatterHelpers.CategoriasToTree(list, ref lsCat, null, "", true);
                SelectList lsCategoriaPadre = new SelectList(lsCat, "Id", "Nombre");
                foreach (var item in lsCategoriaPadre)
                {
                    if (model.Documento.CategoriaId == Convert.ToInt32(item.Value))
                    {
                        item.Selected = true;
                    }
                }
                ViewBag.CategoriaId = lsCategoriaPadre;
                model.FiltroConfig.Filtros = _filtroService.GetFiltros(empresaId);
                model.FiltroConfig.Segmentacion = _filtroService.GetSegmentacion(empresaId, moduloId, model.Documento.Id);
                return View(model);

            }

        }

        private void UpdateModel(DocumentoViewModel model)
        {
            using (DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    HttpPostedFileBase file = Request.Files["File"];
                    if (file != null && file.ContentLength != 0)
                    {
                        model.Documento.Filepath = SaveFile(file);
                    }
                    model.Documento.CreatedDate = DateTime.Now;
                    model.Documento.Enabled = true;
                    model.Documento.CreationUserId = User.Identity.GetUserId();

                    db.Entry(model.Documento).State = EntityState.Modified;
                    db.SaveChanges();

                    // Segmentación
                    db.Segmentacions.RemoveRange(db.Segmentacions.Where(x => x.EmpresaId == empresaId && x.ModuloId == moduloId && x.EntidadId == model.Documento.Id));
                    foreach (var segmento in model.FiltroConfig.Segmentacion.SegmentoCollection)
                    {
                        if (segmento.Action != DELETE_MARK)
                        {
                            var segmentoDb = new Segmentacion
                            {
                                EmpresaId = empresaId,
                                ModuloId = moduloId,
                                EntidadId = model.Documento.Id,
                                FiltroId = segmento.TipoFiltroId,
                                FiltroValor = segmento.ValorFiltroId
                            };
                            db.Segmentacions.Add(segmentoDb);
                        }
                    }
                    db.SaveChanges();

                    dbTran.Commit();
                }
                catch (Exception ex)
                {
                    dbTran.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        // GET: Documents/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Document document = db.Documents.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            if (document == null)
            {
                return HttpNotFound();
            }
            ViewBag.SubTitle = getSubTitle(document.DocumentLibraryId);
            return View(document);
        }

        // POST: Documents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Document document = db.Documents.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            document.CreationUserId = User.Identity.GetUserId();
            document.Enabled = false;
            db.SaveChanges();
            return RedirectToAction("Index", new { iDocumentLibrary = document.DocumentLibraryId });
        }

        // GET: Documents
        public ActionResult IndexDeleted(int iDocumentLibrary)
        {
            ViewBag.iDocumentLibrary = iDocumentLibrary;
            ViewBag.SubTitle = getSubTitle(iDocumentLibrary);
            return View(db.Documents.Where(w => w.EmpresaId == empresaId).Where(x => x.Enabled == false).Where(w => w.DocumentTypeId == iDocumentLibrary).ToList().OrderByDescending(x => x.CreatedDate));
        }

        public ActionResult Enable(int id)
        {
            Document document = db.Documents.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            document.Enabled = true;
            db.SaveChanges();
            return RedirectToAction("Index", new { iDocumentLibrary = document.DocumentLibraryId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Internal members
        private string SaveFile(HttpPostedFileBase file)
        {
            var parametros = _settingsService.GetParametrosGlobales();
            var fileStorage = _fileStorageFactory.CreateFileStorage(parametros);
            var filename = FileStorageHelper.FormatFilename(file, parametros);
            var container = parametros["ContainerDocumentos"];
            return fileStorage.Save(file, filename, container);
        }
        #endregion
    }
}
