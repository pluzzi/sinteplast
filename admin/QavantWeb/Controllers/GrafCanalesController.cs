﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QavantWeb.Models;
using Microsoft.AspNet.Identity;
using QavantWeb.Common;

namespace QavantWeb.Controllers
{
    public class GrafCanalesController : Controller
    {
        private QavantEntities db = new QavantEntities();
        private int empresaId = SecurityHelpers.getEmpresaId();

        private string fechaDesde = DateTime.Now.Date.AddDays(-30).ToShortDateString();
        private string fechaHasta = DateTime.Now.Date.ToShortDateString();

        // GET: Canales
        [Authorize(Roles = "Administrador")]
        public ActionResult Index()
        {
            DateTime desde = DateTime.Parse(fechaDesde);
            DateTime hasta = DateTime.Parse(fechaHasta);


            #region WEB VS APP - VISITAS POR DÍA  
            var visitasPorDia = db.est_VisitasPorDia(empresaId, desde, hasta);

            var cantApp = "";
            var cantWeb = "";
            foreach (var item in visitasPorDia)
            {
                if (item.Nombre == "App")
                    cantApp += item.Cantidad + ",";
                else
                    cantWeb += item.Cantidad + ",";
            }
            if (cantApp != "")
                cantApp = cantApp.Substring(0, cantApp.Length - 1);
            ViewBag.Est_visitasApp = cantApp;
            if (cantWeb != "")
                cantWeb = cantWeb.Substring(0, cantWeb.Length - 1);
            ViewBag.Est_visitasWeb = cantWeb;

            ViewBag.fDia = desde.Day ;
            ViewBag.fMes = desde.Month - 1;
            ViewBag.fAnio = desde.Year;
            #endregion


            #region WEB VS APP - VISITAS POR HORA
            var visitasPorHora = db.est_Canales_VisitasPorHora(empresaId, DateTime.Now.AddDays(-1) , DateTime.Now);
            ViewBag.fAyer = DateTime.Now.AddDays(-1).ToShortDateString();
            var cantVisitApp = "";
            var cantVisitWeb = "";
            foreach (var item in visitasPorHora)
            {
                if(item.Nombre == "App")
                    cantVisitApp += item.Cantidad + ",";
                else
                    cantVisitWeb += item.Cantidad + ",";
            }
            if (cantVisitApp != "")
                cantVisitApp = cantVisitApp.Substring(0, cantVisitApp.Length - 1);
            ViewBag.cantVisitHoraApp = cantVisitApp;
            if (cantVisitWeb != "")
                cantVisitWeb = cantVisitWeb.Substring(0, cantVisitWeb.Length - 1);
            ViewBag.cantVisitHoraWeb = cantVisitWeb;
            #endregion


            #region TABLA contenido con mayor repercucion
            var contMayorRepercucion = db.est_ContenidosMayorRepercusion(empresaId, desde, hasta);
            List<est_ContenidosMayorRepercusion_Result> contMRep = new List<est_ContenidosMayorRepercusion_Result>();
            foreach (var item in contMayorRepercucion)
            {
                contMRep.Add(item);
            }
            ViewBag.listaContenidoTabla = contMRep;
            #endregion



            return View();
        }




        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
