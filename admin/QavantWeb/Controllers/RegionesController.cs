﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QavantWeb.Common;
using QavantWeb.Models;
using QavantWeb.Services;

namespace QavantWeb.Controllers
{
    public class RegionesController : Controller
    {
        private QavantEntities db = new QavantEntities();
        private int empresaId = SecurityHelpers.getEmpresaId();
        private readonly IFiltroService _filtroService;

        public RegionesController() : this(new FiltroService()) { }

        public RegionesController(IFiltroService filtroService)
        {
            _filtroService = filtroService;
        }



        // GET: Regiones
        public ActionResult Index()
        {
            var regiones = db.Regiones.Include(r => r.Empresa).Where(w => w.EmpresaId == empresaId);
            return View(regiones.ToList());
        }

        // GET: Regiones/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Regione regione = db.Regiones.Find(id);
            if (regione == null)
            {
                return HttpNotFound();
            }
            return View(regione);
        }

        // GET: Regiones/Create
        public ActionResult Create()
        {            
            return View();
        }

        // POST: Regiones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Regione region)
        {
            if (ModelState.IsValid)
            {
                region.EmpresaId = empresaId;
                db.Regiones.Add(region);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            var regiones = db.Regiones.Include(r => r.Empresa).Where(w => w.EmpresaId == empresaId);
            return View(regiones.ToList());
        }

        // GET: Regiones/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Regione regione = db.Regiones.Where(w => w.EmpresaId == empresaId).FirstOrDefault(w => w.Id == id);
            if (regione == null)
            {
                return HttpNotFound();
            }
            return View(regione);
        }

        // POST: Regiones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Regione regione)
        {
            if(regione.EmpresaId != empresaId)
                ModelState.AddModelError("", "Error al actualizar el registro.");
            if (ModelState.IsValid)
            {
                db.Entry(regione).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var regiones = db.Regiones.Include(r => r.Empresa).Where(w => w.EmpresaId == empresaId);
            return View(regiones.ToList());
        }

        // GET: Regiones/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Regione regione = db.Regiones.Where(w => w.EmpresaId == empresaId).FirstOrDefault(w => w.Id == id);
            if (regione == null)
            {
                return HttpNotFound();
            }
            return View(regione);
        }

        // POST: Regiones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Regione region = db.Regiones.FirstOrDefault(reg => reg.Id == id && reg.EmpresaId == empresaId);

            if (region == null)
            {
                return HttpNotFound();
            }

            // valida usuarios asociados.
            if (region.Users.Count > 0)
            {
                ModelState.AddModelError("", "No es posible eliminar la región ya que posee usuarios asociados.");
            }

            // valida filtros asociados.
            var cantFiltros = _filtroService.GetSegmentationCountByFilterValue(empresaId, Enums.Filtros.Region, id);
            if (cantFiltros > 0)
            {
                ModelState.AddModelError("", "No es posible eliminar la región ya que posee filtros asociados.");
            }

            if (ModelState.IsValid)
            {
                db.Regiones.Remove(region);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(region);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
