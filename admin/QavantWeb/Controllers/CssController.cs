﻿using Microsoft.AspNet.Identity;
using QavantWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace QavantWeb.Controllers
{
    public class CssController : Controller
    {
        private QavantEntities db = new QavantEntities();

        public ContentResult GetTheme()
        {
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    string userId = User.Identity.GetUserId();
                    string strCSS = db.EmpresasCanales
                                        .Where(wh=>wh.Empresa.AspNetUsers.Any(a => a.Id == userId))
                                        .Where(w=>w.CanalId==(short)Enums.Canales.Admin)
                                        .FirstOrDefault().CustomCSS;

                    foreach(EmpresasLayout emp in db.EmpresasLayouts.ToList())
                    {
                        strCSS += ".qlayout_" + emp.Codigo + "{";
                        if (!string.IsNullOrEmpty(emp.ColorFondo))
                            strCSS += "background-color: #" + emp.ColorFondo + ";";
                        if (!string.IsNullOrEmpty(emp.ColorTipografia))
                            strCSS += "color: #" + emp.ColorTipografia + ";";
                        strCSS += "}";
                    }


                    return Content(strCSS, "text/css");
                }
                else
                    return Content("", "text/css");
            }
            catch (Exception ex) { return Content("", "text/css"); }
        }
    }
}