﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QavantWeb.Models;
using Microsoft.AspNet.Identity;
using QavantWeb.Common;

namespace QavantWeb.Controllers
{
    public class GrafAudienciaController : Controller
    {
        private QavantEntities db = new QavantEntities();
        private int empresaId = SecurityHelpers.getEmpresaId();

        // GET: Dashboard
        [Authorize(Roles = "Administrador")]
        public ActionResult Index()
        {
            #region DISTRIBUCIÓN POR ÁREA
            var audPorArea = db.est_DistribuciónPorArea(empresaId).ToList();

            string AudienciaPorArea = "";
            var cantActPorArea = "";
            var cantInactPorArea = "";
            var nombresAreas = "";
            List<int> todosLosIds = new List<int>();
            List<string> todosLosNombres = new List<string>();
            foreach (var item in audPorArea)
            {
                todosLosIds.Add(Convert.ToInt32(item.area_id));
                todosLosNombres.Add(item.Nombre);
            }
            var todosLosIdsDistinct = todosLosIds.Distinct().ToList();
            var todosLosNombresDinstinct = todosLosNombres.Distinct().ToList();
            if(todosLosNombresDinstinct.Count > 0)
            {
                foreach (var item in todosLosNombresDinstinct)
                {
                    nombresAreas += "\'" + item.ReemplazaCaracteresEspeciales() + "\'" + ",";
                }
            }
            if (nombresAreas != "")
                nombresAreas = nombresAreas.Substring(0, nombresAreas.Length - 1);
            ViewBag.Est_AudienciaPorArea = nombresAreas;

            for (int i = 0; i < todosLosIdsDistinct.Count; i++)
            {
                for (int j = 0; j < audPorArea.Count; j++)
                {
                    if (todosLosIdsDistinct[i] == audPorArea[j].area_id)
                    {
                        if(audPorArea[j].Active)
                            cantActPorArea += audPorArea[j].cantidad + ",";
                        else
                            cantInactPorArea += audPorArea[j].cantidad + ",";
                    }
                }
            }

            if (cantActPorArea != "")
                cantActPorArea = cantActPorArea.Substring(0, cantActPorArea.Length - 1);
            ViewBag.Est_ActPorArea = cantActPorArea;
            if (cantInactPorArea != "")
                cantInactPorArea = cantInactPorArea.Substring(0, cantInactPorArea.Length - 1);
            ViewBag.Est_InactPorArea = cantInactPorArea;
            #endregion

            #region AUDIENCIA POR EDAD
            var audPorEdad = db.est_AudienciaPorEdades(empresaId, DateTime.Now).ToList();
            string porEdadDecenaUnida = "";
            var cantActPorEdad = "";
            var cantInactPorEdad = "";

            // Calcular las etiquetas sin que se repitan los rangos.
            List<String> etiquetasRangoEdad;
            porEdadDecenaUnida = GetEtiquetasAudienciaPorEdad(audPorEdad, porEdadDecenaUnida, out etiquetasRangoEdad);

            GetCantidadesAudienciaPorEdad(audPorEdad, ref cantActPorEdad, ref cantInactPorEdad, etiquetasRangoEdad);


            if (porEdadDecenaUnida != "")
                porEdadDecenaUnida = porEdadDecenaUnida.Substring(0, porEdadDecenaUnida.Length - 1);
            ViewBag.porEdadDecenaUnida = porEdadDecenaUnida;
            if (cantActPorEdad != "")
                cantActPorEdad = cantActPorEdad.Substring(0, cantActPorEdad.Length - 1);
            ViewBag.cantActivosPorEdad = cantActPorEdad;
            if (cantInactPorEdad != "")
                cantInactPorEdad = cantInactPorEdad.Substring(0, cantInactPorEdad.Length - 1);
            ViewBag.cantInactivosPorEdad = cantInactPorEdad;
            #endregion

            #region AUDIENCIA POR GENERO          
            var audPorGenero = db.est_AudienciaPorGenero(empresaId);

            string dataGenero = "";
            foreach (var item in audPorGenero)
            {
                dataGenero += "['" + item.DescripcionLarga.ReemplazaCaracteresEspeciales() + "'," + item.cantidad + "],";
            }
            if (dataGenero != "")
                dataGenero = dataGenero.Substring(0, dataGenero.Length - 1);

            ViewBag.Est_AudienciaPorGenero = dataGenero;
            #endregion

            return View();
        }

        private static void GetCantidadesAudienciaPorEdad(List<est_AudienciaPorEdades_Result> audPorEdad, ref string cantActPorEdad, ref string cantInactPorEdad, List<String> etiquetasRangoEdad)
        {
            foreach (string etiqueta in etiquetasRangoEdad)
            {
                // Buscamos la información de activos para este rango de edades.
                var audDataActivo = audPorEdad.FirstOrDefault(aud => aud.DecenaUnida == etiqueta && aud.Activo.HasValue && aud.Activo == 1);
                if (audDataActivo != null && audDataActivo.cantidad.HasValue)
                {
                    cantActPorEdad += audDataActivo.cantidad + ",";
                }
                else
                {
                    cantActPorEdad += "0,";
                }

                // Buscamos la información de inactivos para este rango de edades.
                var audDataInactivo = audPorEdad.FirstOrDefault(aud => aud.DecenaUnida == etiqueta && aud.Activo.HasValue && aud.Activo == 0);
                if (audDataInactivo != null && audDataInactivo.cantidad.HasValue)
                {
                    cantInactPorEdad += audDataInactivo.cantidad + ",";
                }
                else
                {
                    cantInactPorEdad += "0,";
                }
            }
        }

        private static string GetEtiquetasAudienciaPorEdad(List<est_AudienciaPorEdades_Result> audPorEdad, string porEdadDecenaUnida, out List<String> etiquetasRangoEdad)
        {
            etiquetasRangoEdad = new List<String>();
            foreach (var item in audPorEdad)
            {
                if (item.DecenaUnida != null)
                {
                    if (etiquetasRangoEdad.Contains(item.DecenaUnida))
                    {
                        continue;
                    }
                    var etiqueta = item.DecenaUnida;
                    etiquetasRangoEdad.Add(etiqueta);
                    porEdadDecenaUnida += "\'" + etiqueta + "\'" + ",";
                }
            }

            return porEdadDecenaUnida;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
