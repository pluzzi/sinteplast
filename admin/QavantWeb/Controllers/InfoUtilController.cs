﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Ganss.XSS;
using Microsoft.AspNet.Identity;
using QavantWeb.Common;
using QavantWeb.Infrastructure.FileStorage;
using QavantWeb.Models;
using QavantWeb.Services;
using QavantWeb.Services.Settings;
using QavantWeb.ViewModels;

namespace QavantWeb.Controllers
{
    public class InfoUtilController : Controller
    {
        private const string DELETE_MARK = "B";
        private QavantEntities db = new QavantEntities();
        private int empresaId = SecurityHelpers.getEmpresaId();
        private int moduloId = (int)Enums.Modulos.InfoUtil;
        private HtmlSanitizer _sanitizer = new HtmlSanitizer();

        private readonly IFiltroService _filterService;
        private readonly ISettingsService _settingsService;
        private readonly IFileStorageFactory _fileStorageFactory;

        public InfoUtilController() : this(new SettingsService(Enums.Modulos.InfoUtil), new FileStorageFactory(), new FiltroService()) { }

        public InfoUtilController(ISettingsService settingsService, IFileStorageFactory fileStorageFactory, IFiltroService filterService)
        {
            _filterService = filterService;
            _settingsService = settingsService;
            _fileStorageFactory = fileStorageFactory;
        }

        // GET: InfoUtil
        public ActionResult Index()
        {
            var InfoUtil = db.InfoUtils.Where(w => w.EmpresaId == empresaId).Where(x => x.Habilitado == true).Include(i => i.AspNetUser).Include(i => i.Categoria);
            return View(InfoUtil.ToList());
        }

        // GET: InfoUtil/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InfoUtil infoPagina = db.InfoUtils.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            if (infoPagina == null)
            {
                return HttpNotFound();
            }
            return View(infoPagina);
        }

        // GET: InfoUtil/Create
        public ActionResult Create()
        {
            List<Categoria> lsCat = new List<Categoria>();
            List<Categoria> list = db.Categorias
                                        .Where(w => w.EmpresaId == empresaId)
                                        .Where(w => w.ModuloId == (int)Enums.Modulos.InfoUtil)
                                        .Where(w => w.Habilitada).ToList();
            Common.FormatterHelpers.CategoriasToTree(list, ref lsCat, null, "", true);
            ViewBag.CategoriaId = new SelectList(lsCat, "Id", "Nombre");

            var viewModel = new InfoUtilViewModel();
            var infoPagina = new InfoUtil();
            viewModel.InfoUtil = infoPagina;
            viewModel.FiltroConfig.Filtros = _filterService.GetFiltros(empresaId);
            return View(viewModel);
        }

        // POST: InfoUtil/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(InfoUtilViewModel model)
        {
            try
            {
                model.InfoUtil.Html = _sanitizer.Sanitize(Request.Form["__Content"]);
                HttpPostedFileBase picture = Request.Files["ImageFile"];
                if ((model.InfoUtil.Html == null || model.InfoUtil.Html.Length == 0) && (picture == null || picture.ContentLength == 0))
                {
                    ModelState.AddModelError("", "Es obligatorio completar los campos 'Imagen' y/o 'Información'.");
                }
                if (!ModelState.IsValid)
                {
                    List<Categoria> lsCat = new List<Categoria>();
                    List<Categoria> list = db.Categorias
                                                .Where(w => w.EmpresaId == empresaId)
                                                .Where(w => w.ModuloId == (int)Enums.Modulos.InfoUtil)
                                                .Where(w => w.Habilitada).ToList();
                    Common.FormatterHelpers.CategoriasToTree(list, ref lsCat, null, "", true);
                    ViewBag.CategoriaId = new SelectList(lsCat, "Id", "Nombre");
                    model.FiltroConfig.Filtros = _filterService.GetFiltros(empresaId);
                    return View(model);
                }
                SaveModel(model);
            }
            catch (Exception ex)
            {
                List<Categoria> lsCat = new List<Categoria>();
                List<Categoria> list = db.Categorias
                                            .Where(w => w.EmpresaId == empresaId)
                                            .Where(w => w.ModuloId == (int)Enums.Modulos.InfoUtil)
                                            .Where(w => w.Habilitada).ToList();
                Common.FormatterHelpers.CategoriasToTree(list, ref lsCat, null, "", true);
                ViewBag.CategoriaId = new SelectList(lsCat, "Id", "Nombre");
                model.FiltroConfig.Filtros = _filterService.GetFiltros(empresaId);
                ModelState.AddModelError("", "Error en el alta.  \n " + ex.Message);
                return View(model);
            }
            return RedirectToAction("Index");
        }

        private void SaveModel(InfoUtilViewModel model)
        {
            using (DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    HttpPostedFileBase file = Request.Files["ImageFile"];
                    if (file != null && file.ContentLength != 0)
                    {
                        model.InfoUtil.Imagen = SaveFile(file);
                    }
                    model.FiltroConfig.Filtros = _filterService.GetFiltros(empresaId);
                    model.InfoUtil.AspNetUsersIdAlta = User.Identity.GetUserId();
                    model.InfoUtil.FechaAlta = DateTime.Now;
                    model.InfoUtil.Habilitado = true;
                    model.InfoUtil.EmpresaId = empresaId;
                    db.InfoUtils.Add(model.InfoUtil);
                    db.SaveChanges();

                    //Segmentación
                    foreach (var item in model.FiltroConfig.Segmentacion.SegmentoCollection)
                    {
                        if (item.Action == DELETE_MARK)
                            continue;
                        var segmentoDb = new Segmentacion
                        {
                            EmpresaId = empresaId,
                            ModuloId = moduloId,
                            EntidadId = model.InfoUtil.Id,
                            FiltroId = item.TipoFiltroId,
                            FiltroValor = item.ValorFiltroId
                        };
                        db.Segmentacions.Add(segmentoDb);
                    }
                    db.SaveChanges();
                    dbTran.Commit();
                }
                catch (Exception ex)
                {
                    dbTran.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        // GET: InfoUtil/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InfoUtil infoPagina = db.InfoUtils.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            if (infoPagina == null)
            {
                return HttpNotFound();
            }
            List<Categoria> lsCat = new List<Categoria>();
            List<Categoria> list = db.Categorias
                                        .Where(w => w.EmpresaId == empresaId)
                                        .Where(w => w.ModuloId == (int)Enums.Modulos.InfoUtil)
                                        .Where(w => w.Habilitada).ToList();
            Common.FormatterHelpers.CategoriasToTree(list, ref lsCat, null, "", true);

            SelectList lsCategoriaPadre = new SelectList(lsCat, "Id", "Nombre");
            foreach (var item in lsCategoriaPadre)
            {
                if (infoPagina.CategoriaId == Convert.ToInt32(item.Value))
                {
                    item.Selected = true;
                }
            }
            ViewBag.CategoriaId = lsCategoriaPadre;
            var viewModel = new InfoUtilViewModel();
            viewModel.InfoUtil = infoPagina;
            viewModel.FiltroConfig.Filtros = _filterService.GetFiltros(empresaId);
            viewModel.FiltroConfig.Segmentacion = _filterService.GetSegmentacion(empresaId, moduloId, infoPagina.Id);

            return View(viewModel);
        }

        // POST: InfoUtil/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(InfoUtilViewModel model, string foto)
        {
            if (model.InfoUtil.EmpresaId != empresaId)
                ModelState.AddModelError("", "Error al actualizar el registro.");
            try
            {
                model.InfoUtil.Imagen = (string.IsNullOrEmpty(foto)) ? string.Empty : foto;
                if (model.InfoUtil.Imagen == "")
                    model.InfoUtil.Imagen = null;

                model.InfoUtil.Html = _sanitizer.Sanitize(Request.Form["__Content"]);  
                
                if ((model.InfoUtil.Html == null || model.InfoUtil.Html.Length == 0) && (model.InfoUtil.Imagen == null || model.InfoUtil.Imagen.Length == 0 ))
                {
                    ModelState.AddModelError("", "Es obligatorio completar los campos 'Imagen' y/o 'Información'.");
                }

                if (!ModelState.IsValid)
                {
                    List<Categoria> lsCat = new List<Categoria>();
                    List<Categoria> list = db.Categorias
                                                .Where(w => w.EmpresaId == empresaId)
                                                .Where(w => w.ModuloId == (int)Enums.Modulos.InfoUtil)
                                                .Where(w => w.Habilitada).ToList();
                    Common.FormatterHelpers.CategoriasToTree(list, ref lsCat, null, "", true);
                    SelectList lsCategoriaPadre = new SelectList(lsCat, "Id", "Nombre");
                    foreach (var item in lsCategoriaPadre)
                    {
                        if (model.InfoUtil.CategoriaId == Convert.ToInt32(item.Value))
                        {
                            item.Selected = true;
                        }
                    }
                    ViewBag.CategoriaId = lsCategoriaPadre;
                    model.FiltroConfig.Filtros = _filterService.GetFiltros(empresaId);
                    model.FiltroConfig.Segmentacion = _filterService.GetSegmentacion(empresaId, moduloId, model.InfoUtil.Id);
                    return View(model);
                }

                UpdateModel(model);
                return RedirectToAction("Index");

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error en el alta.  \n " + ex.Message);
                List<Categoria> lsCat = new List<Categoria>();
                List<Categoria> list = db.Categorias
                                            .Where(w => w.EmpresaId == empresaId)
                                            .Where(w => w.ModuloId == (int)Enums.Modulos.InfoUtil)
                                            .Where(w => w.Habilitada).ToList();
                Common.FormatterHelpers.CategoriasToTree(list, ref lsCat, null, "", true);
                SelectList lsCategoriaPadre = new SelectList(lsCat, "Id", "Nombre");
                foreach (var item in lsCategoriaPadre)
                {
                    if (model.InfoUtil.CategoriaId == Convert.ToInt32(item.Value))
                    {
                        item.Selected = true;
                    }
                }
                ViewBag.CategoriaId = lsCategoriaPadre;
                model.FiltroConfig.Filtros = _filterService.GetFiltros(empresaId);
                model.FiltroConfig.Segmentacion = _filterService.GetSegmentacion(empresaId, moduloId, model.InfoUtil.Id);
                return View(model);
            }
        }

        private void UpdateModel(InfoUtilViewModel model)
        {
            using (DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    HttpPostedFileBase file = Request.Files["ImageFile"];
                    if (file != null && file.ContentLength != 0)
                    {
                        model.InfoUtil.Imagen = SaveFile(file);
                    }
                    model.InfoUtil.AspNetUsersIdAlta = User.Identity.GetUserId();
                    model.InfoUtil.FechaAlta = DateTime.Now;
                    model.InfoUtil.Habilitado = true;

                    db.Entry(model.InfoUtil).State = EntityState.Modified;
                    db.SaveChanges();


                    // Segmentación
                    db.Segmentacions.RemoveRange(db.Segmentacions.Where(x => x.EmpresaId == empresaId && x.ModuloId == moduloId && x.EntidadId == model.InfoUtil.Id));
                    foreach (var segmento in model.FiltroConfig.Segmentacion.SegmentoCollection)
                    {
                        if (segmento.Action != DELETE_MARK)
                        {
                            var segmentoDb = new Segmentacion
                            {
                                EmpresaId = empresaId,
                                ModuloId = moduloId,
                                EntidadId = model.InfoUtil.Id,
                                FiltroId = segmento.TipoFiltroId,
                                FiltroValor = segmento.ValorFiltroId
                            };
                            db.Segmentacions.Add(segmentoDb);
                        }
                    }
                    db.SaveChanges();
                    dbTran.Commit();
                }
                catch (Exception ex)
                {
                    dbTran.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        // GET: InfoUtil/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InfoUtil infoPagina = db.InfoUtils.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            if (infoPagina == null)
            {
                return HttpNotFound();
            }
            return View(infoPagina);
        }

        // POST: InfoUtil/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            InfoUtil infoPagina = db.InfoUtils.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            infoPagina.AspNetUsersIdBaja = User.Identity.GetUserId();
            infoPagina.FechaBaja = DateTime.Now;
            infoPagina.Habilitado = false;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Noticias        
        [Authorize(Roles = "Administrador")]
        public ActionResult IndexDeleted()
        {
            var InfoUtil = db.InfoUtils.Where(w => w.EmpresaId == empresaId).Where(x => x.Habilitado == false).Include(i => i.AspNetUser).Include(i => i.Categoria);
            return View(InfoUtil.ToList());
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult Enable(int id)
        {
            InfoUtil info = db.InfoUtils.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            info.AspNetUsersIdAlta = User.Identity.GetUserId();
            info.Habilitado = true;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Internal members
        private string SaveFile(HttpPostedFileBase file)
        {
            var parametros = _settingsService.GetParametrosGlobales();
            var fileStorage = _fileStorageFactory.CreateFileStorage(parametros);
            var filename = FileStorageHelper.FormatFilename(file, parametros);
            var container = parametros["ContainerInfoUtil"];
            return fileStorage.Save(file, filename, container);
        }
        #endregion

    }
}
