﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QavantWeb.Common;
using QavantWeb.Models;

namespace QavantWeb.Controllers
{
    public class EstablecimientoController : Controller
    {
        private QavantEntities db = new QavantEntities();
        private int empresaId = SecurityHelpers.getEmpresaId();

        // GET: Establecimiento
        public ActionResult Index()
        {
            var establecimientos = db.Establecimientos.Include(e => e.Empresa).Where(w => w.EmpresaId == empresaId);
            return View(establecimientos.ToList());
        }

        // GET: Establecimiento/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Establecimiento establecimiento = db.Establecimientos.Find(id);
            if (establecimiento == null)
            {
                return HttpNotFound();
            }
            return View(establecimiento);
        }

        // GET: Establecimiento/Create
        public ActionResult Create()
        {           
            return View();
        }

        // POST: Establecimiento/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Establecimiento establecimiento)
        {
            if (ModelState.IsValid)
            {
                establecimiento.EmpresaId = empresaId;
                db.Establecimientos.Add(establecimiento);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            var establecimientos = db.Establecimientos.Include(e => e.Empresa).Where(w => w.EmpresaId == empresaId);
            return View(establecimientos);
        }

        // GET: Establecimiento/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Establecimiento establecimiento = db.Establecimientos.Where(w => w.EmpresaId == empresaId).FirstOrDefault(w => w.Id == id);
            if (establecimiento == null)
            {
                return HttpNotFound();
            }
           
            return View(establecimiento);
        }

        // POST: Establecimiento/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Establecimiento establecimiento)
        {
            if (ModelState.IsValid)
            {
                db.Entry(establecimiento).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EmpresaId = new SelectList(db.Empresas, "Id", "Nombre", establecimiento.EmpresaId);
            return View(establecimiento);
        }

        // GET: Establecimiento/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Establecimiento establecimiento = db.Establecimientos.Where(w => w.EmpresaId == empresaId).FirstOrDefault(w => w.Id == id);
            if (establecimiento == null)
            {
                return HttpNotFound();
            }
            return View(establecimiento);
        }

        // POST: Establecimiento/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Establecimiento establecimiento = db.Establecimientos.Where(w => w.EmpresaId == empresaId).FirstOrDefault(w => w.Id == id);
            if(establecimiento.Estudios.Count > 0)
            {
                ModelState.AddModelError("", "No es posible eliminar el establecimiento. El establecimiento posee usuarios asociados. ");
            }
            else
            {
                db.Establecimientos.Remove(establecimiento);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(establecimiento);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
