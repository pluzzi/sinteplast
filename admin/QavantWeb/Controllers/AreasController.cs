﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QavantWeb.Models;
using QavantWeb.Common;
using QavantWeb.Services;

namespace QavantWeb.Controllers
{
    public class AreasController : Controller
    {
        private QavantEntities db = new QavantEntities();
        private int empresaId = SecurityHelpers.getEmpresaId();
        private readonly IFiltroService _filtroService;

        public AreasController() : this(new FiltroService()) { }

        public AreasController(IFiltroService filtroService)
        {
            _filtroService = filtroService;
        }




        // GET: Areas
        [Authorize(Roles = "Administrador")]
        public ActionResult Index()
        {
            List<Area> lsArea = new List<Area>();
            Common.FormatterHelpers.AreasToTree(db.Areas.Where(w => w.EmpresaId == empresaId).ToList(), ref lsArea, null, "", true);
            return View(lsArea);
        }

        // GET: Areas deshabilitadas    
        [Authorize(Roles = "Administrador")]
        public ActionResult IndexDeleted()
        {
            var areasDeshabilitadas = db.Areas.Where(w => w.EmpresaId == empresaId).Where(x => x.Habilitada == false).ToList().OrderByDescending(x => x.Nombre).ToList();
            return View(areasDeshabilitadas);
        }

        // GET: Areas/Details/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Area area = db.Areas.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            if (area == null)
            {
                return HttpNotFound();
            }
            return View(area);
        }

        // GET: Areas/Create
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            List<Area> lsArea = new List<Area>();
            Common.FormatterHelpers.AreasToTree(db.Areas.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitada).ToList(), ref lsArea, null, "", true);
            //lsAreaPadre.Insert(0, new SelectListItem { Text = " Sin padre", Value = "0" });
            List<SelectListItem> lsAreaPadre = new SelectList(lsArea, "Id", "Nombre").ToList();
            lsAreaPadre.Insert(0, new SelectListItem { Text = " Sin padre", Value = "0" });
            ViewBag.AreaPadreId = lsAreaPadre;

            return View();
        }

        // POST: Areas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult Create(Area area)
        {
            if (ModelState.IsValid)
            {
                if (area.AreaPadreId == 0)
                    area.AreaPadreId = null;
                area.EmpresaId = empresaId;
               // area.Habilitada = true;
                db.Areas.Add(area);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            List<Area> lsArea = new List<Area>();
            Common.FormatterHelpers.AreasToTree(db.Areas.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitada).ToList(), ref lsArea, null, "", true);
            List<SelectListItem> lsAreaPadre = new SelectList(lsArea, "Id", "Nombre", area.AreaPadreId).ToList();
            lsAreaPadre.Insert(0, new SelectListItem { Text = " Sin padre", Value = "0" });
            ViewBag.AreaPadreId = lsAreaPadre;

            return View(area);
        }

        // GET: Areas/Edit/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Area area = db.Areas.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            if (area == null)
            {
                return HttpNotFound();
            }

            List<Area> lsArea = new List<Area>();
            Common.FormatterHelpers.AreasToTree(db.Areas.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitada).ToList(), ref lsArea, null, "", true);
            List<SelectListItem> lsAreaPadre = new SelectList(lsArea, "Id", "Nombre", area.AreaPadreId).ToList();
            lsAreaPadre.Insert(0, new SelectListItem { Text = " Sin padre", Value = "0" });
            ViewBag.AreaPadreId = lsAreaPadre;

            return View(area);
        }

        // POST: Areas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(Area area)
        {
            if(area.Id == area.AreaPadreId)
            {
                ModelState.AddModelError("", "El área padre debe diferir del área actual.");
            }
            if (area.EmpresaId != empresaId)
                ModelState.AddModelError("", "Error al actualizar el registro.");

            if (ModelState.IsValid)
            {
                if (area.AreaPadreId == 0)
                {
                    area.AreaPadreId = null;
                }
                db.Entry(area).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            List<Area> lsArea = new List<Area>();
            Common.FormatterHelpers.AreasToTree(db.Areas.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitada).ToList(), ref lsArea, null, "", true);
            List<SelectListItem> lsAreaPadre = new SelectList(lsArea, "Id", "Nombre", area.AreaPadreId).ToList();
            lsAreaPadre.Insert(0, new SelectListItem { Text = " Sin padre", Value = "0" });
            ViewBag.AreaPadreId = lsAreaPadre;
            return View(area);
        }

        // GET: Areas/Delete/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {

            if (id != 0)
            {
                if (db.Areas.FirstOrDefault(f => f.Id == id).EmpresaId != empresaId)
                    //Intenton hackear por empresaId
                    return RedirectToAction("Index");
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Area area = db.Areas.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            if (area == null)
            {
                return HttpNotFound();
            }

            List<Area> lsArea = new List<Area>();
            Common.FormatterHelpers.AreasToTree(db.Areas.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitada).ToList(), ref lsArea, null, "", true);
            List<SelectListItem> lsAreaPadre = new SelectList(lsArea, "Id", "Nombre", area.AreaPadreId).ToList();
            lsAreaPadre.Insert(0, new SelectListItem { Text = " Sin padre", Value = "0" });
            ViewBag.AreaPadreId = lsAreaPadre;
            return View(area);
        }

        // POST: Areas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult DeleteConfirmed(int id)
        {
            Area area = db.Areas.FirstOrDefault(a => a.Id == id &&  a.EmpresaId == empresaId);

            if (area == null)
            {
                return HttpNotFound();
            }

            // valida usuarios asociados.
            if (area.Users.Count > 0)
            {
                ModelState.AddModelError("", "No es posible eliminar el área. El área posee usuarios asociados. ");
            }

            // valida subáreas asociadas.
            if (area.Areas1.Count > 0)
            {
                ModelState.AddModelError("", "No es posible eliminar el área. El área posee subáreas asociadas. ");
            }

            // valida filtros asociados.
            var cantFiltros = _filtroService.GetSegmentationCountByFilterValue(empresaId, Enums.Filtros.Areas, id);
            if (cantFiltros > 0)
            {
                ModelState.AddModelError("", "No es posible eliminar el área. El área posee filtros asociados.");
            }

            if (ModelState.IsValid)
            {
                db.Areas.Remove(area);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            List<Area> lsArea = new List<Area>();
            Common.FormatterHelpers.AreasToTree(db.Areas.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitada).ToList(), ref lsArea, null, "", true);
            List<SelectListItem> lsAreaPadre = new SelectList(lsArea, "Id", "Nombre", area.AreaPadreId).ToList();
            lsAreaPadre.Insert(0, new SelectListItem { Text = " Sin padre", Value = "0" });
            ViewBag.AreaPadreId = lsAreaPadre;
            return View(area);
        }



        [Authorize(Roles = "Administrador")]
        public ActionResult Enable(int id)
        {
            Area area = db.Areas.Where(w => w.EmpresaId == empresaId).Single(f => f.Id == id);
            area.Habilitada = true;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
