﻿//using DotNet.Highcharts;
//using DotNet.Highcharts.Enums;
//using DotNet.Highcharts.Helpers;
//using DotNet.Highcharts.Options;
//using QavantWeb.Models;
//using System;
//using System.Collections.Generic;
//using System.Drawing;
//using System.Linq;
//using System.Linq.Dynamic;
//using System.Web.Mvc;
//using QavantWeb.extensions;
//using System.IO;
//using System.Text;

//namespace QavantWeb.Controllers
//{
//    public class ResultList
//    {
//        List<object> Data;
//        double Avarage;

//    }
//    public class PerfomanceReviewController : BaseController
//    {
//        readonly Repository<Area> areaRepository;

//        readonly Repository<FormsQuestionsOption> formQuestionsOptionRepository;

//        readonly Repository<Form> formRepository;

//        // GET: PerfomanceReview
//        readonly Repository<FormsPerformanceEvaluation> formsperfomancereviewRepository;

//        readonly Repository<User> userRepository;

//        public PerfomanceReviewController()
//        {
//            formsperfomancereviewRepository = unitOfWork.Repository<FormsPerformanceEvaluation>();
//            userRepository = unitOfWork.Repository<User>();
//            formRepository = unitOfWork.Repository<Form>();
//            formQuestionsOptionRepository = unitOfWork.Repository<FormsQuestionsOption>();
//            areaRepository = unitOfWork.Repository<Area>();
//        }

//        public ActionResult Details(int id)
//        {
//            var form = formsperfomancereviewRepository.GetById(id);
//            return View(form);
//        }

//        public ActionResult Create(int formId)
//        {
//            var form = formRepository.Table.Where(w => w.Enabled).FirstOrDefault(w => w.Id == formId);
//            if (form == null)
//            {
//                return RedirectToAction(nameof(CreateResult), new { success = false, message = $"Ha ocurrido un error: {ResourceSite.FORM_DO_NOT_EXISTS}", formId = 0, formtitle = string.Empty });
//            }

//            if (form.FormTypeId != (int)Enums.FormsTypes.EvaluacionDesempenio)
//            {
//                return RedirectToAction(nameof(CreateResult), new { success = false, message = $"Ha ocurrido un error: {ResourceSite.FORM_IS_NOT_PERFOMANCE_REVIEW}", formId = 0, formtitle = string.Empty });
//            }

//            if (form.DateFrom > DateTime.Now || form.DateTo < DateTime.Now)
//            {
//                return RedirectToAction(nameof(CreateResult), new { success = false, message = $"Ha ocurrido un error: {ResourceSite.FORM_IS_NOT_OPEN_DUE_DATE}. Periodo de Evaluacion:{form.DateFrom.ToShortDateString()} - {form.DateTo.ToShortDateString()}", formId = 0, formtitle = string.Empty });
//            }

//            var lsUsers = userRepository.Table.Where(w => w.Enable).OrderBy(o => o.Surname).ThenBy(t => t.Name).ToList();
//            ViewBag.Users = lsUsers.Select(s => new
//            {
//                s.Id,
//                Name = s.Surname + ", " + s.Name
//            });
//            ViewBag.Evaluators = lsUsers.Where(w => w.IsEvaluator).Select(s => new
//            {
//                s.Id,
//                Name = s.Surname + ", " + s.Name
//            });

//            var formsPerfomanceReview = new FormsPerformanceEvaluation();

//            formsPerfomanceReview.Form = formRepository.Table.Where(w => w.Enabled).FirstOrDefault(w => w.Id == formId);
//            formsPerfomanceReview.Form_Id = formsPerfomanceReview.Form.Id;
//            formsPerfomanceReview.Form.FormsQuestions.ToList().ForEach(x => formsPerfomanceReview.FormsAnswers.Add(new FormsAnswer { Form_Id = formsPerfomanceReview.Form_Id, FormsQuestion_Id = x.Id, Date = DateTime.Now }));
//            formsPerfomanceReview.StartedDate = DateTime.Now;
//            return View(formsPerfomanceReview);
//        }

//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create(FormsPerformanceEvaluation f)
//        {
//            var perfomanceReview = formsperfomancereviewRepository.Table.Where(x => x.Evaluated_Id == f.Evaluated_Id && x.Form_Id == f.Form_Id).FirstOrDefault();
//            if (perfomanceReview != null)
//            {
//                return RedirectToAction(nameof(CreateResult), new { success = false, message = $"Ha ocurrido un error: {ResourceSite.EVALUATION_ALREADY_ENTERED}", formId = 0, formtitle = string.Empty });
//            }
//            if (ModelState.IsValid)
//            {
//                try
//                {
//                    f.EvaluationStatus_Id = 1;
//                    foreach (var item in f.FormsAnswers)
//                    {
//                        item.User_Id = f.Evaluator_Id;
//                    }
//                    formsperfomancereviewRepository.Insert(f);
//                    return RedirectToAction(nameof(CreateResult), new { success = true, message = "La Evaluacion de Desempeño fue creada satisfactoriamente", formId = f.Form_Id, formtitle = formRepository.GetById(f.Form_Id).Title });
//                }
//                catch (Exception exc)
//                {
//                    return RedirectToAction(nameof(CreateResult), new { success = false, message = $"Ha ocurrido un error: {exc.Message}", formId = f.Form_Id, formtitle = formRepository.GetById(f.Form_Id).Title });
//                }
//            }
//            var lsUsers = userRepository.Table.Where(w => w.Enable).OrderBy(o => o.Surname).ThenBy(t => t.Name).ToList();
//            ViewBag.Users = lsUsers.Select(s => new
//            {
//                s.Id,
//                Name = s.Surname + ", " + s.Name
//            });
//            ViewBag.Evaluators = lsUsers.Where(w => w.IsEvaluator).Select(s => new
//            {
//                s.Id,
//                Name = s.Surname + ", " + s.Name
//            });
//            return View(f);
//        }

//        public ActionResult CreateResult(bool success, string message, string formtitle, int formId)
//        {
//            ViewBag.FormTitle = formtitle;
//            ViewBag.Success = success;
//            ViewBag.Message = message;
//            ViewBag.FormId = formId.ToString();
//            return View();
//        }

//        public ActionResult DrawChart(int formId)
//        {
//            var form = formRepository.GetById(formId);
//            ViewBag.FormName = form.Title;
//            ViewBag.TotalAverageForm = form.FormsPerformanceEvaluations.ToList().DefaultIfEmpty().Average(x => x.GetTotalAverage());
//            ViewBag.EvaluationsTotalQty = form.FormsPerformanceEvaluations.Count;
//            //By Groups
//            var charts = new List<Highcharts>();
//            foreach (var group in form.FormsQuestionGroups.Where(x => x.ShowsAverage))
//            {
//                var groupAnswers = new List<FormsAnswer>();
//                foreach (var question in group.FormsQuestions)
//                {
//                    var answers = formsperfomancereviewRepository.Table.Where(x => x.Form_Id == formId).Select(x => x.FormsAnswers.Where(a => a.FormsQuestion_Id == question.Id));
//                    answers.ToList().ForEach(x => groupAnswers.AddRange(x));
//                }
//                var result = groupAnswers.GroupBy(x => x.FormsQuestionsOption.Description ).Select(g => new object[] { "#" + g.Key, g.Count() * 100 / groupAnswers.Count }).ToArray();
//                var chart = createPieChart($"chart{group.Id}", group.Description, result);
//                charts.Add(chart);
//            }
//            ViewBag.GroupCharts = charts;
//            ////Qty By Evaluators
//            //var dataPerfomanceReviews = formsperfomancereviewRepository.Table.Where(x => x.Form_Id == formId);
//            //var dataByEvaluator = dataPerfomanceReviews.GroupBy(x => new { x.Evaluator_Id, UserName = x.User1.Name + " " + x.User1.Surname }).Select(g => new { g.Key.UserName, percent = g.Count() * 100 / dataPerfomanceReviews.ToList().Count }).ToList();

//            //object[][] array = new object[dataByEvaluator.Count][];
//            //for (int i = 0; i < dataByEvaluator.Count; i++)
//            //{
//            //    array[i] = new object[] { dataByEvaluator[i].UserName, dataByEvaluator[i].percent };
//            //}
//            //Highcharts chartEvaluator = createPieChart("chartEvaluator", "Grafico por Evaluador", array);
//            //ViewBag.EvaluatorChart = chartEvaluator;
//            //Opions By Evaluator
//            var dataOptionsByEvaluator = formsperfomancereviewRepository.Table.Where(x => x.Form_Id == formId).SelectMany(x => x.FormsAnswers.Where(a => a.FormsQuestion.FormsQuestionGroup.ShowsAverage)).Select(x => new { Evaluator = x.FormsPerformanceEvaluation.User1, SelectedValue = x.FormsQuestionsOption.Description }).AsEnumerable().GroupBy(x => new { x.Evaluator, SelectedValue = x.SelectedValue }).Select(x => new { Name = $"{ x.Key.Evaluator.Name} { x.Key.Evaluator.Surname}", x.Key.SelectedValue, qty = x.Count() });//     (x=> new { Evaluador = x.User1, option = x.FormsAnswers }).GroupBy(x => new { x.Evaluador, x.FormsAnswers.Select(a => a.Answer) });

//            var arrayEvaluators = dataOptionsByEvaluator.GroupBy(x => x.Name).Select(x => x.Key).ToArray();
//            var dataSeries = dataOptionsByEvaluator.GroupBy(x => x.SelectedValue).Select(x => new Series { Name = "#" + x.Key.ToString() }).ToArray();
//            foreach (var numbeValue in dataSeries)
//            {
//                var qtyByNumberValue = new object[arrayEvaluators.Count()];
//                var i = 0;
//                foreach (var evaluator in arrayEvaluators)
//                {
//                    var qty = dataOptionsByEvaluator.FirstOrDefault(x => x.Name == evaluator && "#" + x.SelectedValue == numbeValue.Name)?.qty;
//                    qty = qty == null ? 0 : qty;
//                    qtyByNumberValue[i] = qty;
//                    i++;
//                }
//                numbeValue.Data = new Data(qtyByNumberValue);
//            }
//            ViewBag.OptionsByEvaluatorChart = createStackedPercentageColumn("chartOptionbyEvaluator", "Seleccion por Evaluador", arrayEvaluators, dataSeries, "");

//            //Opions By Area
//            var dataOptionsByArea = formsperfomancereviewRepository.Table.Where(x => x.Form_Id == formId).SelectMany(x => x.FormsAnswers.Where(a => a.FormsQuestion.FormsQuestionGroup.ShowsAverage)).Select(x => new { AreaEvaluated = x.FormsPerformanceEvaluation.Area, SelectedValue = x.FormsQuestionsOption.Description }).AsEnumerable().GroupBy(x => new { x.AreaEvaluated, SelectedValue = x.SelectedValue}).Select(x => new { Name = x.Key.AreaEvaluated.Nombre, x.Key.SelectedValue, qty = x.Count() });//     (x=> new { Evaluador = x.User1, option = x.FormsAnswers }).GroupBy(x => new { x.Evaluador, x.FormsAnswers.Select(a => a.Answer) });

//            var arrayAreasEvaluated = dataOptionsByArea.GroupBy(x => x.Name).Select(x => x.Key).ToArray();
//            dataSeries = dataOptionsByArea.GroupBy(x => x.SelectedValue).Select(x => new Series { Name = "#" + x.Key.ToString() }).ToArray();
//            foreach (var numbeValue in dataSeries)
//            {
//                var qtyByNumberValue = new object[arrayAreasEvaluated.Count()];
//                var i = 0;
//                foreach (var evaluator in arrayAreasEvaluated)
//                {
//                    var qty = dataOptionsByArea.FirstOrDefault(x => x.Name == evaluator && "#" + x.SelectedValue == numbeValue.Name)?.qty;
//                    qty = qty == null ? 0 : qty;
//                    qtyByNumberValue[i] = qty;
//                    i++;
//                }
//                numbeValue.Data = new Data(qtyByNumberValue);
//            }
//            ViewBag.OptionsByAreaEvaluatedChart = createStackedPercentageColumn("chartOptionbyAreaEvaluated", "Por Area", arrayAreasEvaluated, dataSeries, "");

//            ////By Area
//            //var databyEvaluatedArea = dataPerfomanceReviews.GroupBy(x => x.Area.Name).Select(g => new { g.Key, percent = g.Count() * 100 / dataPerfomanceReviews.Count() }).ToList();

//            //array = new object[databyEvaluatedArea.Count][];
//            //for (int i = 0; i < databyEvaluatedArea.Count; i++)
//            //{
//            //    array[i] = new object[] { databyEvaluatedArea[i].Key, databyEvaluatedArea[i].percent };
//            //}
//            //Highcharts chartEvaluatedArea = createPieChart("chartEvaluatedArea", "Grafico por Area", array);
//            //ViewBag.EvaluatedAreaChart = chartEvaluatedArea;

//            //progress of evaluations
//            //var areaData = userRepository.Table.Where(x => x.Enable).GroupBy(u => new { u.Area_Id , u.Area.Name}).Select(x => new { AreaName = x.Key.Name, QtyUserArea=x.Count(), QtyEvaluated = dataPerfomanceReviews.Where(p => p.Area.Id == x.Key.Area_Id ).Count() }).ToList();
//            //object[] arrayEvaluatedByArea = new object[areaData.Count()];
//            //object[] arrayPendingByArea = new object[areaData.Count()];
//            //for (int i = 0; i < areaData.Count(); i++)
//            //{
//            //    arrayEvaluatedByArea[i] = areaData[i].QtyEvaluated;
//            //    arrayPendingByArea[i] = areaData[i].QtyUserArea - areaData[i].QtyEvaluated;
//            //}
//            //var dataCategories = areaData.Select(x => x.AreaName).ToArray();
//            //dataSeries = new Series[]
//            //{
//            //    new Series { Name = "Evaluados", Data = new Data(arrayEvaluatedByArea) },
//            //    new Series { Name = "Pendientes", Data = new Data(arrayPendingByArea) }
//            //};

//            //Highcharts chartEvaluatesByArea = createStackChart("chartEvaluatedbyArea", "Evaluados vs Pendientes de Evaluar", dataCategories, dataSeries, "Avance por areas");
//            //ViewBag.EvaluatedByAreaChart = chartEvaluatesByArea;
//            var lsUsers = userRepository.Table.Where(w => w.Enable).OrderBy(o => o.Surname).ThenBy(t => t.Name).ToList();
//            ViewBag.Users = lsUsers.Select(s => new
//            {
//                s.Id,
//                Name = s.Surname + ", " + s.Name
//            });
//            ViewBag.Evaluators = lsUsers.Where(w => w.IsEvaluator).Select(s => new SelectListItem 
//            {
//                Value = s.Id.ToString(),
//                Text = s.Surname + ", " + s.Name
//            });
//            ViewBag.Area_Id = new SelectList(areaRepository.Table.OrderBy(p => p.Nombre), "Id", "Nombre");
//            ViewBag.formId = formId;
//            return View();
//        }

//        [HttpPost]
//        public ActionResult GetPerfomanceReview(int formId)
//        {
//            var draw = Request.Form.GetValues("draw").FirstOrDefault();
//            var start = Request.Form.GetValues("start").FirstOrDefault();
//            var length = Request.Form.GetValues("length").FirstOrDefault();
//            //var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault()
//            //                        + "][name]").FirstOrDefault();
//            //var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
//            var Evaluated_Id = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
//            var Area_Id = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
//            var Evaluator_Id = Request.Form.GetValues("columns[3][search][value]").FirstOrDefault();

//            var pageSize = length != null ? Convert.ToInt32(length) : 0;
//            var skip = start != null ? Convert.ToInt16(start) : 0;
//            var recordsTotal = 0;

//            var v = formsperfomancereviewRepository.Table.Where(f => f.Form_Id == formId);//.Select( x=> new { PerfomanceReview = x, x.Id, x.Evaluated_Id, x.Evaluator_Id, x.EvaluatedArea_Id, StartedDate = x.StartedDate , EvaluatedName = x.User.Name+" "+x.User.Surname, EvaluatedArea = x.Area.Name, EvaluatorName = x.User1.Name + " " + x.User1.Surname, EvaluatorArea = x.Area1.Name, TotalAverage=string.Empty });
            
//            if (!string.IsNullOrEmpty(Evaluated_Id))
//            {
//                var evaluated_Id = int.Parse(Evaluated_Id);
//                v = v.Where(a => a.Evaluated_Id == evaluated_Id);
//            }
//            if (!string.IsNullOrEmpty(Area_Id))
//            {
//                var area_Id = int.Parse(Area_Id);
//                v = v.Where(a => a.EvaluatedArea_Id == area_Id);
//            }
//            if (!string.IsNullOrEmpty(Evaluator_Id))
//            {
//                var evaluator_Id = Evaluator_Id.Split(',');
//                v = v.Where(a => evaluator_Id.Contains(a.Evaluator_Id.ToString()));// a.Evaluator_Id == evaluator_Id);
//            }
//            //if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
//            //{
//            //    v = v.OrderBy(sortColumn + " " + sortColumnDir);
//            //}
//            recordsTotal = v.Count();
//            //var data = v.Skip(skip).Take(pageSize);
//            var data = v.Take(pageSize);

//            var TotalAverageForm = data.ToList().DefaultIfEmpty().Average(x => x.GetTotalAverage());
//            //.ToList();//.AsEnumerable().Select(x => new { StartedDate = x.StartedDate.ToShortDateString(), EvaluatedName = $"{x.User.Name} {x.User.Surname}", EvaluatedArea = x.Area.Name, EvaluatorName = $"{x.User1.Name} {x.User1.Surname}", EvaluatorArea = x.Area1.Name });
//            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data.AsEnumerable().Select( x => new { x.Id, x.Evaluated_Id, x.Evaluator_Id, x.EvaluatedArea_Id, StartedDate = x.StartedDate, EvaluatedName = x.User.Name + " " + x.User.Surname, EvaluatedArea = x.Area.Nombre, EvaluatorName = x.User1.Name + " " + x.User1.Surname, EvaluatorArea = x.Area1.Nombre,  TotalAverage = x.GetTotalAverage().ToString("0.##") }) },
//                JsonRequestBehavior.AllowGet);
//        }

//        [HttpPost]
//        public JsonResult GetUser(int userId)
//        {
//            var obj = userRepository.GetById(userId);
//            return Json(new { AreaName = obj.Area.Nombre, obj.JobTitle, obj.Area_Id });
//        }
//        [HttpPost]
//        public JsonResult GetEvaluateds(int EvaluatorId)
//        {
//            var evaluateds = userRepository.Table.Where(x => x.Enable && x.EvaluatorUser_Id == EvaluatorId).Select(p => new SelectListItem { Value = p.Id.ToString(), Text = p.Name + " " + p.Surname });
//            return Json(new SelectList(evaluateds, "Value", "Text", JsonRequestBehavior.AllowGet));
//        }

//        [HttpPost]
//        public JsonResult GetEvaluatedsByEvaluatorsList(string[] EvaluatorsId)
//        {
//            var evaluatedQuery = userRepository.Table.Where(x => x.Enable && x.EvaluatorUser_Id != null);
//            if (EvaluatorsId != null)
//            {
//                var evaluatedsByEvaluators = evaluatedQuery.Where(x => EvaluatorsId.Contains(x.EvaluatorUser_Id.ToString())).Select(p => new SelectListItem { Value = p.Id.ToString(), Text = p.Name + " " + p.Surname });
//                return Json(new SelectList(evaluatedsByEvaluators, "Value", "Text", JsonRequestBehavior.AllowGet));
//            }
//            var evaluateds = evaluatedQuery.Select(p => new SelectListItem { Value = p.Id.ToString(), Text = p.Name + " " + p.Surname });
//            return Json(new SelectList(evaluateds, "Value", "Text", JsonRequestBehavior.AllowGet));
//        }
//        public ActionResult Index()
//        {
//            return View();
//        }

//        private static Highcharts createPieChart(string chartid, string chartdescription, object[][] datachart)
//        {
//            var chart = new Highcharts(chartid)
//                .InitChart(new Chart { PlotBackgroundColor = null, PlotBorderWidth = null, PlotShadow = false })
//                .SetTitle(new Title { Text = chartdescription })
//                //.SetTooltip(new Tooltip { Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %'; }" })
//                .SetTooltip(new Tooltip { PointFormat = "{series.name}: <b>{point.percentage}%</b>" /*, percentageDecimals: 1*/ })

//                .SetPlotOptions(new PlotOptions
//                {
//                    Pie = new PlotOptionsPie
//                    {
//                        AllowPointSelect = true,
//                        Cursor = Cursors.Pointer,
//                        DataLabels = new PlotOptionsPieDataLabels
//                        {
//                            Color = ColorTranslator.FromHtml("#000000"),
//                            ConnectorColor = ColorTranslator.FromHtml("#000000"),
//                            Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %'; }"
//                        },
//                        ShowInLegend = true
//                    }
//                })
//                .SetSeries(new Series
//                {
//                    Type = ChartTypes.Pie,
//                    Name = "Browser share",
//                    Data = new Data(
//                        datachart
//                    )
//                });
//            return chart;
//        }

//        private static Highcharts createStackChart(string chartid, string title, string[] categoriesData, Series[] dataSeries, string yAxisTitle)
//        {
//            var chart = new Highcharts(chartid)
//                .InitChart(new Chart { Type = ChartTypes.Bar })
//                .SetTitle(new Title { Text = title })
//                .SetXAxis(new XAxis { Categories = categoriesData })
//                .SetYAxis(new YAxis
//                {
//                    Min = 0,
//                    Title = new YAxisTitle { Text = yAxisTitle }
//                })
//                .SetTooltip(new Tooltip { Formatter = "function() { return ''+ this.series.name +': '+ this.y +''; }" })
//                .SetPlotOptions(new PlotOptions { Bar = new PlotOptionsBar { Stacking = Stackings.Normal } })
//                .SetSeries(dataSeries);
//            return chart;
//        }

//        private static Highcharts createStackedPercentageColumn(string chartid, string title, string[] categoriesData, Series[] dataSeries, string yAxisTitle)
//        {
//            var chart = new Highcharts(chartid)
//                .InitChart(new Chart { Type = ChartTypes.Column })
//                .SetTitle(new Title { Text = title })
//                .SetXAxis(new XAxis { Categories = categoriesData })
//                .SetYAxis(new YAxis
//                {
//                    Min = 0,
//                    Title = new YAxisTitle { Text = yAxisTitle }
//                })
//                .SetTooltip(new Tooltip { Formatter = "function() { return ''+ this.series.name +': '+ this.y +' ('+ Math.round(this.percentage) +'%)'; }" })
//                .SetPlotOptions(new PlotOptions { Column = new PlotOptionsColumn { Stacking = Stackings.Percent } })
//                .SetSeries(dataSeries);
//            return chart;
//        }


//        protected string RenderRazorViewToString(string viewName, object model)
//        {
//            if (model != null)
//            {
//                ViewData.Model = model;
//            }
//            using (StringWriter sw = new StringWriter())
//            {
//                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
//                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
//                viewResult.View.Render(viewContext, sw);
//                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
//                return sw.GetStringBuilder().ToString();
//            }
//        }

//        public void ExporttoExcel(int FormPerfomanceEvaluationId)
//        {
//            string Filename = "ExcelFrom" + DateTime.Now.ToString("mm_dd_yyy_hh_ss_tt") + ".xls";
//            string FolderPath = HttpContext.Server.MapPath("/ExcelFiles/");
//            string FilePath = Path.Combine(FolderPath, Filename);

//            //Step-1: Checking: the file name exist in server, if it is found then remove from server.------------------
//            if (System.IO.File.Exists(FilePath))
//            {
//                System.IO.File.Delete(FilePath);
//            }

//            //Step-2: Get Html Data & Converted to String----------------------------------------------------------------
//            string HtmlResult = RenderRazorViewToString("~/Views/PerfomanceReview/ExportExcel.cshtml", formsperfomancereviewRepository.GetById(FormPerfomanceEvaluationId));

//            //Step-4: Html Result store in Byte[] array------------------------------------------------------------------
//            byte[] ExcelBytes = Encoding.ASCII.GetBytes(HtmlResult);

//            //Step-5: byte[] array converted to file Stream and save in Server------------------------------------------- 
//            using (Stream file = System.IO.File.OpenWrite(FilePath))
//            {
//                file.Write(ExcelBytes, 0, ExcelBytes.Length);
//            }

//            //Step-6: Download Excel file 
//            Response.ContentType = "application/vnd.ms-excel";
//            Response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(Filename));
//            Response.WriteFile(FilePath);
//            Response.End();
//            Response.Flush();
//        }
//    }
//}