﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QavantWeb.Common;
using QavantWeb.Models;

namespace QavantWeb.Controllers
{
    public class FamiliaresController : Controller
    {
        private QavantEntities db = new QavantEntities();
        private int empresaId = SecurityHelpers.getEmpresaId();

        // GET: Familiares
        public ActionResult Index(int ? id)
        {
            ViewData["userId"] = id;
            var familiares = db.Familiares.Where( w => w.UserId == id && w.User.EmpresaId == empresaId).Include(f => f.Catalogo).Include(f => f.Catalogo1).Include(f => f.User);
            return View(familiares.ToList());
        }

        // GET: Familiares/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Familiare familiare = db.Familiares.Find(id);
            if (familiare == null)
            {
                return HttpNotFound();
            }
            return View(familiare);
        }

        // GET: Familiares/Create
        public ActionResult Create(int? id)
        {
            ViewData["myUserId"] = id;
            List<CatalogosTipo> lstCatDefault = db.CatalogosTipoes.ToList();
            List<CatalogosTipo> lstCatCustom = new List<CatalogosTipo>();
            int catGenderId = 1;
            int catVinculoId = 3;

            foreach (CatalogosTipo item in lstCatDefault)
            {
                if (item.EmpresaId == empresaId && item.Nemonico == "GENE")
                {
                    lstCatCustom.Add(item);
                    catGenderId = item.id;                   
                }
                if (item.EmpresaId == empresaId && item.Nemonico == "FAMI")
                {
                    lstCatCustom.Add(item);
                    catVinculoId = item.id;
                }
            }
            List<Catalogo> lstCatalogoGenero = new List<Catalogo>();
            if (lstCatCustom.Count() > 0)
            {
                lstCatalogoGenero = db.Catalogos.Where(w => w.CatalogoTipoId == catGenderId && w.Habilitado == true).ToList();
            }
            else
            {
                lstCatalogoGenero = db.Catalogos.Where(w => w.CatalogoTipoId == catGenderId && w.Habilitado == true).ToList();
            }

            List<Catalogo> lstCatalogoFamilia = new List<Catalogo>();
            if (lstCatCustom.Count() > 0)
            {
                lstCatalogoFamilia = db.Catalogos.Where(w => w.CatalogoTipoId == catVinculoId && w.Habilitado == true).ToList();
            }
            else
            {
                lstCatalogoFamilia = db.Catalogos.Where(w => w.CatalogoTipoId == catVinculoId && w.Habilitado == true).ToList();
            }

            ViewBag.GeneroCatalogoId = new SelectList(lstCatalogoGenero, "Id", "DescripcionLarga");
            ViewBag.VinculoCatalogoId = new SelectList(lstCatalogoFamilia, "Id", "DescripcionLarga");
            ViewBag.UserId = new SelectList(db.Users.Where(w => w.EmpresaId == empresaId), "Id", "Name");
            return View();
        }

        // POST: Familiares/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Familiare familiare, int id)
        {
            ViewData["myUserId"] = id;
            if (id != null)
                familiare.UserId = id;
            if (ModelState.IsValid)
            {
                db.Familiares.Add(familiare);
                db.SaveChanges();
              
                return RedirectToAction("Index", new { id = familiare.UserId});
            }

            ViewBag.GeneroCatalogoId = new SelectList(db.Catalogos.Where(w => w.CatalogoTipoId == 1 && w.Habilitado == true), "Id", "DescripcionLarga", familiare.GeneroCatalogoId);
            ViewBag.VinculoCatalogoId = new SelectList(db.Catalogos.Where(w => w.CatalogoTipoId == 3 && w.Habilitado == true), "Id", "DescripcionLarga", familiare.VinculoCatalogoId);
            ViewBag.UserId = new SelectList(db.Users, "Id", "Name", familiare.UserId);
            return View(familiare);
        }

        // GET: Familiares/Edit/5
        public ActionResult Edit(int? id, int? uId)
        {
            ViewData["myUserId"] = id;
            ViewData["uId"] = uId;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Familiare familiar = db.Familiares.Find(id);
            if (familiar == null)
            {
                return HttpNotFound();
            }
            List<CatalogosTipo> lstCatDefault = db.CatalogosTipoes.ToList();
            List<CatalogosTipo> lstCatCustom = new List<CatalogosTipo>();
            int catGenderId = 1;
            int catVinculoId = 3;

            foreach (CatalogosTipo item in lstCatDefault)
            {
                if (item.EmpresaId == empresaId && item.Nemonico == "GENE")
                {
                    lstCatCustom.Add(item);
                    catGenderId = item.id;
                }
                if (item.EmpresaId == empresaId && item.Nemonico == "FAMI")
                {
                    lstCatCustom.Add(item);
                    catVinculoId = item.id;
                }
            }
            List<Catalogo> lstCatalogoGenero = new List<Catalogo>();
            if (lstCatCustom.Count() > 0)
            {
                lstCatalogoGenero = db.Catalogos.Where(w => w.CatalogoTipoId == catGenderId && w.Habilitado == true).ToList();
            }
            else
            {
                lstCatalogoGenero = db.Catalogos.Where(w => w.CatalogoTipoId == catGenderId && w.Habilitado == true).ToList();
            }

            List<Catalogo> lstCatalogoFamilia = new List<Catalogo>();
            if (lstCatCustom.Count() > 0)
            {
                lstCatalogoFamilia = db.Catalogos.Where(w => w.CatalogoTipoId == catVinculoId && w.Habilitado == true).ToList();
            }
            else
            {
                lstCatalogoFamilia = db.Catalogos.Where(w => w.CatalogoTipoId == catVinculoId && w.Habilitado == true).ToList();
            }

            ViewBag.GeneroCatalogoId = new SelectList(lstCatalogoGenero, "Id", "DescripcionLarga");
            ViewBag.VinculoCatalogoId = new SelectList(lstCatalogoFamilia, "Id", "DescripcionLarga");            
            ViewBag.UserId = new SelectList(db.Users, "Id", "Name", familiar.UserId);
            return View(familiar);
        }

        // POST: Familiares/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( Familiare familiare, int id, int uId)
        {
            familiare.UserId = uId;
            ViewData["myUserId"] = id;
            ViewData["uId"] = uId;
            if (ModelState.IsValid)
            {
                db.Entry(familiare).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Familiares", new { id = uId });
                
            }

            List<CatalogosTipo> lstCatDefault = db.CatalogosTipoes.ToList();
            List<CatalogosTipo> lstCatCustom = new List<CatalogosTipo>();
            int catGenderId = 1;
            int catVinculoId = 3;

            foreach (CatalogosTipo item in lstCatDefault)
            {
                if (item.EmpresaId == empresaId && item.Nemonico == "GENE")
                {
                    lstCatCustom.Add(item);
                    catGenderId = item.id;
                }
                if (item.EmpresaId == empresaId && item.Nemonico == "FAMI")
                {
                    lstCatCustom.Add(item);
                    catVinculoId = item.id;
                }
            }
            List<Catalogo> lstCatalogoGenero = new List<Catalogo>();
            if (lstCatCustom.Count() > 0)
            {
                lstCatalogoGenero = db.Catalogos.Where(w => w.CatalogoTipoId == catGenderId && w.Habilitado == true).ToList();
            }
            else
            {
                lstCatalogoGenero = db.Catalogos.Where(w => w.CatalogoTipoId == catGenderId && w.Habilitado == true).ToList();
            }

            List<Catalogo> lstCatalogoFamilia = new List<Catalogo>();
            if (lstCatCustom.Count() > 0)
            {
                lstCatalogoFamilia = db.Catalogos.Where(w => w.CatalogoTipoId == catVinculoId && w.Habilitado == true).ToList();
            }
            else
            {
                lstCatalogoFamilia = db.Catalogos.Where(w => w.CatalogoTipoId == catVinculoId && w.Habilitado == true).ToList();
            }

            ViewBag.GeneroCatalogoId = new SelectList(lstCatalogoGenero, "Id", "DescripcionLarga");
            ViewBag.VinculoCatalogoId = new SelectList(lstCatalogoFamilia, "Id", "DescripcionLarga");
            ViewBag.UserId = new SelectList(db.Users, "Id", "Name", familiare.UserId);
            return View(familiare);
        }

        // GET: Familiares/Delete/5
        public ActionResult Delete(int? id, int? uId)
        {
            ViewData["uId"] = uId;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Familiare familiare = db.Familiares.Find(id);
            if (familiare == null)
            {
                return HttpNotFound();
            }
            db.Familiares.Remove(familiare);
            db.SaveChanges();
            return RedirectToAction("Index", "Familiares", new { id = uId });
        }

        // POST: Familiares/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Familiare familiare = db.Familiares.Find(id);
            db.Familiares.Remove(familiare);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
