﻿using QavantWeb.Models;
using System;
using System.Web.Mvc;

namespace QavantWeb.Controllers
{
    public abstract class BaseController : Controller
    {
        public UnitOfWork unitOfWork;

        protected BaseController()
        {
            unitOfWork = new UnitOfWork();
        }

        public String ErrorMessage
        {
            get { return TempData[nameof(ErrorMessage)] == null ? String.Empty : TempData[nameof(ErrorMessage)].ToString(); }
            set { TempData[nameof(ErrorMessage)] = value; }
        }

        protected override void Dispose(bool disposing)
        {
            unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}