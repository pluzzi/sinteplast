﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QavantWeb.Models;
using System.Net.Http;
using System.Text;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using System.IO;
using Newtonsoft.Json;
using QavantWeb.Common;
using QavantWeb.Infrastructure.Notifications;

namespace QavantWeb.Controllers
{
    public class NotificationsController : Controller
    {
        private QavantEntities db = new QavantEntities();
        private int empresaId = SecurityHelpers.getEmpresaId();
        private readonly INotification _notification;

        public NotificationsController() : this(new FirebaseNotification()) { }

        public NotificationsController(INotification notification)
        {
            _notification = notification;
        }

        // GET: Notifications
        [Authorize(Roles = "Administrador")]
        public ActionResult Index()
        {
            var notifications = db.Notifications
                                  //.Include(n => n.Benefit)
                                  //.Include(n => n.Noticia)
                                  //.Include(n => n.Section)
                                  .Where(w => w.EmpresaId == empresaId)
                                  .OrderByDescending(x => x.CreationDate);

            return View(notifications.ToList());
        }

        // GET: Notifications/Details/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Notification notification = db.Notifications.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f=>f.Id==id);
            if (notification == null)
            {
                return HttpNotFound();
            }
            return View(notification);
        }

        // GET: Notifications/Create
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            ViewBag.BenefitId = new SelectList(db.Benefits.Where(w => w.EmpresaId == empresaId).Where(o => o.From <= DateTime.Today && o.To >= DateTime.Today).Where(o => o.Enabled).OrderBy(o => o.Title), "Id", "Title");
            ViewBag.NewsId = new SelectList(db.Noticias.Where(w => w.EmpresaId == empresaId).OrderBy(o => o.Titulo).Where(o => o.Habilitada), "Id", "Titulo");
            ViewBag.CourseId = new SelectList(db.Courses.Where(w => w.EmpresaId == empresaId).Where(o => o.From <= DateTime.Today && o.To >= DateTime.Today).Where(o => o.Enabled).OrderBy(o => o.Title), "Id", "Title");
            ViewBag.SurveysId = new SelectList(db.Forms.Where(w => w.EmpresaId == empresaId).Where(o => o.DateFrom <= DateTime.Today && o.DateTo >= DateTime.Today).Where(o => o.Enabled && o.FormTypeId == (byte)Models.Enums.FormsTypes.Encuesta).OrderBy(o => o.Title), "Id", "Title");
            ViewBag.FormsId = new SelectList(db.Forms.Where(w => w.EmpresaId == empresaId).Where(o => o.DateFrom <= DateTime.Today && o.DateTo >= DateTime.Today).Where(o => o.Enabled && o.FormTypeId == (byte)Models.Enums.FormsTypes.Formulario).OrderBy(o => o.Title), "Id", "Title");
            ViewBag.InfoUtilId = new SelectList(db.InfoUtils.Where(w => w.EmpresaId == empresaId).OrderBy(o => o.Titulo).Where(o => o.Habilitado == true), "Id", "Titulo");
            ViewBag.GaleriaId = new SelectList(db.Galerias.Where(w => w.EmpresaId == empresaId).OrderBy(o => o.Titulo).Where(o => o.Habilitada == true), "Id", "Titulo");

            ViewBag.SectionId = new SelectList(db.Sections.Where(s => s.Enabled == true).OrderBy(o => o.Name), "Id", "Name", "AppLink");
            ViewBag.FilterAreas = new SelectList(db.Areas.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitada).OrderBy(o => o.Nombre), "Id", "Nombre");
            ViewBag.FilterGroups = new SelectList(db.Groups.Where(w => w.EmpresaId == empresaId).Where(w => w.Enabled).OrderBy(o => o.Name), "Id", "Name");
            List<Section> lista = db.Sections.ToList();
            ViewBag.lista = lista;
            return View();
        }

        // POST: Notifications/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult Create(Notification notification)
        {
            notification.CreationUserId = User.Identity.GetUserId();
            notification.CreationDate = DateTime.Now;
            Section section = db.Sections.Where(x => x.Id == notification.SectionId).FirstOrDefault();


            if (ModelState.IsValid)
            {
                notification.Status = "NO ENVIADO";
                notification.EmpresaId = empresaId;
                db.Notifications.Add(notification);
                db.SaveChanges();

                //Envio notificaciones
                //TRAER LOS TOKENS A ENVIAR
                List<Device> devs = new List<Device>();
                string tokens = "";

                List<NotificationsFilter> lsFilters = db.NotificationsFilters.Where(w => w.NotificationId == notification.Id).ToList(); //tendría que de Segmentación  get_filtros 
                if (lsFilters.Count == 0)
                    devs.AddRange(db.Devices.Include(i => i.User).Where(w => w.User.EmpresaId == empresaId).Where(w => w.User.Enable && w.User.Active).ToList());
                else
                {
                    List<int> lsUsersToAdd = new List<int>();
                    foreach (NotificationsFilter oFilter in lsFilters)
                    {
                        switch (oFilter.FilteType)
                        {
                            case "GROUP":
                                lsUsersToAdd.AddRange(db.Users
                                                            .Include(i => i.UsersGroups)
                                                            .ToList()
                                                            .Where(w => w.EmpresaId == empresaId)
                                                            .Where(w => w.UsersGroups.Any(a => a.GroupId.ToString() == oFilter.FilterValue))
                                                            .Where(w => !lsUsersToAdd.Contains(w.Id))
                                                            .Select(s => s.Id));
                                break;
                            case "AREA":
                                lsUsersToAdd.AddRange(db.Users
                                                            .Include(i => i.UsersGroups)
                                                            .ToList()
                                                            .Where(w => w.EmpresaId == empresaId)
                                                            .Where(w => w.Area_Id != null && w.Area_Id.ToString() == oFilter.FilterValue)
                                                            .Where(w => !lsUsersToAdd.Contains(w.Id))
                                                            .Select(s => s.Id));
                                break;
                            case "SEX":
                                lsUsersToAdd.AddRange(db.Users
                                                            .Include(i => i.UsersGroups)
                                                            .ToList()
                                                            .Where(w => w.EmpresaId == empresaId)
                                                            .Where(w => !lsUsersToAdd.Contains(w.Id))
                                                            .Select(s => s.Id));
                                break;
                        }
                    }
                    devs.AddRange(db.Devices.Include(i => i.User)
                                    .Where(w => w.User.EmpresaId == empresaId)
                                    .Where(w => w.User.Enable && w.User.Active)
                                    .Where(w => lsUsersToAdd.Contains(w.UserId))
                                    .ToList());
                }

                var tokensForNotification = new List<string>();
                foreach (Device d in devs)
                {
                    tokensForNotification.Add(d.TokenNotification);
                }

                if (tokensForNotification.Count > 0)
                {
                    string idSeleccionado = "";
                    if (notification.SectionId == (byte)Models.Enums.SectionsIds.Beneficios &&
                        notification.BenefitId != null)
                    {
                        idSeleccionado = "/" + notification.BenefitId.ToString();
                    }
                    else if (notification.SectionId == (byte)Models.Enums.SectionsIds.Noticias &&
                            notification.NewsId != null)
                    {
                        idSeleccionado = "/" + notification.NewsId.ToString();
                    }
                    else if (notification.SectionId == (byte)Models.Enums.SectionsIds.Cursos &&
                            notification.CourseId != null)
                    {
                        idSeleccionado = "/" + notification.CourseId.ToString();
                    }
                    else if (notification.SectionId == (byte)Models.Enums.SectionsIds.InfoUtil &&
                        notification.InfoUtilId != null)
                    {
                        idSeleccionado = "/" + notification.InfoUtilId.ToString();
                    }
                    else if (notification.SectionId == (byte)Models.Enums.SectionsIds.Encuestas &&
                            notification.SurveysId != null)
                    {
                        idSeleccionado = "/" + notification.SurveysId.ToString();
                    }
                    else if (notification.SectionId == (byte)Models.Enums.SectionsIds.Formularios &&
                            notification.FormsId != null)
                    {
                        idSeleccionado = "/" + notification.FormsId.ToString();
                    }
                    else if (notification.SectionId == (byte)Models.Enums.SectionsIds.Galeria &&
                       notification.GaleriaId != null)
                    {
                        idSeleccionado = "/" + notification.GaleriaId.ToString();
                    }

                    string state = section.AppLink + idSeleccionado;

                    // Configuro y envio notificaciones
                    NotificationSettings notificationSettings = new NotificationSettings();
                    notificationSettings.ServerKey = WebConfigurationManager.AppSettings["SecurityToken"].ToString(); 
                    notificationSettings.Endpoint = WebConfigurationManager.AppSettings["IonicURL"].ToString();
                    notificationSettings.Title = HttpUtility.JavaScriptStringEncode(notification.Title);
                    notificationSettings.Message = HttpUtility.JavaScriptStringEncode(notification.Message);
                    notificationSettings.RedirectTo = state;
                    notificationSettings.Tokens = tokensForNotification;
                    int successfullySended = _notification.Send(notificationSettings);
                    
                    // evaluo el resultado del servicio de notificaciones
                    Notification notError = db.Notifications.Where(w => w.EmpresaId == empresaId).Where(w => w.Id == notification.Id).FirstOrDefault();
                    if (successfullySended > 0)
                    {
                        notError.Status = "ENVIADO A " + successfullySended + " USUARIOS";
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        notError.Status = "ERROR: No se pudo enviar la notificación.";
                        db.SaveChanges();
                        ViewBag.BenefitId = new SelectList(db.Benefits.Where(w => w.EmpresaId == empresaId).Where(o => o.From <= DateTime.Today && o.To >= DateTime.Today).Where(o => o.Enabled).OrderBy(o => o.Title), "Id", "Title", notification.BenefitId);
                        ViewBag.NewsId = new SelectList(db.Noticias.Where(w => w.EmpresaId == empresaId).OrderByDescending(o => o.FechaAlta), "Id", "Titulo", notification.NewsId);
                        ViewBag.SectionId = new SelectList(db.Sections, "Id", "Name", notification.SectionId);
                        ViewBag.CourseId = new SelectList(db.Courses.Where(w => w.EmpresaId == empresaId).Where(o => o.From <= DateTime.Today && o.To >= DateTime.Today).Where(o => o.Enabled).OrderBy(o => o.Title), "Id", "Title", notification.CourseId);
                        ViewBag.SurveysId = new SelectList(db.Forms.Where(w => w.EmpresaId == empresaId).Where(o => o.DateFrom <= DateTime.Today && o.DateTo >= DateTime.Today).Where(o => o.Enabled && o.FormTypeId == (byte)Models.Enums.FormsTypes.Encuesta).OrderBy(o => o.Title), "Id", "Title", notification.FormsId);
                        ViewBag.FormsId = new SelectList(db.Forms.Where(w => w.EmpresaId == empresaId).Where(o => o.DateFrom <= DateTime.Today && o.DateTo >= DateTime.Today).Where(o => o.Enabled && o.FormTypeId == (byte)Models.Enums.FormsTypes.Formulario).OrderBy(o => o.Title), "Id", "Title", notification.FormsId);
                        ViewBag.InfoUtilId = new SelectList(db.InfoUtils.Where(w => w.EmpresaId == empresaId).OrderBy(o => o.Titulo).Where(o => o.Habilitado == true), "Id", "Titulo", notification.InfoUtilId);
                        ViewBag.GaleriaId = new SelectList(db.Galerias.Where(w => w.EmpresaId == empresaId).OrderBy(o => o.Titulo).Where(o => o.Habilitada == true), "Id", "Titulo", notification.GaleriaId);
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    Notification notError2 = db.Notifications.Where(w => w.EmpresaId == empresaId).Where(w => w.Id == notification.Id).FirstOrDefault();
                    notError2.Status = "Sin destinatarios ";
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }

            ViewBag.BenefitId = new SelectList(db.Benefits.Where(w => w.EmpresaId == empresaId).Where(o => o.From <= DateTime.Today && o.To >= DateTime.Today).Where(o => o.Enabled).OrderBy(o => o.Title), "Id", "Title", notification.BenefitId);
            ViewBag.NewsId = new SelectList(db.Noticias.Where(w => w.EmpresaId == empresaId).OrderByDescending(o => o.FechaAlta), "Id", "Titulo", notification.NewsId);
            ViewBag.CourseId = new SelectList(db.Courses.Where(w => w.EmpresaId == empresaId).Where(o => o.From <= DateTime.Today && o.To >= DateTime.Today).Where(o => o.Enabled).OrderBy(o => o.Title), "Id", "Title", notification.CourseId);
            ViewBag.SurveysId = new SelectList(db.Forms.Where(w => w.EmpresaId == empresaId).Where(o => o.DateFrom <= DateTime.Today && o.DateTo >= DateTime.Today).Where(o => o.Enabled && o.FormTypeId == (byte)Models.Enums.FormsTypes.Encuesta).OrderBy(o => o.Title), "Id", "Title", notification.FormsId);
            ViewBag.FormsId = new SelectList(db.Forms.Where(w => w.EmpresaId == empresaId).Where(o => o.DateFrom <= DateTime.Today && o.DateTo >= DateTime.Today).Where(o => o.Enabled && o.FormTypeId == (byte)Models.Enums.FormsTypes.Formulario).OrderBy(o => o.Title), "Id", "Title", notification.FormsId);
            ViewBag.InfoUtilId = new SelectList(db.InfoUtils.Where(w => w.EmpresaId == empresaId).OrderBy(o => o.Titulo).Where(o => o.Habilitado == true), "Id", "Titulo", notification.InfoUtilId);
            ViewBag.GaleriaId = new SelectList(db.Galerias.Where(w => w.EmpresaId == empresaId).OrderBy(o => o.Titulo).Where(o => o.Habilitada == true), "Id", "Titulo", notification.GaleriaId);
            ViewBag.SectionId = new SelectList(db.Sections.Where(s => s.Enabled == true).OrderBy(o => o.Name), "Id", "Name", notification.SectionId);
            //ViewBag.SectionId = new SelectList(db.Sections, "Id", "Name", notification.SectionId);
            ViewBag.FilterAreas = new SelectList(db.Areas.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitada).OrderBy(o => o.Nombre), "Id", "Nombre");
            ViewBag.FilterGroups = new SelectList(db.Groups.Where(w => w.EmpresaId == empresaId).Where(w => w.Enabled).OrderBy(o => o.Name), "Id", "Name");
            return View(notification);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
