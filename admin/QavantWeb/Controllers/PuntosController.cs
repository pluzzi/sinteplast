﻿using ClosedXML.Excel;
using QavantWeb.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QavantWeb.Controllers
{
    public class PuntosController : Controller
    {
        private QavantEntities db = new QavantEntities();

        // GET: Puntos
        public ActionResult Index()
        {
            ViewBag.Lista = new List<PuntoExcelRow>();
            return View();
        }

        [HttpPost]
        public ActionResult Import(HttpPostedFileBase excelFile)
        {
            ViewBag.Lista = new List<PuntoExcelRow>();

            if (excelFile == null || excelFile.ContentLength == 0)
            {
                ViewBag.Error = "Por favor seleccione un archivo Excel.";
                return View("Index");
            }
            if (!(excelFile.FileName.EndsWith("xls") || excelFile.FileName.EndsWith("xlsx")))
            {
                ViewBag.Error = "Por favor seleccione un archivo con formato Excel.";
                return View("Index");
            }

            string path = Server.MapPath("~/ExcelFiles/" + excelFile.FileName);

            if (System.IO.File.Exists(path))
            {
                try
                {
                    System.IO.File.Delete(path);
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }

            }

            excelFile.SaveAs(path);

            var workbook = new XLWorkbook(path);
            var worksheet = workbook.Worksheet(1);
            var rows = worksheet.RangeUsed().RowsUsed().Skip(1);

            if (rows.Count() == 0)
            {
                workbook.Dispose();
                ViewBag.Error = "El archivo no tiene datos.";
                return View("Index");
            }

            List<PuntoExcelRow> list = new List<PuntoExcelRow>();

            try
            {
                foreach (var row in rows)
                {
                    PuntoExcelRow item = new PuntoExcelRow();
                    item.User = row.Cell(1).Value.ToString();
                    item.Catalogo = row.Cell(2).Value.ToString();
                    item.Puntos = int.Parse(row.Cell(3).Value.ToString());

                    User user = db.Users.Where(w => w.UserName == item.User).FirstOrDefault();
                    if (user == null)
                    {
                        item.Mensaje = "No se encontró el usuario.";
                        list.Add(item);
                        continue;
                    }

                    TipoCatalogoProducto catalogo = db.TipoCatalogoProductos.Where(w => w.Descripcion == item.Catalogo).FirstOrDefault();
                    if (catalogo == null)
                    {
                        item.Mensaje = "No se encontró el catálogo.";
                        list.Add(item);
                        continue;
                    }

                    TipoCatalogoProductosxUsuario catalogoUsuario = db.TipoCatalogoProductosxUsuarios
                        .Where(w => w.UserId == user.Id && w.TipoCatalogoProductoId == catalogo.Id)
                        .FirstOrDefault();

                    if (catalogoUsuario == null)
                    {
                        TipoCatalogoProductosxUsuario nuevo = new TipoCatalogoProductosxUsuario();
                        nuevo.UserId = user.Id;
                        nuevo.TipoCatalogoProductoId = catalogo.Id;
                        nuevo.PuntosAcumulados = item.Puntos;
                        nuevo.FechaActualizacionPuntos = DateTime.Now;

                        db.TipoCatalogoProductosxUsuarios.Add(nuevo);
                        db.SaveChanges();

                    }
                    else
                    {
                        catalogoUsuario.PuntosAcumulados = item.Puntos;
                        catalogoUsuario.FechaActualizacionPuntos = DateTime.Now;
                        db.SaveChanges();
                    }

                    list.Add(item);
                }
            }
            catch(Exception e)
            {
                workbook.Dispose();
                ViewBag.Error = "El archivo no tiene el formato correcto.";
                return View("Index");
            }

            workbook.Dispose();
            
            ViewBag.Lista = list;

            return View("Index");
        }
    }
}