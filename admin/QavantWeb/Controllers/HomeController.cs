﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QavantWeb.Models;
using QavantWeb.Common;
using Microsoft.AspNet.Identity;
using System.Data.Entity;

namespace QavantWeb.Controllers
{
    public class HomeController : Controller
    {
        //[Authorize]
        //[Authorize(Roles = "Administrador")]
        //[Authorize(Roles = "RRHH")]
        public ActionResult Index()
        {
            return View();
            //return RedirectToAction("Index","Dashboard");
        }
        [ChildActionOnly]
        public ActionResult GetMenuNav()
        {
            List<MenuItem> menu = new List<MenuItem>();
            if (User.Identity.IsAuthenticated)
            {
                QavantEntities db = new QavantEntities();
                string userId = User.Identity.GetUserId();
                int empresaId = SecurityHelpers.getEmpresaId();

                menu = db.MenuItems
                        .Include(i => i.MenuItems1)
                        .Where(w => w.CanalId == (short)Enums.Canales.Admin)
                        .Where(w => w.EmpresaId == empresaId)
                        .Where(w => w.AspNetRoles.Any(a => a.AspNetUsers.Any(an => an.Id == userId))) //Pregunto por el usuario logueado, hereda entonces de la empresa logueada
                        .OrderBy(o => o.Orden)
                        .ToList();


                if (menu.Count == 0)
                {
                    menu = db.MenuItems
                    .Include(i => i.MenuItems1)
                    .Where(w => w.CanalId == (short)Enums.Canales.Admin)
                    .Where(w => w.EmpresaId == null)
                    .Where(w => w.AspNetRoles.Any(a => a.AspNetUsers.Any(an => an.Id == userId))) //Pregunto por el usuario logueado, hereda entonces de la empresa logueada
                    .OrderBy(o => o.Orden)
                    .ToList();
                }




                db.Dispose();
                db = null;
            }
            return PartialView("_MenuNav", menu);
            //return RedirectToAction("Index","Dashboard");
        }

        [Authorize]
        [Authorize(Roles = "Administrador")]
        //[Authorize(Roles = "RRHH")]
        public ActionResult Users()
        {
            return View();
        }

        //public ActionResult About()
        //{
        //    ViewBag.Message = "Your application description page.";
        //    return View();
        //}
    }
}