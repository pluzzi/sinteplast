﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QavantWeb.Services.Settings
{
    public interface ISettingsService
    {
        Dictionary<string, string> GetParametrosGlobales();
    }
}