﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QavantWeb.Infrastructure.ImageResizer
{
    public interface IImageResizer
    {
        Stream Resize(Stream stream, int width, int height);
    }
}
