﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantWeb.Infrastructure.FileStorage
{
    public enum TypeFileStorageEnum
    {
        // On-Premise
        FileSystem,

        // Cloud
        AzureStorage
    }
}