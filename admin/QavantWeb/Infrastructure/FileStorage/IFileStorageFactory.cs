﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QavantWeb.Infrastructure.FileStorage
{
    public interface IFileStorageFactory
    {
        IFileStorage CreateFileStorage(Dictionary<string, string> parameters);
    }
}
