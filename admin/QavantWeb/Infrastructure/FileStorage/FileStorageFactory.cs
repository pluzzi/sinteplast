﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantWeb.Infrastructure.FileStorage
{
    public class FileStorageFactory : IFileStorageFactory
    {
        public IFileStorage CreateFileStorage(Dictionary<string, string> parameters)
        {
            IFileStorage fileStorage;
            TypeFileStorageEnum typeFileStorage;
            Enum.TryParse(parameters["TypeFileStorage"], out typeFileStorage);

            switch (typeFileStorage)
            {
                case TypeFileStorageEnum.FileSystem:
                    fileStorage = new FileSystem(parameters);
                    break;
                case TypeFileStorageEnum.AzureStorage:
                    fileStorage = new AzureStorage(parameters);
                    break;
                default:
                    fileStorage = null;
                    break;
            }


            if (fileStorage == null)
                throw new Exception("Repositorio de archivos no definido.");

            return fileStorage;
        }
    }
}