USE [qavant]
GO
/****** Object:  StoredProcedure [dbo].[seg_GetSegmentacion]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP PROCEDURE [dbo].[seg_GetSegmentacion]
GO
/****** Object:  StoredProcedure [dbo].[seg_GetFiltros]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP PROCEDURE [dbo].[seg_GetFiltros]
GO
/****** Object:  StoredProcedure [dbo].[mur_GetDatosMuro]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP PROCEDURE [dbo].[mur_GetDatosMuro]
GO
/****** Object:  StoredProcedure [dbo].[log_GetFactorValidacion]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP PROCEDURE [dbo].[log_GetFactorValidacion]
GO
/****** Object:  StoredProcedure [dbo].[est_VisitasPorDia]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP PROCEDURE [dbo].[est_VisitasPorDia]
GO
/****** Object:  StoredProcedure [dbo].[est_DistribuciónPorArea]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP PROCEDURE [dbo].[est_DistribuciónPorArea]
GO
/****** Object:  StoredProcedure [dbo].[est_Dashboard_VisitasPorHora]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP PROCEDURE [dbo].[est_Dashboard_VisitasPorHora]
GO
/****** Object:  StoredProcedure [dbo].[est_Dashboard_VisitasPorDia]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP PROCEDURE [dbo].[est_Dashboard_VisitasPorDia]
GO
/****** Object:  StoredProcedure [dbo].[est_Dashboard_UsuariosMasParticipativos]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP PROCEDURE [dbo].[est_Dashboard_UsuariosMasParticipativos]
GO
/****** Object:  StoredProcedure [dbo].[est_Dashboard_ModulosMasRepercucion]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP PROCEDURE [dbo].[est_Dashboard_ModulosMasRepercucion]
GO
/****** Object:  StoredProcedure [dbo].[est_ContenidosMayorRepercusion]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP PROCEDURE [dbo].[est_ContenidosMayorRepercusion]
GO
/****** Object:  StoredProcedure [dbo].[est_Comportamiento_ContenidoMasReacciones]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP PROCEDURE [dbo].[est_Comportamiento_ContenidoMasReacciones]
GO
/****** Object:  StoredProcedure [dbo].[est_Comportamiento_ComparacionReacciones]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP PROCEDURE [dbo].[est_Comportamiento_ComparacionReacciones]
GO
/****** Object:  StoredProcedure [dbo].[est_Canales_VisitasPorHora]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP PROCEDURE [dbo].[est_Canales_VisitasPorHora]
GO
/****** Object:  StoredProcedure [dbo].[est_AudienciaPorGenero]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP PROCEDURE [dbo].[est_AudienciaPorGenero]
GO
/****** Object:  StoredProcedure [dbo].[est_AudienciaPorEdades]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP PROCEDURE [dbo].[est_AudienciaPorEdades]
GO
/****** Object:  StoredProcedure [dbo].[can_CanjeProducto]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP PROCEDURE [dbo].[can_CanjeProducto]
GO
ALTER TABLE [dbo].[UsersGroups] DROP CONSTRAINT [FK_UsersGroups_Users]
GO
ALTER TABLE [dbo].[UsersGroups] DROP CONSTRAINT [FK_UsersGroups_Groups]
GO
ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_Users_Users]
GO
ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_Users_Sucursales]
GO
ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_Users_Regiones]
GO
ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_Users_Empresas]
GO
ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_Users_Companias]
GO
ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_dbo.Users_dbo.Areas_Area_Id]
GO
ALTER TABLE [dbo].[Titulos] DROP CONSTRAINT [FK_Titulos_Empresas]
GO
ALTER TABLE [dbo].[Segmentacion] DROP CONSTRAINT [FK_Segmentacion_Modulos]
GO
ALTER TABLE [dbo].[Segmentacion] DROP CONSTRAINT [FK_Segmentacion_Filtros]
GO
ALTER TABLE [dbo].[Segmentacion] DROP CONSTRAINT [FK_Segmentacion_Empresas]
GO
ALTER TABLE [dbo].[Regiones] DROP CONSTRAINT [FK_Regiones_Empresas]
GO
ALTER TABLE [dbo].[ReaccionesUsers] DROP CONSTRAINT [FK_ReaccionesUsers_Users]
GO
ALTER TABLE [dbo].[ReaccionesUsers] DROP CONSTRAINT [FK_ReaccionesUsers_Reacciones]
GO
ALTER TABLE [dbo].[ReaccionesUsers] DROP CONSTRAINT [FK_ReaccionesUsers_Empresas]
GO
ALTER TABLE [dbo].[PuestosUsers] DROP CONSTRAINT [FK_PuestosUsers_Users]
GO
ALTER TABLE [dbo].[PuestosUsers] DROP CONSTRAINT [FK_PuestosUsers_Puestos]
GO
ALTER TABLE [dbo].[Puestos] DROP CONSTRAINT [FK_Puestos_Empresas]
GO
ALTER TABLE [dbo].[ParametrosEmpresasCanal] DROP CONSTRAINT [FK_ParametrosEmpresasCanal_Parametros]
GO
ALTER TABLE [dbo].[ParametrosEmpresasCanal] DROP CONSTRAINT [FK_ParametrosEmpresasCanal_Empresas]
GO
ALTER TABLE [dbo].[ParametrosEmpresasCanal] DROP CONSTRAINT [FK_ParametrosEmpresasCanal_Canales]
GO
ALTER TABLE [dbo].[Parametros] DROP CONSTRAINT [FK_Parametros_Modulos]
GO
ALTER TABLE [dbo].[NotificationsFilters] DROP CONSTRAINT [FK_NotificationsFilters_Notifications]
GO
ALTER TABLE [dbo].[Notifications] DROP CONSTRAINT [FK_Notifications_Sections]
GO
ALTER TABLE [dbo].[Notifications] DROP CONSTRAINT [FK_Notifications_News]
GO
ALTER TABLE [dbo].[Notifications] DROP CONSTRAINT [FK_Notifications_InfoPaginas]
GO
ALTER TABLE [dbo].[Notifications] DROP CONSTRAINT [FK_Notifications_Forms]
GO
ALTER TABLE [dbo].[Notifications] DROP CONSTRAINT [FK_Notifications_Empresas]
GO
ALTER TABLE [dbo].[Notifications] DROP CONSTRAINT [FK_Notifications_Courses]
GO
ALTER TABLE [dbo].[Notifications] DROP CONSTRAINT [FK_Notifications_Benefits]
GO
ALTER TABLE [dbo].[Notifications] DROP CONSTRAINT [FK_Notifications_AspNetUsers]
GO
ALTER TABLE [dbo].[Noticias] DROP CONSTRAINT [FK_Noticias_Empresas]
GO
ALTER TABLE [dbo].[Multimedia] DROP CONSTRAINT [FK_Multimedia_Modulos]
GO
ALTER TABLE [dbo].[Multimedia] DROP CONSTRAINT [FK_Multimedia_Galerias]
GO
ALTER TABLE [dbo].[MenuItemsRoles] DROP CONSTRAINT [FK_MenuItemsRoles_MenuItems]
GO
ALTER TABLE [dbo].[MenuItemsRoles] DROP CONSTRAINT [FK_MenuItemsRoles_AspNetRoles]
GO
ALTER TABLE [dbo].[MenuItems] DROP CONSTRAINT [FK_MenuItems_Modulos]
GO
ALTER TABLE [dbo].[MenuItems] DROP CONSTRAINT [FK_MenuItems_MenuItems]
GO
ALTER TABLE [dbo].[MenuItems] DROP CONSTRAINT [FK_MenuItems_Empresas]
GO
ALTER TABLE [dbo].[MenuItems] DROP CONSTRAINT [FK_MenuItems_Canales]
GO
ALTER TABLE [dbo].[LogActividades] DROP CONSTRAINT [FK_LogActividades_Users]
GO
ALTER TABLE [dbo].[LogActividades] DROP CONSTRAINT [FK_LogActividades_Modulos]
GO
ALTER TABLE [dbo].[LogActividades] DROP CONSTRAINT [FK_LogActividades_LogTipos]
GO
ALTER TABLE [dbo].[LogActividades] DROP CONSTRAINT [FK_LogActividades_Empresas]
GO
ALTER TABLE [dbo].[InfoUtil] DROP CONSTRAINT [FK_InfoPaginas_Empresas]
GO
ALTER TABLE [dbo].[InfoUtil] DROP CONSTRAINT [FK_InfoPaginas_Categorias]
GO
ALTER TABLE [dbo].[InfoUtil] DROP CONSTRAINT [FK_InfoPaginas_AspNetUsers1]
GO
ALTER TABLE [dbo].[InfoUtil] DROP CONSTRAINT [FK_InfoPaginas_AspNetUsers]
GO
ALTER TABLE [dbo].[Groups] DROP CONSTRAINT [FK_Groups_Empresas]
GO
ALTER TABLE [dbo].[Galerias] DROP CONSTRAINT [FK_Galerias_Empresas]
GO
ALTER TABLE [dbo].[Galerias] DROP CONSTRAINT [FK_Galerias_Categorias]
GO
ALTER TABLE [dbo].[FormsRespDetalle] DROP CONSTRAINT [FK_FormsRespDetalle_FormsRespCabecera]
GO
ALTER TABLE [dbo].[FormsRespDetalle] DROP CONSTRAINT [FK_FormsRespDetalle_FormsQuestionsOptions]
GO
ALTER TABLE [dbo].[FormsRespDetalle] DROP CONSTRAINT [FK_FormsRespDetalle_FormsQuestions]
GO
ALTER TABLE [dbo].[FormsRespCabecera] DROP CONSTRAINT [FK_FormsRespCabecera_Users]
GO
ALTER TABLE [dbo].[FormsRespCabecera] DROP CONSTRAINT [FK_FormsRespCabecera_FormsEstados]
GO
ALTER TABLE [dbo].[FormsRespCabecera] DROP CONSTRAINT [FK_FormsRespCabecera_Forms]
GO
ALTER TABLE [dbo].[FormsQuestionsOptions] DROP CONSTRAINT [FK_SurveysQuestionsOptions_SurveysQuestions]
GO
ALTER TABLE [dbo].[FormsQuestions] DROP CONSTRAINT [FK_SurveysQuestions_SurveysQuestionsTypes]
GO
ALTER TABLE [dbo].[FormsQuestions] DROP CONSTRAINT [FK_SurveysQuestions_Surveys]
GO
ALTER TABLE [dbo].[Forms] DROP CONSTRAINT [FK_Surveys_FormTypes]
GO
ALTER TABLE [dbo].[Forms] DROP CONSTRAINT [FK_Surveys_AspNetUsers]
GO
ALTER TABLE [dbo].[Forms] DROP CONSTRAINT [FK_Forms_Empresas]
GO
ALTER TABLE [dbo].[Forms] DROP CONSTRAINT [FK_Forms_Categorias]
GO
ALTER TABLE [dbo].[Familiares] DROP CONSTRAINT [FK_UsersFamiliares_Catalogos1]
GO
ALTER TABLE [dbo].[Familiares] DROP CONSTRAINT [FK_UsersFamiliares_Catalogos]
GO
ALTER TABLE [dbo].[Familiares] DROP CONSTRAINT [FK_Familiares_Users]
GO
ALTER TABLE [dbo].[Estudios] DROP CONSTRAINT [FK_Estudios_Users]
GO
ALTER TABLE [dbo].[Estudios] DROP CONSTRAINT [FK_Estudios_Titulos]
GO
ALTER TABLE [dbo].[Estudios] DROP CONSTRAINT [FK_Estudios_Establecimientos]
GO
ALTER TABLE [dbo].[Estudios] DROP CONSTRAINT [FK_Estudios_Catalogos]
GO
ALTER TABLE [dbo].[Establecimientos] DROP CONSTRAINT [FK_Establecimientos_Empresas]
GO
ALTER TABLE [dbo].[EmpresasModulos] DROP CONSTRAINT [FK_EmpresasModulos_Modulos]
GO
ALTER TABLE [dbo].[EmpresasModulos] DROP CONSTRAINT [FK_EmpresasModulos_Empresas]
GO
ALTER TABLE [dbo].[EmpresasLayout] DROP CONSTRAINT [FK_EmpresasLayout_Empresas]
GO
ALTER TABLE [dbo].[EmpresasCanales] DROP CONSTRAINT [FK_EmpresasCanales_Empresas]
GO
ALTER TABLE [dbo].[EmpresasCanales] DROP CONSTRAINT [FK_EmpresasCanales_Canales]
GO
ALTER TABLE [dbo].[Empresas] DROP CONSTRAINT [FK_Empresas_TipoValidacion]
GO
ALTER TABLE [dbo].[Documents] DROP CONSTRAINT [FK_Documents_Empresas]
GO
ALTER TABLE [dbo].[Documents] DROP CONSTRAINT [FK_Documents_DocumentTypes]
GO
ALTER TABLE [dbo].[Documents] DROP CONSTRAINT [FK_Documents_DocumentLibrary]
GO
ALTER TABLE [dbo].[Documents] DROP CONSTRAINT [FK_Documents_Categorias]
GO
ALTER TABLE [dbo].[Documents] DROP CONSTRAINT [FK_Documents_AspNetUsers]
GO
ALTER TABLE [dbo].[Devices] DROP CONSTRAINT [FK_Devices_User]
GO
ALTER TABLE [dbo].[CoursesFilters] DROP CONSTRAINT [FK_CoursesFilters_Courses]
GO
ALTER TABLE [dbo].[Courses] DROP CONSTRAINT [FK_Courses_Forms]
GO
ALTER TABLE [dbo].[Courses] DROP CONSTRAINT [FK_Courses_Empresas]
GO
ALTER TABLE [dbo].[Courses] DROP CONSTRAINT [FK_Courses_CoursesTypes]
GO
ALTER TABLE [dbo].[Courses] DROP CONSTRAINT [FK_Courses_Categorias]
GO
ALTER TABLE [dbo].[Courses] DROP CONSTRAINT [FK_Courses_AspNetUsers]
GO
ALTER TABLE [dbo].[Contacts] DROP CONSTRAINT [FK_Contacts_Users]
GO
ALTER TABLE [dbo].[Contacts] DROP CONSTRAINT [FK_Contacts_ReasonContacts]
GO
ALTER TABLE [dbo].[Companias] DROP CONSTRAINT [FK_Companias_Empresas]
GO
ALTER TABLE [dbo].[Categorias] DROP CONSTRAINT [FK_Categorias_Modulos]
GO
ALTER TABLE [dbo].[Categorias] DROP CONSTRAINT [FK_Categorias_Empresas]
GO
ALTER TABLE [dbo].[Categorias] DROP CONSTRAINT [FK_Categorias_Categorias]
GO
ALTER TABLE [dbo].[CatalogosTipo] DROP CONSTRAINT [FK_CatalogosTipo_Empresas]
GO
ALTER TABLE [dbo].[Catalogos] DROP CONSTRAINT [FK_Catalogos_CatalogosTipo]
GO
ALTER TABLE [dbo].[Bills] DROP CONSTRAINT [FK_Bills_Users]
GO
ALTER TABLE [dbo].[Benefits] DROP CONSTRAINT [FK_dbo.Benefits_dbo.Users_Author_Id]
GO
ALTER TABLE [dbo].[Benefits] DROP CONSTRAINT [FK_Benefits_Empresas]
GO
ALTER TABLE [dbo].[Benefits] DROP CONSTRAINT [FK_Benefits_AspNetUsers]
GO
ALTER TABLE [dbo].[BenefitLikes] DROP CONSTRAINT [FK_BenefitLikes_Users]
GO
ALTER TABLE [dbo].[BenefitLikes] DROP CONSTRAINT [FK_BenefitLikes_Benefits]
GO
ALTER TABLE [dbo].[BenefitCategories] DROP CONSTRAINT [FK_BenefitCategories_Categories]
GO
ALTER TABLE [dbo].[BenefitCategories] DROP CONSTRAINT [FK_BenefitCategories_Benefits]
GO
ALTER TABLE [dbo].[AspNetUsers] DROP CONSTRAINT [FK_AspNetUsers_Empresas]
GO
ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserLogins] DROP CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserClaims] DROP CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Areas] DROP CONSTRAINT [FK_Areas_Empresas]
GO
ALTER TABLE [dbo].[Areas] DROP CONSTRAINT [FK_Areas_Areas]
GO
ALTER TABLE [dbo].[ActionLogs] DROP CONSTRAINT [FK_dbo.ActionLogs_dbo.Users_User_Id]
GO
ALTER TABLE [dbo].[ActionLogs] DROP CONSTRAINT [FK_ActionLogs_Sections]
GO
ALTER TABLE [dbo].[ActionLogs] DROP CONSTRAINT [FK_ActionLogs_ActionLogType]
GO
ALTER TABLE [dbo].[ReasonContacts] DROP CONSTRAINT [DF_ReasonContacts_Enabled]
GO
ALTER TABLE [dbo].[ReasonContacts] DROP CONSTRAINT [DF_ReasonContacts_Name]
GO
/****** Object:  View [dbo].[vw_trj_DatosTarjetas]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP VIEW [dbo].[vw_trj_DatosTarjetas]
GO
/****** Object:  View [dbo].[vw_gra_VisitasHora]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP VIEW [dbo].[vw_gra_VisitasHora]
GO
/****** Object:  View [dbo].[vw_gra_UsuariosActivos]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP VIEW [dbo].[vw_gra_UsuariosActivos]
GO
/****** Object:  View [dbo].[vw_gra_UsoUltimos30Dias]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP VIEW [dbo].[vw_gra_UsoUltimos30Dias]
GO
/****** Object:  View [dbo].[vw_gra_PaginasVisitadas]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP VIEW [dbo].[vw_gra_PaginasVisitadas]
GO
/****** Object:  View [dbo].[vw_gra_NoticiasVisitadas]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP VIEW [dbo].[vw_gra_NoticiasVisitadas]
GO
/****** Object:  View [dbo].[vw_gra_ModulosVisitados]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP VIEW [dbo].[vw_gra_ModulosVisitados]
GO
/****** Object:  View [dbo].[vw_gra_DetalleContenidos]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP VIEW [dbo].[vw_gra_DetalleContenidos]
GO
/****** Object:  View [dbo].[vw_gra_BeneficiosVisitados]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP VIEW [dbo].[vw_gra_BeneficiosVisitados]
GO
/****** Object:  View [dbo].[vw_gra_BeneficiosValorados]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP VIEW [dbo].[vw_gra_BeneficiosValorados]
GO
/****** Object:  Table [dbo].[UsersGroups]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[UsersGroups]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Users]
GO
/****** Object:  Table [dbo].[Titulos]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Titulos]
GO
/****** Object:  Table [dbo].[TipoValidacion]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[TipoValidacion]
GO
/****** Object:  Table [dbo].[TipoCatalogoProductosxUsuario]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[TipoCatalogoProductosxUsuario]
GO
/****** Object:  Table [dbo].[TipoCatalogoProductos]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[TipoCatalogoProductos]
GO
/****** Object:  Table [dbo].[Sucursales]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Sucursales]
GO
/****** Object:  Table [dbo].[Segmentacion]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Segmentacion]
GO
/****** Object:  Table [dbo].[Sections]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Sections]
GO
/****** Object:  Table [dbo].[Regiones]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Regiones]
GO
/****** Object:  Table [dbo].[ReasonContacts]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[ReasonContacts]
GO
/****** Object:  Table [dbo].[ReaccionesUsers]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[ReaccionesUsers]
GO
/****** Object:  Table [dbo].[Reacciones]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Reacciones]
GO
/****** Object:  Table [dbo].[PuestosUsers]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[PuestosUsers]
GO
/****** Object:  Table [dbo].[Puestos]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Puestos]
GO
/****** Object:  Table [dbo].[ProductosCanjeados]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[ProductosCanjeados]
GO
/****** Object:  Table [dbo].[ParametrosEmpresasCanal]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[ParametrosEmpresasCanal]
GO
/****** Object:  Table [dbo].[Parametros]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Parametros]
GO
/****** Object:  Table [dbo].[NotificationsFilters]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[NotificationsFilters]
GO
/****** Object:  Table [dbo].[Notifications]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Notifications]
GO
/****** Object:  Table [dbo].[Noticias]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Noticias]
GO
/****** Object:  Table [dbo].[MultimediaTipo]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[MultimediaTipo]
GO
/****** Object:  Table [dbo].[Multimedia]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Multimedia]
GO
/****** Object:  Table [dbo].[Modulos]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Modulos]
GO
/****** Object:  Table [dbo].[MenuItemsRoles]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[MenuItemsRoles]
GO
/****** Object:  Table [dbo].[MenuItems]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[MenuItems]
GO
/****** Object:  Table [dbo].[LogTipos]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[LogTipos]
GO
/****** Object:  Table [dbo].[LogActividades]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[LogActividades]
GO
/****** Object:  Table [dbo].[InfoUtil]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[InfoUtil]
GO
/****** Object:  Table [dbo].[HisLogActividades]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[HisLogActividades]
GO
/****** Object:  Table [dbo].[Groups]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Groups]
GO
/****** Object:  Table [dbo].[Galerias]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Galerias]
GO
/****** Object:  Table [dbo].[FormsTypes]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[FormsTypes]
GO
/****** Object:  Table [dbo].[FormsRespDetalle]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[FormsRespDetalle]
GO
/****** Object:  Table [dbo].[FormsRespCabecera]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[FormsRespCabecera]
GO
/****** Object:  Table [dbo].[FormsQuestionsTypes]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[FormsQuestionsTypes]
GO
/****** Object:  Table [dbo].[FormsQuestionsOptions]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[FormsQuestionsOptions]
GO
/****** Object:  Table [dbo].[FormsQuestions]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[FormsQuestions]
GO
/****** Object:  Table [dbo].[FormsEstados]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[FormsEstados]
GO
/****** Object:  Table [dbo].[Forms]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Forms]
GO
/****** Object:  Table [dbo].[Filtros]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Filtros]
GO
/****** Object:  Table [dbo].[Familiares]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Familiares]
GO
/****** Object:  Table [dbo].[Estudios]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Estudios]
GO
/****** Object:  Table [dbo].[Establecimientos]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Establecimientos]
GO
/****** Object:  Table [dbo].[EmpresasModulos]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[EmpresasModulos]
GO
/****** Object:  Table [dbo].[EmpresasLayout]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[EmpresasLayout]
GO
/****** Object:  Table [dbo].[EmpresasCanales]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[EmpresasCanales]
GO
/****** Object:  Table [dbo].[Empresas]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Empresas]
GO
/****** Object:  Table [dbo].[DocumentTypes]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[DocumentTypes]
GO
/****** Object:  Table [dbo].[Documents]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Documents]
GO
/****** Object:  Table [dbo].[DocumentLibrary]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[DocumentLibrary]
GO
/****** Object:  Table [dbo].[Devices]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Devices]
GO
/****** Object:  Table [dbo].[Credenciales]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Credenciales]
GO
/****** Object:  Table [dbo].[CoursesTypes]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[CoursesTypes]
GO
/****** Object:  Table [dbo].[CoursesFilters]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[CoursesFilters]
GO
/****** Object:  Table [dbo].[Courses]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Courses]
GO
/****** Object:  Table [dbo].[Contacts]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Contacts]
GO
/****** Object:  Table [dbo].[Companias]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Companias]
GO
/****** Object:  Table [dbo].[Categorias]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Categorias]
GO
/****** Object:  Table [dbo].[CatalogosTipo]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[CatalogosTipo]
GO
/****** Object:  Table [dbo].[Catalogos]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Catalogos]
GO
/****** Object:  Table [dbo].[Canales]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Canales]
GO
/****** Object:  Table [dbo].[Bills]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Bills]
GO
/****** Object:  Table [dbo].[Benefits]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Benefits]
GO
/****** Object:  Table [dbo].[BenefitLikes]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[BenefitLikes]
GO
/****** Object:  Table [dbo].[BenefitCategories]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[BenefitCategories]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[AspNetUsers]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[AspNetUserRoles]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[AspNetUserLogins]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[AspNetUserClaims]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[AspNetRoles]
GO
/****** Object:  Table [dbo].[Areas]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[Areas]
GO
/****** Object:  Table [dbo].[ActionLogTypes]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[ActionLogTypes]
GO
/****** Object:  Table [dbo].[ActionLogs]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP TABLE [dbo].[ActionLogs]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CapitalizeFirstLetter]    Script Date: 16/9/2019 10:15:33 a. m. ******/
DROP FUNCTION [dbo].[fn_CapitalizeFirstLetter]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CapitalizeFirstLetter]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =========================================================================================================================================
-- Descripcion : pasa las primeras letras de un texto a mayuscula
-- Ejecucion  : select [dbo].[fn_CapitalizeFirstLetter] ('santiago nuin')
-- Versionado  :
-- <Detalle  Version='1' Fecha='12/02/2019' Autor='Santiago Nuin' Detalle= 'Version inicial'>
-- =========================================================================================================================================


CREATE FUNCTION [dbo].[fn_CapitalizeFirstLetter]
(
	@string VARCHAR(200)
)
RETURNS VARCHAR(200)
AS

BEGIN
	--Declare Variables
	DECLARE @Index        INT,
			@ResultString VARCHAR(200)--result string size should equal to the @string variable size

	--Initialize the variables
	SET @Index = 1
	SET @ResultString = ''


	--Run the Loop until END of the string
	WHILE (@Index <LEN(@string)+1)
	BEGIN
		IF (@Index = 1)--first letter of the string
		BEGIN
			--make the first letter capital
			SET @ResultString = @ResultString + UPPER(SUBSTRING(@string, @Index, 1))
			SET @Index = @Index+ 1--increase the index
		END
		-- IF the previous character is space or '-' or next character is '-'
		ELSE IF ((SUBSTRING(@string, @Index-1, 1) =' 'or SUBSTRING(@string, @Index-1, 1) ='-' or SUBSTRING(@string, @Index+1, 1) ='-') and @Index+1 <> LEN(@string))
		BEGIN
			--make the letter capital
			SET @ResultString = @ResultString + UPPER(SUBSTRING(@string,@Index, 1))
			SET @Index = @Index +1--increase the index
		END
		ELSE-- all others
		BEGIN
			-- make the letter simple
			SET @ResultString = @ResultString + LOWER(SUBSTRING(@string,@Index, 1))
			SET @Index = @Index +1--incerase the index
		END
	END--END of the loop

	IF (@@ERROR <> 0)-- any error occur return the sEND string
	BEGIN
		SET @ResultString = @string
	END

	-- IF no error found return the new string
	RETURN @ResultString

END

GO
/****** Object:  Table [dbo].[ActionLogs]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActionLogs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SectionId] [tinyint] NOT NULL,
	[Date] [datetime] NOT NULL,
	[ActionLogTypeId] [tinyint] NOT NULL,
	[User_Id] [int] NOT NULL,
	[ItemId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.ActionLogs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ActionLogTypes]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActionLogTypes](
	[Id] [tinyint] NOT NULL,
	[Name] [varchar](100) NOT NULL CONSTRAINT [DF_ActionLogTypes_Name]  DEFAULT (''),
	[Enabled] [bit] NOT NULL CONSTRAINT [DF_ActionLogTypes_Enabled]  DEFAULT ((1)),
 CONSTRAINT [PK_ActionLogTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Areas]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Areas](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmpresaId] [int] NOT NULL CONSTRAINT [DF_Areas_EmpresaId]  DEFAULT ((1)),
	[Nombre] [varchar](100) NOT NULL,
	[Habilitada] [bit] NOT NULL CONSTRAINT [DF_Areas_Enabled]  DEFAULT ((1)),
	[AreaPadreId] [int] NULL,
 CONSTRAINT [PK_dbo.Areas] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Discriminator] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Surname] [nvarchar](max) NULL,
	[Image] [nvarchar](max) NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[Enable] [bit] NOT NULL CONSTRAINT [DF_AspNetUsers_Enabled]  DEFAULT ((1)),
	[EmpresaId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BenefitCategories]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BenefitCategories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BenefitId] [int] NOT NULL,
	[CategoriaId] [int] NOT NULL,
 CONSTRAINT [PK_BenefitCategories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BenefitLikes]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BenefitLikes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BenefitId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
 CONSTRAINT [PK_BenefitLikes_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Benefits]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Benefits](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](150) NOT NULL,
	[Content] [nvarchar](max) NULL,
	[Image] [nvarchar](max) NULL,
	[Author_Id] [int] NULL,
	[Date] [datetime] NULL,
	[Subtitle] [varchar](300) NULL,
	[Likes] [int] NOT NULL CONSTRAINT [DF_Benefits_Likes]  DEFAULT ((0)),
	[From] [datetime] NULL,
	[To] [datetime] NULL,
	[Enabled] [bit] NOT NULL CONSTRAINT [DF_Benefits_Enabled]  DEFAULT ((1)),
	[CreationUserId] [nvarchar](128) NULL,
	[EmpresaId] [int] NOT NULL CONSTRAINT [DF_Benefits_EmpresaId]  DEFAULT ((1)),
	[Puntos] [decimal](12, 4) NULL,
	[TipoCatalogoProductosId] [int] NULL,
	[EsNuevo] [bit] NULL DEFAULT ((0)),
 CONSTRAINT [PK_dbo.Benefits] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bills]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bills](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Path] [varchar](255) NOT NULL,
	[BillNumber] [varchar](255) NOT NULL,
	[BillDate] [varchar](50) NOT NULL,
	[Comments] [varchar](500) NOT NULL,
	[CreationUserId] [int] NOT NULL,
 CONSTRAINT [PK_Bills] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Canales]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Canales](
	[Id] [smallint] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Nemonico] [varchar](5) NOT NULL,
 CONSTRAINT [PK_Canales] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Catalogos]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Catalogos](
	[Id] [int] NOT NULL,
	[CatalogoTipoId] [int] NOT NULL,
	[DescripcionLarga] [varchar](100) NOT NULL,
	[DescripcionCorta] [varchar](10) NULL,
	[Habilitado] [bit] NULL,
 CONSTRAINT [PK_Parametros] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CatalogosTipo]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CatalogosTipo](
	[id] [int] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[EmpresaId] [int] NULL,
	[Habilitado] [bit] NOT NULL,
	[Nemonico] [varchar](5) NULL,
 CONSTRAINT [PK_ParametrosTipo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Categorias]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Categorias](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ModuloId] [int] NOT NULL,
	[EmpresaId] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[PadreCategoriaId] [int] NULL,
	[Imagen] [varchar](100) NULL,
	[Habilitada] [bit] NOT NULL,
 CONSTRAINT [PK_Categorias] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Companias]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Companias](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
	[EmpresaId] [int] NOT NULL,
	[Habilitada] [bit] NOT NULL,
 CONSTRAINT [PK_UsersCompania] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Contacts]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contacts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ReasonId] [tinyint] NOT NULL,
	[Comment] [nvarchar](max) NULL,
	[User_Id] [int] NULL,
 CONSTRAINT [PK_Contacts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Courses]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Courses](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](150) NOT NULL,
	[Description] [varchar](300) NULL,
	[CourseType_Id] [int] NOT NULL,
	[DocumentURL] [varchar](255) NULL,
	[VideoURL] [varchar](255) NULL,
	[LinkURL] [varchar](255) NULL,
	[From] [datetime] NOT NULL,
	[To] [datetime] NOT NULL,
	[RequiresEvaluation] [bit] NOT NULL,
	[Form_Id] [int] NULL,
	[Enabled] [bit] NOT NULL CONSTRAINT [DF_Courses_Enabled]  DEFAULT ((1)),
	[Author_Id] [nvarchar](128) NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_Courses_CreationDate]  DEFAULT (getdate()),
	[ApprovedPerc] [int] NOT NULL CONSTRAINT [DF_Courses_ApprovedPerc]  DEFAULT ((0)),
	[EmpresaId] [int] NOT NULL CONSTRAINT [DF_Courses_EmpresaId]  DEFAULT ((1)),
	[CategoriaId] [int] NOT NULL CONSTRAINT [DF_Courses_CategoriaId]  DEFAULT ((1)),
 CONSTRAINT [PK_Courses] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CoursesFilters]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CoursesFilters](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FilteType] [varchar](100) NOT NULL,
	[CourseId] [int] NOT NULL,
	[FilterValue] [varchar](100) NOT NULL,
 CONSTRAINT [PK_CoursesFilters] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CoursesTypes]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CoursesTypes](
	[Id] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
 CONSTRAINT [PK_CourseTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Credenciales]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Credenciales](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmpresaId] [int] NOT NULL,
	[Html] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Credenciales] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Devices]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Devices](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[Uuid] [varchar](100) NOT NULL,
	[TokenNotification] [varchar](255) NOT NULL,
 CONSTRAINT [PK_Devices] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DocumentLibrary]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DocumentLibrary](
	[Id] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_DocumentLibrary] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Documents]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Documents](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmpresaId] [int] NOT NULL,
	[Title] [varchar](150) NOT NULL,
	[Description] [varchar](300) NULL,
	[Filepath] [varchar](255) NOT NULL,
	[Enabled] [bit] NOT NULL CONSTRAINT [DF_Documents_Enabled]  DEFAULT ((1)),
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_Documents_CreatedDate]  DEFAULT (getdate()),
	[CreationUserId] [nvarchar](128) NOT NULL,
	[DocumentTypeId] [int] NOT NULL CONSTRAINT [DF_Documents_DocumentTypeId]  DEFAULT ((1)),
	[DocumentLibraryId] [int] NOT NULL CONSTRAINT [DF_Documents_DocumentLibraryId]  DEFAULT ((1)),
	[CategoriaId] [int] NULL,
 CONSTRAINT [PK_Documents] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DocumentTypes]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DocumentTypes](
	[Id] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Enabled] [bit] NOT NULL,
 CONSTRAINT [PK_DocumentTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Empresas]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Empresas](
	[Id] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[DireccionWeb] [varchar](200) NULL,
	[DireccionApp] [varchar](200) NULL,
	[TipoValidacionId] [int] NOT NULL,
	[Habilitada] [bit] NOT NULL,
	[LogoUrl] [varchar](255) NULL,
	[Nemonico] [varchar](5) NOT NULL CONSTRAINT [DF_Empresas_Nemonico]  DEFAULT (''),
 CONSTRAINT [PK_Empresas_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmpresasCanales]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmpresasCanales](
	[CanalId] [smallint] NOT NULL,
	[EmpresaId] [int] NOT NULL,
	[HomeURL] [varchar](255) NOT NULL CONSTRAINT [DF_EmpresasCanales_HomeURL]  DEFAULT ('Pagina Inicial'),
	[CustomCSS] [varchar](max) NULL,
 CONSTRAINT [PK_EmpresasCanales] PRIMARY KEY CLUSTERED 
(
	[CanalId] ASC,
	[EmpresaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmpresasLayout]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmpresasLayout](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmpresaId] [int] NOT NULL,
	[Codigo] [varchar](100) NOT NULL,
	[Color] [varchar](8) NOT NULL,
	[ColorFondo] [varchar](8) NULL,
	[ColorTipografia] [varchar](8) NULL,
 CONSTRAINT [PK_EmpresasLayout] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmpresasModulos]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmpresasModulos](
	[EmpresaId] [int] NOT NULL,
	[ModuloId] [int] NOT NULL,
 CONSTRAINT [PK_EmpresasModulos] PRIMARY KEY CLUSTERED 
(
	[EmpresaId] ASC,
	[ModuloId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Establecimientos]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Establecimientos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[EmpresaId] [int] NOT NULL,
	[Habilitado] [bit] NOT NULL,
 CONSTRAINT [PK_UsersEstablecimientos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Estudios]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estudios](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[TipoEstudioCatalogoId] [int] NOT NULL,
	[TituloId] [int] NULL,
	[EstablecimientoId] [int] NULL,
	[FechaDesde] [datetime] NULL,
	[FechaHasta] [datetime] NULL,
	[EstudioCompleto] [bit] NULL,
 CONSTRAINT [PK_UsersEstudio] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Familiares]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Familiares](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[Nombre] [varchar](150) NOT NULL,
	[Apellido] [varchar](150) NOT NULL,
	[FechaNacimiento] [datetime] NULL,
	[GeneroCatalogoId] [int] NULL,
	[VinculoCatalogoId] [int] NULL,
 CONSTRAINT [PK_UsersFamily] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Filtros]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Filtros](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Nemonico] [varchar](3) NOT NULL,
 CONSTRAINT [PK_Filtros] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Forms]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Forms](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](150) NOT NULL,
	[Description] [varchar](300) NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_Surveys_CreationDate]  DEFAULT (getdate()),
	[DateFrom] [date] NOT NULL,
	[DateTo] [date] NOT NULL,
	[CreationUserId] [nvarchar](128) NOT NULL,
	[Enabled] [bit] NOT NULL CONSTRAINT [DF_Surveys_Enabled]  DEFAULT ((1)),
	[UniqueReply] [bit] NOT NULL CONSTRAINT [DF_Surveys_UniqueReply]  DEFAULT ((1)),
	[SendMails] [bit] NOT NULL CONSTRAINT [DF_Surveys_SendMails]  DEFAULT ((0)),
	[MailsTo] [varchar](1000) NULL,
	[MailSubject] [varchar](255) NULL,
	[MailTitle] [varchar](255) NULL,
	[MailIntro] [varchar](1000) NULL,
	[Code] [varchar](50) NULL,
	[FormTypeId] [tinyint] NOT NULL,
	[Deleted] [bit] NOT NULL CONSTRAINT [DF_Forms_Deleted]  DEFAULT ((0)),
	[CanTheySelfEvaluate] [bit] NOT NULL CONSTRAINT [DF_Forms_CanTheySelfEvaluate]  DEFAULT ((1)),
	[EmpresaId] [int] NOT NULL CONSTRAINT [DF_Forms_EmpresaId]  DEFAULT ((1)),
	[CategoriaId] [int] NULL,
 CONSTRAINT [PK_Surveys] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FormsEstados]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FormsEstados](
	[Id] [int] NOT NULL,
	[Descripcion] [nchar](10) NOT NULL,
 CONSTRAINT [PK_FormsEstados] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FormsQuestions]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FormsQuestions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Form_Id] [int] NOT NULL,
	[Question] [varchar](255) NOT NULL,
	[FormsQuestionsType_Id] [varchar](10) NOT NULL,
	[Order] [smallint] NOT NULL,
	[Enabled] [bit] NOT NULL CONSTRAINT [DF_SurveysQuestions_Enabled]  DEFAULT ((1)),
	[Required] [bit] NOT NULL CONSTRAINT [DF_SurveysQuestions_Required]  DEFAULT ((0)),
 CONSTRAINT [PK_SurveysQuestions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FormsQuestionsOptions]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FormsQuestionsOptions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FormsQuestion_Id] [int] NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[Order] [smallint] NOT NULL,
	[Enabled] [bit] NOT NULL CONSTRAINT [DF_SurveysQuestionsOptions_Enabled]  DEFAULT ((1)),
	[IsRightAnswer] [bit] NULL,
	[NumberValue] [int] NULL,
 CONSTRAINT [PK_SurveysQuestionsOptions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FormsQuestionsTypes]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FormsQuestionsTypes](
	[Id] [varchar](10) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Enabled] [bit] NOT NULL CONSTRAINT [DF_SurveysQuestionsTypes_Enabled]  DEFAULT ((1)),
 CONSTRAINT [PK_SurveysQuestionsTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FormsRespCabecera]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FormsRespCabecera](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FormsId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FormsEstadoId] [int] NOT NULL,
 CONSTRAINT [PK_FormsRespCabecera] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FormsRespDetalle]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FormsRespDetalle](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FormsRespCabeceraId] [int] NOT NULL,
	[FormsQuestionId] [int] NOT NULL,
	[FormsQuestionsOptionId] [int] NULL,
	[Respuesta] [varchar](1000) NULL,
 CONSTRAINT [PK_FormsRespDetalle] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FormsTypes]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FormsTypes](
	[Id] [tinyint] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Enabled] [bit] NOT NULL CONSTRAINT [DF_FormTypes_Enabled]  DEFAULT ((1)),
	[ChosenByUser] [bit] NOT NULL CONSTRAINT [DF_FormsTypes_ChosenByUser]  DEFAULT ((0)),
	[Descripcion] [varchar](500) NULL,
 CONSTRAINT [PK_FormTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Galerias]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Galerias](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmpresaId] [int] NOT NULL,
	[CategoriaId] [int] NOT NULL,
	[Titulo] [varchar](150) NOT NULL,
	[Copete] [varchar](300) NOT NULL,
	[FechaAlta] [datetime] NOT NULL,
	[AspNetUsersIdAlta] [nvarchar](128) NOT NULL,
	[FechaDesde] [date] NOT NULL,
	[FechaHasta] [date] NOT NULL,
	[ImagenPortada] [varchar](max) NOT NULL,
	[Destacada] [bit] NOT NULL,
	[Habilitada] [bit] NOT NULL,
 CONSTRAINT [PK_Galerias] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Groups]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Groups](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Enabled] [bit] NOT NULL,
	[EmpresaId] [int] NOT NULL CONSTRAINT [DF_Groups_EmpresaId]  DEFAULT ((1)),
 CONSTRAINT [PK_Groups] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HisLogActividades]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HisLogActividades](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LogActividadId] [int] NOT NULL,
	[UserId] [int] NULL,
	[EmpresaId] [int] NOT NULL,
	[ModuloId] [int] NOT NULL,
	[EntidadId] [int] NULL,
	[CanalId] [int] NOT NULL,
	[LogTipoId] [int] NOT NULL,
	[Detalle] [varchar](max) NULL,
	[FechaActividad] [datetime] NOT NULL,
	[FechaAltaHistorico] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InfoUtil]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InfoUtil](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmpresaId] [int] NOT NULL,
	[CategoriaId] [int] NULL,
	[Titulo] [varchar](150) NOT NULL,
	[Copete] [varchar](300) NULL,
	[Html] [varchar](max) NOT NULL,
	[Icono] [varchar](50) NULL,
	[Orden] [smallint] NULL,
	[Destacada] [bit] NOT NULL,
	[FechaAlta] [datetime] NOT NULL,
	[AspNetUsersIdAlta] [nvarchar](128) NOT NULL,
	[FechaBaja] [datetime] NULL,
	[AspNetUsersIdBaja] [nvarchar](128) NULL,
	[Imagen] [varchar](200) NULL,
	[Habilitado] [bit] NULL,
 CONSTRAINT [PK_InfoPaginas] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LogActividades]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LogActividades](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[EmpresaId] [int] NOT NULL,
	[ModuloId] [int] NOT NULL,
	[EntidadId] [int] NULL,
	[CanalId] [int] NOT NULL,
	[LogTipoId] [int] NOT NULL,
	[Detalle] [varchar](max) NULL,
	[FechaActividad] [datetime] NOT NULL,
 CONSTRAINT [PK_LogActividades] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LogTipos]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LogTipos](
	[Id] [int] NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
 CONSTRAINT [PK_LogAcciones] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MenuItems]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MenuItems](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmpresaId] [int] NULL,
	[CanalId] [smallint] NOT NULL,
	[ModuloId] [int] NULL,
	[NombreItem] [varchar](500) NOT NULL,
	[TargetURL] [varchar](255) NOT NULL,
	[TargetDestino] [varchar](100) NULL CONSTRAINT [DF_MenuItems_TargetDestino]  DEFAULT ('Nulo si abre en ventana propia, "_blank" si abre en ventana nueva, y "otroValor" si va a un iframe o algo por el estilo.'),
	[Icono] [varchar](225) NULL,
	[IconoTipo] [varchar](255) NULL,
	[Orden] [smallint] NOT NULL,
	[MenuItemPadreId] [int] NULL,
 CONSTRAINT [PK_MenuItems] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MenuItemsRoles]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuItemsRoles](
	[AspNetUserRoleId] [nvarchar](128) NOT NULL,
	[MenuItemId] [int] NOT NULL,
 CONSTRAINT [PK_MenuItemsRoles] PRIMARY KEY CLUSTERED 
(
	[AspNetUserRoleId] ASC,
	[MenuItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Modulos]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Modulos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Nemonico] [varchar](5) NOT NULL,
	[UsoEnCategorias] [bit] NOT NULL,
 CONSTRAINT [PK_Modulos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Multimedia]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Multimedia](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MultimediaTipoId] [smallint] NOT NULL,
	[EntidadId] [int] NOT NULL,
	[EmpresaId] [int] NOT NULL,
	[ModuloId] [int] NOT NULL,
	[RutaArchivo] [varchar](max) NOT NULL,
	[FechaAlta] [datetime] NOT NULL,
	[Peso] [int] NULL,
 CONSTRAINT [PK_Multimedia] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MultimediaTipo]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MultimediaTipo](
	[Id] [smallint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Nemonico] [varchar](3) NOT NULL,
 CONSTRAINT [PK_MultimediaTipo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Noticias]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Noticias](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmpresaId] [int] NOT NULL,
	[Titulo] [varchar](150) NOT NULL,
	[Copete] [varchar](300) NULL,
	[Contenido] [nvarchar](max) NULL,
	[Image] [nvarchar](max) NULL,
	[AreaId] [int] NOT NULL,
	[FechaAlta] [datetime] NOT NULL,
	[Destacada] [bit] NOT NULL CONSTRAINT [DF_News_Featured]  DEFAULT ((0)),
	[Habilitada] [bit] NOT NULL CONSTRAINT [DF_News_Enabled]  DEFAULT ((1)),
	[FechaDesde] [date] NOT NULL CONSTRAINT [DF_Noticias_Fecha]  DEFAULT (getdate()),
 CONSTRAINT [PK_dbo.News] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Notifications]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Notifications](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmpresaId] [int] NOT NULL CONSTRAINT [DF_Notifications_EmpresaId]  DEFAULT ((1)),
	[Title] [varchar](150) NOT NULL,
	[Message] [varchar](500) NOT NULL,
	[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_Notifications_CreationDate]  DEFAULT (getdate()),
	[SectionId] [tinyint] NOT NULL,
	[BenefitId] [int] NULL,
	[NewsId] [int] NULL,
	[Status] [varchar](150) NULL,
	[CreationUserId] [nvarchar](128) NULL,
	[CourseId] [int] NULL,
	[FormsId] [int] NULL,
	[InfoUtilId] [int] NULL,
 CONSTRAINT [PK_Notifications] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NotificationsFilters]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NotificationsFilters](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FilteType] [varchar](100) NOT NULL,
	[NotificationId] [int] NOT NULL,
	[FilterValue] [varchar](100) NOT NULL,
 CONSTRAINT [PK_NotificationsFilters] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Parametros]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Parametros](
	[Id] [int] NOT NULL,
	[ModuloId] [int] NOT NULL,
	[Nemonico] [varchar](50) NOT NULL,
	[Descripcion] [varchar](500) NOT NULL,
 CONSTRAINT [PK_Parametros_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ParametrosEmpresasCanal]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ParametrosEmpresasCanal](
	[EmpresaId] [int] NOT NULL,
	[ParametroId] [int] NOT NULL,
	[CanalId] [smallint] NOT NULL,
	[Valor] [varchar](500) NOT NULL,
 CONSTRAINT [PK_ParametrosEmpresasCanal] PRIMARY KEY CLUSTERED 
(
	[EmpresaId] ASC,
	[ParametroId] ASC,
	[CanalId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductosCanjeados]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductosCanjeados](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[BenefitsId] [int] NOT NULL,
	[FechaCanje] [datetime] NOT NULL DEFAULT (getdate()),
	[Puntos] [decimal](12, 4) NOT NULL,
	[PuntosUsados] [decimal](12, 4) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Puestos]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Puestos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
	[EmpresaId] [int] NOT NULL,
	[Habilitado] [bit] NOT NULL,
 CONSTRAINT [PK_cat_Puestos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PuestosUsers]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PuestosUsers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PuestoId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[FechaDesde] [datetime] NULL,
	[Notas] [varchar](400) NULL,
 CONSTRAINT [PK_PuestosUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Reacciones]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Reacciones](
	[Id] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Icono] [varchar](200) NOT NULL,
	[Valor] [int] NOT NULL,
	[Orden] [int] NOT NULL,
 CONSTRAINT [PK_Reacciones] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ReaccionesUsers]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReaccionesUsers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmpresaId] [int] NOT NULL,
	[EntidadId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[ModuloId] [int] NOT NULL,
	[ReaccionId] [int] NOT NULL,
	[FechaAlta] [datetime] NOT NULL,
 CONSTRAINT [PK_ReaccionesUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReasonContacts]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ReasonContacts](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Enabled] [bit] NOT NULL,
 CONSTRAINT [PK_ReasonContacts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Regiones]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Regiones](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
	[EmpresaId] [int] NOT NULL,
	[Habilitada] [bit] NOT NULL,
 CONSTRAINT [PK_cat_Regiones] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sections]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sections](
	[Id] [tinyint] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[AppLink] [varchar](50) NOT NULL,
	[Enabled] [bit] NOT NULL CONSTRAINT [DF_Sections_Enabled]  DEFAULT ((1)),
 CONSTRAINT [PK_Sections] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Segmentacion]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Segmentacion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmpresaId] [int] NOT NULL,
	[ModuloId] [int] NOT NULL,
	[EntidadId] [int] NOT NULL,
	[FiltroId] [tinyint] NOT NULL,
	[FiltroValor] [int] NOT NULL,
 CONSTRAINT [PK_Segmentacion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sucursales]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sucursales](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
	[EmpresaId] [int] NOT NULL,
	[Habilitada] [bit] NOT NULL,
 CONSTRAINT [PK_Sucursales] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TipoCatalogoProductos]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoCatalogoProductos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](1024) NOT NULL,
	[IconoCatalogo] [varchar](max) NOT NULL,
	[EmpresaId] [int] NOT NULL,
	[Mail] [varchar](1048) NOT NULL,
	[FondoCatalogo] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TipoCatalogoProductosxUsuario]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoCatalogoProductosxUsuario](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[TipoCatalogoProductoId] [int] NOT NULL,
	[PuntosAcumulados] [decimal](12, 4) NOT NULL,
	[FechaActualizacionPuntos] [datetime] NOT NULL DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TipoValidacion]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoValidacion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
	[Nemonico] [varchar](5) NOT NULL,
 CONSTRAINT [PK_TipoValidacion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Titulos]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Titulos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
	[EmpresaId] [int] NOT NULL,
	[Habilitado] [bit] NOT NULL,
 CONSTRAINT [PK_UsersTitulos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NULL,
	[Surname] [nvarchar](max) NULL,
	[UserName] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[Password] [nvarchar](max) NULL,
	[Image] [nvarchar](max) NULL,
	[Phone] [varchar](25) NULL,
	[Mobile] [varchar](25) NULL,
	[Intern] [varchar](10) NULL,
	[Address] [varchar](200) NULL,
	[City] [varchar](200) NULL,
	[Hobby] [nvarchar](max) NULL,
	[BirthDate] [datetime] NOT NULL,
	[Team] [nvarchar](max) NULL,
	[Active] [bit] NOT NULL,
	[Enable] [bit] NOT NULL,
	[RegistrationDate] [datetime] NOT NULL,
	[Area_Id] [int] NULL,
	[Genero] [varchar](1) NOT NULL CONSTRAINT [DF_Users_Gender]  DEFAULT (''),
	[FileNumber] [varchar](20) NULL,
	[Dni] [varchar](20) NULL,
	[EvaluatorUser_Id] [int] NULL,
	[IsEvaluator] [bit] NOT NULL CONSTRAINT [DF__Users__IsEvaluat__10216507]  DEFAULT ((0)),
	[JobTitle] [varchar](150) NULL,
	[EmpresaId] [int] NULL,
	[RegionId] [int] NULL,
	[CompaniaId] [int] NULL,
	[Cuil] [varchar](20) NULL,
	[TelefonoPersonal] [varchar](20) NULL,
	[TelefonoUrgencias] [varchar](20) NULL,
	[FechaIngreso] [datetime] NULL,
	[FechaEgreso] [datetime] NULL,
	[SucursalId] [int] NULL,
	[ContactoUrgencias] [varchar](200) NULL,
	[GeneroCatalogoId] [int] NULL,
	[CP] [varchar](256) NULL,
	[Pintureria] [varchar](256) NULL,
 CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UsersGroups]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsersGroups](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
 CONSTRAINT [PK_UsersGroups] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[vw_gra_BeneficiosValorados]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- ======================================================================================================
-- Author:		Ariel Cabrejos
-- Create date: 17/11/2018
-- Description:	Devuelve los beneficios con la cantidad de likes de cada uno
-- select * from vw_gra_BenficiosValorados
-- =====================================================================================================

CREATE view [dbo].[vw_gra_BeneficiosValorados] as

--TODO: Cambiar por el modulo de sentimientos! y por MT
SELECT TOP 10
	Title as Titulo,
	Likes as CantLikes,
	1 as EmpresaId
FROM BENEFITS
WHERE [Enabled] = 1
ORDER BY CantLikes desc

GO
/****** Object:  View [dbo].[vw_gra_BeneficiosVisitados]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ======================================================================================================
-- Author:		Ariel Cabrejos
-- Create date: 17/11/2018
-- Description:	Devuelve los beneficios con la cantidad de veces que se visito cada uno
-- select * from vw_gra_BenficiosVisitados
-- =====================================================================================================

Create view [dbo].[vw_gra_BeneficiosVisitados] as

select top 10 
	nt.Titulo,
	la.Empresaid,
	count(1) as 'CantVisitas'
from [dbo].[LogActividades] la
	inner join [dbo].[LogTipos] lt
		on lt.id = la.LogTipoId
	inner join modulos mo
		on mo.id = la.ModuloId 
	inner join Noticias nt
		on nt.id = la.EntidadId
where lt.id in (2)          -- Visita detalle
and   mo.nemonico = 'BENEF' -- Modulo Noticias 
group by la.EmpresaID, nt.Titulo, nt.id
order by CantVisitas desc

GO
/****** Object:  View [dbo].[vw_gra_DetalleContenidos]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
<Versionado> 

<Descripcion>  
Vista que contiene toda la informacion de los distintos modulos
</Descripcion>

<Ejecucion>
select * from [vw_gra_DetalleContenidos]
</Ejecucion>

<Historia>
<Detalle  Version='1' Fecha='17/11/2018' Autor='Ariel Cabrejos'>Version inicial</Detalle>
<Detalle  Version='2' Fecha='14/02/2019' Autor='Santiago Nuin' >Se agrega a la vista el id de la empresa y se modifica el nombre de campo id por entidadId</Detalle>
<Detalle  Version='3' Fecha='22/02/2019' Autor='Santiago Nuin' >Se modifico el nombre de la tabla infopaginas por InfoUtil</Detalle>
</Historia>

</Versionado>
*/

CREATE view [dbo].[vw_gra_DetalleContenidos] as

	------------------------------------------------------------------------------
	-- Documentos
	------------------------------------------------------------------------------
	SELECT 'ModuloId' = (select id from modulos where Nemonico='DOCUM'),
	       'ModuloTitulo'= (select nombre from modulos where Nemonico='DOCUM'), 
	        Id as 'EntidadId',  
	        title as 'Titulo',
			EmpresaId as 'EmpresaId' 
	FROM Documents

	UNION ALL

	------------------------------------------------------------------------------
	-- Capacitaciones
	------------------------------------------------------------------------------
	SELECT 'ModuloId' = (select id from modulos where Nemonico='CAPAC'),
	       'ModuloTitulo'= (select nombre from modulos where Nemonico='CAPAC'), 
	        Id as 'EntidadId',  
	        title as 'Titulo',
			EmpresaId as 'EmpresaId'  
	FROM Courses

	UNION ALL

	------------------------------------------------------------------------------
	--Noticicas
	------------------------------------------------------------------------------
	SELECT 'ModuloId' = (select id from modulos where Nemonico='NOTIC'),
	       'ModuloTitulo'= (select nombre from modulos where Nemonico='NOTIC'), 
	        Id as 'EntidadId',  
	        titulo as 'Titulo',
			EmpresaId as 'EmpresaId'  
	FROM Noticias

	UNION ALL

	------------------------------------------------------------------------------
	--Beneficios
	------------------------------------------------------------------------------
	select 'ModuloId' = (select id from modulos where Nemonico='BENEF'),
	       'ModuloTitulo'= (select nombre from modulos where Nemonico='BENEF'), 
	       Id as 'EntidadId',  
	       title as 'Titulo',
		   EmpresaId as 'EmpresaId'  
	FROM Benefits

	UNION ALL

	------------------------------------------------------------------------------
	--Info Util
	------------------------------------------------------------------------------
	select 'ModuloId' = (select id from modulos where Nemonico='INFUT'),
	       'ModuloTitulo'= (select nombre from modulos where Nemonico='INFUT'), 
	       Id as 'EntidadId',  
	       Titulo as 'Titulo',
		   EmpresaId as 'EmpresaId' 
	FROM infoUtil
	
	UNION ALL

	------------------------------------------------------------------------------
	-- Formularios Generales
	------------------------------------------------------------------------------
	select 'ModuloId' = (select id from modulos where Nemonico='FORGE'),
	       'ModuloTitulo'= (select nombre from modulos where Nemonico='FORGE'), 
	       Id as 'EntidadId',  
	       title as 'Titulo',
		   EmpresaId as 'EmpresaId' 
	from forms 
	where formTypeId = 3

	UNION ALL

	------------------------------------------------------------------------------
	-- Encuestas
	------------------------------------------------------------------------------
	select 'ModuloId' = (select id from modulos where Nemonico='ENCUE'),
	       'ModuloTitulo'= (select nombre from modulos where Nemonico='ENCUE'), 
	       Id as 'EntidadId',  
	       title as 'Titulo',
		   EmpresaId as 'EmpresaId' 
	from forms 
	where formTypeId = 1

	UNION ALL

	------------------------------------------------------------------------------
	-- Evaluaciones
	------------------------------------------------------------------------------
	select 'ModuloId' = (select id from modulos where Nemonico='CAPAC'),
	       'ModuloTitulo'= (select nombre from modulos where Nemonico='CAPAC'), 
	       Id as 'EntidadId',  
	       title as 'Titulo',
		   EmpresaId as 'EmpresaId' 
	from forms 
	where formTypeId = 3

GO
/****** Object:  View [dbo].[vw_gra_ModulosVisitados]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ======================================================================================================
-- Author:		Ariel Cabrejos
-- Create date: 17/11/2018
-- Description:	Devuelve la cantidad de visitas por modulo (listado, detalle o categorias)
-- select * from [vw_gra_ModulosVisitados]
-- =====================================================================================================

CREATE view [dbo].[vw_gra_ModulosVisitados] as

select	EmpresaId,
		Nombre,
		count(1) as 'cantidad' 
from [dbo].[LogActividades] la
	inner join [dbo].[LogTipos] lt
		on la.LogTipoId = lt.Id
	inner join Modulos m
		on m.Id = ModuloId
where lt.id in (1,2,3) --Distintos tipos de visitas
group by Empresaid, Nombre

GO
/****** Object:  View [dbo].[vw_gra_NoticiasVisitadas]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================
-- Author:		Santiago Nuin
-- Create date: 06/11/2018
-- Description:	Devuelve las noticias con la cantidad de veces que se visito cada una
-- select * from vw_gra_NoticiasVisitadas
-- =====================================================================================================

Create view [dbo].[vw_gra_NoticiasVisitadas] as

select top 10 
	nt.Titulo,
	la.Empresaid,
	count(1) as 'CantVisitas'
from [dbo].[LogActividades] la
	inner join [dbo].[LogTipos] lt
		on lt.id = la.LogTipoId
	inner join modulos mo
		on mo.id = la.ModuloId 
	inner join Noticias nt
		on nt.id = la.EntidadId
where lt.id in (2)          -- Visita detalle
and   mo.nemonico = 'NOTIC' -- Modulo Noticias 
group by la.EmpresaID, nt.Titulo, nt.id
order by CantVisitas desc

GO
/****** Object:  View [dbo].[vw_gra_PaginasVisitadas]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================
-- Author:		Santiago Nuin
-- Create date: 06/11/2018
-- Description:	Devuelve la cantidad de Paginas visiatdas (listado, detalle o categorias)
-- select * from vw_gra_PaginasVisitadas
-- =====================================================================================================

Create view [dbo].[vw_gra_PaginasVisitadas] as

select	EmpresaId,
		count(1) as 'cantidad' 
from [dbo].[LogActividades] la
	inner join [dbo].[LogTipos] lt
		on la.LogTipoId = lt.Id
where lt.id in (1,2,3) --Distintos tipos de visitas
group by Empresaid

GO
/****** Object:  View [dbo].[vw_gra_UsoUltimos30Dias]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================
-- Author:		Santiago Nuin
-- Create date: 05/11/2018
-- Description:	Devuelve los datos de uso de la app en los últimos 30 dias
-- select * from vw_gra_UsoUltimos30Dias
-- =====================================================================================================

Create view [dbo].[vw_gra_UsoUltimos30Dias] as

select top 40 -- El top se pone para que la vista acepte el order by 
       EmpresaId,
	   Convert(Date, FechaActividad) as 'fecha', 
       count(FechaActividad) as 'cantidad'
from LogActividades
where FechaActividad >= dateadd(day, -30, Convert(Date, getdate()))
group by EmpresaId, Convert(Date, FechaActividad)
order by fecha

GO
/****** Object:  View [dbo].[vw_gra_UsuariosActivos]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================
-- Author:		Santiago Nuin
-- Create date: 06/11/2018
-- Description:	Devuelve la cantidad de usuarios activos
-- select * from vw_gra_UsuariosActivos
-- =====================================================================================================

Create view [dbo].[vw_gra_UsuariosActivos] as

select	EmpresaId, 
		count(1) as 'cantidad'
from Users
where Active = 1
group by Empresaid

GO
/****** Object:  View [dbo].[vw_gra_VisitasHora]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================
-- Author:		Santiago Nuin
-- Create date: 06/11/2018
-- Description:	Devuelve los datos de la cantidad de visitas en una hora
-- select * from vw_gra_VisitasHora where empresaid = 1
-- =====================================================================================================

Create view [dbo].[vw_gra_VisitasHora] as
select top 30 -- El top se pone para que la vista acepte el order by
	   empresaid,
       convert(int, datepart(hour, FechaActividad)) as 'Orden',
       convert(varchar, datepart(hour, FechaActividad)) + ':00' as 'Hora', 
       count(FechaActividad) as 'Cantidad' 
from LogActividades
group by EmpresaId, datepart(hour, FechaActividad)
order by Orden

GO
/****** Object:  View [dbo].[vw_trj_DatosTarjetas]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================
-- Author:		Santiago Nuin
-- Create date: 12/12/2018
-- Description:	obtiene todos los datos que se pueden necesitar para el armado de una tarjeta. tener en 
--              cuenta que cada uno de los campos que devuelva esta vista tendra que tener una relacion 
--              de nombre en la tabla TarjetasRefernciaDato
-- select * from vw_trj_DatosTarjetas
-- =====================================================================================================

CREATE view [dbo].[vw_trj_DatosTarjetas] as

select us.Id
      ,us.EmpresaId
      ,us.Name
      ,us.Surname
      ,us.Image
      ,ar.Nombre as 'Area'
      ,us.Dni
from Users us
	inner join Areas ar
		on us.Area_id = ar.id
where us.Enable = 1
and   us.Active = 1

GO
ALTER TABLE [dbo].[ReasonContacts] ADD  CONSTRAINT [DF_ReasonContacts_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [dbo].[ReasonContacts] ADD  CONSTRAINT [DF_ReasonContacts_Enabled]  DEFAULT ((1)) FOR [Enabled]
GO
ALTER TABLE [dbo].[ActionLogs]  WITH CHECK ADD  CONSTRAINT [FK_ActionLogs_ActionLogType] FOREIGN KEY([ActionLogTypeId])
REFERENCES [dbo].[ActionLogTypes] ([Id])
GO
ALTER TABLE [dbo].[ActionLogs] CHECK CONSTRAINT [FK_ActionLogs_ActionLogType]
GO
ALTER TABLE [dbo].[ActionLogs]  WITH CHECK ADD  CONSTRAINT [FK_ActionLogs_Sections] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Sections] ([Id])
GO
ALTER TABLE [dbo].[ActionLogs] CHECK CONSTRAINT [FK_ActionLogs_Sections]
GO
ALTER TABLE [dbo].[ActionLogs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ActionLogs_dbo.Users_User_Id] FOREIGN KEY([User_Id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[ActionLogs] CHECK CONSTRAINT [FK_dbo.ActionLogs_dbo.Users_User_Id]
GO
ALTER TABLE [dbo].[Areas]  WITH CHECK ADD  CONSTRAINT [FK_Areas_Areas] FOREIGN KEY([AreaPadreId])
REFERENCES [dbo].[Areas] ([Id])
GO
ALTER TABLE [dbo].[Areas] CHECK CONSTRAINT [FK_Areas_Areas]
GO
ALTER TABLE [dbo].[Areas]  WITH CHECK ADD  CONSTRAINT [FK_Areas_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[Areas] CHECK CONSTRAINT [FK_Areas_Empresas]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUsers]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUsers_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[AspNetUsers] CHECK CONSTRAINT [FK_AspNetUsers_Empresas]
GO
ALTER TABLE [dbo].[BenefitCategories]  WITH CHECK ADD  CONSTRAINT [FK_BenefitCategories_Benefits] FOREIGN KEY([BenefitId])
REFERENCES [dbo].[Benefits] ([Id])
GO
ALTER TABLE [dbo].[BenefitCategories] CHECK CONSTRAINT [FK_BenefitCategories_Benefits]
GO
ALTER TABLE [dbo].[BenefitCategories]  WITH CHECK ADD  CONSTRAINT [FK_BenefitCategories_Categories] FOREIGN KEY([CategoriaId])
REFERENCES [dbo].[Categorias] ([Id])
GO
ALTER TABLE [dbo].[BenefitCategories] CHECK CONSTRAINT [FK_BenefitCategories_Categories]
GO
ALTER TABLE [dbo].[BenefitLikes]  WITH CHECK ADD  CONSTRAINT [FK_BenefitLikes_Benefits] FOREIGN KEY([BenefitId])
REFERENCES [dbo].[Benefits] ([Id])
GO
ALTER TABLE [dbo].[BenefitLikes] CHECK CONSTRAINT [FK_BenefitLikes_Benefits]
GO
ALTER TABLE [dbo].[BenefitLikes]  WITH CHECK ADD  CONSTRAINT [FK_BenefitLikes_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[BenefitLikes] CHECK CONSTRAINT [FK_BenefitLikes_Users]
GO
ALTER TABLE [dbo].[Benefits]  WITH CHECK ADD  CONSTRAINT [FK_Benefits_AspNetUsers] FOREIGN KEY([CreationUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Benefits] CHECK CONSTRAINT [FK_Benefits_AspNetUsers]
GO
ALTER TABLE [dbo].[Benefits]  WITH CHECK ADD  CONSTRAINT [FK_Benefits_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[Benefits] CHECK CONSTRAINT [FK_Benefits_Empresas]
GO
ALTER TABLE [dbo].[Benefits]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Benefits_dbo.Users_Author_Id] FOREIGN KEY([Author_Id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Benefits] CHECK CONSTRAINT [FK_dbo.Benefits_dbo.Users_Author_Id]
GO
ALTER TABLE [dbo].[Bills]  WITH CHECK ADD  CONSTRAINT [FK_Bills_Users] FOREIGN KEY([CreationUserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Bills] CHECK CONSTRAINT [FK_Bills_Users]
GO
ALTER TABLE [dbo].[Catalogos]  WITH CHECK ADD  CONSTRAINT [FK_Catalogos_CatalogosTipo] FOREIGN KEY([CatalogoTipoId])
REFERENCES [dbo].[CatalogosTipo] ([id])
GO
ALTER TABLE [dbo].[Catalogos] CHECK CONSTRAINT [FK_Catalogos_CatalogosTipo]
GO
ALTER TABLE [dbo].[CatalogosTipo]  WITH CHECK ADD  CONSTRAINT [FK_CatalogosTipo_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[CatalogosTipo] CHECK CONSTRAINT [FK_CatalogosTipo_Empresas]
GO
ALTER TABLE [dbo].[Categorias]  WITH CHECK ADD  CONSTRAINT [FK_Categorias_Categorias] FOREIGN KEY([PadreCategoriaId])
REFERENCES [dbo].[Categorias] ([Id])
GO
ALTER TABLE [dbo].[Categorias] CHECK CONSTRAINT [FK_Categorias_Categorias]
GO
ALTER TABLE [dbo].[Categorias]  WITH CHECK ADD  CONSTRAINT [FK_Categorias_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[Categorias] CHECK CONSTRAINT [FK_Categorias_Empresas]
GO
ALTER TABLE [dbo].[Categorias]  WITH CHECK ADD  CONSTRAINT [FK_Categorias_Modulos] FOREIGN KEY([ModuloId])
REFERENCES [dbo].[Modulos] ([Id])
GO
ALTER TABLE [dbo].[Categorias] CHECK CONSTRAINT [FK_Categorias_Modulos]
GO
ALTER TABLE [dbo].[Companias]  WITH CHECK ADD  CONSTRAINT [FK_Companias_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[Companias] CHECK CONSTRAINT [FK_Companias_Empresas]
GO
ALTER TABLE [dbo].[Contacts]  WITH CHECK ADD  CONSTRAINT [FK_Contacts_ReasonContacts] FOREIGN KEY([ReasonId])
REFERENCES [dbo].[ReasonContacts] ([Id])
GO
ALTER TABLE [dbo].[Contacts] CHECK CONSTRAINT [FK_Contacts_ReasonContacts]
GO
ALTER TABLE [dbo].[Contacts]  WITH CHECK ADD  CONSTRAINT [FK_Contacts_Users] FOREIGN KEY([User_Id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Contacts] CHECK CONSTRAINT [FK_Contacts_Users]
GO
ALTER TABLE [dbo].[Courses]  WITH CHECK ADD  CONSTRAINT [FK_Courses_AspNetUsers] FOREIGN KEY([Author_Id])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Courses] CHECK CONSTRAINT [FK_Courses_AspNetUsers]
GO
ALTER TABLE [dbo].[Courses]  WITH CHECK ADD  CONSTRAINT [FK_Courses_Categorias] FOREIGN KEY([CategoriaId])
REFERENCES [dbo].[Categorias] ([Id])
GO
ALTER TABLE [dbo].[Courses] CHECK CONSTRAINT [FK_Courses_Categorias]
GO
ALTER TABLE [dbo].[Courses]  WITH CHECK ADD  CONSTRAINT [FK_Courses_CoursesTypes] FOREIGN KEY([CourseType_Id])
REFERENCES [dbo].[CoursesTypes] ([Id])
GO
ALTER TABLE [dbo].[Courses] CHECK CONSTRAINT [FK_Courses_CoursesTypes]
GO
ALTER TABLE [dbo].[Courses]  WITH CHECK ADD  CONSTRAINT [FK_Courses_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[Courses] CHECK CONSTRAINT [FK_Courses_Empresas]
GO
ALTER TABLE [dbo].[Courses]  WITH CHECK ADD  CONSTRAINT [FK_Courses_Forms] FOREIGN KEY([Form_Id])
REFERENCES [dbo].[Forms] ([Id])
GO
ALTER TABLE [dbo].[Courses] CHECK CONSTRAINT [FK_Courses_Forms]
GO
ALTER TABLE [dbo].[CoursesFilters]  WITH CHECK ADD  CONSTRAINT [FK_CoursesFilters_Courses] FOREIGN KEY([CourseId])
REFERENCES [dbo].[Courses] ([Id])
GO
ALTER TABLE [dbo].[CoursesFilters] CHECK CONSTRAINT [FK_CoursesFilters_Courses]
GO
ALTER TABLE [dbo].[Devices]  WITH CHECK ADD  CONSTRAINT [FK_Devices_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Devices] CHECK CONSTRAINT [FK_Devices_User]
GO
ALTER TABLE [dbo].[Documents]  WITH CHECK ADD  CONSTRAINT [FK_Documents_AspNetUsers] FOREIGN KEY([CreationUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Documents] CHECK CONSTRAINT [FK_Documents_AspNetUsers]
GO
ALTER TABLE [dbo].[Documents]  WITH CHECK ADD  CONSTRAINT [FK_Documents_Categorias] FOREIGN KEY([CategoriaId])
REFERENCES [dbo].[Categorias] ([Id])
GO
ALTER TABLE [dbo].[Documents] CHECK CONSTRAINT [FK_Documents_Categorias]
GO
ALTER TABLE [dbo].[Documents]  WITH CHECK ADD  CONSTRAINT [FK_Documents_DocumentLibrary] FOREIGN KEY([DocumentLibraryId])
REFERENCES [dbo].[DocumentLibrary] ([Id])
GO
ALTER TABLE [dbo].[Documents] CHECK CONSTRAINT [FK_Documents_DocumentLibrary]
GO
ALTER TABLE [dbo].[Documents]  WITH CHECK ADD  CONSTRAINT [FK_Documents_DocumentTypes] FOREIGN KEY([DocumentTypeId])
REFERENCES [dbo].[DocumentTypes] ([Id])
GO
ALTER TABLE [dbo].[Documents] CHECK CONSTRAINT [FK_Documents_DocumentTypes]
GO
ALTER TABLE [dbo].[Documents]  WITH CHECK ADD  CONSTRAINT [FK_Documents_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[Documents] CHECK CONSTRAINT [FK_Documents_Empresas]
GO
ALTER TABLE [dbo].[Empresas]  WITH CHECK ADD  CONSTRAINT [FK_Empresas_TipoValidacion] FOREIGN KEY([TipoValidacionId])
REFERENCES [dbo].[TipoValidacion] ([Id])
GO
ALTER TABLE [dbo].[Empresas] CHECK CONSTRAINT [FK_Empresas_TipoValidacion]
GO
ALTER TABLE [dbo].[EmpresasCanales]  WITH CHECK ADD  CONSTRAINT [FK_EmpresasCanales_Canales] FOREIGN KEY([CanalId])
REFERENCES [dbo].[Canales] ([Id])
GO
ALTER TABLE [dbo].[EmpresasCanales] CHECK CONSTRAINT [FK_EmpresasCanales_Canales]
GO
ALTER TABLE [dbo].[EmpresasCanales]  WITH CHECK ADD  CONSTRAINT [FK_EmpresasCanales_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[EmpresasCanales] CHECK CONSTRAINT [FK_EmpresasCanales_Empresas]
GO
ALTER TABLE [dbo].[EmpresasLayout]  WITH CHECK ADD  CONSTRAINT [FK_EmpresasLayout_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[EmpresasLayout] CHECK CONSTRAINT [FK_EmpresasLayout_Empresas]
GO
ALTER TABLE [dbo].[EmpresasModulos]  WITH CHECK ADD  CONSTRAINT [FK_EmpresasModulos_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[EmpresasModulos] CHECK CONSTRAINT [FK_EmpresasModulos_Empresas]
GO
ALTER TABLE [dbo].[EmpresasModulos]  WITH CHECK ADD  CONSTRAINT [FK_EmpresasModulos_Modulos] FOREIGN KEY([ModuloId])
REFERENCES [dbo].[Modulos] ([Id])
GO
ALTER TABLE [dbo].[EmpresasModulos] CHECK CONSTRAINT [FK_EmpresasModulos_Modulos]
GO
ALTER TABLE [dbo].[Establecimientos]  WITH CHECK ADD  CONSTRAINT [FK_Establecimientos_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[Establecimientos] CHECK CONSTRAINT [FK_Establecimientos_Empresas]
GO
ALTER TABLE [dbo].[Estudios]  WITH CHECK ADD  CONSTRAINT [FK_Estudios_Catalogos] FOREIGN KEY([TipoEstudioCatalogoId])
REFERENCES [dbo].[Catalogos] ([Id])
GO
ALTER TABLE [dbo].[Estudios] CHECK CONSTRAINT [FK_Estudios_Catalogos]
GO
ALTER TABLE [dbo].[Estudios]  WITH CHECK ADD  CONSTRAINT [FK_Estudios_Establecimientos] FOREIGN KEY([EstablecimientoId])
REFERENCES [dbo].[Establecimientos] ([Id])
GO
ALTER TABLE [dbo].[Estudios] CHECK CONSTRAINT [FK_Estudios_Establecimientos]
GO
ALTER TABLE [dbo].[Estudios]  WITH CHECK ADD  CONSTRAINT [FK_Estudios_Titulos] FOREIGN KEY([TituloId])
REFERENCES [dbo].[Titulos] ([Id])
GO
ALTER TABLE [dbo].[Estudios] CHECK CONSTRAINT [FK_Estudios_Titulos]
GO
ALTER TABLE [dbo].[Estudios]  WITH CHECK ADD  CONSTRAINT [FK_Estudios_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Estudios] CHECK CONSTRAINT [FK_Estudios_Users]
GO
ALTER TABLE [dbo].[Familiares]  WITH CHECK ADD  CONSTRAINT [FK_Familiares_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Familiares] CHECK CONSTRAINT [FK_Familiares_Users]
GO
ALTER TABLE [dbo].[Familiares]  WITH CHECK ADD  CONSTRAINT [FK_UsersFamiliares_Catalogos] FOREIGN KEY([GeneroCatalogoId])
REFERENCES [dbo].[Catalogos] ([Id])
GO
ALTER TABLE [dbo].[Familiares] CHECK CONSTRAINT [FK_UsersFamiliares_Catalogos]
GO
ALTER TABLE [dbo].[Familiares]  WITH CHECK ADD  CONSTRAINT [FK_UsersFamiliares_Catalogos1] FOREIGN KEY([VinculoCatalogoId])
REFERENCES [dbo].[Catalogos] ([Id])
GO
ALTER TABLE [dbo].[Familiares] CHECK CONSTRAINT [FK_UsersFamiliares_Catalogos1]
GO
ALTER TABLE [dbo].[Forms]  WITH CHECK ADD  CONSTRAINT [FK_Forms_Categorias] FOREIGN KEY([CategoriaId])
REFERENCES [dbo].[Categorias] ([Id])
GO
ALTER TABLE [dbo].[Forms] CHECK CONSTRAINT [FK_Forms_Categorias]
GO
ALTER TABLE [dbo].[Forms]  WITH CHECK ADD  CONSTRAINT [FK_Forms_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[Forms] CHECK CONSTRAINT [FK_Forms_Empresas]
GO
ALTER TABLE [dbo].[Forms]  WITH CHECK ADD  CONSTRAINT [FK_Surveys_AspNetUsers] FOREIGN KEY([CreationUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Forms] CHECK CONSTRAINT [FK_Surveys_AspNetUsers]
GO
ALTER TABLE [dbo].[Forms]  WITH CHECK ADD  CONSTRAINT [FK_Surveys_FormTypes] FOREIGN KEY([FormTypeId])
REFERENCES [dbo].[FormsTypes] ([Id])
GO
ALTER TABLE [dbo].[Forms] CHECK CONSTRAINT [FK_Surveys_FormTypes]
GO
ALTER TABLE [dbo].[FormsQuestions]  WITH CHECK ADD  CONSTRAINT [FK_SurveysQuestions_Surveys] FOREIGN KEY([Form_Id])
REFERENCES [dbo].[Forms] ([Id])
GO
ALTER TABLE [dbo].[FormsQuestions] CHECK CONSTRAINT [FK_SurveysQuestions_Surveys]
GO
ALTER TABLE [dbo].[FormsQuestions]  WITH CHECK ADD  CONSTRAINT [FK_SurveysQuestions_SurveysQuestionsTypes] FOREIGN KEY([FormsQuestionsType_Id])
REFERENCES [dbo].[FormsQuestionsTypes] ([Id])
GO
ALTER TABLE [dbo].[FormsQuestions] CHECK CONSTRAINT [FK_SurveysQuestions_SurveysQuestionsTypes]
GO
ALTER TABLE [dbo].[FormsQuestionsOptions]  WITH CHECK ADD  CONSTRAINT [FK_SurveysQuestionsOptions_SurveysQuestions] FOREIGN KEY([FormsQuestion_Id])
REFERENCES [dbo].[FormsQuestions] ([Id])
GO
ALTER TABLE [dbo].[FormsQuestionsOptions] CHECK CONSTRAINT [FK_SurveysQuestionsOptions_SurveysQuestions]
GO
ALTER TABLE [dbo].[FormsRespCabecera]  WITH CHECK ADD  CONSTRAINT [FK_FormsRespCabecera_Forms] FOREIGN KEY([FormsId])
REFERENCES [dbo].[Forms] ([Id])
GO
ALTER TABLE [dbo].[FormsRespCabecera] CHECK CONSTRAINT [FK_FormsRespCabecera_Forms]
GO
ALTER TABLE [dbo].[FormsRespCabecera]  WITH CHECK ADD  CONSTRAINT [FK_FormsRespCabecera_FormsEstados] FOREIGN KEY([FormsEstadoId])
REFERENCES [dbo].[FormsEstados] ([Id])
GO
ALTER TABLE [dbo].[FormsRespCabecera] CHECK CONSTRAINT [FK_FormsRespCabecera_FormsEstados]
GO
ALTER TABLE [dbo].[FormsRespCabecera]  WITH CHECK ADD  CONSTRAINT [FK_FormsRespCabecera_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[FormsRespCabecera] CHECK CONSTRAINT [FK_FormsRespCabecera_Users]
GO
ALTER TABLE [dbo].[FormsRespDetalle]  WITH CHECK ADD  CONSTRAINT [FK_FormsRespDetalle_FormsQuestions] FOREIGN KEY([FormsQuestionId])
REFERENCES [dbo].[FormsQuestions] ([Id])
GO
ALTER TABLE [dbo].[FormsRespDetalle] CHECK CONSTRAINT [FK_FormsRespDetalle_FormsQuestions]
GO
ALTER TABLE [dbo].[FormsRespDetalle]  WITH CHECK ADD  CONSTRAINT [FK_FormsRespDetalle_FormsQuestionsOptions] FOREIGN KEY([FormsQuestionsOptionId])
REFERENCES [dbo].[FormsQuestionsOptions] ([Id])
GO
ALTER TABLE [dbo].[FormsRespDetalle] CHECK CONSTRAINT [FK_FormsRespDetalle_FormsQuestionsOptions]
GO
ALTER TABLE [dbo].[FormsRespDetalle]  WITH CHECK ADD  CONSTRAINT [FK_FormsRespDetalle_FormsRespCabecera] FOREIGN KEY([FormsRespCabeceraId])
REFERENCES [dbo].[FormsRespCabecera] ([Id])
GO
ALTER TABLE [dbo].[FormsRespDetalle] CHECK CONSTRAINT [FK_FormsRespDetalle_FormsRespCabecera]
GO
ALTER TABLE [dbo].[Galerias]  WITH CHECK ADD  CONSTRAINT [FK_Galerias_Categorias] FOREIGN KEY([CategoriaId])
REFERENCES [dbo].[Categorias] ([Id])
GO
ALTER TABLE [dbo].[Galerias] CHECK CONSTRAINT [FK_Galerias_Categorias]
GO
ALTER TABLE [dbo].[Galerias]  WITH CHECK ADD  CONSTRAINT [FK_Galerias_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[Galerias] CHECK CONSTRAINT [FK_Galerias_Empresas]
GO
ALTER TABLE [dbo].[Groups]  WITH CHECK ADD  CONSTRAINT [FK_Groups_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[Groups] CHECK CONSTRAINT [FK_Groups_Empresas]
GO
ALTER TABLE [dbo].[InfoUtil]  WITH CHECK ADD  CONSTRAINT [FK_InfoPaginas_AspNetUsers] FOREIGN KEY([AspNetUsersIdAlta])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[InfoUtil] CHECK CONSTRAINT [FK_InfoPaginas_AspNetUsers]
GO
ALTER TABLE [dbo].[InfoUtil]  WITH CHECK ADD  CONSTRAINT [FK_InfoPaginas_AspNetUsers1] FOREIGN KEY([AspNetUsersIdBaja])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[InfoUtil] CHECK CONSTRAINT [FK_InfoPaginas_AspNetUsers1]
GO
ALTER TABLE [dbo].[InfoUtil]  WITH CHECK ADD  CONSTRAINT [FK_InfoPaginas_Categorias] FOREIGN KEY([CategoriaId])
REFERENCES [dbo].[Categorias] ([Id])
GO
ALTER TABLE [dbo].[InfoUtil] CHECK CONSTRAINT [FK_InfoPaginas_Categorias]
GO
ALTER TABLE [dbo].[InfoUtil]  WITH CHECK ADD  CONSTRAINT [FK_InfoPaginas_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[InfoUtil] CHECK CONSTRAINT [FK_InfoPaginas_Empresas]
GO
ALTER TABLE [dbo].[LogActividades]  WITH CHECK ADD  CONSTRAINT [FK_LogActividades_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[LogActividades] CHECK CONSTRAINT [FK_LogActividades_Empresas]
GO
ALTER TABLE [dbo].[LogActividades]  WITH CHECK ADD  CONSTRAINT [FK_LogActividades_LogTipos] FOREIGN KEY([LogTipoId])
REFERENCES [dbo].[LogTipos] ([Id])
GO
ALTER TABLE [dbo].[LogActividades] CHECK CONSTRAINT [FK_LogActividades_LogTipos]
GO
ALTER TABLE [dbo].[LogActividades]  WITH CHECK ADD  CONSTRAINT [FK_LogActividades_Modulos] FOREIGN KEY([ModuloId])
REFERENCES [dbo].[Modulos] ([Id])
GO
ALTER TABLE [dbo].[LogActividades] CHECK CONSTRAINT [FK_LogActividades_Modulos]
GO
ALTER TABLE [dbo].[LogActividades]  WITH CHECK ADD  CONSTRAINT [FK_LogActividades_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[LogActividades] CHECK CONSTRAINT [FK_LogActividades_Users]
GO
ALTER TABLE [dbo].[MenuItems]  WITH CHECK ADD  CONSTRAINT [FK_MenuItems_Canales] FOREIGN KEY([CanalId])
REFERENCES [dbo].[Canales] ([Id])
GO
ALTER TABLE [dbo].[MenuItems] CHECK CONSTRAINT [FK_MenuItems_Canales]
GO
ALTER TABLE [dbo].[MenuItems]  WITH CHECK ADD  CONSTRAINT [FK_MenuItems_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[MenuItems] CHECK CONSTRAINT [FK_MenuItems_Empresas]
GO
ALTER TABLE [dbo].[MenuItems]  WITH CHECK ADD  CONSTRAINT [FK_MenuItems_MenuItems] FOREIGN KEY([MenuItemPadreId])
REFERENCES [dbo].[MenuItems] ([Id])
GO
ALTER TABLE [dbo].[MenuItems] CHECK CONSTRAINT [FK_MenuItems_MenuItems]
GO
ALTER TABLE [dbo].[MenuItems]  WITH CHECK ADD  CONSTRAINT [FK_MenuItems_Modulos] FOREIGN KEY([ModuloId])
REFERENCES [dbo].[Modulos] ([Id])
GO
ALTER TABLE [dbo].[MenuItems] CHECK CONSTRAINT [FK_MenuItems_Modulos]
GO
ALTER TABLE [dbo].[MenuItemsRoles]  WITH CHECK ADD  CONSTRAINT [FK_MenuItemsRoles_AspNetRoles] FOREIGN KEY([AspNetUserRoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
GO
ALTER TABLE [dbo].[MenuItemsRoles] CHECK CONSTRAINT [FK_MenuItemsRoles_AspNetRoles]
GO
ALTER TABLE [dbo].[MenuItemsRoles]  WITH CHECK ADD  CONSTRAINT [FK_MenuItemsRoles_MenuItems] FOREIGN KEY([MenuItemId])
REFERENCES [dbo].[MenuItems] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MenuItemsRoles] CHECK CONSTRAINT [FK_MenuItemsRoles_MenuItems]
GO
ALTER TABLE [dbo].[Multimedia]  WITH CHECK ADD  CONSTRAINT [FK_Multimedia_Galerias] FOREIGN KEY([EntidadId])
REFERENCES [dbo].[Galerias] ([Id])
GO
ALTER TABLE [dbo].[Multimedia] CHECK CONSTRAINT [FK_Multimedia_Galerias]
GO
ALTER TABLE [dbo].[Multimedia]  WITH CHECK ADD  CONSTRAINT [FK_Multimedia_Modulos] FOREIGN KEY([ModuloId])
REFERENCES [dbo].[Modulos] ([Id])
GO
ALTER TABLE [dbo].[Multimedia] CHECK CONSTRAINT [FK_Multimedia_Modulos]
GO
ALTER TABLE [dbo].[Noticias]  WITH CHECK ADD  CONSTRAINT [FK_Noticias_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[Noticias] CHECK CONSTRAINT [FK_Noticias_Empresas]
GO
ALTER TABLE [dbo].[Notifications]  WITH CHECK ADD  CONSTRAINT [FK_Notifications_AspNetUsers] FOREIGN KEY([CreationUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Notifications] CHECK CONSTRAINT [FK_Notifications_AspNetUsers]
GO
ALTER TABLE [dbo].[Notifications]  WITH CHECK ADD  CONSTRAINT [FK_Notifications_Benefits] FOREIGN KEY([BenefitId])
REFERENCES [dbo].[Benefits] ([Id])
GO
ALTER TABLE [dbo].[Notifications] CHECK CONSTRAINT [FK_Notifications_Benefits]
GO
ALTER TABLE [dbo].[Notifications]  WITH CHECK ADD  CONSTRAINT [FK_Notifications_Courses] FOREIGN KEY([CourseId])
REFERENCES [dbo].[Courses] ([Id])
GO
ALTER TABLE [dbo].[Notifications] CHECK CONSTRAINT [FK_Notifications_Courses]
GO
ALTER TABLE [dbo].[Notifications]  WITH CHECK ADD  CONSTRAINT [FK_Notifications_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[Notifications] CHECK CONSTRAINT [FK_Notifications_Empresas]
GO
ALTER TABLE [dbo].[Notifications]  WITH CHECK ADD  CONSTRAINT [FK_Notifications_Forms] FOREIGN KEY([FormsId])
REFERENCES [dbo].[Forms] ([Id])
GO
ALTER TABLE [dbo].[Notifications] CHECK CONSTRAINT [FK_Notifications_Forms]
GO
ALTER TABLE [dbo].[Notifications]  WITH CHECK ADD  CONSTRAINT [FK_Notifications_InfoPaginas] FOREIGN KEY([InfoUtilId])
REFERENCES [dbo].[InfoUtil] ([Id])
GO
ALTER TABLE [dbo].[Notifications] CHECK CONSTRAINT [FK_Notifications_InfoPaginas]
GO
ALTER TABLE [dbo].[Notifications]  WITH CHECK ADD  CONSTRAINT [FK_Notifications_News] FOREIGN KEY([NewsId])
REFERENCES [dbo].[Noticias] ([Id])
GO
ALTER TABLE [dbo].[Notifications] CHECK CONSTRAINT [FK_Notifications_News]
GO
ALTER TABLE [dbo].[Notifications]  WITH CHECK ADD  CONSTRAINT [FK_Notifications_Sections] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Sections] ([Id])
GO
ALTER TABLE [dbo].[Notifications] CHECK CONSTRAINT [FK_Notifications_Sections]
GO
ALTER TABLE [dbo].[NotificationsFilters]  WITH CHECK ADD  CONSTRAINT [FK_NotificationsFilters_Notifications] FOREIGN KEY([NotificationId])
REFERENCES [dbo].[Notifications] ([Id])
GO
ALTER TABLE [dbo].[NotificationsFilters] CHECK CONSTRAINT [FK_NotificationsFilters_Notifications]
GO
ALTER TABLE [dbo].[Parametros]  WITH CHECK ADD  CONSTRAINT [FK_Parametros_Modulos] FOREIGN KEY([ModuloId])
REFERENCES [dbo].[Modulos] ([Id])
GO
ALTER TABLE [dbo].[Parametros] CHECK CONSTRAINT [FK_Parametros_Modulos]
GO
ALTER TABLE [dbo].[ParametrosEmpresasCanal]  WITH CHECK ADD  CONSTRAINT [FK_ParametrosEmpresasCanal_Canales] FOREIGN KEY([CanalId])
REFERENCES [dbo].[Canales] ([Id])
GO
ALTER TABLE [dbo].[ParametrosEmpresasCanal] CHECK CONSTRAINT [FK_ParametrosEmpresasCanal_Canales]
GO
ALTER TABLE [dbo].[ParametrosEmpresasCanal]  WITH CHECK ADD  CONSTRAINT [FK_ParametrosEmpresasCanal_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[ParametrosEmpresasCanal] CHECK CONSTRAINT [FK_ParametrosEmpresasCanal_Empresas]
GO
ALTER TABLE [dbo].[ParametrosEmpresasCanal]  WITH CHECK ADD  CONSTRAINT [FK_ParametrosEmpresasCanal_Parametros] FOREIGN KEY([ParametroId])
REFERENCES [dbo].[Parametros] ([Id])
GO
ALTER TABLE [dbo].[ParametrosEmpresasCanal] CHECK CONSTRAINT [FK_ParametrosEmpresasCanal_Parametros]
GO
ALTER TABLE [dbo].[Puestos]  WITH CHECK ADD  CONSTRAINT [FK_Puestos_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[Puestos] CHECK CONSTRAINT [FK_Puestos_Empresas]
GO
ALTER TABLE [dbo].[PuestosUsers]  WITH CHECK ADD  CONSTRAINT [FK_PuestosUsers_Puestos] FOREIGN KEY([PuestoId])
REFERENCES [dbo].[Puestos] ([Id])
GO
ALTER TABLE [dbo].[PuestosUsers] CHECK CONSTRAINT [FK_PuestosUsers_Puestos]
GO
ALTER TABLE [dbo].[PuestosUsers]  WITH CHECK ADD  CONSTRAINT [FK_PuestosUsers_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[PuestosUsers] CHECK CONSTRAINT [FK_PuestosUsers_Users]
GO
ALTER TABLE [dbo].[ReaccionesUsers]  WITH CHECK ADD  CONSTRAINT [FK_ReaccionesUsers_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[ReaccionesUsers] CHECK CONSTRAINT [FK_ReaccionesUsers_Empresas]
GO
ALTER TABLE [dbo].[ReaccionesUsers]  WITH CHECK ADD  CONSTRAINT [FK_ReaccionesUsers_Reacciones] FOREIGN KEY([ReaccionId])
REFERENCES [dbo].[Reacciones] ([Id])
GO
ALTER TABLE [dbo].[ReaccionesUsers] CHECK CONSTRAINT [FK_ReaccionesUsers_Reacciones]
GO
ALTER TABLE [dbo].[ReaccionesUsers]  WITH CHECK ADD  CONSTRAINT [FK_ReaccionesUsers_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[ReaccionesUsers] CHECK CONSTRAINT [FK_ReaccionesUsers_Users]
GO
ALTER TABLE [dbo].[Regiones]  WITH CHECK ADD  CONSTRAINT [FK_Regiones_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[Regiones] CHECK CONSTRAINT [FK_Regiones_Empresas]
GO
ALTER TABLE [dbo].[Segmentacion]  WITH CHECK ADD  CONSTRAINT [FK_Segmentacion_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[Segmentacion] CHECK CONSTRAINT [FK_Segmentacion_Empresas]
GO
ALTER TABLE [dbo].[Segmentacion]  WITH CHECK ADD  CONSTRAINT [FK_Segmentacion_Filtros] FOREIGN KEY([FiltroId])
REFERENCES [dbo].[Filtros] ([Id])
GO
ALTER TABLE [dbo].[Segmentacion] CHECK CONSTRAINT [FK_Segmentacion_Filtros]
GO
ALTER TABLE [dbo].[Segmentacion]  WITH CHECK ADD  CONSTRAINT [FK_Segmentacion_Modulos] FOREIGN KEY([ModuloId])
REFERENCES [dbo].[Modulos] ([Id])
GO
ALTER TABLE [dbo].[Segmentacion] CHECK CONSTRAINT [FK_Segmentacion_Modulos]
GO
ALTER TABLE [dbo].[Titulos]  WITH CHECK ADD  CONSTRAINT [FK_Titulos_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[Titulos] CHECK CONSTRAINT [FK_Titulos_Empresas]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Users_dbo.Areas_Area_Id] FOREIGN KEY([Area_Id])
REFERENCES [dbo].[Areas] ([Id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_dbo.Users_dbo.Areas_Area_Id]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Companias] FOREIGN KEY([CompaniaId])
REFERENCES [dbo].[Companias] ([Id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Companias]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Empresas] FOREIGN KEY([EmpresaId])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Empresas]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Regiones] FOREIGN KEY([RegionId])
REFERENCES [dbo].[Regiones] ([Id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Regiones]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Sucursales] FOREIGN KEY([SucursalId])
REFERENCES [dbo].[Sucursales] ([Id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Sucursales]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Users] FOREIGN KEY([EvaluatorUser_Id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Users]
GO
ALTER TABLE [dbo].[UsersGroups]  WITH CHECK ADD  CONSTRAINT [FK_UsersGroups_Groups] FOREIGN KEY([GroupId])
REFERENCES [dbo].[Groups] ([Id])
GO
ALTER TABLE [dbo].[UsersGroups] CHECK CONSTRAINT [FK_UsersGroups_Groups]
GO
ALTER TABLE [dbo].[UsersGroups]  WITH CHECK ADD  CONSTRAINT [FK_UsersGroups_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[UsersGroups] CHECK CONSTRAINT [FK_UsersGroups_Users]
GO
/****** Object:  StoredProcedure [dbo].[can_CanjeProducto]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Patricio Luzzi>
-- Create date: <04/09/2019>
-- Description:	<Registra operación de canje de productos>
-- =============================================
CREATE PROCEDURE [dbo].[can_CanjeProducto]
(
   @UserId int,
   @BenefitsId int
   
)
AS
BEGIN
	DECLARE 
	@alcanza int,
	@puntos int,
	@puntosUsados int,
	@productoCanjeadoId int,
	@tipoCatalogoProductoId int

	-- Validar que alcanzan los puntos
	select 
		@puntosUsados = Puntos,
		@tipoCatalogoProductoId = TipoCatalogoProductosId
	from Benefits
	where Id = @BenefitsId
	
	select 
		@puntos = (PuntosAcumulados - @puntosUsados)
	from TipoCatalogoProductosxUsuario
	where UserId = @UserId
	and TipoCatalogoProductoId = @tipoCatalogoProductoId
	
	if(@puntos<0)
		return

	-- Registrar producto canjeado
	insert into ProductosCanjeados(UserId, BenefitsId, FechaCanje, Puntos, PuntosUsados)
	values(@UserId, @BenefitsId, getdate(), @puntos, @puntosUsados)

	select @productoCanjeadoId = @@IDENTITY

	-- Descontar puntos
	update TipoCatalogoProductosxUsuario set
	PuntosAcumulados = (PuntosAcumulados - @puntosUsados)
	where UserId = @UserId
	and TipoCatalogoProductoId = @tipoCatalogoProductoId

	-- Mostar resultado
	select
	*
	from ProductosCanjeados
	where id = @productoCanjeadoId

END

GO
/****** Object:  StoredProcedure [dbo].[est_AudienciaPorEdades]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ======================================================================================================
-- Author     :	Santiago Nuin
-- Create date: 29/11/2018
-- Description:	Devuelve la cantidad de usuarios activos e inactivos agrupados por edades
-- Ejecucion  : exec est_AudienciaPorEdades 1, '2018-11-29'
-- =====================================================================================================

CREATE PROCEDURE [dbo].[est_AudienciaPorEdades]
(
   @EmpresaId int,
   @FechaActual date
)
AS
BEGIN
	-- Creacion Tabla temporal de trabajo
	declare	@tmp TABLE ( Activo       int, 
						 FechaNac     date,
						 Edad         int,
						 DecenaDesde  int,
						 DecenaHasta  int,
						 DecenaUnida  varchar(10)
						)

	-- Calculo de edad
	-- En este caso convertimos la fecha al formato clásico de base de datos como una cadena que 
	-- aglutina pegados Fecha, mes y día y lo convertimos a un entero (hoy nos quedaría por ejemplo 20151001), 
	-- se lo restamos a la fecha de nacimiento, dividimos entre diez mil para obtener el año y redondeamos por 
	-- defecto con la función floor()
	insert into @tmp
	Select Active,
	       BirthDate,
	       floor((cast(convert(varchar(8),getdate(),112) as int)-cast(convert(varchar(8),BirthDate,112) as int) ) / 10000), 
		   0,
		   0,
		  ''
	from users
	where EmpresaId = @EmpresaId


	-- Con la edad Actualizo las decenas para luego poder agrupar por las mimas
	update @tmp
	set DecenaDesde = floor(Edad/10) * 10,
		DecenaHasta = (floor(Edad/10) * 10) + 9,
		DecenaUnida = convert(varchar(3), floor(Edad/10) * 10) + ' - ' + convert(varchar(3),(floor(Edad/10) * 10) + 9)

	-- Con la edad Actualizo las decenas para luego poder agrupar por las mimas


	-- Salida
	Select Activo, 
	       DecenaDesde, 
		   DecenaHasta, 
		   count(1) as 'cantidad', 
		   DecenaUnida	   
	from @tmp
	group by Activo, DecenaDesde, DecenaHasta, DecenaUnida 
	order by DecenaDesde


	Return 0

END

GO
/****** Object:  StoredProcedure [dbo].[est_AudienciaPorGenero]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =========================================================================================================================================
-- <Detalle  Version='1' Fecha='30/01/2019' Autor='Santiago Nuin' Detalle= 'Devuelve dividido por genro el porcentaje de usuarios activos'>
-- <Detalle  Version='2' Fecha='12/02/2019' Autor='Santiago Nuin' Detalle= 'Se arreglo para que tenga en cuenta la empresa id'>

-- Ejecucion  : exec est_AudienciaPorGenero 1
-- =========================================================================================================================================

CREATE procedure [dbo].[est_AudienciaPorGenero]
(
   @EmpresaId int
)
AS
begin

	declare @Total int

	-- Obtengo el total de usuarios activos y habilitados para poder calcular el porcentaje
	select @total = count (*) 
	from users
	where active  = 1
	and [Enable]  = 1
	and EmpresaId = @EmpresaId

	-- Devuelvo los porcentajes de cada genero
	select us.GeneroCatalogoId, 
		   ca.DescripcionLarga,
		   count(*) as 'cantidad', 
		   convert(dec(10,2), (count(*) * 100.00 / @total)) as 'porcentaje'
	FROM users us
		inner join Catalogos ca
			on us.GeneroCatalogoId = ca.id
	where us.active  = 1
	and us.[Enable]  = 1
	and us.EmpresaId = @EmpresaId
	group by us.GeneroCatalogoId,ca.DescripcionLarga

	Return 0

end

GO
/****** Object:  StoredProcedure [dbo].[est_Canales_VisitasPorHora]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ======================================================================================================
-- Author:		Ariel Cabrejos
-- Create date: 19/11/2018
-- Description:	
-- Ejecucion  : exec est_Canales_VisitasPorHora 1,'2019-01-30','2019-02-01'
-- =====================================================================================================

CREATE PROCEDURE [dbo].[est_Canales_VisitasPorHora]
(
   @EmpresaId int,
   @FechaDesde date, --Se debe enviar la fecha para no usar el GetDate ya que el horario del server SQL es diferente al WebServer
   @FechaHasta date  --Se debe enviar la fecha para no usar el GetDate ya que el horario del server SQL es diferente al WebServer
)
AS
BEGIN

	DECLARE @startnum INT=0
	DECLARE @endnum   INT=23
	;
	WITH gen AS (
		SELECT @startnum AS num
		UNION ALL
		SELECT num + 1 FROM gen WHERE num + 1 <= @endnum
	)
	SELECT num as Hora, c.Nombre, count(l.id) as Cantidad 
	FROM gen
		LEFT OUTER JOIN dbo.Canales c ON c.Id = c.id AND c.Id <> 3
		LEFT JOIN [LogActividades] l 
			ON datepart(hh,l.FechaActividad) = num
				AND l.EmpresaId = @EmpresaId
				AND c.Id = l.CanalId
	GROUP BY num, c.Nombre
	Order by num, c.Nombre
	OPTION (maxrecursion 0)


	Return 0

END

GO
/****** Object:  StoredProcedure [dbo].[est_Comportamiento_ComparacionReacciones]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
============================================================================================================================================
<Versionado> 

<Descripcion>  
Devuelve los datos para el grafico comparacion de reacciones de la seccion comportamiento, en el admin 
</Descripcion>

<Ejecucion>
EXEC est_Comportamiento_ComparacionReacciones 1,'2019-04-07','2019-04-09'
</Ejecucion>

<Historia>
<Detalle  Version='1' Fecha='08/04/2098' Autor='Santiago Nuin'>Version inicial</Detalle>
</Historia>

</Versionado>
============================================================================================================================================
*/

CREATE procedure [dbo].[est_Comportamiento_ComparacionReacciones]
(
   @EmpresaId int,
   @FechaDesde date, --Se debe enviar la fecha para no usar el GetDate ya que el horario del server SQL es diferente al WebServer
   @FechaHasta date  --Se debe enviar la fecha para no usar el GetDate ya que el horario del server SQL es diferente al WebServer
)
AS
begin
SET FMTONLY OFF
	declare @Total int

	-- Obtengo cada reaccion (solo la ultima que marco cada usuario)
	select id, Empresaid, Entidadid, Userid, ModuloId, ReaccionId, FechaAlta 
	into #tmp
	from [dbo].[ReaccionesUsers] ru
	where FechaAlta = (select max(reus.FechaAlta) from reaccionesUsers reus where reus.Entidadid = ru.Entidadid and reus.moduloid = ru.moduloid and reus.userid=ru.userid)
	and Empresaid = @EmpresaId
	and convert(date,ru.FechaAlta) BETWEEN @FechaDesde AND @FechaHasta

	-- Obtengo cantidad total de reacciones
	select @total = count (*) from #tmp

	select rea.id, 
	       rea.Nombre,
		   rea.icono,
		   count(tmp.ReaccionId) as 'cantidad',
		   convert(dec(10,2), (count(tmp.ReaccionId) * 100.00 / @total)) as 'porcentaje'
	from Reacciones rea
		left join  #tmp tmp
			on rea.id = tmp.ReaccionId 
	group by rea.id,rea.Nombre,rea.icono

	Return 0

end

GO
/****** Object:  StoredProcedure [dbo].[est_Comportamiento_ContenidoMasReacciones]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
============================================================================================================================================
<Versionado> 

<Descripcion>  
Devuelve los datos para el grafico contenido con mas reacciones, de la seccion comportamiento, en el admin 
Si no se pasa @NemonicoModulo : Se mostraran los 5 contenidos (noticias, beneficios, etc) con mayor cantidad de reacciones
Si se pasa @NemonicoModulo    : Se mostrara los 5 contenidos con mas reaccion correspondientes al contenido seleccionado a traves de su 
                                nemonico
</Descripcion>

<Ejecucion>
EXEC est_Comportamiento_ContenidoMasReacciones 1,'2019-04-09','2019-05-10',''
EXEC est_Comportamiento_ContenidoMasReacciones 1,'2019-04-07','2019-04-09', 'NOTIC'
</Ejecucion>

<Historia>
<Detalle  Version='1' Fecha='09/04/2098' Autor='Santiago Nuin'>Version inicial</Detalle>
<Detalle  Version='2' Fecha='10/05/2098' Autor='Cristaldo Tamara'>Se agrega Id para agrupar por Id y  título </Detalle>
</Historia>

</Versionado>
============================================================================================================================================
*/

CREATE PROCEDURE [dbo].[est_Comportamiento_ContenidoMasReacciones]
(
   @EmpresaId       int,   
   @FechaDesde      date,           -- Se debe enviar la fecha para no usar el GetDate ya que el horario del server SQL es diferente al WebServer
   @FechaHasta      date,           -- Se debe enviar la fecha para no usar el GetDate ya que el horario del server SQL es diferente al WebServer
   @NemonicoModulo  varchar(5) = '' -- '' = Todos los modulos /  'nemonico'= Modulo correspondiente
)
AS
BEGIN
SET FMTONLY OFF
declare @Moduloid int


CREATE TABLE #temp  
(  
        Ranking       INT,
		Id			  INT,
		Titulo        varchar(150),  
        MeEncanta     INT,
		MeGusta       INT,
		MeSorprende   INT,
		NoMeGusta     INT,
		MeEnfurece    INT,
		CantidadTotal INT
		  
);  


--------------------------------------------------------------------------------------------------------------------
-- Noticias
--------------------------------------------------------------------------------------------------------------------

	if @NemonicoModulo = '' or @NemonicoModulo = 'NOTIC' 
	begin

		select @Moduloid = id from Modulos where Nemonico='NOTIC'

		insert into #temp 
		select 
				ROW_NUMBER() OVER(ORDER BY Count(1) DESC),  --'Ranking',
				na.Id,
				na.Titulo, 
				SUM(CASE WHEN re.id = 1 THEN 1 ELSE 0 END), --'Me encanta',
				SUM(CASE WHEN re.id = 2 THEN 1 ELSE 0 END), --'Me gusta',
				SUM(CASE WHEN re.id = 3 THEN 1 ELSE 0 END), --'Me sorprende',
				SUM(CASE WHEN re.id = 4 THEN 1 ELSE 0 END), --'No me gusta',
				SUM(CASE WHEN re.id = 5 THEN 1 ELSE 0 END), --'Me enfurece',
				Count(1)                                    --CantidadTotal
		from [dbo].[ReaccionesUsers] ru
			inner join Noticias na
				on ru.entidadid = na.id
			inner join Reacciones re
				on ru.reaccionid = re.id
		where ru.FechaAlta = (select max(reus.FechaAlta) from reaccionesUsers reus where reus.Entidadid = ru.Entidadid and reus.moduloid = ru.moduloid and reus.userid=ru.userid)
		and   ru.Empresaid = @EmpresaId
		and   ru.Moduloid  = @Moduloid 
		and convert(date,ru.FechaAlta) BETWEEN @FechaDesde AND @FechaHasta
		group by na.Id, na.Titulo
		ORDER BY Count(1) Desc
	end

--------------------------------------------------------------------------------------------------------------------
-- Beneficios
--------------------------------------------------------------------------------------------------------------------

	if @NemonicoModulo = '' or @NemonicoModulo = 'BENEF' 
	begin

		select @Moduloid = id from Modulos where Nemonico='BENEF'

		insert into #temp 
		select 
				ROW_NUMBER() OVER(ORDER BY Count(1) DESC),  --'Ranking',
				be.Id, be.title, 
				SUM(CASE WHEN re.id = 1 THEN 1 ELSE 0 END), --'Me encanta',
				SUM(CASE WHEN re.id = 2 THEN 1 ELSE 0 END), --'Me gusta',
				SUM(CASE WHEN re.id = 3 THEN 1 ELSE 0 END), --'Me sorprende',
				SUM(CASE WHEN re.id = 4 THEN 1 ELSE 0 END), --'No me gusta',
				SUM(CASE WHEN re.id = 5 THEN 1 ELSE 0 END), --'Me enfurece',
				Count(1)                                    --CantidadTotal
		from [dbo].[ReaccionesUsers] ru
			inner join Benefits be
				on ru.entidadid = be.id
			inner join Reacciones re
				on ru.reaccionid = re.id
		where ru.FechaAlta = (select max(reus.FechaAlta) from reaccionesUsers reus where reus.Entidadid = ru.Entidadid and reus.moduloid = ru.moduloid and reus.userid=ru.userid)
		and   ru.Empresaid = @EmpresaId
		and   ru.Moduloid  = @Moduloid
		and   convert(date,ru.FechaAlta) BETWEEN @FechaDesde AND @FechaHasta
		group by be.Id, be.title
		ORDER BY Count(1) Desc

	end

--------------------------------------------------------------------------------------------------------------------
-- Top Five resultado final
--------------------------------------------------------------------------------------------------------------------

	select top 5 *
	from #temp 
	order by CantidadTotal Desc

	drop table #temp

	Return 0
END

GO
/****** Object:  StoredProcedure [dbo].[est_ContenidosMayorRepercusion]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
=========================================================================================================================
<Versionado> 

<Descripcion>  
Devuelve los contenidos mas visitados en una fecha dada, divididos por canal
</Descripcion>

<Ejecucion>
EXEC est_ContenidosMayorRepercusion 1,'2019-03-15','2019-04-25'
</Ejecucion>

<Historia>
<Detalle  Version='1' Fecha='14/02/2019' Autor='Santiago Nuin'>Version inicial</Detalle>
<Detalle  Version='2' Fecha='25/04/2019' Autor='Santiago Nuin'>se modifico para agregarle a las fechas un horario para solucionar el problema que no tenia en cuuenta el dia actual</Detalle>
</Historia>

</Versionado>
=========================================================================================================================
*/

CREATE PROCEDURE [dbo].[est_ContenidosMayorRepercusion]
(
   @EmpresaId  int,
   @FechaDesde date, --Se debe enviar la fecha para no usar el GetDate ya que el horario del server SQL es diferente al WebServer
   @FechaHasta date  --Se debe enviar la fecha para no usar el GetDate ya que el horario del server SQL es diferente al WebServer
)
AS
BEGIN
	
	-- Del log de actividades obtengo los registros que pertenecen a un contenido en particular, de una empresa en particular, 
	-- en un rango de fechas. los ordeno por cantidad de ocurrencias y muestro un detalle de los primeros 20

	Declare @FechaHoraDesde datetime,
			@FechaHoraHasta datetime

	set @FechaHoraDesde = convert(varchar(10), @FechaDesde) + ' 00:00:00.001'
	set @FechaHoraHasta = convert(varchar(10), @FechaHasta) + ' 23:59:59.607'


	select top (20)
		   ROW_NUMBER() OVER(ORDER BY Count(1) DESC) as 'Ranking',
		   la.moduloid,
		   vw.ModuloTitulo, 
		   vw.Titulo,
		   la.entidadid,
		   SUM(CASE WHEN LogTipoId = 2 and canalid = 1 THEN 1 ELSE 0 END) as 'CantidadVisitasApp',
		   SUM(CASE WHEN LogTipoId = 8 and canalid = 1 THEN 1 ELSE 0 END) as 'CantidadComentariosApp',
		   SUM(CASE WHEN LogTipoId = 9 and canalid = 1 THEN 1 ELSE 0 END) as 'CantidadSentimientosApp',
		   SUM(CASE WHEN canalid = 1 THEN 1 ELSE 0 END)                   as 'CantidadTotalApp',
		   SUM(CASE WHEN LogTipoId = 2 and canalid = 2 THEN 1 ELSE 0 END) as 'CantidadVisitasWeb',
		   SUM(CASE WHEN LogTipoId = 8 and canalid = 2 THEN 1 ELSE 0 END) as 'CantidadComentariosWeb',
		   SUM(CASE WHEN LogTipoId = 9 and canalid = 2 THEN 1 ELSE 0 END) as 'CantidadSentimientosWeb',
		   SUM(CASE WHEN canalid = 2 THEN 1 ELSE 0 END)                   as 'CantidadTotalWeb',
		   Count(1) as 'CantidadTotal' 
	from logactividades la
		inner join [vw_gra_DetalleContenidos] vw
			on la.entidadid = vw.entidadId
			and la.Moduloid = vw.ModuloId
	where la.fechaActividad between @FechaHoraDesde and @FechaHoraHasta
	and la.empresaid = @EmpresaId
	and la.entidadid is not null -- No necesito el log de los detalles sino de una entidad en particular
	and la.LogTipoId in (2,8,9)  -- 2) Visita Detalle - 8) Comentarios - 9) Sentimientos
	group by la.moduloid, vw.ModuloTitulo, vw.Titulo, la.entidadid 
	order by CantidadTotal desc

	Return 0

END

GO
/****** Object:  StoredProcedure [dbo].[est_Dashboard_ModulosMasRepercucion]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================================================================
-- Author     :	Santiago Nuin
-- Create date: 19/11/2018
-- Update author: Ornella Tallante
-- Last update date: 15/03/2019
-- Description:	
-- Ejecucion  : exec est_Dashboard_ModulosMasRepercucion 2,'2019-01-01','2019-02-20'
-- ======================================================================================================

CREATE PROCEDURE [dbo].[est_Dashboard_ModulosMasRepercucion]
(
   @EmpresaId int,
   @FechaDesde date, --Se debe enviar la fecha para no usar el GetDate ya que el horario del server SQL es diferente al WebServer
   @FechaHasta date  --Se debe enviar la fecha para no usar el GetDate ya que el horario del server SQL es diferente al WebServer
)
AS
BEGIN
	SELECT TOP 5 
		ROW_NUMBER() OVER(ORDER BY Count(1) DESC) as 'Ranking',
		Modulos.Nombre,
		SUM(CASE WHEN LogTipoId = 2 THEN 1 ELSE 0 END) as CantidadVisitas,
		SUM(CASE WHEN LogTipoId = 8 THEN 1 ELSE 0 END) as CantidadComentarios,
		SUM(CASE WHEN LogTipoId = 9 THEN 1 ELSE 0 END) as CantidadSentimientos,
		Count(1) as CantidadTotal
	FROM LogActividades as l
		INNER JOIN Modulos ON Modulos.Id = l.ModuloId
	WHERE l.EmpresaId = @EmpresaId 
		AND LogTipoId in (2, --Visita Detalle
						8, --Comentarios
						9) --Sentimientos
		AND convert(date,FechaActividad) BETWEEN @FechaDesde AND @FechaHasta
	GROUP BY l.ModuloId, Modulos.Nombre
	ORDER BY Count(1) Desc

	Return 0
END

GO
/****** Object:  StoredProcedure [dbo].[est_Dashboard_UsuariosMasParticipativos]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
============================================================================================================================================
<Versionado> 

<Descripcion>  
Devuelve un listado de los 10 usuarios con mas uso de la app y la web
</Descripcion>

<Ejecucion>
exec est_Dashboard_UsuariosMasParticipativos 1,'2019-01-01','2019-04-25'
</Ejecucion>

<Historia>
<Detalle  Version='1' Fecha='30/01/2019' Autor='Santiago Nuin'>Version inicial</Detalle>
<Detalle  Version='2' Fecha='18/02/2019' Autor='Santiago Nuin'>Se devuelve el nombre del usuario</Detalle>
<Detalle  Version='3' Fecha='22/04/2019' Autor='Tamara Cristaldo'>Se devuelve el nombre del usuario con el método Trim para quitar los espacios, más los usuarios activos</Detalle>
<Detalle  Version='4' Fecha='25/04/2019' Autor='Santiago Nuin'>se modifico para agregarle a las fechas un horario para solucionar el problema que no tenia en cuuenta el dia actual</Detalle>

</Historia>

</Versionado>
============================================================================================================================================
*/

CREATE PROCEDURE [dbo].[est_Dashboard_UsuariosMasParticipativos]
(
   @EmpresaId   int,
   @FechaDesde datetime,
   @FechaHasta datetime
)
AS
BEGIN

	-- Para que le lleguen los datos al .NET
	SET FMTONLY OFF

	Declare @FechaHoraDesde datetime,
			@FechaHoraHasta datetime

	set @FechaHoraDesde = @FechaDesde + ' 00:00:00.001'
	set @FechaHoraHasta = @FechaHasta + ' 23:59:59.607'


	CREATE TABLE #tmp 
	(  
		Ranking              int, 
	    UsuarioId            int,
		UsuarioNombre        Varchar(300),
		CantTotal            int,
		CantVisitas          int,
		CantComentarios      int,
		CantSentimientos     int,
		FechaUltimaActividad datetime,
		imagen               varchar(max) 
	)  


	-- Las 10 personan que mas uso tienen en la app y web
	insert into #tmp
	select top(10) 
		   ROW_NUMBER() OVER(ORDER BY Count(1) DESC), -- Posicion en el ranking
		   la.UserId,
		   us.[Name] + ' ' + us.Surname,
		   count(*),
		   SUM(CASE WHEN LogTipoId = 2 THEN 1 ELSE 0 END), -- Cantidad Visitas
		   SUM(CASE WHEN LogTipoId = 8 THEN 1 ELSE 0 END), -- Cantidad Comentarios
		   SUM(CASE WHEN LogTipoId = 9 THEN 1 ELSE 0 END), -- Cantidad Sentimientos,
		   '',
		   ''
	from [dbo].[LogActividades] la
		inner join Users us
			on la.UserId = us.id
	where la.EmpresaId = @EmpresaId 
	and   la.FechaActividad between @FechaHoraDesde and @FechaHoraHasta
	and	us.Enable = 1
	group by la.UserId,us.[Name],us.Surname
	ORDER BY Count(1) Desc



	-- Actualizo la tabla temporal con la imagen del usuario
	update #tmp 
	set imagen = (select us.image from users us where us.Id = UsuarioId)


    -- Actualizo la tabla temporal con la fecha de uso mas nueva del usuario
	update #tmp 
	set FechaUltimaActividad = (select max(la.FechaActividad) from LogActividades la where la.UserId = UsuarioId)


	-- Salida final del SP
	select * from #tmp


	-- Elimino tabla de trabajo
	drop table #tmp


	Return 0

END

GO
/****** Object:  StoredProcedure [dbo].[est_Dashboard_VisitasPorDia]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================================================================
-- Author     :	Ariel Cabrejos
-- Create date: 19/11/2018
-- Description:	
-- Ejecucion  : exec est_Dashboard_VisitasPorDia 1,'2019-01-01','2019-02-20'
-- ======================================================================================================

CREATE PROCEDURE [dbo].[est_Dashboard_VisitasPorDia]
(
   @EmpresaId int,
   @FechaDesde date, --Se debe enviar la fecha para no usar el GetDate ya que el horario del server SQL es diferente al WebServer
   @FechaHasta date  --Se debe enviar la fecha para no usar el GetDate ya que el horario del server SQL es diferente al WebServer
)
AS
BEGIN

	------------------------------------------------------------------------
	-- obtengo el rango de dias para despues poblarlo con datos
	------------------------------------------------------------------------
	
	WITH DateRange(DateData) AS 
	(
		SELECT @FechaDesde as Date
		UNION ALL
		SELECT DATEADD(d,1,DateData)
		FROM DateRange 
		WHERE DateData < @FechaHasta
	)

	SELECT	DateData as Fecha, 
			'Visitas' as nombre, 
			Count(l.Id) as Cantidad 
	FROM DateRange
		LEFT JOIN [LogActividades] l 
			ON CONVERT(date,l.FechaActividad) = CONVERT(date,DateData)
				AND l.EmpresaId = @EmpresaId

	GROUP BY DateData
	Order by DateData
	OPTION (MAXRECURSION 0)


	Return 0

END

GO
/****** Object:  StoredProcedure [dbo].[est_Dashboard_VisitasPorHora]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ======================================================================================================
-- Author:		Ariel Cabrejos
-- Create date: 19/11/2018
-- Description:	
-- Ejecucion  : exec est_Dashboard_VisitasPorHora 1,'2019-04-20','2019-05-21'
-- =====================================================================================================

CREATE PROCEDURE [dbo].[est_Dashboard_VisitasPorHora]
(
   @EmpresaId int,
   @FechaDesde date, --Se debe enviar la fecha para no usar el GetDate ya que el horario del server SQL es diferente al WebServer
   @FechaHasta date  --Se debe enviar la fecha para no usar el GetDate ya que el horario del server SQL es diferente al WebServer
)
AS
BEGIN

	DECLARE @startnum INT=0
	DECLARE @endnum INT=23
	;
	WITH gen AS 
	(
		SELECT @startnum AS num
		UNION ALL
		SELECT num + 1 FROM gen WHERE num + 1 <= @endnum
	)

	SELECT num as 'Hora', 
			'Visitas' as 'Nombre', 
			count(l.id) as 'Cantidad' 
	FROM gen
		LEFT JOIN [LogActividades] l 
			ON datepart(hh,l.FechaActividad) = num
				AND l.EmpresaId = @EmpresaId	
				and l.fechaActividad between @FechaDesde and @FechaHasta	
	GROUP BY num
	Order by num
	OPTION (maxrecursion 0)
	
	Return 0

END

GO
/****** Object:  StoredProcedure [dbo].[est_DistribuciónPorArea]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[est_DistribuciónPorArea]
(
   @EmpresaId int
)
AS
BEGIN

	select us.area_id, 
	       ar.Nombre, 
		   isnull(count(us.area_id),0) as 'cantidad', 
		   us.Active
	from users us
	full OUTER JOIN Areas ar
		on us.area_Id = ar.Id
	where us.Empresaid = @EmpresaId
	group by us.area_id, ar.Nombre, us.Active
	order by us.Active, us.area_id




/*

	EXEC est_DistribuciónPorArea 1

SELECT 
	tArticulo.art_Clave AS 'SKU', 
	tArticulo.art_Nombre AS 'NOMBRE', 
	art_Existencia AS 'EXISTENCIAS', 
	ISNULL(SUM(vdet_Cantidad), 0) As 'TOTAL DE VENTAS' 
FROM 
	tArticulo 
	LEFT JOIN tVentaDetalle ON tArticulo.art_Clave = tVentaDetalle.art_Clave 
		AND (vdet_Fecha BETWEEN @desde and @hasta) 
WHERE 
	tArticulo.art_Clave IN " & archivo & " 	
GROUP BY 
	tArticulo.art_Clave, 
	tArticulo.art_Nombre, 
	art_Existencia

*/






	Return 0

END

/* */

/*
<Ejecucion>

	EXEC est_DistribuciónPorArea 1
	select * from Users where Area_Id = 5
	select * from Areas
	 where EmpresaId = 1
	 and Habilitada = 1

</Ejecucion>
*/

GO
/****** Object:  StoredProcedure [dbo].[est_VisitasPorDia]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================================================================
-- Author     :	Ariel Cabrejos
-- Create date: 19/11/2018
-- Description:	
-- Ejecucion  : exec est_Canales_VisitasPorDia 1,'2019-01-01','2019-01-30'
-- ======================================================================================================
--
CREATE PROCEDURE [dbo].[est_VisitasPorDia]
(
   @EmpresaId int,
   @FechaDesde date, --Se debe enviar la fecha para no usar el GetDate ya que el horario del server SQL es diferente al WebServer
   @FechaHasta date  --Se debe enviar la fecha para no usar el GetDate ya que el horario del server SQL es diferente al WebServer
)
AS
BEGIN

	WITH DateRange(DateData) AS 
	(
		SELECT @FechaDesde as Date
		UNION ALL
		SELECT DATEADD(d,1,DateData)
		FROM DateRange 
		WHERE DateData < @FechaHasta
	)

	SELECT DateData as Fecha, c.Nombre, Count(l.Id) as Cantidad 
	FROM DateRange
		LEFT OUTER JOIN dbo.Canales c ON c.Id = c.id AND c.Id <> 3
		LEFT JOIN [LogActividades] l 
			ON CONVERT(date,l.FechaActividad) = CONVERT(date,DateData)
				AND l.EmpresaId = @EmpresaId
				AND c.Id = l.CanalId
	GROUP BY DateData, c.Nombre
	Order by DateData, c.Nombre
	OPTION (MAXRECURSION 0)

	Return 0

END

GO
/****** Object:  StoredProcedure [dbo].[log_GetFactorValidacion]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================================================================
-- Author:		Santiago Nuin
-- Create date: 19/10/2018
-- Description:	
-- EXEC Log_GetFactorValidacion 1,144
-- =====================================================================================================
CREATE PROCEDURE [dbo].[log_GetFactorValidacion]

	@EmpresaId int,
	@UserId    int 
	
AS
BEGIN

	declare @TipoValidacionId   int,
		    @NemonicoValidacion varchar(5),
			@DescripcionFactor  Varchar(300), 
			@idFijo1            int,
			@IdFijo2            int,
			@MaxId              int,
			@MinId              int

	declare	@tmp TABLE (id int, Dni varchar(20), FechaNac datetime, Legajo varchar(20), RegistroOk bit)

	select @TipoValidacionId = TipoValidacionId from Empresas where id= @empresaid

	select @NemonicoValidacion = Nemonico, 
	       @DescripcionFactor  = Descripcion
	from TipoValidacion 
	where id = @TipoValidacionId
	
	-- Datos del usuario Correcto
	insert into @tmp
	select id, Dni, BirthDate, FileNumber, 1 
	from users 
	where id = @UserId
	and EmpresaId = @EmpresaId

	-- Si No encontró al usuario correcto devuelvo error
	if @@ROWCOUNT = 0
	begin
		Return 1
	End
	
	-----------------------------------------------------------------------------------------------------
	-- Dentro de los numeros que devuelvo hay 2 que se deberan repetir siempre por un tema de seguridad
	-- para eso obtengo el id anterior y posterior al empleado. excepto cuando el empleado sea el primer
	-- id o el ultimo
	-----------------------------------------------------------------------------------------------------
	select @MaxId = max(id) from users
	select @MinId = 1

	-- Para todos los id menos para el primer y ultimo
	set @idFijo1 = @UserId - 1      
	set @idFijo2 = @UserId + 1

	-- Si es el id 1
	if @UserId = @MinId
	begin 
		set @idFijo1 = @UserId + 1      
		set @idFijo2 = @UserId + 2      
	end

	-- Si es el ultimo id
	if @UserId = @MaxId
	begin 
		set @idFijo1 = @UserId - 1      
		set @idFijo2 = @UserId - 2      
	end

	-- Obtengo los Id fijos
	insert into @tmp
	select id, Dni, BirthDate, FileNumber, 0 
	from users 
	where id = @idFijo1
		
	insert into @tmp
	select id, Dni, BirthDate, FileNumber, 0 
	from users 
	where id = @IdFijo2

	-- Obtengo datos random de toda la tabla
	insert into @tmp
	select top 4 id, Dni, BirthDate, FileNumber, 0  
	from users
	order by NEWID()

	-----------------------------------------------------------------------------------------------------
	-- Devuelvo los datos
	-----------------------------------------------------------------------------------------------------
	select   id, 
			RegistroOk , 
			Factor =  case @NemonicoValidacion  
						 when 'LEG' then Legajo 
						 when 'NAC' then convert(varchar,FechaNac)  
						 when 'DOC' then Dni  
						 else NULL 
					  end,
			Descripcion = @DescripcionFactor 
	from @tmp  
	order by NEWID() 



	Return 0

END

GO
/****** Object:  StoredProcedure [dbo].[mur_GetDatosMuro]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
============================================================================================================================================
<Versionado> 

<Descripcion>  
Devuelve los datos para mostrar en el muro
</Descripcion>

<Ejecucion>
EXEC mur_GetDatosMuro 2,1,0,30,'2019-04-26 00:45:20.390'
</Ejecucion>

<Historia>
<Detalle  Version='1' Fecha='26/10/2018' Autor='Santiago Nuin'>Version inicial</Detalle>
<Detalle  Version='2' Fecha='12/02/2019' Autor='Santiago Nuin'>Se agrego cumpleaños</Detalle>
<Detalle  Version='3' Fecha='14/02/2019' Autor='Santiago Nuin'>Se agrego infoutil</Detalle>
<Detalle  Version='4' Fecha='22/02/2019' Autor='Santiago Nuin'>Se cambio el nemonico en infoutil 'INFUT'</Detalle>
<Detalle  Version='5' Fecha='27/02/2019' Autor='Javier Guerrero'>Se agrega campo 'Copete' a Noticias</Detalle>
<Detalle  Version='6' Fecha='07/03/2019' Autor='Santiago Nuin' >se agrega a la consulta final que traiga sentimientos</Detalle>
<Detalle  Version='7' Fecha='02/04/2019' Autor='Ornella Tallante'>Se agrega columna 'ModuloId' a tabla @tmp de resultado.</Detalle>
<Detalle  Version='8' Fecha='03/04/2019' Autor='Javier Guerrero'>Se modifica nombres de tablas sentimientos a reacciones</Detalle>
<Detalle  Version='9' Fecha='05/04/2019' Autor='Javier Guerrero'>Se agrega campo 'TotalReacciones'</Detalle>
<Detalle  Version='10' Fecha='13/05/2019' Autor='Javier Guerrero'>Se agrega galerias</Detalle>
<Detalle  Version='11' Fecha='30/05/2019' Autor='Javier Guerrero'>Se agrega filtro 'FechaDesde' y 'FechaHasta' en galerías</Detalle>
</Historia>

</Versionado>
============================================================================================================================================
*/


CREATE PROCEDURE [dbo].[mur_GetDatosMuro]
(
   @EmpresaId int,
   @Paginacion bit,
   @SiguienteId int,
   @UserId int,
   @FechaActual datetime
)
AS
BEGIN


	declare	@tmp TABLE ( id           int,
						 ModuloId     int, 
	                     TipoRegistro Varchar(5),
						 DescRegistro Varchar(50),
						 Titulo       varchar(max),
						 Copete       varchar(max), 
						 Contenido    varchar(max), 
						 Imagen       varchar(max), 
						 FechaAlta    datetime,
						 Destacada    bit
						)


	------------------------------------------------------------------------
	-- Busco Noticias
	-- select * from news
	------------------------------------------------------------------------
	insert into @tmp
    select top (7) 
	id, 
	moduloId = (select id from modulos where Nemonico = 'NOTIC'),
	'NOTIC', 
	'NOTICIAS', 
	Titulo, 
	Copete, 
	Contenido, 
	[Image], 
	[FechaAlta], 
	Destacada
	from Noticias
	where Habilitada = 1
	and EmpresaId = @EmpresaId
	and [FechaAlta] <= @FechaActual
	order by [FechaAlta] desc

	
	------------------------------------------------------------------------
	-- Busco Documentos
	-- select * from [dbo].[documents]
	------------------------------------------------------------------------
	/*
	insert into @tmp
    select top (2) 
	id, 
	moduloId = (select id from modulos where Nemonico = 'DOCUM'),
	'DOCUM', 
	'DOCUMENTOS', 
	Title, 
	Description, 
	'', 
	'', 
	CreatedDate, 
	0 
	from documents
	where [Enabled] = 1
	and EmpresaId   = @EmpresaId
	order by CreatedDate DESC
	*/
	
	------------------------------------------------------------------------
	-- Busco Capacitaciones
	-- ultimmo mas uno aleatorio
	-- select * from [dbo].[Courses]
	------------------------------------------------------------------------
	/*
	insert into @tmp
    select top (3) 
	id, 
	moduloId = (select id from modulos where Nemonico = 'CAPAC'),
	'CAPAC',
	'CAPACITACIONES', 
	title, 
	Description, 
	'', 
	'', 
	[From], 
	0 
	from Courses
	where [Enabled] = 1
	and EmpresaId = @EmpresaId
	and @FechaActual between [From] and [To] 
	order by [From] DESC
	*/

	-----------------------------------------------------------------------
	-- Busco Encuestas
	------------------------------------------------------------------------
	/*
	insert into @tmp
    select top (2) 
	id, 
	moduloId = (select id from modulos where Nemonico = 'ENCUE'),
	'ENCUE',
	'ENCUESTA', 
	Title, 
	[Description],
	'', 
	'', 
	DateFrom, 
	0
	from [Forms]
	where[Enabled] = 1
	and EmpresaId  = @EmpresaId
	and FormTypeId = 1
	and @FechaActual between [DateFrom] and [DateTo] 
	order by [DateFrom] DESC

	--select * from [dbo].[Forms]
	--select * from [dbo].[FormsTypes]
	*/
	------------------------------------------------------------------------
	-- Busco Beneficios
	-- select * from Benefits
	------------------------------------------------------------------------
	/*
	insert into @tmp
    select top (4) 
	id, 
	moduloId = (select id from modulos where Nemonico = 'BENEF'),
	'BENEF',
	'BENEFICIOS', 
	Title, 
	Subtitle, 
	'', 
	[Image], 
	[From], 
	0 
	from Benefits
	where [Enabled] = 1
	and EmpresaId   = @EmpresaId
	and @FechaActual between [From] and [To] 
	order by [From] DESC
	*/

	------------------------------------------------------------------------
	-- Busco Paginas de informacion
	------------------------------------------------------------------------
	/*
	insert into @tmp
	select top(3) 
	id, 
	moduloId = (select id from modulos where Nemonico = 'INFUT'),
	'INFUT', 
	'INFO UTIL', 
	Titulo, 
	copete, 
	'', 
	'', 
	FechaAlta,
	0
	from infoutil
	where Habilitado = 1
	and EmpresaId   = @EmpresaId
	order by FechaAlta DESC
	*/
	------------------------------------------------------------------------
	-- Busco Cumpleaños
	------------------------------------------------------------------------
	/*
	insert into @tmp
    select  us.id, 
	        moduloId = (select id from modulos where Nemonico = 'CUMPL'),
	        'CUMPL',
			'CUMPLEAÑOS', 
			[dbo].[fn_CapitalizeFirstLetter](us.[Name] + ' ' + us.Surname),
			us.email, 
			ar.[Nombre], 
			us.[Image], 
			us.BirthDate, 0 
	from users us
		inner join Areas ar
			on ar.id = Area_id 
	where us.[Enable]         = 1
	and   us.EmpresaId        = @EmpresaId
	and   us.Active           = 1
	and   MONTH(us.BirthDate) = MONTH(@FechaActual)
	and   DAY(us.BirthDate)   = DAY(@FechaActual)
	*/

	------------------------------------------------------------------------
	-- Busco Nuevos usuarios
	------------------------------------------------------------------------
	/*
	--VER QUE MOSTRAMOS

	insert into @tmp
    select  id, 'QUIEN','QUIEN ES QUIEN', Name, Surname, '', Image, RegistrationDate, 0 
	from users
	where empresaid = @EmpresaId
	and [Enabled]   = 1
	and Active      = 1
	and getdate()   = RegistrationDate 
	*/
	

	------------------------------------------------------------------------
	-- Busco Galerías
	-- select * from [dbo].[Galerias]
	------------------------------------------------------------------------

	insert into @tmp
    select top (3) 
	id, 
	moduloId = (select id from modulos where Nemonico = 'GALER'),
	'GALER', 
	'GALERIAS', 
	Titulo, 
	Copete, 
	'', 
	ImagenPortada, 
	FechaDesde, 
	0 
	from Galerias
	where [Habilitada] = 1
	and EmpresaId   = @EmpresaId
	and @FechaActual between [FechaDesde] and [FechaHasta] 
	order by FechaAlta DESC


    ------------------------------------------------------------------------
	-- Devuelvo la informacion
	-- Ademas agrego a cada registro y para el usuario que esta conectado, 
	-- obtengo el ultimo sentimiento que haya cargado a cada entidad 
	------------------------------------------------------------------------
	select tm.id,
		   tm.ModuloId,
	       tm.TipoRegistro,
		   tm.DescRegistro, 
		   tm.Titulo,
		   tm.Copete, 
		   tm.Contenido,
		   tm.Imagen,
		   tm.FechaAlta,
		   tm.Destacada,
		   isnull(su.ReaccionId,0) as 'ReaccionId',
		   (select count(distinct UserId) from ReaccionesUsers where ModuloId=tm.ModuloId and EntidadId=tm.id) as 'TotalReacciones'
	From @tmp tm
		left join reaccionesUsers su
			on  su.Entidadid = tm.id
			and su.moduloid  = tm.ModuloId
			and su.userid    = @UserId
			and su.FechaAlta = (select max(seus.FechaAlta) from reaccionesUsers seus where seus.Entidadid = su.Entidadid and seus.moduloid = su.moduloid and seus.userid=su.userid)
			
	order by tm.destacada desc, tm.FechaAlta desc

	Return 0

END

GO
/****** Object:  StoredProcedure [dbo].[seg_GetFiltros]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================
-- Author     :			Santiago Nuin
-- Create date:			07/02/2019
-- Last update author:	Ornella Tallante
-- Last update date:	15/03/2019
-- Description:			Devuelve los filtros que se utilizaran para la segmentacion de los distintos módulos
-- Ejecucion  :			exec seg_GetFiltros 1
-- =====================================================================================================


CREATE PROCEDURE [dbo].[seg_GetFiltros]
(
   @EmpresaId int
)
AS
BEGIN

	SET FMTONLY OFF

	Declare	@tmpFiltros TABLE ( FiltroTipoId          int, 
						        FiltroTipoDescripcion varchar(50),
		                        FiltroTipoNemonico    varchar(3),
		                        FiltroId			  varchar(10),
		                        FiltroDescripcion     varchar(100)
						      )

	Declare
		@tmp_FiltroTipoId          int, 
	    @tmp_FiltroTipoDescripcion varchar(50),
		@tmp_FiltroTipoNemonico    varchar(3),
		@ResultadoCatalogo         int



	----------------------------------------------------------------------------
	-- Filtros Areas
	----------------------------------------------------------------------------
	select @tmp_FiltroTipoNemonico    = 'ARE'
	select @tmp_FiltroTipoId          = id          from filtros where Nemonico = @tmp_FiltroTipoNemonico
	select @tmp_FiltroTipoDescripcion = Descripcion from filtros where Nemonico = @tmp_FiltroTipoNemonico

	insert into @tmpFiltros 
    select @tmp_FiltroTipoId, @tmp_FiltroTipoDescripcion, @tmp_FiltroTipoNemonico, id, Nombre
	from [dbo].[Areas]
	where Empresaid = @EmpresaId
	and Habilitada  = 1

	----------------------------------------------------------------------------
	-- Filtros Grupos
	----------------------------------------------------------------------------
	select @tmp_FiltroTipoNemonico    = 'GRU'
	select @tmp_FiltroTipoId          = id          from filtros where Nemonico = @tmp_FiltroTipoNemonico
	select @tmp_FiltroTipoDescripcion = Descripcion from filtros where Nemonico = @tmp_FiltroTipoNemonico

	insert into @tmpFiltros
    select @tmp_FiltroTipoId, @tmp_FiltroTipoDescripcion, @tmp_FiltroTipoNemonico, id, [Name]
	from [dbo].[Groups]
	where Empresaid = @EmpresaId
	and [Enabled]   = 1
	

	----------------------------------------------------------------------------
	-- Filtros Región
	----------------------------------------------------------------------------
	select @tmp_FiltroTipoNemonico    = 'REG'
	select @tmp_FiltroTipoId          = id          from filtros where Nemonico = @tmp_FiltroTipoNemonico
	select @tmp_FiltroTipoDescripcion = Descripcion from filtros where Nemonico = @tmp_FiltroTipoNemonico

	insert into @tmpFiltros
    select @tmp_FiltroTipoId, @tmp_FiltroTipoDescripcion, @tmp_FiltroTipoNemonico, id, [Descripcion]
	from [dbo].[Regiones]
	where Empresaid = @EmpresaId
	and [Habilitada]   = 1


	----------------------------------------------------------------------------
	-- Filtros Sucursal
	----------------------------------------------------------------------------
	select @tmp_FiltroTipoNemonico    = 'SUC'
	select @tmp_FiltroTipoId          = id          from filtros where Nemonico = @tmp_FiltroTipoNemonico
	select @tmp_FiltroTipoDescripcion = Descripcion from filtros where Nemonico = @tmp_FiltroTipoNemonico

	insert into @tmpFiltros
    select @tmp_FiltroTipoId, @tmp_FiltroTipoDescripcion, @tmp_FiltroTipoNemonico, id, [Descripcion]
	from [dbo].[Sucursales]
	where Empresaid = @EmpresaId
	and [Habilitada]   = 1

	----------------------------------------------------------------------------
	-- Filtros Genero
	----------------------------------------------------------------------------
	select @tmp_FiltroTipoNemonico    = 'GEN'
	select @tmp_FiltroTipoId          = id          from filtros where Nemonico = @tmp_FiltroTipoNemonico
	select @tmp_FiltroTipoDescripcion = Descripcion from filtros where Nemonico = @tmp_FiltroTipoNemonico


	-- Primero busco si tiene un catalogo propio la empresa
	insert into @tmpFiltros
    Select @tmp_FiltroTipoId, @tmp_FiltroTipoDescripcion, @tmp_FiltroTipoNemonico, ca.id, ca.DescripcionLarga--ca.DescripcionCorta
	from Catalogos ca
		inner join catalogostipo ct
			on ca.CatalogoTipoid = ct.id
			and ct.Nemonico='GENE'
			and ct.EmpresaId = @EmpresaId
			and ct.habilitado = 1
	where ca.habilitado = 1


	-- Si no tiene catalogo Propio busco el catalogo general
	set @ResultadoCatalogo = @@rowcount

	if @ResultadoCatalogo = 0
	begin

		insert into @tmpFiltros
		select @tmp_FiltroTipoId, @tmp_FiltroTipoDescripcion, @tmp_FiltroTipoNemonico, ca.id, ca.DescripcionLarga--ca.DescripcionCorta
		from Catalogos ca
			inner join catalogostipo ct
				on ca.CatalogoTipoid = ct.id
				and ct.Nemonico='GENE'
				and ct.EmpresaId is null
				and ct.habilitado = 1
		where ca.habilitado = 1
	end


	----------------------------------------------------------------------------
	-- Devuelvo Resultados
	----------------------------------------------------------------------------

	select  FiltroTipoId,         
			FiltroTipoDescripcion, 
			FiltroTipoNemonico,    
			FiltroId,			 
			FiltroDescripcion   
	from @tmpFiltros


	Return 0

END

GO
/****** Object:  StoredProcedure [dbo].[seg_GetSegmentacion]    Script Date: 16/9/2019 10:15:33 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================================================
-- Author     :			Santiago Nuin
-- Create date:			07/02/2019
-- Last update Author:	Ornella Tallante
-- Last update date:	15/03/2019
-- Description:			Devuelve los filtros que se utilizaron para la segmentar un modulo en particular
-- Ejecucion  :			exec seg_GetSegmentacion 1,20,5
-- =====================================================================================================


CREATE PROCEDURE [dbo].[seg_GetSegmentacion]
(
   @EmpresaId       int,
   @Entidad			int,
   @Modulo          int
)
AS
BEGIN

	-- Para que le lleguen los datos al .NET
	SET FMTONLY OFF


	----------------------------------------------------------------------------
	-- Datos generales de la segmentacion
	----------------------------------------------------------------------------
	select se.id, 
	       se.filtroId, 
		   fi.Descripcion, 
		   fi.nemonico, 
		   se.FiltroValor,
		   FiltroDescripcion = case fi.nemonico
                                  when 'GRU' then (select [Name]             from groups    where id = se.FiltroValor)
								  when 'ARE' then (select [Nombre]           from Areas     where id = se.FiltroValor)
								  when 'GEN' then (select [DescripcionLarga] from catalogos where id = se.FiltroValor)
								  when 'REG' then (select [Descripcion]      from Regiones  where id = se.FiltroValor)
								  when 'SUC' then (select [Descripcion]		 from Sucursales where id = se.FiltroValor)	
							   end

	from segmentacion se
		inner join filtros fi
			on se.filtroid = fi.id
	where se.EmpresaId = @EmpresaId
	and   se.ModuloId  = @Modulo
	and   se.EntidadId = @Entidad


END

GO
