import { BrowserModule } from '@angular/platform-browser';
import { MyApp } from './app.component';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { NgxErrorsModule } from '@ultimate/ngxerrors';
import { HttpClientModule } from '@angular/common/http';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';
import { FileOpener } from '@ionic-native/file-opener';
import { Device } from '@ionic-native/device';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

//custom modules
import { InterceptorModule } from './core/interceptor.module';
import { ErrorsModule } from './core/errors/errors.module';

//pipes
import { FilterPipe } from '../pipes/filter.pipe';
import { TruncatePipe } from '../pipes/truncate.pipe';
import { YoutubePipe} from '../pipes/youtube.pipe';
import { SafePipe} from '../pipes/Safe.pipe';

//pages
import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { ProfilePage } from '../pages/profile/profile';
import { NewsListPage } from '../pages/news-list/news-list';
import { NewsDetailPage } from '../pages/news-detail/news-detail';
import { BirthdayListPage } from '../pages/birthday-list/birthday-list';
import { WhoiswhoListPage } from '../pages/whoiswho-list/whoiswho-list';
import { UsefulInfoListPage } from '../pages/useful-info-list/useful-info-list';
import { UsefulInfoDetailPage } from '../pages/useful-info-detail/useful-info-detail';
import { SurveysListPage } from '../pages/surveys-list/surveys-list';
import { SurveysDetailPage } from '../pages/surveys-detail/surveys-detail';
import { CoursesListPage } from '../pages/courses-list/courses-list';
import { CoursesDetailPage } from '../pages/courses-detail/courses-detail';
import { CredencialesPage } from '../pages/credenciales/credenciales';
import { CataloguePage } from '../pages/catalogue/catalogue';
import { OrderPage } from '../pages/order/order';

//providers
import { AuthenticationProvider } from '../providers/authentication/authentication.provider';
import { UserProvider } from '../providers/user/user.provider';
import { NoticiaProvider } from '../providers/noticia/noticia.provider';
import { SettingsProvider } from '../providers/settings/settings.provider';
import { UsefulInfoProvider } from '../providers/useful-info/useful-info.provider';
import { BirthdayProvider } from '../providers/birthday/birthday.provider';
import { UserDetailPage } from '../pages/user-detail/user-detail';
import { ResetPasswordPage } from '../pages/reset-password/reset-password';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import { TermsPage } from '../pages/terms/terms';
import { DocumentsProvider } from '../providers/documents/documents.provider';
import { DocumentsPage } from '../pages/documents/documents';
import { BeneficioProvider } from '../providers/benefits/benefits.provider';
import { BenefitsListPage } from '../pages/benefits-list/benefits-list';
import { BenefitsDetailPage } from '../pages/benefits-detail/benefits-detail';
import { DeviceProvider } from '../providers/device/device.provider';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { FormularioProvider } from '../providers/surveys/surveys.provider';
import { CoursesProvider } from '../providers/courses/courses.provider';
import { ReaccionProvider } from '../providers/reaccion/reaccion.provider';

import { Firebase } from '@ionic-native/firebase';
import { ActivateAccountValidatePage } from '../pages/activate-account-validate/activate-account-validate';
import { ActivateAccountPasswordPage } from '../pages/activate-account-password/activate-account-password';
import { MuroProvider } from '../providers/muro/muro.provider';
import { StorageProvider } from '../providers/storage/storage.provider';
import { QavantHeaderComponent } from '../components/qavant-header/qavant-header';
import { MuroPage } from '../pages/muro/muro';
import { ChatbotPage } from '../pages/chatbot/chatbot';
import { FormularioGenericoListPage } from '../pages/formulario-generico-list/formulario-generico-list';
import { NotificationsPage } from '../pages/notifications/notifications';
import { QavantCardCumpleComponent } from '../components/qavant-card-cumple/qavant-card-cumple';
import { QavantCardEncuestaComponent } from '../components/qavant-card-encuesta/qavant-card-encuesta';
import { QavantCardNoticiaComponent } from '../components/qavant-card-noticia/qavant-card-noticia';
import { CredencialesProvider } from '../providers/credenciales/credenciales.provider';
import { QavantSocialComponent } from '../components/qavant-social/qavant-social';
import { HelperProvider } from '../providers/helper/helper.provider';
import { QavantCardPublicacionComponent } from '../components/qavant-card-publicacion/qavant-card-publicacion';
import { QavantCardBeneficioComponent } from '../components/qavant-card-beneficio/qavant-card-beneficio';
import { QavantCardBeneficioMuroComponent } from '../components/qavant-card-beneficio-muro/qavant-card-beneficio-muro';

import { GaleriaProvider } from '../providers/galeria/galeria.provider';
import { QavantCardGaleriaComponent } from '../components/qavant-card-galeria/qavant-card-galeria';
import { GaleriaListPage } from '../pages/galeria-list/galeria-list';
import { GaleriaDetailPage } from '../pages/galeria-detail/galeria-detail';

import { PhotoViewer } from '@ionic-native/photo-viewer';
import { StreamingMedia} from '@ionic-native/streaming-media';
import { CategoriaProvider } from '../providers/categoria/categoria';
import { QavantCardPuntosComponent } from '../components/qavant-card-puntos/qavant-card-puntos';
import { QavantCardPuntosMuroComponent } from '../components/qavant-card-puntos-muro/qavant-card-puntos-muro';
import { QavantCardPuntosCatalogoComponent } from '../components/qavant-card-puntos-catalogo/qavant-card-puntos-catalogo';
import { TipoCatalogoProvider } from '../providers/tipo-catalogo/tipo-catalogo.provider';

@NgModule({
  declarations: [
    MyApp,
    FilterPipe,
    TruncatePipe,
    HomePage,
    LoginPage,
    ProfilePage,
    NewsListPage,
    BirthdayListPage,
    NewsDetailPage,
    WhoiswhoListPage,
    UsefulInfoListPage,
    UsefulInfoDetailPage,
    UserDetailPage,
    ResetPasswordPage,
    ChangePasswordPage,
    TermsPage,
    DocumentsPage,
    BenefitsListPage,
    BenefitsDetailPage,
    SurveysListPage,
    SurveysDetailPage,
    ActivateAccountValidatePage,
    ActivateAccountPasswordPage,
    CoursesListPage,
    CoursesDetailPage,
    CataloguePage,
    OrderPage,
    YoutubePipe,
    QavantHeaderComponent,
    QavantCardCumpleComponent,
    QavantCardEncuestaComponent,
    QavantCardNoticiaComponent,
    QavantCardPublicacionComponent,
    QavantCardBeneficioComponent,
    QavantCardBeneficioMuroComponent,
    QavantCardPuntosComponent,
    QavantCardPuntosMuroComponent,
    QavantCardPuntosCatalogoComponent,
    MuroPage,
    ChatbotPage,
    FormularioGenericoListPage,
    NotificationsPage,
    CredencialesPage,
    SafePipe,
    QavantSocialComponent,
    QavantCardGaleriaComponent,
    GaleriaListPage,
    GaleriaDetailPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp, {backButtonText: ''}),
    IonicStorageModule.forRoot(),
    NgxErrorsModule,
    InterceptorModule,
    ErrorsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    ProfilePage,
    NewsListPage,
    BirthdayListPage,
    NewsDetailPage,
    WhoiswhoListPage,
    UsefulInfoListPage,
    UsefulInfoDetailPage,
    UserDetailPage,
    ResetPasswordPage,
    ChangePasswordPage,
    TermsPage,
    DocumentsPage,
    BenefitsListPage,
    BenefitsDetailPage,
    SurveysListPage,
    SurveysDetailPage,
    ActivateAccountValidatePage,
    ActivateAccountPasswordPage,
    CoursesListPage,
    CoursesDetailPage,
    MuroPage,
    ChatbotPage,
    CataloguePage,
    OrderPage,
    QavantHeaderComponent,
    QavantCardCumpleComponent,
    QavantCardEncuestaComponent,
    QavantCardNoticiaComponent,
    QavantCardPublicacionComponent,
    QavantCardBeneficioComponent,
    QavantCardBeneficioMuroComponent,
    QavantCardPuntosComponent,
    QavantCardPuntosMuroComponent,
    QavantCardPuntosCatalogoComponent,
    FormularioGenericoListPage,
    NotificationsPage,
    CredencialesPage,
    QavantCardGaleriaComponent,
    GaleriaListPage,
    GaleriaDetailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthenticationProvider,
    UserProvider,
    NoticiaProvider,
    UsefulInfoProvider,
    SettingsProvider,
    BirthdayProvider,
    DocumentsProvider,
    FileOpener,
    BeneficioProvider,
    Firebase,    
    Device,
    LocalNotifications,
    DeviceProvider,
    FileTransfer,
    File,
    Device,
    FormularioProvider,
    CoursesProvider,
    InAppBrowser,
    MuroProvider,
    StorageProvider,
    CredencialesProvider,
    ReaccionProvider,
    HelperProvider,
    PhotoViewer,
    StreamingMedia,
    GaleriaProvider,
    CategoriaProvider,
    TipoCatalogoProvider
  ]
})
export class AppModule {}
