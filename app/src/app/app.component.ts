import { Component, ViewChild, OnInit } from '@angular/core';
import { Nav, Platform, MenuController, AlertController, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// providers
import { FcmProvider } from '../providers/fcm/fcm.provider';
import { AuthenticationProvider } from '../providers/authentication/authentication.provider';
import { SettingsProvider } from '../providers/settings/settings.provider';

// pages

import { UsefulInfoListPage } from '../pages/useful-info-list/useful-info-list';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { SurveysListPage } from '../pages/surveys-list/surveys-list';
import { ToastController } from 'ionic-angular';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { CoursesListPage } from '../pages/courses-list/courses-list';
import { UserProvider } from '../providers/user/user.provider';
import { MuroPage } from '../pages/muro/muro';
import { FormularioGenericoListPage } from '../pages/formulario-generico-list/formulario-generico-list';
import { LocalRequestOrigin } from '../models/LocalRequestOrigin.enum';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { CataloguePage } from '../pages/catalogue/catalogue';
import { NewsListPage } from '../pages/news-list/news-list';

declare var cordova: any;

@Component({
  templateUrl: 'app.html',
  providers: [FcmProvider]
})
export class MyApp implements OnInit{
  @ViewChild(Nav) nav: Nav;
  rootPage: any;
  menuItems: Array<{ title:string, target:any, params:any }>;
  pages:Array<{ component:any, code:string }>;
  appVersion: string;

  constructor(
    public events: Events,
    public menu: MenuController,
    public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen,
    public alertCtrl: AlertController,
    public authProvider: AuthenticationProvider,
    public userProvider: UserProvider,
    public settingsProvider: SettingsProvider,
    public fcm: FcmProvider, 
    private toastCtrl: ToastController,
    public localNotifications: LocalNotifications,
    private theInAppBrowser: InAppBrowser,
    public alertController: AlertController) {
      this.configureTheme();
      this.configureMenu();
      this.initializeApp();
  }

  ngOnInit() {
  }

  initializeApp(){ 
    this.platform.ready().then(() => {
      this.checkSession();
      this.listenIncomingMessages();
      this.getVersionNumber()
      this.statusBar.styleDefault();
      //this.splashScreen.hide();
    });
  }

  getVersionNumber(){
    if (this.platform.is('cordova')) {
      var me = this;
      cordova.getAppVersion.getVersionNumber(function(value) { 
        me.appVersion= "v" + value;
      });
    }
  }

  configureTheme(){
    this.events.subscribe('theme:populate', settings => {
      console.log("qmt::aplicacion de custom CSS");
      let styleSheetId: string = "customCss";
      let sheet = document.getElementById(styleSheetId);
      if (sheet){
        sheet.parentNode.removeChild(sheet);
      }
      let linkElement: HTMLLinkElement = document.createElement('link');
      let head = document.getElementsByTagName('head')[0];
      linkElement.id = styleSheetId;
      linkElement.setAttribute('rel', 'stylesheet');
      linkElement.setAttribute('type', 'text/css');
      linkElement.setAttribute('href', settings.CustomCss);
      head.appendChild(linkElement);

      console.log("qmt::aplicacion de colores");
      document.body.style.setProperty('--PrimaryColor', settings.PrimaryColor);
      document.body.style.setProperty('--SecondaryColor', settings.SecondaryColor);
      document.body.style.setProperty('--TertiaryColor', settings.TertiaryColor);
    });
  }

  configureMenu(){
    this.events.subscribe('menu:populate', settings => {
      // init
      this.menuItems = [];
      this.pages = [];

      // defino listado de paginas pre-existentes en la App
      this.pages = [        
        { component: MuroPage, code:'app.home' },
        //{ component: ProfilePage, code:'app.profile' },
        { component: NewsListPage, code:'app.newsList' },
        //{ component: WhoiswhoListPage, code:'app.whoiswhoList' },
        //{ component: BirthdayListPage, code:'app.birthdays' },
        //{ component: DocumentsPage,  code:'app.documents' },
        //{ component: BenefitsListPage, code:'app.benefits' },
        { component: CoursesListPage, code:'app.courses' },
        { component: SurveysListPage, code:'app.surveyslist'},
        { component: FormularioGenericoListPage, code:'app.generalformsList' },
        { component: UsefulInfoListPage, code:'app.infoList' },
        //{ component: TermsPage, code:'app.terms' },
        //{ component: CredencialesPage, code: 'app.credenciales'},
        //{ component: ChatbotPage, code: 'app.chatbot'},
        //{ component: GaleriaListPage, code: 'app.gallerieslist'},

        //{ component: NewsDetailPage, code: 'app.news'},
        //{ component: BenefitsDetailPage, code: 'app.benefit'},
        //{ component: SurveysDetailPage, code: 'app.surveyslist'},
        //{ component: CoursesDetailPage, code: 'app.coursesList'},
        //{ component: UsefulInfoDetailPage, code: 'app.infoDetail'},
        //{ component: GaleriaDetailPage, code: 'app.gallery'}     
        
        { component: CataloguePage, code:'app.catalogue' }
        //{ component: OrderPage, code:'app.order' }
      ];

      // Configuro el menu según los settings del tenant que vienen de la BD      
      for (let i in settings) {
        let params:any = this.stringToJSON(settings[i].routerLink);
        if (!params.target)
          continue;

        let menuItem:any;
        if(params.type == "external") {
          continue;
          /*menuItem = {
            title: settings[i].label,
            target: params.target,
            params: params
          };*/
        } else {
          let page :any = this.pages.find(x => x.code == params.target);
          if(page) {
            menuItem = {
              title: settings[i].label,
              target: page.component,
              params: params,
              icon: settings[i].icon
            };
          } else {
            continue;
          }
        }
        this.menuItems.push(menuItem);
      }
    });

  }

  checkSession() {
    if (this.authProvider.isLogged()) {

      // Obtenemos settings de tenant
      this.settingsProvider.getSettings().subscribe(
        settings => {
          this.events.publish('menu:populate', settings.menu);
          this.events.publish('theme:populate', settings.theme);

          // obtenemos info de usuario actual
          this.userProvider.getUserProfile().subscribe(
            user => { 
              this.userProvider.setCurrentUser(user);
              this.rootPage = MuroPage;
            },
            error => { 
              this.rootPage = LoginPage;
              this.showError(error);
            }
          );
        },
        error => {
          this.rootPage = LoginPage;
          this.showError(error);
        }
      );
    } else {
      this.rootPage = LoginPage;
    }
  }

  listenIncomingMessages() {
    console.log("qmt::MyApp>>listenIncomingMessages");
    if (this.platform.is('cordova')) {
      this.fcm.listenToNotifications().subscribe(res => {
        if(res.tap) {
          // background mode
          if(res)
            this.showAlertNotification(res.title, res.body, res.state);
        } else if (!res.tap) {
          // foreground mode
          if(res)
            this.showAlertNotification(res.title, res.body, res.state);
        }
      });
    }
  }

  showAlertNotification(title:string, message:string, page:string){
    let alert = this.alertCtrl.create({
      title: title,
      message: message,
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {

            let pageCode:string = page.split('/')[0];
            let contentId:string = page.split('/')[1];

            if(contentId){
              let pageDetail:any = this.pages.find(x => x.code == pageCode);
              if(pageDetail) {
                this.nav.push(pageDetail.component, {id: contentId});
              } else {
                this.nav.setRoot(HomePage);
              }
            } else {
              if (pageCode) {
                let page:any =  this.pages.find(x => x.code == pageCode);
                if(page){
                  this.nav.setRoot(page.component);
                } else {
                  this.nav.setRoot(HomePage);
                }
              } else {
                this.nav.setRoot(HomePage);
              }
            }
          }
        }
      ]
    });
    alert.present();
  }

  logout() {
    let alert = this.alertCtrl.create({
      title: 'Cerrar sesión',
      message: '¿Confirma que desea cerrar sesión?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => { console.log('Cancel clicked'); }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.menu.swipeEnable(false);
            this.menu.close();
            this.authProvider.signOut(this.userProvider.currentUser.id);
            this.nav.setRoot(LoginPage);
          }
        }
      ]
    });
    alert.present();
  }

  openPage(menuItem) {
    if (menuItem.params.type == "external") {
      this.openBrowser(menuItem.target);
    } else {
      this.nav.setRoot(menuItem.target, {params:menuItem.params, localRequestOrigin: LocalRequestOrigin.Menu });
    }
  }

  openBrowser(url){
    const options: InAppBrowserOptions = {location:'no'};
    this.theInAppBrowser.create(url, '_system', options);
  }

  showError(error:any){
    let toast = this.toastCtrl.create({
      message: (error ? error : "Error"),
      cssClass: "toast-danger",
      duration: 3000
    });
    toast.present();
  }

  stringToJSON(routerLink:any) {            
    var pairs = routerLink.split('&');
    var result = {};
    pairs.forEach(function(pair) {
        pair = pair.split('=');
        result[pair[0]] = decodeURIComponent(pair[1] || '');
    });

    return JSON.parse(JSON.stringify(result));
  }
}