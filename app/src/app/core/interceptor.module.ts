import { Injectable, NgModule} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { StorageProvider } from '../../providers/storage/storage.provider';

@Injectable()
export class HttpsRequestInterceptor implements HttpInterceptor {
    constructor(private storageProvider: StorageProvider) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let token = this.storageProvider.getToken();
        if (token) {
            req = req.clone({ headers: req.headers.set('x-token', token) });
        }
        return next.handle(req);
    }
};

@NgModule({
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: HttpsRequestInterceptor, multi: true }
    ]
})
export class InterceptorModule { }