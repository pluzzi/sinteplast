import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ToastController, AlertController, LoadingController } from 'ionic-angular';

@Injectable()
export class HelperProvider {

  constructor(
    public splashScreen: SplashScreen,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController) { }

    public showToast(message: string, dismissOnPageChange: boolean = false) {
      const toast = this.toastCtrl.create({
        duration: 3000,
        message: message,
        dismissOnPageChange: dismissOnPageChange
      });
      toast.present();
      return toast;
    }
    public showError(message: string) {
      return this.showToast(message);
    }
    
}