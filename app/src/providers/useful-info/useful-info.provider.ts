import { HttpClient } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { GlobalErrorHandler } from '../../app/core/errors/errors-handler';
import { Info } from '../../models/usefulinfo.model';
import { Categoria } from '../../models/categorias';
import { SettingsProvider } from '../settings/settings.provider';

@Injectable()
export class UsefulInfoProvider {
  constructor(
    public httpClient: HttpClient,
    public globalErrorHandler: GlobalErrorHandler,
    public settingsProvider:SettingsProvider) {}

  getInfoById(id:number): Observable<Info>{
    let apiUrl =  this.settingsProvider.getApiUrl() + '/infoById/' + id;
    return this.httpClient.get(apiUrl).pipe(
      map(dbInfoList => {
        return this.mapper(dbInfoList);
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  getUsefulInfo(): Observable <Info[]>{
    let infoList: Info[] = [];
    let apiUrl =  this.settingsProvider.getApiUrl() + '/paginasInfo';    
    return this.httpClient.get(apiUrl).pipe(
      map(dbInfoList => {
        for (let i in dbInfoList){
          infoList.push(this.mapper(dbInfoList[i]));
        }
        return infoList;
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  getInfoPaginasByCatId(categoriaId: number): Observable <Info[]>{
    let infoList: Info[]=[]; 
    let apiUrl = this.settingsProvider.getApiUrl() + '/infoPaginas/categoriaId/' + categoriaId;

    return this.httpClient.get(apiUrl).pipe(
      map(dbInfoList => {
        for (let i in dbInfoList){
          infoList.push(this.mapper(dbInfoList[i]));
        }
        return infoList;
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  getCategorias(moduloId: number){
    let categoriaList: Categoria[]=[];
    let apiUrl = this.settingsProvider.getApiUrl() + '/categorias/ModuloId/' + moduloId;
    let cat: Categoria = new Categoria()
    cat.id = 0;
    cat.name = "Todas";
    categoriaList.push(cat);
    return this.httpClient.get(apiUrl).pipe(
      map(dbCategoriaList => {  
        for (let i in dbCategoriaList) {
          let categoria: Categoria = new Categoria();
          categoria.id = dbCategoriaList[i]['Id'];
          categoria.name = dbCategoriaList[i]['Nombre'];       
          categoriaList.push(categoria);
        }
        return categoriaList;         
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  private mapper(dto: any): Info {
    let info: Info = new Info();
    info.id = dto['Id'];
    info.categoriaId = dto['CategoriaId'];
    info.title = dto['Titulo'];
    info.copete = dto['Copete'];
    info.html = dto['Html'];
    info.icon = dto['Icon'];
    info.order = dto['Orden']; 
    info.image = dto['Imagen']; 
    info.reaccionId = dto['ReaccionId'];
    info.totalReacciones = dto['TotalReacciones'];    
    return info;
  }
}