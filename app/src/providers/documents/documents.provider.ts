import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { GlobalErrorHandler } from '../../app/core/errors/errors-handler';
import { Documento } from '../../models/documents.model';
import { DocumentCategory } from '../../models/documentsCategory';
import { SettingsProvider } from '../settings/settings.provider';

@Injectable()
export class DocumentsProvider {

  constructor(
    public httpClient: HttpClient,
    public globalErrorHandler: GlobalErrorHandler,
    public settingsProvider:SettingsProvider) {}

  obtenerDocumentos(page:number): Observable <Documento[]>{
    let documentList: Documento[] = [];
    let apiUrl =  this.settingsProvider.getApiUrl() + '/documents/page/' + page;    
    return this.httpClient.get(apiUrl).pipe(
      map(dbDocumentoList => {
        for (let i in dbDocumentoList){         
          documentList.push(this.mapper(dbDocumentoList[i]));
        }  
        return documentList;
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  obtenerDocumentosPorCategoriaId(categoriaId: number,page:number){
    let documentoList: Documento[]=[]; 
    let apiUrl = this.settingsProvider.getApiUrl() + '/documentos/categoriaId/' + categoriaId + '/'+ page;
    return this.httpClient.get(apiUrl).pipe(
      map(dbDocumentoList => {
        for (let i in dbDocumentoList){        
          documentoList.push(this.mapper(dbDocumentoList[i]));
        }  
        return documentoList;
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  obtenerCategorias(moduloId: number){
    let categoriaList: DocumentCategory[]=[];
    let apiUrl = this.settingsProvider.getApiUrl() + '/categorias/ModuloId/' + moduloId;

    let categoria: DocumentCategory = new DocumentCategory();
    categoria.id = 0;
    categoria.name = "Todas";
    categoriaList.push(categoria);

    return this.httpClient.get(apiUrl).pipe(
      map(dbcategoriesList => {  
        for (let i in dbcategoriesList) {
          let categoria: DocumentCategory = new DocumentCategory();
          categoria.id = dbcategoriesList[i]['Id'];
          categoria.name = dbcategoriesList[i]['Nombre'];       
          categoriaList.push(categoria);
        }
        return categoriaList;         
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  private mapper(dto: any): Documento {
    let documento: Documento = new Documento();
    documento.id = dto['Id'];
    documento.title = dto['Title'];
    documento.description = dto['Description'];
    documento.filePath = dto['FilePath'];
    documento.extension = dto['Extension'];
    documento.reaccionId = dto['ReaccionId'];
    documento.totalReacciones = dto['TotalReacciones'];
    return documento;
  }

}