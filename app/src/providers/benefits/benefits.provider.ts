import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';

import { GlobalErrorHandler } from '../../app/core/errors/errors-handler';
import { Beneficio } from '../../models/benefits.model';
import { BeneficioCategoria } from '../../models/benefitCategory.model';
import { SettingsProvider } from '../settings/settings.provider';

@Injectable()
export class BeneficioProvider {

  constructor(
    public httpClient: HttpClient,
    public globalErrorHandler: GlobalErrorHandler,
    public settingsProvider:SettingsProvider
  ) {}

  getBeneficios(page: number, tipoCatalogoProductosId: number): Observable <Beneficio[]>{
    let beneficioList: Beneficio[]=[];
    let apiUrl =  this.settingsProvider.getApiUrl() + '/benefits/page/'+page+'/'+tipoCatalogoProductosId;
    return this.httpClient.get(apiUrl).pipe(
      map(dbBeneficioList => {
        for (let i in dbBeneficioList) {
          beneficioList.push(this.mapper(dbBeneficioList[i]));
        }
        return beneficioList;
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  getBenefitsLikes(pageLikes: number): Observable <Beneficio[]>{
    let benefitsList: Beneficio[]=[];
    let apiUrl =  this.settingsProvider.getApiUrl() + '/benefits/likes/'+ pageLikes;

    return this.httpClient.get(apiUrl).pipe(
      map(dbBeneficioList => {
        for (let i in dbBeneficioList) {
          benefitsList.push(this.mapper(dbBeneficioList[i]));
        }
        return benefitsList;
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }
  
  getBeneficioById(id:number): Observable<Beneficio>{
    let apiUrl =  this.settingsProvider.getApiUrl() + '/benefits/' + id;
    return this.httpClient.get(apiUrl).pipe(
      map(dbBeneficio => {
        return this.mapper(dbBeneficio);
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  getBeneficiosByCategoriaId(categoriaId: number, page: number){
    let beneficioList: Beneficio[]=[];
    let apiUrl =  this.settingsProvider.getApiUrl() + '/benefits/category/'+categoriaId+ '/page/'+ page;

    return this.httpClient.get(apiUrl).pipe(
      map(dbBeneficioList => {
        for (let i in dbBeneficioList) {
          beneficioList.push(this.mapper(dbBeneficioList[i]));
        }
        return beneficioList;
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  getLikes(beneficioId: any): Observable <Beneficio[]>{
    let benefitsList: Beneficio[]=[];
    let apiUrl =  this.settingsProvider.getApiUrl() + '/benefits/'+ beneficioId + '/like';
    return this.httpClient.get(apiUrl).pipe(
      map(dbBeneficioList => {        
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  getCategorias(){
    let categoriaList: BeneficioCategoria[] = [];
    let apiUrl =  this.settingsProvider.getApiUrl() + '/categorias/ModuloId/8';
    return this.httpClient.get(apiUrl).pipe(
      map(dbCategoriaList => {  
        for (let i in dbCategoriaList) {
          let categoria: BeneficioCategoria = new BeneficioCategoria();
          categoria.id = dbCategoriaList[i]['Id'];
          categoria.name = dbCategoriaList[i]['Nombre'];       
          categoriaList.push(categoria);
        }
        return categoriaList;        
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  canjear(userId: number, benefitsId: number){
    let apiUrl =  this.settingsProvider.getApiUrl() + '/benefits/exchange';
    this.httpClient.post(apiUrl, {UserId: userId, BenefitsId: benefitsId}).subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.log(err);
      }
    );
  }

  private mapper(dto: any): Beneficio {
    let beneficio: Beneficio = new Beneficio();
    beneficio.id = dto['Id'];
    beneficio.titulo = dto['Title'];
    beneficio.copete = dto['Subtitle'];
    beneficio.contenido = dto['Content'];
    beneficio.imagen = dto['Image'];
    beneficio.liked = dto['Liked'];
    beneficio.fechaAlta = dto['To'];
    beneficio.reaccionId = dto['ReaccionId'];
    beneficio.totalReacciones = dto['TotalReacciones']; 
    beneficio.puntos = dto['Puntos'];
    beneficio.tipoCatalogoProductosId = dto['TipoCatalogoProductosId'];
    beneficio.esNuevo = dto['EsNuevo'];
    return beneficio;
  }
}

