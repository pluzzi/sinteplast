import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { catchError, map } from 'rxjs/operators';
import { Device } from '@ionic-native/device';
import { DeviceProvider } from '../device/device.provider';
import { GlobalErrorHandler } from '../../app/core/errors/errors-handler';
import { AuthUser } from '../../models/authUser.model';
import { SettingsProvider } from '../settings/settings.provider';
import 'rxjs/add/observable/throw';
import { StorageProvider } from '../storage/storage.provider';

@Injectable()
export class AuthenticationProvider {
  authUser: AuthUser;
  constructor(
    public httpClient: HttpClient,
    public device: Device,
    public deviceProvider: DeviceProvider,
    public globalErrorHandler: GlobalErrorHandler,
    private storageProvider : StorageProvider,
    public settingsProvider:SettingsProvider
  ) {}
  
  get authenticated(): boolean {
    return this.authUser !== null;
  }

  isLogged() : boolean{
    return this.storageProvider.existsToken();
  }

	login(data: any):Observable<AuthUser> {
    console.log("qmt::AuthenticationProvider>>login");
    let apiUrl =  this.settingsProvider.getApiUrl() + '/login';    
    let headers: HttpHeaders = new HttpHeaders();
    headers = new HttpHeaders({
      'x-tenant': data.DireccionApp,
      'x-canal': "app"
    });
    
    return this.httpClient.post(apiUrl, data, {headers: headers}).pipe(
      map(db => {
        console.log("qmt::autenticación exitosa");  
        this.authUser = new AuthUser();
        this.authUser.shouldModifyPassword = db['shouldModifyPassword'];
        this.authUser.authToken = db['token'];
        return this.authUser;   
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }  

  signOut(userId: number) {
    let uuid:string = this.device.uuid;
    let data = {
      UserId: userId,
      Uuid: uuid
    };
    this.deviceProvider.removeDevice(data).subscribe(
      () => { localStorage.clear(); },
      error => { console.log(error); }
    );
  }
  
  validarUsuario(data: any):Observable<String> {
    let apiUrl =  this.settingsProvider.getApiUrl() + '/users/toactivate';
    let headers: HttpHeaders = new HttpHeaders();
    headers = new HttpHeaders({
      'x-tenant': data.DireccionApp,
      'x-canal': "app"
    });

    return this.httpClient.post(apiUrl, data, {headers: headers}).pipe(
      map(response => {
        let email:string = response["Email"];
        return email;
      }),
      catchError(this.globalErrorHandler.handleError)
    );    
  }

  activarUsuario(data: any) {
    let apiUrl =  this.settingsProvider.getApiUrl() + '/users/activate';
    let headers: HttpHeaders = new HttpHeaders();
    headers = new HttpHeaders({
      'x-tenant': data.Empresa,
      'x-canal': "app"
    });

    return this.httpClient.post(apiUrl, data, {headers: headers}).pipe(
      map(result => {
        return result;
      }),
      catchError(error => {
        error.message = 'Ocurrió un error al activar su usuario. Intente nuevamente.';
        return this.globalErrorHandler.handleError(error);
      })
    );    
  }

  resetPassword(data: any) {
    let apiUrl =  this.settingsProvider.getApiUrl() + '/users/forgotpassword';
    let headers: HttpHeaders = new HttpHeaders();
    headers = new HttpHeaders({
      'x-tenant': data.DireccionApp,
      'x-canal': "app"
    });

    return this.httpClient.post(apiUrl, data, {headers: headers}).pipe(
      map(response => {
        console.log(response);
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  changePassword(data: any) {
    let apiUrl =  this.settingsProvider.getApiUrl() + '/users/modifypassword';
    return this.httpClient.post(apiUrl, data).pipe(
      map(response => {
        console.log(response);
      }),
      catchError(error => {
        console.log(error);
        return this.globalErrorHandler.handleError(error);
      })
    );
  }

}