import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { GlobalErrorHandler } from '../../app/core/errors/errors-handler';
import { Credencial } from '../../models/credenciales.model';
import { SettingsProvider } from '../settings/settings.provider';

@Injectable()
export class CredencialesProvider {

  constructor(
    public httpClient: HttpClient,
    public globalErrorHandler: GlobalErrorHandler,
    public settingsProvider:SettingsProvider    
    ) {}

    getCredencialBydId(id:number): Observable<Credencial>{
      let apiUrl =  this.settingsProvider.getApiUrl() + '/credencial/' + id;
      return this.httpClient.get(apiUrl).pipe(
        map(dbCredencial => {
          let credencial: Credencial = new Credencial(dbCredencial['Id']);
          credencial.html = dbCredencial['Html'];
          return credencial;
        }),
        catchError(this.globalErrorHandler.handleError)
      );
    }

}
