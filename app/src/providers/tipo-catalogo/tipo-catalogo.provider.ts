import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TipoCatalogo } from '../../models/tipoCatalogo.model';
import { TipoCatalogoUsuario } from '../../models/tipoCatalogoUsuario.model';
import { SettingsProvider } from '../settings/settings.provider';
import { Observable } from 'rxjs/Observable';
import { GlobalErrorHandler } from '../../app/core/errors/errors-handler';
import { catchError, map } from 'rxjs/operators';

@Injectable()
export class TipoCatalogoProvider {

  constructor(
    public http: HttpClient,
    public settingsProvider:SettingsProvider,
    public globalErrorHandler: GlobalErrorHandler
  ) { }

  obtenerTipoCatalogoPorId(id: string): Observable<TipoCatalogo> {
    let apiUrl =  this.settingsProvider.getApiUrl() + '/tipoCatalogo/id/' + id;

    return this.http.get(apiUrl).pipe(
      map(result => {
        let tipoCatalogo: TipoCatalogo = new TipoCatalogo();

        tipoCatalogo.id = result['Id'];
        tipoCatalogo.descripcion = result['Descripcion'];
        tipoCatalogo.iconoCatalogo = result['IconoCatalogo'];
        tipoCatalogo.fondoCatalogo = result['FondoCatalogo'];
        tipoCatalogo.empresaId = result['EmpresaId'];
        tipoCatalogo.mail = result['Mail'];
        
        return tipoCatalogo;
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  obtenerTipoCatalogos(): Observable<TipoCatalogo[]> {
    let tipoCatalogoList: TipoCatalogo[] = [];
    let apiUrl =  this.settingsProvider.getApiUrl() + '/tipoCatalogo';
    return this.http.get(apiUrl).pipe(
      map(result => {
        for (let i in result) {
          let tipoCatalogo: TipoCatalogo = new TipoCatalogo();
          tipoCatalogo.id = result[i]['Id'];
          tipoCatalogo.descripcion = result[i]['Descripcion'];
          tipoCatalogo.iconoCatalogo = result[i]['IconoCatalogo'];
          tipoCatalogo.fondoCatalogo = result[i]['FondoCatalogo'];
          tipoCatalogo.empresaId = result[i]['EmpresaId'];
          tipoCatalogo.mail = result[i]['Mail'];
          tipoCatalogoList.push(tipoCatalogo);
        }
        return tipoCatalogoList;
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  obtenerTipoCatalogosPorUsuarioId(userId: string): Observable<TipoCatalogoUsuario[]> {
    let tipoCattalogoUsuarioList: TipoCatalogoUsuario[] = [];

    let apiUrl =  this.settingsProvider.getApiUrl() + '/tipoCatalogo/usuario/' + userId;
    return this.http.get(apiUrl).pipe(
      map(result => {
        console.log("result:");
        console.log(result);
        
        for (let i in result) {
          let elemento: TipoCatalogoUsuario = new TipoCatalogoUsuario();
          elemento.userId = result[i]['UserId'];
          elemento.puntosAcumulados = result[i]['PuntosAcumulados'];
          elemento.fechaActualizacionPuntos = result[i]['FechaActualizacionPuntos'];

          let tipoCatalogo: TipoCatalogo = new TipoCatalogo();
          console.log(result[i]['TipoCatalogoOutput']);
          tipoCatalogo.id = result[i]['TipoCatalogoOutput']['Id'];
          tipoCatalogo.descripcion = result[i]['TipoCatalogoOutput']['Descripcion'];
          tipoCatalogo.iconoCatalogo = result[i]['TipoCatalogoOutput']['IconoCatalogo'];
          tipoCatalogo.fondoCatalogo = result[i]['TipoCatalogoOutput']['FondoCatalogo'];
          tipoCatalogo.empresaId = result[i]['TipoCatalogoOutput']['EmpresaId'];
          tipoCatalogo.mail = result[i]['TipoCatalogoOutput']['Mail'];

          elemento.tipoCatalogo = tipoCatalogo;

          tipoCattalogoUsuarioList.push(elemento);
        }
        return tipoCattalogoUsuarioList;
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

}
