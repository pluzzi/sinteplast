import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { GlobalErrorHandler } from '../../app/core/errors/errors-handler';
import { Reaccion } from '../../models/reaccion.model';

@Injectable()
export class SettingsProvider {
  private _apiUrl: string =   "http://api-sinteplast-test.qavant.com"; //"https://api-sinteplast.qavant.com/"; //"http://api-sinteplast-test.qavant.com"; //"http://localhost:33224/"; //"https://api-sinteplast.qavant.com/";
  private _passwordHash: string = "95153659a5461d15c00f1d83566d65f2";
  private _pendingRefresh: boolean;
  private _reacciones: Reaccion[];
  private _empresa: string = "Goiar";

  constructor(
    private httpClient: HttpClient, 
    public globalErrorHandler: GlobalErrorHandler) {
    console.log("qmt::SettingsProvider>>constructor");
  }

  getEmpresa(): string{
    return this._empresa;
  }

  getApiUrl(): string {
    return this._apiUrl;
  }

  getPasswordHash(){
    return this._passwordHash;
  }

  getPendingRefresh(){
    return this._pendingRefresh;
  }

  setPendingRefresh(value: boolean) {
    this._pendingRefresh = value;
  }

  getSettings(): Observable<any>{
    console.log("qmt::SettingsProvider>>getSettings");
    console.log("qmt::recuperando settings del tenant");
    
    let apiUrl =  this.getApiUrl() + '/tenant/settings';
    return this.httpClient.get(apiUrl).pipe(
      map(data => {
        console.log("qmt::settings de tenant recuperados");
        this.setReacciones(data['reacciones']);
        return data;
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  setReacciones(data:any) {  
    let reacciones: Reaccion[] = [];
    for(let i in data){
      let reaccion:Reaccion = new Reaccion();
      reaccion.id = data[i]['id'];
      reaccion.nombre = data[i]['nombre'];
      reaccion.icono = data[i]['icono'];
      reacciones.push(reaccion);
    }
    this._reacciones = reacciones;
  }

  getReacciones(): Reaccion[]{
    return this._reacciones;
  }
}
