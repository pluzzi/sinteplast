import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { GlobalErrorHandler } from '../../app/core/errors/errors-handler';
import { SettingsProvider } from '../settings/settings.provider';

@Injectable()
export class DeviceProvider {

  constructor (
    public httpClient: HttpClient,
    public globalErrorHandler: GlobalErrorHandler,
    public settingsProvider:SettingsProvider
    ) { }

  registerDevice(data: any){
    let apiUrl =  this.settingsProvider.getApiUrl() + '/register_device';
    const req = this.httpClient.post(apiUrl, data).subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.log("Error occured: " + err);
      }
    );
  }

  removeDevice(data: any){
    let apiUrl =  this.settingsProvider.getApiUrl() + '/remove_device';
    return this.httpClient.post(apiUrl, data).pipe(
      map(response => {
        console.log(response);
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }
}
