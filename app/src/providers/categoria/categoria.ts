import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Categoria } from '../../models/categorias';
import { SettingsProvider } from '../settings/settings.provider';
import { map, catchError } from 'rxjs/operators';
import { GlobalErrorHandler } from '../../app/core/errors/errors-handler';


@Injectable()
export class CategoriaProvider {

  constructor(
    public httpClient: HttpClient,
    public settingsProvider:SettingsProvider,
    public globalErrorHandler: GlobalErrorHandler) {
  }

  obtenerCategorias(modulo: string){
    let categoriaList: Categoria[]=[];
    let apiUrl = this.settingsProvider.getApiUrl() + '/categorias/modulo/' + modulo;
    let cat: Categoria = new Categoria()
    cat.id = 0;
    cat.name = "Todas";
    categoriaList.push(cat);
    return this.httpClient.get(apiUrl).pipe(
      map(dbCategoriaList => {  
        for (let i in dbCategoriaList) {
          let categoria: Categoria = new Categoria();
          categoria.id = dbCategoriaList[i]['Id'];
          categoria.name = dbCategoriaList[i]['Nombre'];       
          categoriaList.push(categoria);
        }
        return categoriaList;         
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

}
