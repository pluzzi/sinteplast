import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { GlobalErrorHandler } from '../../app/core/errors/errors-handler';
import { Formulario } from '../../models/survey.model';
import { Question } from '../../models/question.model';
import { Option } from '../../models/option.model';
import { SettingsProvider } from '../settings/settings.provider';
import { FormularioGenerico } from '../../models/generalForms.model';

@Injectable()
export class FormularioProvider {

  constructor(
    public httpClient: HttpClient,
    public globalErrorHandler: GlobalErrorHandler,
    public settingsProvider:SettingsProvider
  ) {}

  getEncuestas(): Observable <Formulario[]>{
    let formularioList: Formulario[] = [];
    let apiUrl =  this.settingsProvider.getApiUrl() + '/forms/getForms/1';    
    return this.httpClient.get(apiUrl).pipe(
      map(dbFormularioList => {
        for (let i in dbFormularioList){
          formularioList.push(this.mapper(dbFormularioList[i]));
        }
        return formularioList;
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  getFormulariosGenericos(): Observable <Formulario[]>{
    let formularioList: Formulario[] = [];
    let apiUrl =  this.settingsProvider.getApiUrl() + '/forms/getForms/2';    
    return this.httpClient.get(apiUrl).pipe(
      map(dbFormularioList => {
        for (let i in dbFormularioList){
          formularioList.push(this.mapper(dbFormularioList[i]));
        }
        return formularioList;
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  getPreguntas(id: number){
    let apiUrl =  this.settingsProvider.getApiUrl() + '/forms/getQuestionsById/'+ id;
    return this.httpClient.get(apiUrl).pipe(
      map(dbQuestionList => {   
          let survey: Formulario = new Formulario();    
          survey.id = dbQuestionList['FormId'];                
          survey.title = dbQuestionList['Title'];
          survey.description = dbQuestionList['Description'];
          survey.formTypeId = dbQuestionList['FormTypeId'];
          let questionsList: Question[] = [];
          let questionsTemp: any = dbQuestionList['Questions'];  

          for(let i in questionsTemp){
            let q:Question = new Question();
            q.id  = questionsTemp[i]['Id'];
            q.question = questionsTemp[i]['Question'];
            q.questionTypeId = questionsTemp[i]['QuestionTypeId'];
            q.required  = questionsTemp[i]['Required'];
            q.answer  = questionsTemp[i]['Answer'];
            q.answerOptionId  = questionsTemp[i]['AnswerOptionId'];

            let optionList: Option[] = [];
            for(let opt in questionsTemp[i]['Options']){
              let op: Option = new Option();
              op.id = questionsTemp[i].Options[opt].Id;
              op.description = questionsTemp[i].Options[opt].Description;
              op.answerSelected = questionsTemp[i].Options[opt].AnswerSelected;
              op.isRightAnswer = questionsTemp[i].Options[opt].IsRightAnswer;
              op.enabled = questionsTemp[i].Options[opt].Enabled;
              optionList.push(op);
            }
            q.options = optionList;
            questionsList.push(q);
          }
          survey.questions = questionsList;
          return survey;
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  guardarForm(surveyResponses: any){
    let apiUrl =  this.settingsProvider.getApiUrl() + '/form/SaveForm';    
    return this.httpClient.post(apiUrl, surveyResponses).pipe(
      map(dbQuestionList => {
       return dbQuestionList;
      }),
      catchError(error => {
        return this.globalErrorHandler.handleError(error);
      })
    );
  }

  private mapper(dto: any): Formulario {
    let form: Formulario = new Formulario();
    form.id = dto['Id'];
    form.title = dto['Title'];
    form.description = dto['Description'];
    form.alreadyAnswered = dto['AlreadyAnswered'];
    form.uniqueReply = dto['UniqueReply'];
    form.reaccionId = dto['ReaccionId'];
    form.totalReacciones = dto['TotalReacciones'];  
    return form;
  }
}