import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GlobalErrorHandler } from '../../app/core/errors/errors-handler';
import { SettingsProvider } from '../settings/settings.provider';
import { Observable } from 'rxjs/Observable';
import { catchError, map } from 'rxjs/operators';
import { Galeria } from '../../models/galeria.model';
import { ElementoMultimedia } from '../../models/elementoMultimedia.model';


@Injectable()
export class GaleriaProvider {

  constructor(
    public httpClient: HttpClient,
    public globalErrorHandler: GlobalErrorHandler,
    public settingsProvider:SettingsProvider) {
  }

  obtenerGaleriaPorId(id:number): Observable<Galeria>{
    let apiUrl =  this.settingsProvider.getApiUrl() + '/galerias/' + id;
    return this.httpClient.get(apiUrl).pipe(
      map(dbGaleria => {
        return this.mapper(dbGaleria, true);
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  obtenerTodoGalerias(): Observable<Galeria[]> {
    let galeriaList: Galeria[] = [];
    let apiUrl =  this.settingsProvider.getApiUrl() + '/galerias';
    return this.httpClient.get(apiUrl).pipe(
      map(dbGaleriaList => {
        for (let i in dbGaleriaList) {
          galeriaList.push(this.mapper(dbGaleriaList[i], false));
        }
        return galeriaList;
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  obtenerGalerias(page:number): Observable<Galeria[]> {
    let galeriaList: Galeria[] = [];
    let apiUrl =  this.settingsProvider.getApiUrl() + '/galerias/page/' + page;
    return this.httpClient.get(apiUrl).pipe(
      map(dbGaleriaList => {
        for (let i in dbGaleriaList) {
          galeriaList.push(this.mapper(dbGaleriaList[i], false));
        }
        return galeriaList;
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  obtenerGaleriasPorCategoria(categoriaId: number): Observable <Galeria[]>{
    let galeriaList: Galeria[]=[]; 
    let apiUrl = this.settingsProvider.getApiUrl() + '/galerias/categoria/' + categoriaId;
    return this.httpClient.get(apiUrl).pipe(
      map(dbGaleriaList => {
        for (let i in dbGaleriaList){
          galeriaList.push(this.mapper(dbGaleriaList[i], false));
        }
        return galeriaList;
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  obtenerElementosMultimedia(galeriaId:number): Observable<ElementoMultimedia[]> {
    let elemMultimediaList: ElementoMultimedia[] = [];
    let apiUrl =  this.settingsProvider.getApiUrl() + '/galerias/' + galeriaId + '/multimedia';
    return this.httpClient.get(apiUrl).pipe(
      map(dbElementoList => {
        for (let i in dbElementoList) {
          let elemento: ElementoMultimedia = new ElementoMultimedia();
          elemento.rutaArchivo = dbElementoList[i]['RutaArchivo'];
          elemento.tipo = dbElementoList[i]['Tipo'];
          elemMultimediaList.push(elemento);
        }
        return elemMultimediaList;
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  private mapper(dto: any, isFull: boolean): Galeria {
    let galeria: Galeria = new Galeria();
    galeria.id = dto['Id'];
    galeria.titulo = dto['Titulo'];
    galeria.copete = dto['Copete'];
    galeria.categoria = dto['Categoria'];
    galeria.imagen = dto['ImagenPortada'];
    galeria.fecha = dto['FechaDesde'];
    galeria.fechaDesde = dto['FechaDesde'];
    galeria.destacada = dto['Destacada'];
    galeria.reaccionId = dto['ReaccionId'];
    galeria.totalReacciones = dto['TotalReacciones'];

    if(isFull){
      let elemMultimediaList: ElementoMultimedia[] = [];
      let elementosTemp: any = dto['ElementosMultimedia'];  
      for(let i in elementosTemp){
        let elemento: ElementoMultimedia = new ElementoMultimedia();
        elemento.rutaArchivo = elementosTemp[i]['RutaArchivo'];
        elemento.tipo = elementosTemp[i]['Tipo'];
        elemMultimediaList.push(elemento);
      }
      galeria.elementosMultimedia = elemMultimediaList;
    }
    return galeria;
  } 
}