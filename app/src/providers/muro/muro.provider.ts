import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GlobalErrorHandler } from '../../app/core/errors/errors-handler';
import { catchError, map } from 'rxjs/operators';
import { Publicacion } from '../../models/publicacion.model';
import { Observable } from 'rxjs/Observable';
import { SettingsProvider } from '../settings/settings.provider';

@Injectable()
export class MuroProvider {
  constructor(    
    public httpClient: HttpClient,
    public settingsProvider:SettingsProvider,
    public globalErrorHandler: GlobalErrorHandler
    ) {}
  
  obtenerPublicaciones(): Observable<Publicacion[]> {
    let publicacionList: Publicacion[] = [];
    let apiUrl =  this.settingsProvider.getApiUrl() + '/muro/all';
    return this.httpClient.get(apiUrl).pipe(
      map(dbPublicacionList => {
        for (let i in dbPublicacionList) {
          let publicacion: Publicacion = new Publicacion();
          publicacion.id = dbPublicacionList[i]['Id'];
          publicacion.tipo = dbPublicacionList[i]['Tipo'];
          publicacion.descripcionRegistro = dbPublicacionList[i]['DescRegistro'];
          publicacion.titulo = dbPublicacionList[i]['Titulo'];
          publicacion.copete = dbPublicacionList[i]['Copete'];
          publicacion.contenido = dbPublicacionList[i]['Contenido'];
          publicacion.imagen = dbPublicacionList[i]['Imagen'];
          publicacion.fecha = dbPublicacionList[i]['FechaAlta'];
          publicacion.reaccionId = dbPublicacionList[i]['ReaccionId'];
          publicacion.totalReacciones = dbPublicacionList[i]['TotalReacciones'];
          publicacionList.push(publicacion);
        }
        return publicacionList;
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }
}