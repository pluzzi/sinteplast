import { Option } from "./option.model";

export class Question{
    private Id: number;
    private Question: string;
    private QuestionTypeId: string;     
    private Required: boolean;
    private Answer: string;
    private AnswerOptionId: number;
    private Options: Option[];
    

    constructor(){}
    get id(): number {
        return this.Id;
    }
    set id(id: number) {
        this.Id = id;
    }
    get question(): string{
        return this.Question;
    }
    set question(question: string){
        this.Question = question;
    }
    get questionTypeId(): string{
        return this.QuestionTypeId;
    }
    set questionTypeId(questionTypeId: string){
        this.QuestionTypeId = questionTypeId;
    }
    get required(): boolean {
        return this.Required;
    }
    set required(required: boolean) {
        this.Required = required;
    }
    get answer(): string{
        return this.Answer;
    }
    set answer(answer: string){
        this.Answer = answer;
    }
    get answerOptionId(): number {
        return this.AnswerOptionId;
    }
    set answerOptionId(answerOptionId: number) {
        this.AnswerOptionId = answerOptionId;
    }
    get options(): Option[]{
        return this.Options;
    }
    set options(options: Option[]){                
        this.Options= options;        
    }    
}