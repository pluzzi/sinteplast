export class Usuario {
  private _id: number;
  private _name: string;
  private _lastName: string;  
  private _fullname: string;
  private _username: string;
  private _photo: string;
  private _area: string;
  private _birthDate: string;
  private _email: string;
  private _intern: string;
  private _phone: string;
  private _mobile: string;
  
  constructor(id:number) {
      this._id = id;
  }

  get id(): number {
      return this._id;
  }
  set id(value: number) {
      this._id = value;
  }
  get name(): string {
    return this._name;
  }
  set name(value: string) {
    this._name = value;
  }
  get fullname(): string {
    return this._fullname;
  }
  set fullname(value: string) {
    this._fullname = value;
  } 
  get lastName(): string {
    return this._lastName;
  }
  set lastName(value: string) {
    this._lastName = value;
  }
  get area(): string {
    return this._area;
  }
  set area(value: string) {
    this._area = value;
  }
  get phone(): string {
    return this._phone;
  }
  set phone(value: string) {
    this._phone = value;
  } 
  get mobile(): string {
    return this._mobile;
  }
  set mobile(value: string) {
    this._mobile = value;
  }   
  get intern(): string {
    return this._intern;
  }
  set intern(value: string) {
    this._intern = value;
  }   
  get username(): string {
    return this._username;
  }
  set username(value: string) {
    this._username = value;
  }
  get photo(): string {
    return this._photo;
  }
  set photo(value: string) {
    this._photo = value;
  }
  get email(): string {
    return this._email;
  }
  set email(value: string) {
    this._email = value;
  }
  get birthDate(): string {
    return this._birthDate;
  }
  set birthDate(value: string) {
    this._birthDate = value;
  }
}

