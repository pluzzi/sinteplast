export class TipoCatalogo{
    private _id: number;
    private _descripcion: string;
    private _iconoCatalogo: string;
    private _fondoCatalogo: string;
    private _empresaId: number;
    private _mail: string;

    get id(): number {
        return this._id;
    }
    set id(id: number) {
        this._id = id;
    }

    get descripcion(): string{
        return this._descripcion;
    }
    set descripcion(descripcion: string){
        this._descripcion = descripcion;
    }
    
    get iconoCatalogo(): string{
        return this._iconoCatalogo;
    }
    set iconoCatalogo(iconoCatalogo: string){
        this._iconoCatalogo = iconoCatalogo;
    }

    get fondoCatalogo(): string{
        return this._fondoCatalogo;
    }
    set fondoCatalogo(fondoCatalogo: string){
        this._fondoCatalogo = fondoCatalogo;
    }

    get empresaId(): number{
        return this._empresaId;
    }
    set empresaId(empresaId: number){
        this._empresaId = empresaId;
    }

    get mail(): string{
        return this._mail;
    }
    set mail(mail: string){
        this._mail = mail;
    }
}