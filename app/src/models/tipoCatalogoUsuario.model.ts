import { TipoCatalogo } from "./tipoCatalogo.model";
import { DateTime } from "ionic-angular";

export class TipoCatalogoUsuario{
    private _id: number;
    private _userId: number;
    private _tipoCatalogo: TipoCatalogo;
    private _puntosAcumulados: number;
    private _fechaActualizacionPuntos: DateTime;

    get id(): number {
        return this._id;
    }
    set id(id: number) {
        this._id = id;
    }

    get userId(): number{
        return this._userId;
    }
    set userId(userId: number){
        this._userId = userId;
    }
    
    get tipoCatalogo(): TipoCatalogo{
        return this._tipoCatalogo;
    }
    set tipoCatalogo(tipoCatalogo: TipoCatalogo){
        this._tipoCatalogo = tipoCatalogo;
    }

    get puntosAcumulados(): number{
        return this._puntosAcumulados;
    }
    set puntosAcumulados(puntosAcumulados: number){
        this._puntosAcumulados = puntosAcumulados;
    }

    get fechaActualizacionPuntos(): DateTime{
        return this._fechaActualizacionPuntos;
    }
    set fechaActualizacionPuntos(fechaActualizacionPuntos: DateTime){
        this._fechaActualizacionPuntos = fechaActualizacionPuntos;
    }
}