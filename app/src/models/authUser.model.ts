export class AuthUser {
    private _shouldModifyPassword: boolean;
    private _token: string;

    get authToken(): string {
        return this._token;
    }
    set authToken(value: string) {
        this._token = value;
    }  
    set shouldModifyPassword(value: boolean) {
        this._shouldModifyPassword = value;
    }
    get shouldModifyPassword() {
        return this._shouldModifyPassword;
    }    
}