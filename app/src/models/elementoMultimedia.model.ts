export class ElementoMultimedia {
    private _rutaArchivo: string;
    private _tipo: string;
     
    get rutaArchivo(): string{
        return this._rutaArchivo;
    }
    set rutaArchivo(value: string){
        this._rutaArchivo = value;
    }

    get tipo(): string{
        return this._tipo;
    }
    set tipo(value: string){
        this._tipo = value;
    }
}