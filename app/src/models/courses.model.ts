export class Capacitacion{
    private _id : number;
    private _title : string;
    private _description: string;
    private  _courseCategoryId : number;
    private  _courseCategory : string;
    private  _courseTypeId : number;
    private  _courseType : string;
    private  _documentURL : string;
    private  _extension : string;
    private  _videoURL : string;
    private  _linkURL :string;
    private  _requiresEvaluation : boolean;  
    private  _userWasAlreadyEvaluated : string;
    private  _approvedPercRequired : string;
    private  _approvedPercUser : string;
    private  _formId : number;
    private  _formUniqueReply : number;
    private  _evaluationDate : string;
    private _reaccionId: number;
    private _totalReacciones: number;      
    
    get id(): number {
        return this._id;
    }
    set id(id: number) {
        this._id = id;
    }
    get title(): string {
        return this._title;
    }
    set title(title: string) {
        this._title = title;
    } 
    get description(): string {
        return this._description;
    }
    set description(description: string) {
        this._description = description;
    } 
    get courseCategoryId(): number {
        return this._courseCategoryId;
    }
    set courseCategoryId(_courseCategoryId: number) {
        this._courseCategoryId = _courseCategoryId;
    }
    get courseCategory(): string {
        return this._courseCategory;
    }
    set courseCategory(courseCategory: string) {
        this._courseCategory = courseCategory;
    } 
    get courseTypeId(): number{
        return this._courseTypeId;
    }
    set courseTypeId(courseTypeId: number){
        this._courseTypeId = courseTypeId;
    }
    get courseType(): string{
        return this._courseType;
    }
    set courseType(courseType: string){
        this._courseType = courseType;
    }
    get documentURL(): string {
        return this._documentURL;
    }
    set documentURL(documentURL: string) {
        this._documentURL = documentURL;
    } 
    get extension(): string {
        return this._extension;
    }
    set extension(extension: string) {
        this._extension = extension;
    } 
    get videoURL(): string {
        return this._videoURL;
    }
    set videoURL(videoURL: string) {
        this._videoURL = videoURL;
    } 
    get linkURL(): string {
        return this._linkURL;
    }
    set linkURL(linkURL: string) {
        this._linkURL = linkURL;
    } 
    get requiresEvaluation(): boolean {
        return this._requiresEvaluation;
    }
    set requiresEvaluation(requiresEvaluation: boolean) {
        this._requiresEvaluation = requiresEvaluation;
    } 
    get userWasAlreadyEvaluated(): string {
        return this._userWasAlreadyEvaluated;
    }
    set userWasAlreadyEvaluated(userWasAlreadyEvaluated: string) {
        this._userWasAlreadyEvaluated = userWasAlreadyEvaluated;
    } 
    get approvedPercRequired(): string {
        return this._approvedPercRequired;
    }
    set approvedPercRequired(approvedPercRequired: string) {
        this._approvedPercRequired = approvedPercRequired;
    } 
    get approvedPercUser(): string {
        return this._approvedPercUser;
    }
    set approvedPercUser(approvedPercUser: string) {
        this._approvedPercUser = approvedPercUser;
    }
    get formId(): number {
        return this._formId;
    }
    set formId(formId: number) {
        this._formId = formId;
    }
    get formUniqueReply(): number {
        return this._formUniqueReply;
    }
    set formUniqueReply(formUniqueReply: number) {
        this._formUniqueReply = formUniqueReply;
    }
    get evaluationDate(): string {
        return this._evaluationDate;
    }
    set evaluationDate(evaluationDate: string) {
        this._evaluationDate = evaluationDate;
    }
    get reaccionId(): number {
        return this._reaccionId;
    }
    set reaccionId(value: number) {
        this._reaccionId = value;
    }
    get totalReacciones(): number {
        return this._totalReacciones;
    }
    set totalReacciones(value: number) {
        this._totalReacciones = value;
    }

}


