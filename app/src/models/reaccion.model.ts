export class Reaccion {
    private _id: number;
    private _nombre: string;
    private _icono: string;

    get id(): number{
        return this._id;
    }
    set id(id: number){
        this._id = id;
    }
    get nombre(): string{
        return this._nombre;
    }
    set nombre(value: string){
        this._nombre = value;
    }
    get icono(): string{
        return this._icono;
    }
    set icono(value: string){
        this._icono = value;
    }    
}