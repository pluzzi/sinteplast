import { ElementoMultimedia } from "./elementoMultimedia.model";

export class Galeria {
    private _id: number;
    private _titulo: string;
    private _copete: string;
    private _categoria: string;
    private _imagenPortada: string;
    private _elementosMultimedia: ElementoMultimedia[];
    private _fecha: string;
    private _fechaDesde: string;
    private _destacada: boolean;
    private _reaccionId: number;
    private _totalReacciones: number;

    get id(): number{
        return this._id;
    }
    set id(id: number){
        this._id = id;
    }
    get titulo(): string{
        return this._titulo;
    }
    set titulo(value: string){
        this._titulo = value;
    }
    get copete(): string{
        return this._copete;
    }
    set copete(value: string){
        this._copete = value;
    }    
    get categoria(): string{
        return this._categoria;
    }
    set categoria(value: string){
        this._categoria = value;
    }   
    get imagen(): string{
        return this._imagenPortada;
    }
    set imagen(value: string){
        this._imagenPortada = value;
    }
    get fecha(): string{
        return this._fecha;
    }
    set fecha(value: string){
        this._fecha = value;
    } 
    get fechaDesde(): string{
        return this._fechaDesde;
    }
    set fechaDesde(value: string){
        this._fechaDesde = value;
    }      
    get destacada(): boolean{
        return this._destacada;
    }
    set destacada(value: boolean){
        this._destacada = value;
    }       
    get elementosMultimedia(): ElementoMultimedia[]{
        return this._elementosMultimedia;
    }
    set elementosMultimedia(value: ElementoMultimedia[]){                
        this._elementosMultimedia= value;        
    }

    get reaccionId(): number {
        return this._reaccionId;
    }
    set reaccionId(value: number) {
        this._reaccionId = value;
    }
    get totalReacciones(): number {
        return this._totalReacciones;
    }
    set totalReacciones(value: number) {
        this._totalReacciones = value;
    }
}