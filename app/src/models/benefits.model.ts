export class Beneficio {
    private _id: number;
    private _title: string;
    private _subtitle: string;
    private _content: string;
    private _image: string;
    private _liked: boolean;
    private _date: string;
    private _reaccionId: number;
    private _totalReacciones: number;
    private _puntos: number;
    private _tipoCatalogoProductosId: number;
    private _esNuevo: boolean;

    get id(): number{
        return this._id;
    }
    set id(id: number){
        this._id = id;
    }
    get titulo(): string{
        return this._title;
    }
    set titulo(title: string){
        this._title = title;
    }
    get copete(): string{
        return this._subtitle;
    }
    set copete(subtitle: string){
        this._subtitle = subtitle;
    }
    get contenido(): string{
        return this._content;
    }
    set contenido(content: string){
        this._content = content;
    }
    get imagen(): string{
        return this._image;
    }
    set imagen(image: string){
        this._image = image;
    }
    get liked(): boolean{
        return this._liked;
    }
    set liked(liked: boolean){
        this._liked = liked;
    }
    get fechaAlta(): string {
        return this._date;
    }
    set fechaAlta(date: string) {
        this._date = date;
    }

    get reaccionId(): number {
        return this._reaccionId;
    }
    set reaccionId(value: number) {
        this._reaccionId = value;
    }
    get totalReacciones(): number {
        return this._totalReacciones;
    }
    set totalReacciones(value: number) {
        this._totalReacciones = value;
    }

    get puntos(): number {
        return this._puntos;
    }
    set puntos(value: number) {
        this._puntos = value;
    }

    get tipoCatalogoProductosId(): number {
        return this._tipoCatalogoProductosId;
    }
    set tipoCatalogoProductosId(value: number) {
        this._tipoCatalogoProductosId = value;
    }

    get esNuevo(): boolean {
        return this._esNuevo;
    }
    set esNuevo(value: boolean) {
        this._esNuevo = value;
    }
}