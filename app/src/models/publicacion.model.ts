export class Publicacion {
    
    private _id: number;
    private _titulo: string;
    private _copete: string;
    private _contenido: string;
    private _fecha: string;
    private _imagen: string;
    private _descRegistro: string;
    private _tipo: string;
    private _reaccionId: number;
    private _totalReacciones: number;

    get id(): number {
        return this._id;
    }
    set id(value: number) {
        this._id = value;
    }
    get titulo(): string {
        return this._titulo;
    }
    set titulo(title: string) {
        this._titulo = title;
    }

    get descripcionRegistro(): string {
        return this._descRegistro;
    }
    set descripcionRegistro(value: string) {
        this._descRegistro = value;
    } 
    
    get copete(): string {
        return this._copete;
    }
    set copete(value: string) {
        this._copete = value;
    }  
    get contenido(): string {
        return this._contenido;
    }
    set contenido(content: string) {
        this._contenido = content;
    }
    get tipo(): string {
        return this._tipo;
    }
    set tipo(value: string) {
        this._tipo = value;
    }
    get fecha(): string {
        return this._fecha;
    }
    set fecha(date: string) {
        this._fecha = date;
    }
    get imagen(): string {
        return this._imagen;
    }
    set imagen(image: string) {
        this._imagen = image;
    }

    get reaccionId(): number {
        return this._reaccionId;
    }
    set reaccionId(value: number) {
        this._reaccionId = value;
    }
    get totalReacciones(): number {
        return this._totalReacciones;
    }
    set totalReacciones(value: number) {
        this._totalReacciones = value;
    }
}