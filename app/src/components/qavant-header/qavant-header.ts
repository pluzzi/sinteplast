import { Component, Input } from '@angular/core';

@Component({
  selector: 'qavant-header',
  templateUrl: 'qavant-header.html'
})
export class QavantHeaderComponent {
  @Input() title : string;
  constructor() {
  }
}
