import { Component } from '@angular/core';

/**
 * Generated class for the QavantCardEncuestaComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'qavant-card-encuesta',
  templateUrl: 'qavant-card-encuesta.html'
})
export class QavantCardEncuestaComponent {

  text: string;

  constructor() {
    console.log('Hello QavantCardEncuestaComponent Component');
    this.text = 'Hello World';
  }

}
