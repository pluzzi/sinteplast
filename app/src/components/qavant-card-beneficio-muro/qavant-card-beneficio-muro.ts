import { Component, Input, OnInit } from '@angular/core';
import { BeneficioProvider } from '../../providers/benefits/benefits.provider';
import { NavController } from 'ionic-angular';
import { BenefitsDetailPage } from '../../pages/benefits-detail/benefits-detail';

@Component({
  selector: 'qavant-card-beneficio-muro',
  templateUrl: 'qavant-card-beneficio-muro.html'
})
export class QavantCardBeneficioMuroComponent implements OnInit {
  @Input() entidad : any;
  puntos: number = 0;

  constructor(
    public beneficioProvider: BeneficioProvider,
    public navCtrl: NavController
  ) {
    
  }

  ngOnInit(){
    this.getBenefitBydId(this.entidad.id);
  }

  getBenefitBydId(id:number){
    this.beneficioProvider.getBeneficioById(id).subscribe(
      data => { 
        this.puntos = data.puntos;
      },
      error => {
        console.log(error);
      }
    );
  }

  goToDetail() {
    this.navCtrl.push(BenefitsDetailPage, {id: this.entidad.id, canjear: true });
    //let mostrarCanjear = beneficio.puntos <= this.puntoAcumulados;
    //this.navCtrl.push(BenefitsDetailPage, {id: beneficio.id, canjear: mostrarCanjear});
  }

}
