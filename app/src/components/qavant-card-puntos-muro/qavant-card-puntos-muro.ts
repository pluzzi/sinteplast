import { Component, Input } from '@angular/core';

@Component({
  selector: 'qavant-card-puntos-muro',
  templateUrl: 'qavant-card-puntos-muro.html'
})
export class QavantCardPuntosMuroComponent {
  @Input() nombre: string;
  @Input() puntos: string;

  constructor() { }

}
