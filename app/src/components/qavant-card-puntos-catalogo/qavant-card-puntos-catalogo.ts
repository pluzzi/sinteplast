import { Component, Input } from '@angular/core';
import { Img, NavController } from 'ionic-angular';
import { BenefitsListPage } from '../../pages/benefits-list/benefits-list';

@Component({
  selector: 'qavant-card-puntos-catalogo',
  templateUrl: 'qavant-card-puntos-catalogo.html'
})
export class QavantCardPuntosCatalogoComponent {
  @Input() img: string;
  @Input() puntos: number;
  @Input() id: number;

  constructor(
    public navCtrl: NavController
  ) { }

  goToBenefit(){
    this.navCtrl.push(BenefitsListPage, { id: this.id, puntos: this.puntos });
  }

}
