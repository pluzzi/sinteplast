import { Component, Input, ViewChild } from '@angular/core';
import { NavController, Slides } from 'ionic-angular';
import { GaleriaProvider } from '../../providers/galeria/galeria.provider';
import { HelperProvider } from '../../providers/helper/helper.provider';
import { ElementoMultimedia } from '../../models/elementoMultimedia.model';
import { GaleriaDetailPage } from '../../pages/galeria-detail/galeria-detail';

@Component({
  selector: 'qavant-card-galeria',
  templateUrl: 'qavant-card-galeria.html'
})
export class QavantCardGaleriaComponent {
  @ViewChild(Slides) slides: Slides;
  @Input() entidad : any;
  isLast:boolean = false;
  elementoMultimediaList: ElementoMultimedia[] = [];

  constructor(
    public navCtrl: NavController, 
    public galeriaProvider: GaleriaProvider,
    public helper: HelperProvider) {
  }

  ngOnInit(){
    let galeriaId: number = this.entidad.id;
    let elementoPortada: ElementoMultimedia = new ElementoMultimedia();
    elementoPortada.rutaArchivo = this.entidad.imagen;
    elementoPortada.tipo = "IMG";

    this.galeriaProvider.obtenerElementosMultimedia(galeriaId).subscribe(
      data => {
        this.elementoMultimediaList = data;
        this.elementoMultimediaList.push(elementoPortada);
        if(this.elementoMultimediaList.length == 1) { this.isLast = true; }
      },
      error => {
        this.helper.showError(error)
      }
    );
  }

  slideChanged() {
    if (this.slides.isEnd()) {
      this.isLast = true;
    } else {
      this.isLast = false;
    }
  }  

  goToGaleria(id: number) {
    this.navCtrl.push(GaleriaDetailPage, {id: id});
  }
}