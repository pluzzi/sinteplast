import { Component, Input, OnInit} from '@angular/core';
import { DateTime } from 'ionic-angular';
import { DatePipe } from '@angular/common';

/**
 * Generated class for the QavantCardPuntosComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'qavant-card-puntos',
  templateUrl: 'qavant-card-puntos.html',
  providers:[DatePipe]
})
export class QavantCardPuntosComponent implements OnInit {
  @Input() nombre: string;
  @Input() fondo: string;
  @Input() puntos: string;
  @Input() fecha: DateTime;

  fechaFormatted: string;

  constructor(
    public datePipe: DatePipe
  ) {}

  ngOnInit(){
    this.fechaFormatted = this.datePipe.transform(this.fecha, "dd/MM/yyyy");
  }

  log(){
    console.log(this.fondo);
  }
}
