import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage.provider';
import { UserProvider } from '../../providers/user/user.provider';

@Component({
  selector: 'page-chatbot',
  templateUrl: 'chatbot.html',
})
export class ChatbotPage implements OnInit{

  constructor(
    public navCtrl: NavController, 
    private storageProvider: StorageProvider,
    private userProvider: UserProvider,
    public navParams: NavParams){

  }

  ngOnInit() {
    var dl = (<any>window).WebChat.createDirectLine({ secret: 'NijajWkN834.cwA.HIs.c22umyfD8DGviGdlIwLbZYIC1rdQL8h9NMSxtYEaPSM' });
    
    (<any>window).WebChat.renderWebChat({
      directLine: dl,
      userID: this.userProvider.getUserId(),
      resize: 'detect'
    }, (<any>document).getElementById('bot'));
    
    dl.postActivity({
      from: {
        id: this.userProvider.getUserId(),
        name: this.userProvider.getUsername()
      },
      name: 'requestWelcomeDialog',
      type: 'event',
      value: ''
    }).subscribe(function (id) {
      console.log('"trigger requestWelcomeDialog" sent');
    });
  }

  ionViewDidLoad() {
  }

  ionViewWillLeave() {
    delete (<any>window).BotChat;
  }
}