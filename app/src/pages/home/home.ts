import { Component, OnInit } from '@angular/core';
import { NavController, ToastController, LoadingController, Loading, NavParams, Events, App } from 'ionic-angular';
import { MuroProvider } from '../../providers/muro/muro.provider';
import { Publicacion } from '../../models/publicacion.model';
import { SettingsProvider } from '../../providers/settings/settings.provider';
import { SurveysDetailPage } from '../surveys-detail/surveys-detail';
import { FormularioProvider } from '../../providers/surveys/surveys.provider';
import { FormularioGenericoListPage } from '../formulario-generico-list/formulario-generico-list';
import { TipoFormulario } from '../../models/tipoForm.enum';
import { Formulario } from '../../models/survey.model';
import { HelperProvider } from '../../providers/helper/helper.provider';
import { TipoCatalogoProvider } from '../../providers/tipo-catalogo/tipo-catalogo.provider';
import { UserProvider } from '../../providers/user/user.provider';
import { TipoCatalogoUsuario } from '../../models/tipoCatalogoUsuario.model';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage implements OnInit {
  muroList: Publicacion[] = [];
  formularios: Formulario[] = [];
  loading: Loading;
  isReady: boolean = false;
  tipoCatalogoUsuario: TipoCatalogoUsuario[] = [];
  sliderConfig = {
    autoplay: 800,
    loop: true
  };

  constructor(
    private app:App,
    public events: Events,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public helper: HelperProvider,
    public muroProvider: MuroProvider,
    public loadingController:LoadingController,
    public toastCtrl: ToastController,
    public formularioProvider: FormularioProvider,
    public settingsProvider: SettingsProvider,
    public tipoCatalogoProvider: TipoCatalogoProvider,
    public userProvider: UserProvider
    ) { }

  ngOnInit(){}

  ionViewWillEnter(){
    this.cargarMuro();
    document.getElementById("search").style.display = "block";
  }

  ionViewWillLeave() {
    this.settingsProvider.setPendingRefresh(true);
    document.getElementById("search").style.display = "none";
  }

  goToFormulario(form: Formulario){
    this.app.getRootNav().setRoot(SurveysDetailPage, {id : form.id, title : "Formulario", tipoForm: TipoFormulario.Generico});
  }

  goToFormularioGenericoList(){
    this.app.getRootNav().setRoot(FormularioGenericoListPage);
  }

  private cargarMuro(){
    window.addEventListener("contextmenu", (e) => { e.preventDefault(); });
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();  

    if( this.settingsProvider.getPendingRefresh() ){
      // Obtenemos settings de tenant
      this.settingsProvider.getSettings().subscribe(
        settings => { 
          this.events.publish('menu:populate', settings.menu);
          this.events.publish('theme:populate', settings.theme);

          // Obtenemos publicaciones del muro
          this.muroProvider.obtenerPublicaciones().subscribe(
            publicaciones => { 
              this.isReady = true;
              this.muroList = publicaciones;

              // Obtenemos los formularios genericos
              this.formularioProvider.getFormulariosGenericos().subscribe(
                formularios => {       
                  this.formularios = formularios;
                  this.loading.dismiss();
                },
                error => { 
                  this.loading.dismiss();
                  this.helper.showError(error);
                }
              );

            },
            error => { 
              this.loading.dismiss();        
              this.helper.showError(error);;
            }
          );

          

        },
        error => { 
          this.loading.dismiss();
          this.helper.showError(error);
        }
      );
    } else {
      // Recien logueado: traer solo publicaciones del muro
      this.muroProvider.obtenerPublicaciones().subscribe(
        data => { 
          this.isReady = true;
          this.muroList = data;

          this.formularioProvider.getFormulariosGenericos().subscribe(
            formularios => {       
              this.formularios = formularios;
              this.loading.dismiss();        
            },
            error => { 
              this.loading.dismiss();
              this.helper.showError(error);
            }
          );
        },
        error => { 
          this.loading.dismiss();        
          this.helper.showError(error);
        }
      );
    }

    // Obtenemos los tipos de catalogos
    this.tipoCatalogoProvider.obtenerTipoCatalogosPorUsuarioId(this.userProvider.getUserId().toString()).subscribe(
      result => { 
        this.tipoCatalogoUsuario = result;
        console.log(this.tipoCatalogoUsuario);
      },
      error => { 
        this.loading.dismiss();        
        this.helper.showError(error);;
      }
    );

  }
}