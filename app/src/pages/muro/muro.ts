import { Component } from '@angular/core';
import { NavController, App } from 'ionic-angular';
import { WhoiswhoListPage } from '../whoiswho-list/whoiswho-list';
import { ProfilePage } from '../profile/profile';
import { SettingsProvider } from '../../providers/settings/settings.provider';
import { CataloguePage } from '../catalogue/catalogue';
import { HomePage } from '../home/home';
import { OrderPage } from '../order/order';
import { NewsListPage } from '../news-list/news-list';
import { SplashScreen } from '@ionic-native/splash-screen';

@Component({
  selector: 'page-muro',
  templateUrl: 'muro.html',
})
export class MuroPage {
  tab1Root = HomePage;
  tab2Root = CataloguePage;
  tab3Root = ProfilePage;
  tab4Root = NewsListPage;

  constructor(
    public navCtrl: NavController, 
    private app: App, 
    public settingsProvider: SettingsProvider
    ) { }

  goToQuienEsQuienPage() {
    this.app.getRootNav().setRoot(WhoiswhoListPage);
  }

  ionViewWillLeave() {
    this.settingsProvider.setPendingRefresh(true);
  }
}