import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Loading } from 'ionic-angular';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { BeneficioProvider } from '../../providers/benefits/benefits.provider';
import { Beneficio } from '../../models/benefits.model';
import { BeneficioCategoria } from '../../models/benefitCategory.model';
import { LocalRequestOrigin } from '../../models/LocalRequestOrigin.enum';
import { HelperProvider } from '../../providers/helper/helper.provider';
import { BenefitsDetailPage } from '../benefits-detail/benefits-detail';


@Component({
  selector: 'page-benefits-list',
  templateUrl: 'benefits-list.html',
})
export class BenefitsListPage implements OnInit {
  beneficioList: Beneficio[] = [];
  page: number;
  pageLikes: number;
  selectCategory: string = "";
  categoriesList: BeneficioCategoria[] = [];
  flagTabSelected = true;
  selectedCat: string = "";
  benefitsFavByCat: Beneficio [] = [];
  loading: Loading;
  tipoCatalogoId: number;
  puntoAcumulados: number;

  beneficioList1: Beneficio[] = [];
  beneficioList2: Beneficio[] = [];
    
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public helper: HelperProvider,
    public loadingController:LoadingController,
    public toastCtrl: ToastController,
    public beneficioProvider: BeneficioProvider) {
      this.page = 1;
      this.pageLikes = 1;
  }

  ngOnInit(){
    this.tipoCatalogoId = this.navParams.get("id");
    this.puntoAcumulados = this.navParams.get("puntos");
    console.log(this.tipoCatalogoId);
    console.log(this.puntoAcumulados);
    this.beneficioList1 = [];
    this.beneficioList2 = [];
    this.getBeneficios(1);
  }  

  ionViewWillEnter(){
    let params:any = this.navParams.get('params');
  }

  getBeneficios(page: number){
    this.page = page;
    this.flagTabSelected = true;
    this.selectedCat = "";
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();
    this.beneficioProvider.getBeneficios(this.page, this.tipoCatalogoId).subscribe(
      data => {
        this.beneficioList = data;
        this.addNewData(data);
        this.loading.dismiss();
        console.log(data);     
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }

  goToDetail(beneficio: Beneficio){
    console.log(this.puntoAcumulados);
    let mostrarCanjear = beneficio.puntos <= this.puntoAcumulados;
    this.navCtrl.push(BenefitsDetailPage, {id: beneficio.id, canjear: mostrarCanjear});
  }

  doInfinite(infiniteScroll) {
    this.page++;
    this.pageLikes++;
    if(this.flagTabSelected){
      setTimeout(() => {
        this.beneficioProvider.getBeneficios(this.page, this.tipoCatalogoId).subscribe(
          data => { 
            console.log(data);
            for(let i=0; i < data.length; i++) {            
              this.beneficioList.push(data[i]);
            }     
            this.addNewData(data);     
          },
          error => {
            this.loading.dismiss();
            this.helper.showError(error);
          }
        );
        infiniteScroll.complete();
      }, 500);
    }else{
      setTimeout(() => {
        this.beneficioProvider.getBenefitsLikes(this.pageLikes).subscribe(
          data => { 
            for(let i=0; i < data.length; i++) {            
              this.beneficioList.push(data[i]);
            }    
            this.addNewData(data);      
          },
          error => {
            this.loading.dismiss();
            this.helper.showError(error);
          }
        );
        infiniteScroll.complete();
      }, 500);
    }
  }

  doRefresh(refresher) {
    if(this.flagTabSelected){
      setTimeout(() => {
        this.beneficioList1 = [];
        this.beneficioList2 = [];
        this.page = 1;
        this.bandera = false;
        this.getBeneficios(this.page);
        refresher.complete();
      }, 2000);
    }else{
      setTimeout(() => {
        this.pageLikes = 1;
        refresher.complete();
      }, 2000);
    }
  }

  bandera: boolean = false;

  addNewData(data){
    for(let i=0; i < data.length; i++) {
      if(this.bandera){
        this.beneficioList2 = this.beneficioList2.concat(data[i]);
        console.log("Add col 2");
      }else{
        this.beneficioList1 = this.beneficioList1.concat(data[i]);
        console.log("Add col 1");
      }
      this.bandera = !this.bandera;
      
    }
  }


}
