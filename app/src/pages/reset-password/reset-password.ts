import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, AlertController, Loading } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationProvider } from '../../providers/authentication/authentication.provider';
import { HelperProvider } from '../../providers/helper/helper.provider';
import { SettingsProvider } from '../../providers/settings/settings.provider';

@Component({
  selector: 'page-reset-password',
  templateUrl: 'reset-password.html',
})
export class ResetPasswordPage implements OnInit {
  resetForm: FormGroup;
  loading: Loading;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public helper: HelperProvider,
    public formBuilder: FormBuilder,
    public loadingController:LoadingController, 
    public toastCtrl: ToastController,
    private alertCtrl: AlertController,
    public authProvider: AuthenticationProvider,
    public settingsProvider: SettingsProvider) { }

  ngOnInit(){
    this.resetForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required])],
      empresa: ['', Validators.compose([Validators.nullValidator])]
    });    
  }

  resetPassword() {
		let form = this.resetForm.value;
		if (!form.username) {
			return;
    }
    let data = { 
      UserName: form.username,
      DireccionApp: this.settingsProvider.getEmpresa()
    };
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();
    this.authProvider.resetPassword(data).subscribe(
      () => {
        this.loading.dismiss();
        this.showAlert();
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }

  showAlert(){
    let alert = this.alertCtrl.create({
      title: 'Resetear contraseña',
      message: 'Se reinició su contraseña. Para volver a activar su usuario utilice su nombre de usuario en los campos USUARIO y CONTRASEÑA.',
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            this.backToLogin();
          }
        }
      ]
    });
    alert.present();
  }

  backToLogin(){
    this.navCtrl.pop();
  }

}
