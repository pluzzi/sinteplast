import { Component, OnInit, HostListener } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Loading } from 'ionic-angular';
import { Info } from '../../models/usefulinfo.model';
import { UsefulInfoProvider } from '../../providers/useful-info/useful-info.provider';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { LocalRequestOrigin } from '../../models/LocalRequestOrigin.enum';
import { HelperProvider } from '../../providers/helper/helper.provider';

@Component({
  selector: 'page-useful-info-detail',
  templateUrl: 'useful-info-detail.html',
})
export class UsefulInfoDetailPage implements OnInit {
  localRequestOrigin:LocalRequestOrigin;
  info: Info;
  loading: Loading;
  isReady:boolean = false;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public helper: HelperProvider,
    public loadingController:LoadingController,
    private theInAppBrowser: InAppBrowser,
    public usefulInfoProvider: UsefulInfoProvider,
    public toastCtrl: ToastController) {
      this.localRequestOrigin = this.navParams.get('localRequestOrigin');
  }

  ngOnInit() {
    let infoId: number;
    if (this.localRequestOrigin == LocalRequestOrigin.Menu) {
      let params:any = this.navParams.get('params');
      infoId = params.entidadId;
    } else {
      infoId = this.navParams.data.id;
    }
    this.getInfoById(infoId);
  }

  getInfoById(id: number){
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();    
    this.usefulInfoProvider.getInfoById(id).subscribe(
      data => { 
        this.isReady = true;
        this.info = data;
        this.loading.dismiss();
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );

  }

  @HostListener('document:click', ['$event'])
  handleClick(event: EventClick) {
     if(event.target && event.target.href){
         this.openBrowser(event.target.href);
     }
     return false; 
  }

  openBrowser(url){
    const options: InAppBrowserOptions = {
      location:'no'
    };
    this.theInAppBrowser.create(url, '_system', options);
  }




}
