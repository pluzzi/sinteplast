import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, AlertController, Loading } from 'ionic-angular';
import { CoursesProvider } from '../../providers/courses/courses.provider';
import { Capacitacion} from '../../models/courses.model';
import { FileOpener } from '@ionic-native/file-opener';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Device } from '@ionic-native/device';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SurveysDetailPage } from '../surveys-detail/surveys-detail';
import { TipoFormulario } from '../../models/tipoForm.enum';
import { HelperProvider } from '../../providers/helper/helper.provider';

@Component({
  selector: 'page-courses-detail',
  templateUrl: 'courses-detail.html',
})
export class CoursesDetailPage implements OnInit{
  course: Capacitacion;  
  btnDocDisable: boolean;
  btnLinkDisable: boolean;
  btnVideoDisable: boolean;
  videoSrc: string;
  loading: Loading;
  isReady:boolean = false;
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public loadingController:LoadingController,
    public helper: HelperProvider,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public coursesProvider: CoursesProvider,
    private fileOpener: FileOpener,
    private transfer: FileTransfer,
    private file: File,
    private device: Device,
    private inAppBrowser: InAppBrowser) {      
  }

  ngOnInit() {
    let capacitacionId: number = this.navParams.data.id;
    this.course = this.navParams.data;
    this.getCapacitacionById(capacitacionId);   
    this.btnDocDisable= true;
    this.btnLinkDisable = true;
    this.btnVideoDisable = true;
  }

  getCapacitacionById(id: number) {
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();    
    this.coursesProvider.getCapacitacionById(id).subscribe(
      data => {     
        this.isReady = true;   
        this.course = data;      
        this.loading.dismiss();        
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }

  openDocument(document){ 
    this.btnDocDisable = false;
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present(); 
    let devicePlatform = this.device.platform;   
    console.log(" Storage "+ this.file.dataDirectory);
    //let targetPath = this.file.externalDataDirectory;
    let targetPath = "";
    if(devicePlatform == 'Android'){ 
      targetPath = this.file.externalDataDirectory;
    }
    if(devicePlatform == 'iOS'){      
      targetPath = this.file.tempDirectory;
    }    
    const fileTransfer: FileTransferObject = this.transfer.create();  
    const url = encodeURI(document.documentURL); 
    var filename = url.split("/").pop(); 
    var trustHosts = true;   

    fileTransfer.download(url, targetPath + filename, trustHosts).then((entry) => {
      console.log('download complete: ' + entry.toURL());     
      var mimeType = '';
      var openErrorMessage = '';
      switch (document.extension.toLowerCase()) {
        case 'pdf':
          mimeType = 'application/pdf';
          openErrorMessage = 'Adobe Acrobat Reader';
          break;
        case 'jpg':
        case 'jpeg':
          mimeType = 'image/jpeg';
          break;
        case 'png':
          mimeType = 'image/png';
          break;
        case 'xls':
          mimeType = 'application/vnd.ms-excel';
          openErrorMessage = 'Microsoft Excel';
          break;
        case 'xlsx':
          mimeType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
          openErrorMessage = 'Microsoft Excel';
          break;
        case 'doc':
          mimeType = 'application/msword';
          openErrorMessage = 'Microsoft Word';
          break;
        case 'docx':
          mimeType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
          openErrorMessage = 'Microsoft Word';
          break;
        case 'ppt':
        case 'pps':
          mimeType = 'application/vnd.ms-powerpoint';
          openErrorMessage = 'Microsoft Power Point';
          break;
        case 'pptx':
          mimeType = 'application/vnd.openxmlformats-officedocument.presentationml.presentation';
          openErrorMessage = 'Microsoft Power Point';
          break;
        case 'ppsx':
          mimeType = 'application/vnd.openxmlformats-officedocument.presentationml.slideshow';
          openErrorMessage = 'Microsoft Power Point';
          break;
      }
      //mimeType= ""; 
      this.fileOpener.open(targetPath + filename , mimeType)
      .then((data) =>{ 
        this.loading.dismissAll();        
        this.btnDocDisable = false;        
        console.log(' File is OPENED!!! ')
      },
      (err)=>{   
        this.loading.dismissAll();         
        if(err.status == 9){
          let alert = this.alertCtrl.create({
            title: 'ERROR AL ABRIR EL DOCUMENTO',
            message: 'No se ha encontrado la aplicación necesaria para abrir el documento.',
            buttons: [
              {
                text: 'Aceptar',
                handler: () => {}
              }
            ]
          });
          alert.present();
        }else{
          let alert = this.alertCtrl.create({
            title: 'NO SE PUDO ABRIR EL DOCUMENTO',
            message: 'No se ha encontrado la aplicación necesaria para abrir el documento.',
            buttons: [
              {
                text: 'Aceptar',
                handler: () => {}
              }
            ]
          });
          alert.present();
        }
      })
    }, 
    (error) => {
      console.log("handle error-> " + JSON.stringify(error));      
    });
  }

  openLink(url: string){
    this.btnLinkDisable = false;
    const browser = this.inAppBrowser.create(url, '_blank', 'location=no');

    let watchs = browser.on('loadstart').subscribe(function(event){
      this.btnLinkDisable = false;
      console.log(' load start -> ' + this.btnLinkDisable);
    });
    let watch = browser.on('exit').subscribe(function(event){
      console.log(' load exit -> ');
      this.btnLinkDisable = false;
    });
    
  }

  openVideo(){      
    this.btnVideoDisable = false;
  }

  openEvaluation(formId: number, uniqueReply: boolean ){
    console.log(formId ,   uniqueReply);
    if(formId != null){
      if(this.course.userWasAlreadyEvaluated && this.course.formUniqueReply){
        let alert = this.alertCtrl.create({
          title: 'NO SE PUEDE RESPONDER',
          message: 'Ya realizaste la evaluación anteriormente. Esta capacitación sólo permite ser respondida una vez.',
          buttons: [
            {
              text: 'Aceptar',
              handler: () => {
                this.navCtrl.pop();
              }
            }
          ]
        });
        alert.present();
      }
      else{
        this.navCtrl.push(SurveysDetailPage, {id : formId, course: 1, tipoForm: TipoFormulario.Evaluacion});
      }
    }
  }
}
