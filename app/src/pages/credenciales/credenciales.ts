import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Loading } from 'ionic-angular';
import { CredencialesProvider } from '../../providers/credenciales/credenciales.provider';
import { Credencial } from '../../models/credenciales.model';
import { UserProvider } from '../../providers/user/user.provider';
import { HelperProvider } from '../../providers/helper/helper.provider';

//@IonicPage()
@Component({
  selector: 'page-credenciales',
  templateUrl: 'credenciales.html',
})
export class CredencialesPage  implements OnInit {
  credencial: Credencial;
  loading: Loading;
  userId: any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public helper: HelperProvider,
    public credencialProvider: CredencialesProvider,
    public loadingController:LoadingController,
    public toastCtrl: ToastController,
    private userProvider: UserProvider,
    ) { }

    ngOnInit() {
      this.userId = this.userProvider.getUserId();  
      this.getCredencialBydId(this.userId);
  }

  getCredencialBydId(id:number){
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();      
    this.credencialProvider.getCredencialBydId(id).subscribe(
      data => { 
        this.credencial = data;
        this.loading.dismiss();
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }




}
