import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Loading, AlertController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user.provider';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Usuario } from '../../models/user.model';
import { HelperProvider } from '../../providers/helper/helper.provider';
import { TipoCatalogoProvider } from '../../providers/tipo-catalogo/tipo-catalogo.provider';
import { TipoCatalogoUsuario } from '../../models/tipoCatalogoUsuario.model';
import { AuthenticationProvider } from '../../providers/authentication/authentication.provider';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage implements OnInit{
  editPasswordForm: FormGroup;
  userProfile: Usuario;
  loading: Loading;
  isReady:boolean = false;
  tipoCatalogoUsuario: TipoCatalogoUsuario[] = [];
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public helper: HelperProvider,
    public userProvider: UserProvider,
    public formBuilder: FormBuilder,
    public loadingController:LoadingController,
    public tipoCatalogoProvider: TipoCatalogoProvider,
    public toastCtrl: ToastController) { }

  ngOnInit() {
    this.getUserProfile();
    this.editPasswordForm = this.formBuilder.group(
      {
        currentPassword: ['', Validators.compose([Validators.required])],
        newPassword: ['', Validators.compose([Validators.required])],
        confirmPassword: ['', Validators.compose([Validators.required])]
      }, 
      {
        validator: this.matchValidator
      }
    );
  }

  matchValidator(formGroup: FormGroup) {
    let newPassword = formGroup.controls.newPassword.value;
    let confirmPassword = formGroup.controls.confirmPassword.value;
    if (confirmPassword.length <= 0) {
        return null;
    }
    if (confirmPassword !== newPassword) {
        return {
            doesMatchPassword: true
        };
    }
    return null;
  }

  editPassword() {
    let form = this.editPasswordForm.value;
    let data = {
      oldPassword: form.currentPassword,
      newPassword: form.newPassword,
      confirmPassword: form.confirmPassword
    };
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();
    this.userProvider.editPassword(data).subscribe(
      user => {
        this.loading.dismiss();
        let toast = this.toastCtrl.create({
          message: 'Datos actualizados correctamente.',
          cssClass: "toast-success",
          duration: 3000
        });
        toast.present();         
      },
      error => {
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }

  getUserProfile(){
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();      
    this.userProvider.getUserProfile().subscribe(
      user => { 
        this.isReady = true;
        this.userProfile = user;
      },
      error => { 
        this.helper.showError(error);
      }
    );

    // Obtenemos los tipos de catalogos
    this.tipoCatalogoProvider.obtenerTipoCatalogosPorUsuarioId(this.userProvider.getUserId().toString()).subscribe(
      result => { 
        this.tipoCatalogoUsuario = result;
        console.log(this.tipoCatalogoUsuario);
        this.loading.dismiss();
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }

}