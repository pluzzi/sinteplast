import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Loading } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user.provider';
import { Usuario } from '../../models/user.model';
import { HelperProvider } from '../../providers/helper/helper.provider';

@Component({
  selector: 'page-user-detail',
  templateUrl: 'user-detail.html',
})
export class UserDetailPage implements OnInit{
  user: Usuario;
  loading: Loading;
  isReady:boolean = false;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public helper: HelperProvider,
    public userProvider: UserProvider,
    public loadingController:LoadingController,
    public toastCtrl: ToastController  ) { }
  
  ngOnInit(){
    let userId: number = this.navParams.data.userId;
    this.getUserBydId(userId);
  }

  getUserBydId(id:number){
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();    
    this.userProvider.getUserBydId(id).subscribe(
      data => { 
        this.isReady = true;
        this.user = data;
        this.loading.dismiss();
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error); 
      }
    );
  }


  

}
