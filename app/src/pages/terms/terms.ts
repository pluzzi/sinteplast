import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';

@Component({
  selector: 'page-terms',
  templateUrl: 'terms.html',
})
export class TermsPage implements OnInit {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingController: LoadingController) {
  }

  ngOnInit(){
    
  }

}
