import { Component, OnInit, HostListener } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Loading, AlertController } from 'ionic-angular';
import { BeneficioProvider } from '../../providers/benefits/benefits.provider';
import { Beneficio } from '../../models/benefits.model';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { HelperProvider } from '../../providers/helper/helper.provider';
import { UserProvider } from '../../providers/user/user.provider';
import { CataloguePage } from '../catalogue/catalogue';

@Component({
  selector: 'page-benefits-detail',
  templateUrl: 'benefits-detail.html',
})
export class BenefitsDetailPage {
  loading: Loading;
  beneficio: Beneficio;
  isReady: boolean = false;
  mostrarCanjear: boolean;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public helper: HelperProvider,
    public beneficioProvider: BeneficioProvider,
    private theInAppBrowser: InAppBrowser,
    public loadingController:LoadingController,
    public toastCtrl: ToastController,
    public userProvider: UserProvider,
    public alertCtrl: AlertController) {
  }

  ngOnInit() {
    let beneficioId: number = this.navParams.data.id;
    this.mostrarCanjear = this.navParams.data.canjear;
    this.getBenefitBydId(beneficioId);    
  }

  getBenefitBydId(id:number){
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();    
    this.beneficioProvider.getBeneficioById(id).subscribe(
      data => { 
        this.isReady = true;
        this.beneficio = data;
        this.loading.dismiss();
      },
      error => {
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }

  like(beneficio: Beneficio){
    this.beneficioProvider.getLikes(beneficio.id).subscribe(
      data => {
        beneficio.liked = !beneficio.liked;                    
      },
      error => { 
        this.helper.showError(error);
      }
    );
  }

  @HostListener('document:click', ['$event'])
  handleClick(event: EventClick) {
     if(event.target && event.target.href){
        this.openBrowser(event.target.href);
     }
     return false; 
  }

  openBrowser(url){
    const options: InAppBrowserOptions = {
      location:'no'
    };
    this.theInAppBrowser.create(url, '_system', options);
  }

  canjear(){
    if(this.mostrarCanjear==true){
      let alert = this.alertCtrl.create({
        title: 'Confirmación',
        message: '¿Desea realizar el canje?',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: () => { console.log('Cancel clicked'); }
          },
          {
            text: 'Aceptar',
            handler: () => {
              this.beneficioProvider.canjear(this.userProvider.getUserId(), this.beneficio.id);
              this.presentToast("El canje se ha realizado.");
              this.navCtrl.setRoot(CataloguePage);
            }
          }
        ]
      });
      alert.present();
    }else{
      this.presentToast("Los puntos no alcanzan.");
    }     
  }

  async presentToast(msg: string) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
}
