import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, ToastController, AlertController } from 'ionic-angular';
import { SurveysDetailPage } from '../surveys-detail/surveys-detail';
import { FormularioGenerico } from '../../models/generalForms.model';
import { FormularioProvider } from '../../providers/surveys/surveys.provider';
import { TipoFormulario } from '../../models/tipoForm.enum';
import { Formulario } from '../../models/survey.model';
import { HelperProvider } from '../../providers/helper/helper.provider';

@Component({
  selector: 'page-formulario-generico-list',
  templateUrl: 'formulario-generico-list.html',
})
export class FormularioGenericoListPage implements OnInit{

  generalFormsList: Formulario[] = [];
  loading: Loading;
  headerTitle: string = 'Formulario';
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public helper: HelperProvider,
    public loadingController:LoadingController,
    public toastCtrl: ToastController,
    public formularioProvider: FormularioProvider,
    public alertCtrl: AlertController) {
  }

  ngOnInit() {
    this.getFormulariosGenericos();
  }

  getFormulariosGenericos(){
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();
    this.formularioProvider.getFormulariosGenericos().subscribe(
      data => {       
        this.generalFormsList = data;
        this.loading.dismiss();        
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }

  goToQuestions(form: Formulario){
    if(form.alreadyAnswered && form.uniqueReply){  
      let alert = this.alertCtrl.create({
        title: 'NO SE PUEDE RESPONDER',
        message: 'El formulario ya fue respondido anteriormente.',
        buttons: [
          {
            text: 'Aceptar',
            handler: () => { }
          }
        ]
      });
      alert.present();
    }
    else{
     this.navCtrl.push(SurveysDetailPage, {id : form.id, title : this.headerTitle, tipoForm: TipoFormulario.Generico});
    }
  }

}
