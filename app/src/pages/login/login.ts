import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, ToastController, Loading, Platform, Events, MenuController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationProvider } from '../../providers/authentication/authentication.provider';
import { ResetPasswordPage } from '../reset-password/reset-password';
import { ChangePasswordPage } from '../change-password/change-password';
import { FcmProvider } from '../../providers/fcm/fcm.provider';
import { ActivateAccountValidatePage } from '../activate-account-validate/activate-account-validate';
import { SettingsProvider } from '../../providers/settings/settings.provider';
import { StorageProvider } from '../../providers/storage/storage.provider';
import { UserProvider } from '../../providers/user/user.provider';
import { MuroPage } from '../muro/muro';
import { HelperProvider } from '../../providers/helper/helper.provider';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage implements OnInit{
	loginForm: FormGroup;
  loading: Loading;

  constructor(
    public events: Events,
    private navCtrl: NavController, 
    public helper: HelperProvider,
    private fcm: FcmProvider,
    private formBuilder: FormBuilder,
    private platform: Platform, 
    private loadingController:LoadingController,
    private toastCtrl: ToastController,
    private authProvider: AuthenticationProvider,
    private userProvider: UserProvider,
    private storageProvider: StorageProvider,
    public settingsProvider: SettingsProvider,
    public menu: MenuController) { 
      console.log("qmt::LoginPage>>constructor");
    }
 
  ngOnInit() { 
    console.log("qmt::LoginPage>>constructor");
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
      empresa: ['', Validators.compose([Validators.nullValidator])]
    });
  }

  login() {
		let data = this.loginForm.value;
		if (!data.username) {
			return;
    }
    let credentials = { 
      UserName: data.username, 
      Password: data.password,
      DireccionApp: this.settingsProvider.getEmpresa()
    };
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();

    console.log("qmt::autenticando"); 
    
    this.authProvider.login(credentials).subscribe(
      data => {
        this.storageProvider.saveToken(data.authToken);
        this.menu.swipeEnable(true);
        this.settingsProvider.getSettings().subscribe(
          settings => {
            this.events.publish('menu:populate', settings.menu);
            this.events.publish('theme:populate', settings.theme);

            this.userProvider.getUserProfile().subscribe(
              user => { 
                this.loading.dismiss();
                this.userProvider.setCurrentUser(user);
                this.getFCMToken(user.id);
                if(data.shouldModifyPassword) {
                  this.navCtrl.push(ChangePasswordPage);
                } else {
                  this.settingsProvider.setPendingRefresh(false);
                  this.navCtrl.setRoot(MuroPage);
                }
              },
              error => { 
                console.log("qmt::error getUserProfile");
                this.loading.dismiss();
                this.helper.showError(error);
              }
            );
          },
          error => {
            console.log("qmt::error getSettings");
            this.loading.dismiss();
            this.helper.showError(error);
          }
        );
      },
      error => {
        console.log("qmt::error login");
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }

  getFCMToken(userId: number) {
    if (this.platform.is('cordova')) {
      this.fcm.getToken(userId);
    }
  }

  goToResetPassword(){
    this.navCtrl.push(ResetPasswordPage);
  }

  goToActiveAccount(){
    this.navCtrl.push(ActivateAccountValidatePage);
  }

}