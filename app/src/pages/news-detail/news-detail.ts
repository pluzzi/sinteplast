import { Component, OnInit, HostListener } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Loading } from 'ionic-angular';
import { NoticiaProvider } from '../../providers/noticia/noticia.provider';
import { Noticia } from '../../models/noticia.model';
import { InAppBrowserOptions, InAppBrowser } from '@ionic-native/in-app-browser';
import { LocalRequestOrigin } from '../../models/LocalRequestOrigin.enum';
import { HelperProvider } from '../../providers/helper/helper.provider';

@Component({
  selector: 'page-news-detail',
  templateUrl: 'news-detail.html',
})
export class NewsDetailPage implements OnInit {
  noticia: Noticia;
  loading: Loading;
  isReady:boolean = false;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public helper: HelperProvider,
    public noticiaProvider: NoticiaProvider,
    private theInAppBrowser: InAppBrowser,
    public loadingController:LoadingController,
    public toastCtrl: ToastController) { }

  ngOnInit() {
    let params:any = this.navParams.get('params');
    let localRequestOrigin:LocalRequestOrigin = this.navParams.get('localRequestOrigin');

    let noticiaId: number;
    if (localRequestOrigin == LocalRequestOrigin.Menu && params.entidadId) {
      noticiaId = params.entidadId;
    } else {
      noticiaId = this.navParams.data.id;
    }
    this.getNoticiaBydId(noticiaId);
  }

  getNoticiaBydId(id:number){
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();    
    this.noticiaProvider.obtenerNoticiaPorId(id).subscribe(
      data => { 
        this.isReady = true;
        this.noticia = data;
        this.loading.dismiss();
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }

  @HostListener('document:click', ['$event'])
  handleClick(event: EventClick) {
     if(event.target && event.target.href){
         this.openBrowser(event.target.href);
     }
     return false; 
  }

  openBrowser(url){
    const options: InAppBrowserOptions = {
      location:'no'
    };
    this.theInAppBrowser.create(url, '_system', options);
  }
}
