import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, AlertController,Loading } from 'ionic-angular';
import { DocumentsProvider } from '../../providers/documents/documents.provider';
import { FileOpener } from '@ionic-native/file-opener';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Device } from '@ionic-native/device';
import { Documento } from '../../models/documents.model';
import { LocalRequestOrigin } from '../../models/LocalRequestOrigin.enum';
import { HelperProvider } from '../../providers/helper/helper.provider';

@Component({
  selector: 'page-documents',
  templateUrl: 'documents.html',
})
export class DocumentsPage implements OnInit{
  public selectedCat;
  documentoList: Documento[] = [];
  page: number = 1;
  categoriaList: Document[]=[];
  moduloId: number = 1;
  loading: Loading;
  categoria: number = 0;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public helper: HelperProvider,
    public loadingController:LoadingController,
    public toastCtrl: ToastController,
    public documentsProvider: DocumentsProvider,
    private fileOpener: FileOpener,
    private transfer: FileTransfer,
    private file: File,
    private device: Device,
    public alertCtrl: AlertController) { }

  ngOnInit() { 
    let params:any = this.navParams.get('params');
    let localRequestOrigin:LocalRequestOrigin = this.navParams.get('localRequestOrigin');

    if (localRequestOrigin == LocalRequestOrigin.Menu && params.categoriaId) {
      this.getCategorias(this.moduloId, params.categoriaId);
      this.getDocumentosByCategoriaId(params.categoriaId, 1);
    } else {
      this.getCategorias(this.moduloId);
      this.getDocumentos(this.page);
    }
  }  

  getDocumentos(page: number){
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present(); 
    this.documentsProvider.obtenerDocumentos(this.page).subscribe(
      data => {
        this.documentoList = data;
        this.getExtension(data);    
        this.loading.dismiss();
      },
      error => {
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }

  getDocumentosByCategoriaId(categoriaId:number, page:number){
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();
    this.documentsProvider.obtenerDocumentosPorCategoriaId(categoriaId, page).subscribe(
      data => {            
        this.documentoList = data; 
        this.getExtension(data);
        this.loading.dismiss();          
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }

  openFile(document){
    console.log(this.documentoList);
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present(); 
    let devicePlatform = this.device.platform;   
    console.log(" Storage "+ this.file.dataDirectory);
    let targetPath = "";
    if(devicePlatform == 'Android'){ 
      targetPath = this.file.externalDataDirectory;
    }
    if(devicePlatform == 'iOS'){
      targetPath = this.file.tempDirectory;
    }    
    const fileTransfer: FileTransferObject = this.transfer.create();         
    //const url = encodeURI(document.filePath);
    const url = document.filePath;
    var filename = url.split("/").pop();
    var trustHosts = true;

    fileTransfer.download(url, targetPath + filename, trustHosts).then((entry) => {
      console.log('download complete: ' + entry.toURL());
      var mimeType = '';
      var openErrorMessage = '';
        switch (document.extension.toLowerCase()) {
        case 'doc-pdf':
          mimeType = 'application/pdf';
          openErrorMessage = 'Adobe Acrobat Reader';
          break;
        case 'doc-jpg':
        case 'doc-jpeg':
          mimeType = 'image/jpeg';
          break;
        case 'doc-png':
        case 'doc-ai':
        case 'doc-psd':
          mimeType = 'image/png';
          break;
        case 'doc-xls':
          mimeType = 'application/vnd.ms-excel';
          openErrorMessage = 'Microsoft Excel';
          break;
        case 'doc-xlsx':
        case 'doc-xlsm':
          mimeType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
          openErrorMessage = 'Microsoft Excel';
          break;
        case 'doc-doc':
          mimeType = 'application/msword';
          openErrorMessage = 'Microsoft Word';
          break;
        case 'doc-docx':
          mimeType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
          openErrorMessage = 'Microsoft Word';
          break;
        case 'doc-ppt':
        case 'doc-pps':
          mimeType = 'application/vnd.ms-powerpoint';
          openErrorMessage = 'Microsoft Power Point';
          break;
        case 'doc-pptx':
          mimeType = 'application/vnd.openxmlformats-officedocument.presentationml.presentation';
          openErrorMessage = 'Microsoft Power Point';
          break;
        case 'doc-ppsx':
          mimeType = 'application/vnd.openxmlformats-officedocument.presentationml.slideshow';
          openErrorMessage = 'Microsoft Power Point';
          break;
      }     
      this.fileOpener.open(targetPath + filename , mimeType).then((data) =>{ 
        this.loading.dismissAll();
        console.log(' File is OPENED!!! ')
      },
      (err)=>{   
        console.log("Error: " + JSON.stringify(err));
        this.loading.dismissAll();         
        if(err.status == 9){
          let alert = this.alertCtrl.create({
            title: 'NO SE PUDO ABRIR EL DOCUMENTO',
            message: 'No se ha encontrado la aplicación necesaria para abrir el documento.',
            buttons: [
              {
                text: 'Aceptar',
                handler: () => {}
              }
            ]
          });
          alert.present();
        }else{
          let alert = this.alertCtrl.create({
            title: 'NO SE PUDO ABRIR EL DOCUMENTO',
            message: 'No se ha encontrado la aplicación necesaria para abrir el documento.',
            buttons: [
              {
                text: 'Aceptar',
                handler: () => {}
              }
            ]
          });
          alert.present();
        }

      })
    }, 
    (error) => {
      console.log("handle error-> " + JSON.stringify(error));
    });
  }
  
  getCategorias(ModId: number, selected?:number){
    this.documentsProvider.obtenerCategorias(ModId).subscribe(
      data => {
        this.categoriaList = data;
        if(selected) {
          this.selectedCat = selected;
        }
      },
      error => {
        this.loading.dismiss();
        this.helper.showError(error);
      }
    )
  }

  onChange(objId){    
    this.categoria = objId;   
    this.documentoList = [];
    this.page = 1;
    if(objId == 0){
      this.getDocumentos(this.page);
    }
    else{
      this.getDocumentosByCategoriaId(objId, this.page);
    }
  }

  doInfinite(infiniteScroll) {
    this.page++;
    setTimeout(() => {
      if(this.categoria == 0){
        this.documentsProvider.obtenerDocumentos(this.page).subscribe(
          data => { 
            for(let i=0; i < data.length; i++) {            
              this.documentoList.push(data[i]);
            }          
          },
          error => {
            this.loading.dismiss();
            this.helper.showError(error);
          }
        );
      }else{
        this.documentsProvider.obtenerDocumentosPorCategoriaId(this.categoria,this.page).subscribe(
          data => {                        
            for(let i=0; i < data.length; i++) {            
              this.documentoList.push(data[i]);
            }          
          },
          error => { 
            this.loading.dismiss();
            this.helper.showError(error);
          }
        );
      }
      infiniteScroll.complete();
    }, 500);
  }

  getExtension(data: any){
    for(let i in data){          
      switch(data[i].extension){
        case "docx": 
          this.documentoList[i].extension = "doc-docx";              
          break;
        case 'pdf':
          this.documentoList[i].extension = 'doc-pdf';
          break;
        case 'jpg':
        case 'jpeg':
          this.documentoList[i].extension = 'doc-jpg';
          break;
        case 'png':
          this.documentoList[i].extension = 'doc-png';
          break;
        case 'xls':
          this.documentoList[i].extension = 'doc-xls';            
          break;
        case 'xlsx':
        case 'xlsm':
          this.documentoList[i].extension = 'doc-xlsx';
          break;
        case 'doc':
          this.documentoList[i].extension = 'doc-doc';
          break;
        case 'docx':
          this.documentoList[i].extension = 'doc-docx';              
          break;
        case 'ppt':
        case 'pps':
          this.documentoList[i].extension = 'doc-ppt';              
          break;
        case 'pptx':
        case 'ppsx':
          this.documentoList[i].extension = 'doc-pptx';              
          break;
        case 'ai':
          this.documentoList[i].extension = 'doc-ai';              
          break;
        case 'psd':
          this.documentoList[i].extension = 'doc-psd';              
          break;
      }
    }  
  }

}
