import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, App } from 'ionic-angular';

/**
 * Generated class for the OrderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-order',
  templateUrl: 'order.html',
})

export class OrderPage implements OnInit{

  constructor(
    private app: App,
    public navCtrl: NavController, 
    public navParams: NavParams) {
  }

  ngOnInit(){}

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderPage');
  }

}
