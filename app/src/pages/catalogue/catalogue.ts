import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, App, LoadingController } from 'ionic-angular';
import { TipoCatalogoProvider } from '../../providers/tipo-catalogo/tipo-catalogo.provider';
import { TipoCatalogoUsuario } from '../../models/tipoCatalogoUsuario.model';
import { HelperProvider } from '../../providers/helper/helper.provider';
import { BenefitsListPage } from '../benefits-list/benefits-list';
import { UserProvider } from '../../providers/user/user.provider';

/**
 * Generated class for the CataloguePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-catalogue',
  templateUrl: 'catalogue.html',
})
export class CataloguePage implements OnInit  {
  tipoCatalogoUsuario: TipoCatalogoUsuario[] = [];
  loading: any;

  constructor(
    private app: App,
    public navCtrl: NavController,
    public navParams: NavParams,
    public tipoCatalogoProvider: TipoCatalogoProvider,
    public helper: HelperProvider,
    public userProvider: UserProvider,
    public loadingController: LoadingController
    ) {
  }

  ngOnInit(){
    //this.getTipoCatalogoUsuario();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CataloguePage');
    this.getTipoCatalogoUsuario();
  }

  getTipoCatalogoUsuario(){
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();  

    // Obtenemos los tipos de catalogos
    this.tipoCatalogoProvider.obtenerTipoCatalogosPorUsuarioId(this.userProvider.getUserId().toString()).subscribe(
      result => { 
        this.tipoCatalogoUsuario = result;
        console.log(this.tipoCatalogoUsuario);
        if(this.tipoCatalogoUsuario.length==1){
          this.navCtrl.setRoot(BenefitsListPage, { id: this.tipoCatalogoUsuario[0].tipoCatalogo.id, puntos: this.tipoCatalogoUsuario[0].puntosAcumulados });
        }
        this.loading.dismiss();
      },
      error => { 
        this.helper.showError(error);
        this.loading.dismiss();
      }
    );
  }
}
