import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, Loading, LoadingController, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationProvider } from '../../providers/authentication/authentication.provider';
import { ActivateAccountPasswordPage } from '../activate-account-password/activate-account-password';
import { SettingsProvider } from '../../providers/settings/settings.provider';

@Component({
  selector: 'page-activate-account-validate',
  templateUrl: 'activate-account-validate.html',
})
export class ActivateAccountValidatePage implements OnInit {
	validateAccountForm: FormGroup;
  loading: Loading;
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public formBuilder: FormBuilder,
    public loadingController:LoadingController,
    public alertController: AlertController,
    public authProvider: AuthenticationProvider,
    public settingsProvider:SettingsProvider
  ) { }

  ngOnInit() {
    this.validateAccountForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required])],
      empresa: ['', Validators.compose([Validators.nullValidator])]
    });
  }

  validate (){
		let form = this.validateAccountForm.value;
		if (!form.username) {
			return;
    }
    let data = { 
      UserName: form.username,
      PasswordHash: this.settingsProvider.getPasswordHash(),
      DireccionApp: this.settingsProvider.getEmpresa()
    };
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();
    this.authProvider.validarUsuario(data).subscribe(
      response => {
        this.loading.dismiss();
        this.navCtrl.push(ActivateAccountPasswordPage, {username:form.username, empresa: this.settingsProvider.getEmpresa(), email:response});
      },
      error => {
        this.loading.dismiss();
        this.showError(error);
      }
    );
  }

  showError(errMsg: string){
    let alert = this.alertController.create({
      title: 'Activar usuario',
      message: errMsg,
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {}
        }
      ]
    });
    alert.present();
  }

  backToLogin (){
    this.navCtrl.pop();
  }
}