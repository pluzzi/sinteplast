import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, AlertController, Loading } from 'ionic-angular';
import { FormularioProvider } from '../../providers/surveys/surveys.provider';
import { Formulario } from '../../models/survey.model';
import { Question } from '../../models/question.model';
import { SurveysListPage } from '../surveys-list/surveys-list';
import { CoursesListPage } from '../courses-list/courses-list';
import { TipoFormulario } from '../../models/tipoForm.enum';
import { FormularioGenericoListPage } from '../formulario-generico-list/formulario-generico-list';
import { MuroPage } from '../muro/muro';
import { HelperProvider } from '../../providers/helper/helper.provider';

@Component({
  selector: 'page-surveys-detail',
  templateUrl: 'surveys-detail.html',
})

export class SurveysDetailPage implements OnInit{
  survey: Formulario;
  answerRequiredErrorMessage:boolean = false;
  loading: Loading;
  course: number;
  tipoFormulario: TipoFormulario;
  isReady:boolean = false;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public helper: HelperProvider,
    public loadingController:LoadingController,
    public toastCtrl: ToastController,
    public surveyProvider: FormularioProvider,
    public alertCtrl: AlertController) {      
      this.tipoFormulario = this.navParams.data.tipoForm;
  }

  ngOnInit() {
    let formId: number = this.navParams.data.id;
    this.getPreguntas(formId);
  }

  getPreguntas(formId: number){
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();    
    this.surveyProvider.getPreguntas(formId).subscribe(
      response => {
        this.isReady = true;
        this.survey = response;      
        this.loading.dismiss();
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }

  guardarForm () {
    //this.course = this.navParams.data.course;
    this.answerRequiredErrorMessage =false;
    let cont = 0;
    let length = 0;
    for (let i in this.survey.questions) {
      let question:Question = this.survey.questions[i];
      if((question.questionTypeId == "OPTION" || question.questionTypeId == "DROPDOWN") && question.required && question.answerOptionId == 0){
        this.answerRequiredErrorMessage = true;
        return;
      }
      if((question.questionTypeId == "DATE" || question.questionTypeId == "TEXTAREA" || question.questionTypeId == "TEXT") && question.required && question.answer == null){
        this.answerRequiredErrorMessage = true;
        return;
      }

      if(question.questionTypeId == "MULTIPLE" && question.required){
        for (let item in this.survey.questions[i].options){          
          length = this.survey.questions[i].options.length;
          if(this.survey.questions[i].options[item].answerSelected == false){
            cont++;            
          }
        }
        console.log(cont);
        if(question.required && length == cont){
          this.answerRequiredErrorMessage = true;
          return;
        }
      }

    }    
    console.log(this.survey); 
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();      
    this.surveyProvider.guardarForm(this.survey).subscribe(
      data => {           
        this.loading.dismiss();        
        let alert = this.alertCtrl.create({
          title: 'Aviso',
          message: 'Los datos se enviaron correctamente.',
          buttons: [
            {
              text: 'Aceptar',
              handler: () => { 

                switch (this.tipoFormulario) {
                  case TipoFormulario.Generico:
                    this.navCtrl.setRoot(FormularioGenericoListPage); 
                    break;
                  case TipoFormulario.Evaluacion:
                    this.navCtrl.setRoot(CoursesListPage);
                    break;
                  case TipoFormulario.Encuesta:
                    this.navCtrl.setRoot(SurveysListPage); 
                    break;
                  default:
                  this.navCtrl.setRoot(MuroPage); 
                    break;
                }

              }
            }
          ]
        });
        alert.present();
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }



}
