import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, AlertController, Loading } from 'ionic-angular';
import { FormularioProvider } from '../../providers/surveys/surveys.provider';
import { SurveysDetailPage } from '../surveys-detail/surveys-detail';
import { Formulario } from '../../models/survey.model';
import { TipoFormulario } from '../../models/tipoForm.enum';
import { HelperProvider } from '../../providers/helper/helper.provider';

@Component({
  selector: 'page-surveys-list',
  templateUrl: 'surveys-list.html',
})
export class SurveysListPage implements OnInit{
  surveysList: Formulario[] = [];
  loading: Loading;
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public helper: HelperProvider,
    public loadingController:LoadingController,
    public toastCtrl: ToastController,
    public formularioProvider: FormularioProvider,
    public alertCtrl: AlertController) {
  }

  ngOnInit() {
    this.getSurveysList();
  }

  getSurveysList(){
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();
    this.formularioProvider.getEncuestas().subscribe(
      data => {       
        this.surveysList = data;
        this.loading.dismiss();        
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }
 
  goToQuestions(surv: Formulario){
    if(surv.alreadyAnswered && surv.uniqueReply){  
      let alert = this.alertCtrl.create({
        title: 'NO SE PUEDE RESPONDER',
        message: 'La encuesta ya fue respondida anteriormente.',
        buttons: [
          {
            text: 'Aceptar',
            handler: () => { }
          }
        ]
      });
      alert.present();
    }
    else{
      this.navCtrl.push(SurveysDetailPage, {id : surv.id, tipoForm: TipoFormulario.Encuesta});
    }
  }


}
