import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController,Loading } from 'ionic-angular';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { UsefulInfoProvider } from '../../providers/useful-info/useful-info.provider';
import { UsefulInfoDetailPage } from '../useful-info-detail/useful-info-detail';
import { Info } from '../../models/usefulinfo.model';
import { Categoria } from '../../models/categorias';
import { LocalRequestOrigin } from '../../models/LocalRequestOrigin.enum';
import { HelperProvider } from '../../providers/helper/helper.provider';

@Component({
  selector: 'page-useful-info-list',
  templateUrl: 'useful-info-list.html',
})
export class UsefulInfoListPage implements OnInit {
  public selectedCat;
  infoList: Info[] = [];
  categoriaList: Categoria[] = [];
  moduloId: number = 10;
  loading: Loading;
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public helper: HelperProvider,
    public usefulInfoProvider: UsefulInfoProvider,
    public loadingController: LoadingController,
    public toastCtrl: ToastController){ 

    }

  ngOnInit() { 
    let params:any = this.navParams.get('params');
    let localRequestOrigin:LocalRequestOrigin = this.navParams.get('localRequestOrigin');

    if (localRequestOrigin == LocalRequestOrigin.Menu && params.categoriaId) {
      this.getCategorias(this.moduloId, params.categoriaId);
      this.getInfoPaginasByCategoriaId(params.categoriaId);
    } else {
      this.getCategorias(this.moduloId);
      this.getInfoPaginas();
    }
  }

  getInfoPaginas(){
     this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
     this.loading.present();   
     this.usefulInfoProvider.getUsefulInfo().subscribe(
      data => {       
        this.infoList = data;
        this.loading.dismiss();       
      },
      error => {
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }
  
  getInfoPaginasByCategoriaId(categoriaId:number){
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present(); 
    this.usefulInfoProvider.getInfoPaginasByCatId(categoriaId).subscribe(
      data => {  
        this.loading.dismiss();          
        this.infoList = data;         
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }

  goToInfoDetail(info: Info){
    this.navCtrl.push(UsefulInfoDetailPage, {id: info.id});
  }

  getCategorias(moduloId: number, selected?:number){
    this.usefulInfoProvider.getCategorias(moduloId).subscribe(
      data => {     
        this.categoriaList = data;
        if(selected) {
          this.selectedCat = selected;
        }
      },
      error => {
        this.helper.showError(error);
      }
    )
  }

  onChange(categoriaId: number){
    this.infoList = [];
    if(categoriaId == 0) {
      this.getInfoPaginas();
    } else{
      this.getInfoPaginasByCategoriaId(categoriaId);
    }
  }
}