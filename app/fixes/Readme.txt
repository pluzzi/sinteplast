


Error compilación Android: Firebase
-----------------------------------

[Descripción]
Revisar el siguiente link
https://forum.ionicframework.com/t/ionic-4-cordova-run-android-firebase-error-all-of-a-sudden/163204/30

[Solución]
project.properties can be found on platforms/android/project.properties

cordova.system.library.4=com.google.android.gms:play-services-tagmanager:16+
cordova.system.library.5=com.google.firebase:firebase-core:16.0.8
cordova.system.library.6=com.google.firebase:firebase-messaging:17.5.0
cordova.system.library.7=com.google.firebase:firebase-crash:+
cordova.system.library.8=com.google.firebase:firebase-config:16.4.1
cordova.system.library.9=com.google.firebase:firebase-perf:16.2.4


Streaming MP4 video files in Azure Storage containers (Blob Storage)
--------------------------------------------------------------------
[Descripción]
https://stackoverflow.com/questions/11503776/serving-video-content-from-azure-blob-storage
https://github.com/OrchardCMS/Orchard/pull/7465

[Solución]
Check your Storage Version
https://blog.thoughtstuff.co.uk/2014/01/streaming-mp4-video-files-in-azure-storage-containers-blob-storage/

