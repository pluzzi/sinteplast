# Requerimientos

## Acceso directo a páginas
### Descripción
**Como** administrador **necesito** crear un menú con accesos directos a páginas locales y externas de la App **para** dar acceso a información necesaria de forma dinámica.
### Criterios de aceptación
* Los items de menú se cargan a través de la BD en la tabla MenuItems.
* Un item de menu puede tener como destino: una lista, una lista filtrada por categoria, un detalle y una página externa.

```
type=list&target=app.infoList
type=list&target=app.infoList&categoriaId=6
type=detail&target=app.infoDetail&entidadId=2
type=external&target=http://www.google.com
```        

