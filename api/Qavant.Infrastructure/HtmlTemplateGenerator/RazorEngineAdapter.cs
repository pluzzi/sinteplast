﻿using Qavant.Application.Infrastructure;
using Qavant.Application.Services.Formularios;
using RazorEngine;
using RazorEngine.Templating;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
https://goo.gl/E5SEM
https://goo.gl/YJsbLP
*/
namespace Qavant.Infrastructure.HtmlTemplateGenerator
{
    public class RazorEngineAdapter : IHtmlTemplateGenerator
    {
        public string GetTemplateEmail(string template, object vm)
        {
            var result = Engine.Razor.RunCompile(template, "templateKey", null, vm);
            return result;
        }
    }
}
