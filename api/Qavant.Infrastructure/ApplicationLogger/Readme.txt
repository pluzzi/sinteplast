﻿
    public class LogActividadRepository: ILogActividadRepository
    {
        private int _tenantId;
        private int _userId;
        private DeviceChannel _channel;

        public LogActividadRepository(int tenantId, int userId, DeviceChannel channel)
        {
            _tenantId = tenantId;
            _userId = userId;
            _channel = channel;
        }

        public void Save(LogActividad log)
        {
            using (var db = new TenantDbContext(_tenantId))
            {
                try
                {
                    string insertSQL = @"INSERT INTO LogActividades (UserId, EmpresasId, ModulosId, EntidadId, CanalId, LogAccionId, FechaActividad) 
                                        VALUES(@UserId, @EmpresasId, @ModulosId, @EntidadId, @CanalId, @LogAccionId, @FechaActividad)";

                    var userId = new SqlParameter("@Id", _userId);
                    var empresaId = new SqlParameter("@Id", _tenantId);
                    var modulosId = new SqlParameter("@Id", log.ModuloId);
                    var entidadId = new SqlParameter("@Id", log.EntidadId);
                    var canalId = new SqlParameter("@Id", (int)_channel);
                    var logAccionId = new SqlParameter("@Id", log.LogAccionId);

                    db.Database.ExecuteSqlCommandAsync(insertSQL, userId, empresaId, modulosId, entidadId, canalId, logAccionId);
                }
                catch (InvalidOperationException ex)
                {
                    throw null;
                }
            }
        }
    }
}