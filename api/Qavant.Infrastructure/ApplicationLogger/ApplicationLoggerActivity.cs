﻿using Qavant.Application;
using Qavant.Application.Infrastructure;
using Qavant.Application.Infrastructure.LoggerActivity;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Qavant.Infrastructure.ApplicationLogger
{
    public class QavantLoggerActivity : IApplicationLoggerActivity
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["QavantDbConnection"].ConnectionString;

        private readonly ILogHolder _logHolder;

        public QavantLoggerActivity(ILogHolder logHolder)
        {
            _logHolder = logHolder;
        }

        public void Save(LogActividad log)
        {
            string query = "INSERT INTO dbo.LogActividades (UserId, EmpresaId, ModuloId, EntidadId, CanalId, LogTipoId, FechaActividad) " +
                           "VALUES (@UserId, @EmpresaId, @ModuloId, @EntidadId, @CanalId, @LogTipoId, @FechaActividad)";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(query, connection))
                {
                    try
                    {
                        cmd.Parameters.Add("@EmpresaId", SqlDbType.Int).Value = _logHolder.TenantId;
                        cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = _logHolder.UsuarioId;
                        cmd.Parameters.Add("@CanalId", SqlDbType.Int).Value = _logHolder.Canal;
                        cmd.Parameters.Add("@ModuloId", SqlDbType.Int).Value = log.Modulo;

                        if (log.EntidadId == null)
                            cmd.Parameters.Add("@EntidadId", SqlDbType.Int).Value = DBNull.Value;
                        else
                            cmd.Parameters.Add("@EntidadId", SqlDbType.Int).Value = log.EntidadId;

                        cmd.Parameters.Add("@LogTipoId", SqlDbType.Int).Value = log.LogTipo;
                        cmd.Parameters.Add("@FechaActividad", SqlDbType.DateTime).Value = log.FechaActividad;

                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        //Guardar en disco
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        public void Save(int usuarioId, LogActividad log)
        {
            //this.userId = usuarioId;
            //Save(log);
        }
    }
}