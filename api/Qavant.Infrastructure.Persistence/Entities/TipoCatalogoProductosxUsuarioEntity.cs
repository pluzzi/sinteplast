﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Entities
{
    public class TipoCatalogoProductosxUsuarioEntity : IEntity
    {
        public int Id { get; set; }

        public int Userid { get; set; }
        public int TipoCatalogoProductoId { get; set; }
        public decimal PuntosAcumulados { get; set; }
        public DateTime FechaActualizacionPuntos { get; set; }
    }
}
