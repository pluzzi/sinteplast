﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Entities
{
    public class ElementoMultimediaEntity
    {
        public int Id { get; set; }
        public int EntidadId { get; set; }
        public int TenantId { get; set; }
        public int ModuloId { get; set; }
        public string RutaArchivo { get; set; }
        public DateTime FechaAlta { get; set; }

        public Int16 MultimediaTipoId { get; set; }
        public Entities.MultimediaTipoEntity MultimediaTipo { get; set; }
    }
}
