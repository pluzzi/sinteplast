﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Entities
{
    public class DocumentoEntity : IEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string FilePath { get; set; }
        public bool Enabled { get; set; }
        public DateTime CreatedDate { get; set; }
        public int DocumentLibraryId { get; set; }
        public int CategoriaId { get; set; }
        public int TenantId { get; set; }

        [NotMapped]
        public int ReaccionId { get; set; }

        [NotMapped]
        public int TotalReacciones { get; set; }
    }
}
