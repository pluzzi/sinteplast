﻿using Qavant.Domain.Formularios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Entities
{
    public class FormsEntity : IEntity
    {
        public int Id { get; set; }
        public int TenantId { get; set; }
        public TipoFormulario Tipo { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public DateTime FechaCreacion { get; set; }

        // Configuración
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
        public bool RespuestaUnica { get; set; }
        public bool EnviaEmail { get; set; }

        // Notificación
        public string EmailPara { get; set; }
        public string EmailAsunto { get; set; }
        public string EmailTitulo { get; set; }
        public string EmailIntroduccion { get; set; }

        // Estados
        public bool Eliminado { get; set; }
        public bool Habilitado { get; set; }
        public bool Respondido { get; set; }
        public ICollection<Entities.FormsPreguntaEntity> Preguntas { get; set; }


        [NotMapped]
        public int ReaccionId { get; set; }
        [NotMapped]
        public int TotalReacciones { get; set; }
    }
}
