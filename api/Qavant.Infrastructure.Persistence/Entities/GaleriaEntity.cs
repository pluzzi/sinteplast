﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Entities
{
    public class GaleriaEntity : IEntity
    {
        public int Id { get; set; }
        public int TenantId { get; set; }
        public string Titulo { get; set; }
        public string Copete { get; set; }
        public string ImagenPortada { get; set; }
        public bool Habilitada { get; set; }
        public DateTime FechaAlta { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
        public bool Destacada { get; set; }
        public int CategoriaId { get; set; }
        public Entities.CategoriaEntity Categoria { get; set; }

        [NotMapped]
        public int ReaccionId { get; set; }
        [NotMapped]
        public int TotalReacciones { get; set; }
    }
}
