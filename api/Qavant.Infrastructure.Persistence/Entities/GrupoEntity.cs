﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Entities
{
    public class GrupoEntity
    {
        public int Id { get; set; }
        public int TenantId { get; set; }
        public string Nombre { get; set; }

        public ICollection<Entities.UsuarioEntity> Usuarios { get; set; }
    }
}