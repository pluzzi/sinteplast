﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Entities
{
    public class CategoriaEntity
    {
        public int Id { get; set; }

        public int TenantId { get; set; }

        public string Nombre { get; set; }

        public int? PadreId { get; set; }

        public string Imagen { get; set; }

        public bool Habilitada { get; set; }

        public int ModuloId { get; set; }

        public Entities.ModuloEntity Modulo { get; set; }

        public virtual ICollection<Entities.BeneficioEntity> Beneficios { get; set; }
    }
}
