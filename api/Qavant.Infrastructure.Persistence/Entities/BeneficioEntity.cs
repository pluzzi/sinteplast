﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qavant.Infrastructure.Persistence.Entities
{
    public class BeneficioEntity : IEntity
    {

        public int Id { get; set; }

        public int TenantId { get; set; }

        public string Titulo { get; set; }

        public string Copete { get; set; }

        public string Contenido { get; set; }

        public string Imagen { get; set; }

        public DateTime FechaAlta { get; set; }

        public DateTime FechaDesde { get; set; }

        public DateTime FechaHasta { get; set; }

        public bool Habilitada { get; set; }

        public int Puntos { get; set; }

        public int TipoCatalogoProductosId { get; set; }

        public bool EsNuevo { get; set; }

        [NotMapped]
        public int ReaccionId { get; set; }

        [NotMapped]
        public int TotalReacciones { get; set; }

        public virtual ICollection<Entities.BeneficioLikeEntity> Likes { get; set; }

        public virtual ICollection<Entities.CategoriaEntity> Categorias { get; set; }
    }
}