﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Entities
{
    public class TipoCatalogoProductosEntity : IEntity
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public string IconoCatalogo { get; set; }
        public string FondoCatalogo { get; set; }
        public int EmpresaId { get; set; }
        public string Mail { get; set; }

    }
}
