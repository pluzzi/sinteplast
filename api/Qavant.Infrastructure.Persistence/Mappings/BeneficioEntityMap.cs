﻿using Qavant.Infrastructure.Persistence.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class BeneficioEntityMap : EntityTypeConfiguration<Entities.BeneficioEntity>
    {
        public BeneficioEntityMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties	
            Property(t => t.TenantId)
               .IsRequired()
               .HasColumnName("EmpresaId");

            Property(t => t.Titulo)
                .IsRequired()
                .HasColumnName("Title");

            Property(t => t.Copete)
                .IsOptional()
                .HasColumnName("SubTitle");

            Property(t => t.Contenido)
                .IsOptional()
                .HasColumnName("Content");

            Property(t => t.Imagen)
                .IsOptional()
                .HasColumnName("Image");

            Property(t => t.FechaAlta)
                .IsOptional()
                .HasColumnName("Date");

            Property(t => t.FechaDesde)
                .IsOptional()
                .HasColumnName("From");

            Property(t => t.FechaHasta)
                .IsOptional()
                .HasColumnName("To");

            Property(t => t.Habilitada)
                .IsOptional()
                .HasColumnName("Enabled");

            Property(t => t.Puntos)
               .IsOptional()
               .HasColumnName("Puntos");

            Property(t => t.TipoCatalogoProductosId)
               .IsOptional()
               .HasColumnName("TipoCatalogoProductosId");

            Property(t => t.EsNuevo)
               .IsOptional()
               .HasColumnName("EsNuevo");

            // Table and relationships 
            ToTable("Benefits");

            HasMany<CategoriaEntity>(b => b.Categorias)
                .WithMany(c => c.Beneficios)
                .Map(bc => {
                    bc.MapLeftKey("BenefitId");
                    bc.MapRightKey("CategoriaId");
                    bc.ToTable("BenefitCategories");
                });
        }
    }
}