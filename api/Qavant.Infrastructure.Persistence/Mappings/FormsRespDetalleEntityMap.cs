﻿using Qavant.Domain.Formularios;
using System.Data.Entity.ModelConfiguration;

namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class FormsRespDetalleEntityMap : EntityTypeConfiguration<Entities.FormsRespDetalleEntity>
    {
        public FormsRespDetalleEntityMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties	
            Property(t => t.CabeceraRespuestasFormId)
                .IsRequired()
                .HasColumnName("FormsRespCabeceraId");

            Property(t => t.PreguntaId)
                .IsRequired()
                .HasColumnName("FormsQuestionId");

            Property(t => t.OpcionIdSeleccionada)
                .IsOptional()
                .HasColumnName("FormsQuestionsOptionId");

            Property(t => t.RespuestaSimple)
                .IsOptional()
                .HasColumnName("Respuesta");

            Ignore(t => t.RespuestaSeleccion);

            // Table and relationships 
            ToTable("FormsRespDetalle");
        }
    }
}