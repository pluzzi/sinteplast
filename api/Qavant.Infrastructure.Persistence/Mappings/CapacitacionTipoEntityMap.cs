﻿using System.Data.Entity.ModelConfiguration;

namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class CapacitacionTipoEntityMap : EntityTypeConfiguration<Entities.CapacitacionTipoEntity>
    {
        public CapacitacionTipoEntityMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties	
            Property(t => t.Nombre)
               .IsRequired()
               .HasColumnName("Name");

            // Table and relationships 
            ToTable("CoursesTypes");
        }
    }
}