﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class TipoCatalogoProductosxUsuarioEntityMap : EntityTypeConfiguration<Entities.TipoCatalogoProductosxUsuarioEntity>
    {
        public TipoCatalogoProductosxUsuarioEntityMap()
        {
            // Primary Key
            //HasKey(t => t.Id);
            //Property(t => t.Id)
            //   .IsRequired()
            //   .HasColumnName("Id");

            // Properties	
            Property(t => t.Userid)
               .IsRequired()
               .HasColumnName("Userid");

            Property(t => t.TipoCatalogoProductoId)
               .IsRequired()
               .HasColumnName("TipoCatalogoProductoId");

            Property(t => t.PuntosAcumulados)
                .IsRequired()
                .HasColumnName("PuntosAcumulados");

            Property(t => t.FechaActualizacionPuntos)
                .IsRequired()
                .HasColumnName("FechaActualizacionPuntos");

            // Table and relationships 
            ToTable("TipoCatalogoProductosxUsuario");
        }
    }
}
