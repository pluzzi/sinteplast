﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class MultimediaTipoEntityMap : EntityTypeConfiguration<Entities.MultimediaTipoEntity>
    {
        public MultimediaTipoEntityMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties			
            Property(t => t.Descripcion)
                .IsRequired()
                .HasColumnName("Descripcion");

            Property(t => t.Nemonico)
                .IsRequired()
                .HasColumnName("Nemonico");

            // Table and relationships 
            ToTable("MultimediaTipo");
        }
    }
}