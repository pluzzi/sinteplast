﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class ElementoMultimediaEntityMap : EntityTypeConfiguration<Entities.ElementoMultimediaEntity>
    {
        public ElementoMultimediaEntityMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties	
            Property(t => t.TenantId)
               .IsRequired()
               .HasColumnName("EmpresaId");

            Property(t => t.EntidadId)
                .IsRequired()
                .HasColumnName("EntidadId");

            Property(t => t.MultimediaTipoId)
                .IsRequired()
                .HasColumnName("MultimediaTipoId");

            Property(t => t.ModuloId)
                .IsRequired()
                .HasColumnName("ModuloId");

            Property(t => t.RutaArchivo)
                .IsRequired()
                .HasColumnName("RutaArchivo");

            Property(t => t.FechaAlta)
                .IsRequired()
                .HasColumnName("FechaAlta");

            // Table and relationships 
            ToTable("Multimedia");
        }
    }
}