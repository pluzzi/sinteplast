﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class GrupoEntityMap : EntityTypeConfiguration<Entities.GrupoEntity>
    {
        public GrupoEntityMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties		
            Property(t => t.TenantId)
                .IsRequired()
                .HasColumnName("EmpresaId");

            Property(t => t.Nombre)
                .IsRequired()
                .HasColumnName("Name");

            // Table and relationships 
            ToTable("Groups");
        }
    }
}