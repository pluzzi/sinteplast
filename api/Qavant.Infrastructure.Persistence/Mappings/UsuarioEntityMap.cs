﻿using Qavant.Domain.Usuarios;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class UsuarioEntityMap : EntityTypeConfiguration<Entities.UsuarioEntity>
    {
        public UsuarioEntityMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties		
            Property(t => t.TenantId)
                .IsRequired()
                .HasColumnName("EmpresaId");

            Property(t => t.RegionId)
                .IsOptional()
                .HasColumnName("RegionId");

            Property(t => t.SucursalId)
                .IsOptional()
                .HasColumnName("SucursalId");

            Property(t => t.Nombre)
                .IsRequired()
                .HasColumnName("Name");

            Property(t => t.Password)
                .IsRequired()
                .HasColumnName("Password");

            Property(t => t.Apellido)
                .IsOptional()
                .HasColumnName("Surname");

            Property(t => t.Email)
                .IsOptional()
                .HasColumnName("Email");

            Property(t => t.Telefono)
                .IsOptional()
                .HasColumnName("Phone");

            Property(t => t.Celular)
                .IsOptional()
                .HasColumnName("Mobile");

            Property(t => t.Direccion)
                .IsOptional()
                .HasColumnName("Address");

            Property(t => t.Ciudad)
                .IsOptional()
                .HasColumnName("City");

            Property(t => t.FechaNacimiento)
                .IsOptional()
                .HasColumnName("BirthDate");

            Property(t => t.Activo)
                .IsOptional()
                .HasColumnName("Active");

            Property(t => t.Habilititado)
                .IsOptional()
                .HasColumnName("Enable");

            Property(t => t.GeneroCatalogoId)
                .IsOptional()
                .HasColumnName("GeneroCatalogoId");

            Property(t => t.Pintureria)
                .IsOptional()
                .HasColumnName("Pintureria");

            Property(t => t.CP)
                .IsOptional()
                .HasColumnName("CP");

            // Table and relationships 
            ToTable("Users");

            HasMany<Entities.GrupoEntity>(b => b.Grupos)
                .WithMany(c => c.Usuarios)
                .Map(bc => {
                    bc.MapLeftKey("UserId");
                    bc.MapRightKey("GroupId");
                    bc.ToTable("UsersGroups");
                });
        }
    }
}
