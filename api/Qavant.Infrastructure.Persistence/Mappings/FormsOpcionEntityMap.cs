﻿using Qavant.Domain.Formularios;
using System.Data.Entity.ModelConfiguration;

namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class FormsOpcionEntityMap : EntityTypeConfiguration<Entities.FormsOpcionEntity>
    {
        public FormsOpcionEntityMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties	
            Property(t => t.PreguntaId)
                .IsRequired()
                .HasColumnName("FormsQuestion_Id");

            Property(t => t.Descripcion)
                .IsOptional()
                .HasColumnName("Description");

            Property(t => t.Orden)
                .IsOptional()
                .HasColumnName("Order");

            Property(t => t.Habilitado)
                .IsOptional()
                .HasColumnName("Enabled");

            // Table and relationships 
            ToTable("FormsQuestionsOptions");
        }
    }
}