﻿using Qavant.Domain.Credencial;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class CredencialMap : EntityTypeConfiguration<Credencial>
    {
        public CredencialMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties	
            Property(t => t.TenantId)
               .IsRequired()
               .HasColumnName("EmpresaId");

            Property(t => t.Html)
                .IsRequired()
                .HasColumnName("Html");

 
            // Table and relationships 
            ToTable("Credenciales");
        }
    }
}