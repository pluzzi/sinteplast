﻿using Qavant.Domain;
using Qavant.Domain.Categorias;
using Qavant.Domain.Filtros;
using System.Data.Entity.ModelConfiguration;


namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class SegmentoMap : EntityTypeConfiguration<Segmento>
    {
        public SegmentoMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties		
            Property(t => t.TenantId)
                .IsRequired()
                .HasColumnName("EmpresaId");

            Property(t => t.ModuloId)
                .IsRequired()
                .HasColumnName("ModuloId");

            Property(t => t.EntidadId)
                .IsRequired()
                .HasColumnName("EntidadId");

            Property(t => t.FiltroId)
                .IsRequired()
                .HasColumnName("FiltroId");

            Property(t => t.FiltroValor)
                .IsRequired()
                .HasColumnName("FiltroValor");

            // Table and relationships 
            ToTable("Segmentacion");
        }
    }

}