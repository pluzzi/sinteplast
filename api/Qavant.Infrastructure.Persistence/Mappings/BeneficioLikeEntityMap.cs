﻿using System.Data.Entity.ModelConfiguration;

namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class BeneficioLikeEntityMap : EntityTypeConfiguration<Entities.BeneficioLikeEntity>
    {
        public BeneficioLikeEntityMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties	
            Property(t => t.BeneficioId)
               .IsRequired()
               .HasColumnName("BenefitId");

            Property(t => t.UsuarioId)
                .IsRequired()
                .HasColumnName("UserId");
            

            // Table and relationships 
            ToTable("BenefitLikes");
        }
    }
}