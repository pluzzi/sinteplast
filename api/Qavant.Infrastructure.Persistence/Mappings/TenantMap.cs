﻿using Qavant.Domain.Tenants;
using System.Data.Entity.ModelConfiguration;


namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class TenantMap : EntityTypeConfiguration<Tenant>
    {
        public TenantMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties			
            Property(t => t.Nombre)
                .IsRequired()
                .HasColumnName("Nombre");

            Property(t => t.TenantKeyWeb)
                .IsRequired()
                .HasColumnName("DireccionWeb");

            Property(t => t.TenantKeyApp)
                .IsRequired()
                .HasColumnName("DireccionApp");

            // Table and relationships 
            ToTable("Empresas");
        }
    }

}