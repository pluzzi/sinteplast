﻿using Qavant.Domain.Info;
using System.Data.Entity.ModelConfiguration;

namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class InfoUtilEntityMap : EntityTypeConfiguration<Entities.InfoUtilEntity>
    {
        public InfoUtilEntityMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties	
            Property(t => t.TenantId)
               .IsRequired()
               .HasColumnName("EmpresaId");

            Property(t => t.Titulo)
                .IsRequired()
                .HasColumnName("Titulo");

            Property(t => t.Copete)
                .IsOptional()
                .HasColumnName("Copete");

            Property(t => t.Html)
                .IsOptional()
                .HasColumnName("Html");

            Property(t => t.Orden)
                .IsOptional()
                .HasColumnName("Orden");

            Property(t => t.Destacada)
                .IsOptional()
                .HasColumnName("Destacada");

            Property(t => t.Imagen)
                .IsOptional()
                .HasColumnName("Imagen");

            Property(t => t.CategoriaId)
                .IsOptional()
                .HasColumnName("CategoriaId");

            Property(t => t.Habilitada)
                .IsOptional()
                .HasColumnName("Habilitado");

            // Table and relationships 
            ToTable("InfoUtil");
        }
    }
}