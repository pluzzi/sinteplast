﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class TipoCatalogoProductosEntityMap : EntityTypeConfiguration<Entities.TipoCatalogoProductosEntity>
    {
        public TipoCatalogoProductosEntityMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties	
            Property(t => t.Descripcion)
               .IsRequired()
               .HasColumnName("Descripcion");

            Property(t => t.IconoCatalogo)
                .IsRequired()
                .HasColumnName("IconoCatalogo");

            Property(t => t.FondoCatalogo)
                .IsRequired()
                .HasColumnName("FondoCatalogo");

            Property(t => t.EmpresaId)
                .IsRequired()
                .HasColumnName("EmpresaId");

            Property(t => t.Mail)
                .IsRequired()
                .HasColumnName("Mail");

            // Table and relationships 
            ToTable("TipoCatalogoProductos");
        }
    }
}
