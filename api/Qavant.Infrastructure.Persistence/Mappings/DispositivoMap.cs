﻿using Qavant.Domain.Dispositivos;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class DispositivoMap : EntityTypeConfiguration<Dispositivo>
    {
        public DispositivoMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            //// Properties	
            //Property(t => t.TenantId)
            //   .IsRequired()
            //   .HasColumnName("EmpresaId");
            Property(t => t.UsuarioId)
                .IsRequired()
                .HasColumnName("UserId");

            Property(t => t.Uuid)
                .IsOptional()
                .HasColumnName("Uuid");

            Property(t => t.TokenNotificacion)
                .IsOptional()
                .HasColumnName("TokenNotification");

            // Table and relationships 
            ToTable("Devices");
        }
    }
}