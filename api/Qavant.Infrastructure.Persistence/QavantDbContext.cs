using Qavant.Domain.Credencial;
using Qavant.Domain.Dispositivos;
using Qavant.Domain.Filtros;
using Qavant.Domain.Info;
using Qavant.Domain.Reacciones;
using Qavant.Domain.Usuarios;
using Qavant.Infrastructure.Persistence.Mappings;
using System;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using Z.EntityFramework.Plus;

namespace Qavant.Infrastructure.Persistence
{
    public class QavantDbContext : DbContext
    {
        public DbSet<Entities.NoticiaEntity> Noticias { get; private set; }
        public DbSet<Entities.CapacitacionEntity> Capacitaciones { get; private set; }
        public DbSet<Entities.CategoriaEntity> Categorias { get; private set; }
        public DbSet<Entities.CapacitacionTipoEntity> CapacitacionTipos { get; private set; }
        public DbSet<Entities.BeneficioEntity> Beneficios { get; private set; }
        public DbSet<Entities.BeneficioLikeEntity> BeneficiosLikes { get; private set; }
        public DbSet<Entities.DocumentoEntity> Documentos { get; private set; }
        public DbSet<Entities.FormsEntity> Formularios { get; private set; }
        public DbSet<Entities.FormsPreguntaEntity> Preguntas { get; private set; }
        public DbSet<Entities.FormsOpcionEntity> Opciones { get; private set; }
        public DbSet<Entities.FormsRespCabeceraEntity> CabeceraRespuestas { get; private set; }
        public DbSet<Entities.FormsRespDetalleEntity> Respuestas { get; private set; }
        public DbSet<Entities.InfoUtilEntity> ListaInfoUtil { get; private set; }
        public DbSet<Entities.UsuarioEntity> Usuarios { get; private set; }
        public DbSet<Entities.GrupoEntity> Grupos { get; private set; }
        public DbSet<Entities.GaleriaEntity> Galerias { get; private set; }
        public DbSet<Entities.ElementoMultimediaEntity> ElementosMultimedia { get; private set; }
        public DbSet<Entities.MultimediaTipoEntity> MultimediaTipos { get; private set; }
        public DbSet<Entities.ModuloEntity> Modulos { get; private set; }
        public DbSet<Entities.TipoCatalogoProductosEntity> TipoCatalogoProductos { get; private set; }
        public DbSet<Entities.TipoCatalogoProductosxUsuarioEntity> TipoCatalogoProductosxUsuario { get; private set; }

        public DbSet<Area> Areas { get; private set; }
        public DbSet<Dispositivo> Dispositivos { get; private set; }
        public DbSet<Segmento> Segmentos { get; private set; }
        public DbSet<Credencial> Credenciales { get; private set; }
        public DbSet<Reaccion> Reacciones { get; private set; }
        public DbSet<ReaccionesUsuario> ReaccionesUsuarios { get; private set; }

        public QavantDbContext(int tenantId) : base(ConfigurationManager.ConnectionStrings["QavantDbConnection"].ConnectionString)
        {
            if (tenantId <= 0)
                throw new ArgumentNullException(nameof(tenantId));

            Capacitaciones = base.Set<Entities.CapacitacionEntity>();
            Noticias = base.Set<Entities.NoticiaEntity>();
            Categorias = base.Set<Entities.CategoriaEntity>();
            CapacitacionTipos = base.Set<Entities.CapacitacionTipoEntity>();
            Beneficios = base.Set<Entities.BeneficioEntity>();
            BeneficiosLikes = base.Set<Entities.BeneficioLikeEntity>();
            Documentos = base.Set<Entities.DocumentoEntity>();
            Formularios = base.Set<Entities.FormsEntity>();
            Preguntas = base.Set<Entities.FormsPreguntaEntity>();
            Opciones = base.Set<Entities.FormsOpcionEntity>();
            CabeceraRespuestas = base.Set<Entities.FormsRespCabeceraEntity>();
            Respuestas = base.Set<Entities.FormsRespDetalleEntity>();
            ListaInfoUtil = base.Set<Entities.InfoUtilEntity>();
            Usuarios = base.Set<Entities.UsuarioEntity>();
            Grupos = base.Set<Entities.GrupoEntity>();
            Galerias = base.Set<Entities.GaleriaEntity>();
            ElementosMultimedia = base.Set<Entities.ElementoMultimediaEntity>();
            MultimediaTipos = base.Set<Entities.MultimediaTipoEntity>();
            Modulos = base.Set<Entities.ModuloEntity>();
            TipoCatalogoProductos = base.Set<Entities.TipoCatalogoProductosEntity>();
            TipoCatalogoProductosxUsuario = base.Set<Entities.TipoCatalogoProductosxUsuarioEntity>();

            Areas = base.Set<Area>();
            Dispositivos = base.Set<Dispositivo>();
            Segmentos = base.Set<Segmento>();
            Reacciones = base.Set<Reaccion>();
            ReaccionesUsuarios = base.Set<ReaccionesUsuario>();
            Credenciales = base.Set<Credencial>();

            // pre-filter any DbSet in our DbContext
            this.Filter<Entities.NoticiaEntity>(x => x.Where(q => q.TenantId == tenantId));
            this.Filter<Entities.CapacitacionEntity>(x => x.Where(q => q.TenantId == tenantId));
            this.Filter<Entities.CategoriaEntity>(x => x.Where(q => q.TenantId == tenantId));
            this.Filter<Entities.BeneficioEntity>(x => x.Where(q => q.TenantId == tenantId));
            this.Filter<Entities.DocumentoEntity>(x => x.Where(q => q.TenantId == tenantId));
            this.Filter<Entities.FormsEntity>(x => x.Where(q => q.TenantId == tenantId));
            this.Filter<Entities.InfoUtilEntity>(x => x.Where(q => q.TenantId == tenantId));
            this.Filter<Entities.UsuarioEntity>(x => x.Where(q => q.TenantId == tenantId));
            this.Filter<Entities.GrupoEntity>(x => x.Where(q => q.TenantId == tenantId));
            this.Filter<Entities.GaleriaEntity>(x => x.Where(q => q.TenantId == tenantId));
            this.Filter<Entities.ElementoMultimediaEntity>(x => x.Where(q => q.TenantId == tenantId));
            this.Filter<Entities.TipoCatalogoProductosEntity>(x => x.Where(q => q.EmpresaId == tenantId));

            this.Filter<Area>(x => x.Where(q => q.TenantId == tenantId));
            this.Filter<Segmento>(x => x.Where(q => q.TenantId == tenantId));
            this.Filter<Credencial>(x => x.Where(q => q.TenantId == tenantId));
            this.Filter<ReaccionesUsuario>(x => x.Where(q => q.TenantId == tenantId));
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Ignore<NullDispositivo>();
            modelBuilder.Ignore<NullCredencial>();
            modelBuilder.Ignore<NullReaccion>();

            modelBuilder.Configurations.Add(new NoticiaEntityMap());
            modelBuilder.Configurations.Add(new CapacitacionEntityMap());
            modelBuilder.Configurations.Add(new CapacitacionTipoEntityMap());
            modelBuilder.Configurations.Add(new CategoriaEntityMap());
            modelBuilder.Configurations.Add(new BeneficioEntityMap());
            modelBuilder.Configurations.Add(new BeneficioLikeEntityMap());
            modelBuilder.Configurations.Add(new DocumentoEntityMap());
            modelBuilder.Configurations.Add(new FormsEntityMap());
            modelBuilder.Configurations.Add(new PreguntaEntityMap());
            modelBuilder.Configurations.Add(new FormsOpcionEntityMap());
            modelBuilder.Configurations.Add(new FormsRespCabeceraEntityMap());
            modelBuilder.Configurations.Add(new FormsRespDetalleEntityMap());
            modelBuilder.Configurations.Add(new InfoUtilEntityMap());
            modelBuilder.Configurations.Add(new UsuarioEntityMap());
            modelBuilder.Configurations.Add(new GrupoEntityMap());
            modelBuilder.Configurations.Add(new GaleriaEntityMap());
            modelBuilder.Configurations.Add(new ElementoMultimediaEntityMap());
            modelBuilder.Configurations.Add(new MultimediaTipoEntityMap());
            modelBuilder.Configurations.Add(new ModuloEntityMap());
            modelBuilder.Configurations.Add(new TipoCatalogoProductosEntityMap());
            modelBuilder.Configurations.Add(new TipoCatalogoProductosxUsuarioEntityMap());


            modelBuilder.Configurations.Add(new AreaMap());
            modelBuilder.Configurations.Add(new DispositivoMap());
            modelBuilder.Configurations.Add(new SegmentoMap());
            modelBuilder.Configurations.Add(new CredencialMap());
            modelBuilder.Configurations.Add(new ReaccionMap());
            modelBuilder.Configurations.Add(new ReaccionesUsuarioMap());
        }
    }
}