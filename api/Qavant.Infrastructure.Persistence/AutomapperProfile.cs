﻿using Qavant.Domain.Beneficios;
using Qavant.Domain.Capacitaciones;
using Qavant.Domain.Categorias;
using Qavant.Domain.Documentos;
using Qavant.Domain.Formularios;
using Qavant.Domain.Galerias;
using Qavant.Domain.Info;
using Qavant.Domain.Multimedia;
using Qavant.Domain.Noticias;
using Qavant.Domain.Usuarios;
using Qavant.Domain.TipoCatalogo;
using Qavant.Infrastructure.Persistence.Entities;

namespace Qavant.Infrastructure.Persistence
{
    public class AutomapperProfile : AutoMapper.Profile
    {
        public AutomapperProfile()
        {
            CreateMap<UsuarioEntity, Usuario>();
            CreateMap<GrupoEntity, Grupo>();
            CreateMap<DocumentoEntity, Documento>();
            CreateMap<CapacitacionEntity, Capacitacion>();
            CreateMap<CategoriaEntity, Categoria>();
            CreateMap<CapacitacionTipoEntity, CapacitacionTipo>();
            CreateMap<NoticiaEntity, Noticia>();
            CreateMap<BeneficioEntity, Beneficio>();
            CreateMap<BeneficioLikeEntity, BeneficioLike>();
            CreateMap<FormsEntity, Formulario>();
            CreateMap<FormsPreguntaEntity, Pregunta>();
            CreateMap<FormsOpcionEntity, Opcion>();
            CreateMap<FormsRespCabeceraEntity, RespuestasEnFormulario>();
            CreateMap<FormsRespDetalleEntity, Respuesta>();
            CreateMap<InfoUtilEntity, InfoUtil>();
            CreateMap<GaleriaEntity, Galeria>();
            CreateMap<TipoCatalogoProductosEntity, TipoCatalogo>();
            CreateMap<TipoCatalogoProductosxUsuarioEntity, TipoCatalogoUsuario>();

            CreateMap<ElementoMultimediaEntity, ElementoMultimedia>().ForMember(destination => destination.Tipo, origin => origin.MapFrom(z => z.MultimediaTipo.Nemonico));



        }

        public static void Run()
        {
            AutoMapper.Mapper.Initialize(a =>
            {
                a.AddProfile<AutomapperProfile>();
            });
        }
    }
}
