﻿using AutoMapper;
using Qavant.Application;
using Qavant.Application.Repositories;
using Qavant.Domain.Modulos;
using Qavant.Domain.Noticias;
using Qavant.Domain.Reacciones;
using Qavant.Infrastructure.Persistence.Entities;
using Qavant.Infrastructure.Persistence.Repositories.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Qavant.Infrastructure.Persistence.Repositories
{
    public class NoticiaRepository: INoticiaRepository
    {
        private readonly ITenantHolder _tenantHolder;

        public NoticiaRepository(ITenantHolder tenantHolder)
        {
            _tenantHolder = tenantHolder;
        }

        public Noticia ObtenerNoticiaPorId(int id)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entity = db.Noticias
                                    .Where(n => n.Id == id)
                                    .Where(n => n.Habilitada)
                                    .Where(CommonFilters.CustomFilters<Entities.NoticiaEntity>(_tenantHolder.UsuarioId, ModuloEnum.Noticias, db))
                                    .Single();

                    Noticia noticia = Mapper.Map<Noticia>(entity);
                    noticia.SetInfoReaccion(new InfoReaccion() {
                        ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Noticias, entity.Id, db),
                        TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Noticias, entity.Id, db)
                    });
                    return noticia;
                }
                catch (InvalidOperationException)
                {
                    return new NullNoticia();
                }
            }
        }

        public ICollection<Noticia> ObtenerNoticias()
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entities = db.Noticias
                                .Where(x => x.Habilitada == true)
                                .OrderByDescending(x => x.FechaAlta)
                                .Where(CommonFilters.CustomFilters<Entities.NoticiaEntity>(_tenantHolder.UsuarioId, ModuloEnum.Noticias, db))
                                .ToList()
                                .Select(n => new NoticiaEntity
                                {
                                    Id = n.Id,
                                    Titulo = n.Titulo,
                                    Copete = n.Copete,
                                    Contenido = n.Contenido,
                                    Imagen = n.Imagen,
                                    FechaAlta = n.FechaAlta,
                                    Destacada = n.Destacada,
                                    Habilitada = n.Habilitada,
                                    ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Noticias, n.Id, db),
                                    TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Noticias, n.Id, db)
                                })
                                .ToList();

                    List<Noticia> noticiasResults = new List<Noticia>();
                    foreach (var entity in entities)
                    {
                        Noticia noticia = Mapper.Map<Noticia>(entity);
                        noticia.SetInfoReaccion(new InfoReaccion() {
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        noticiasResults.Add(noticia);
                    }
                    return noticiasResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }

        public ICollection<Noticia> ObtenerNoticias(int page)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    int recordsPerPage = Convert.ToInt32(_tenantHolder.Tenant.ParametrosPublicos["global"]["RecordsPerPage"]);
                    var entities = db.Noticias
                                        .Where(x => x.Habilitada == true)
                                        .OrderByDescending(x => x.FechaAlta)
                                        .Skip((page - 1) * recordsPerPage).Take(recordsPerPage)
                                        .Where(CommonFilters.CustomFilters<Entities.NoticiaEntity>(_tenantHolder.UsuarioId, ModuloEnum.Noticias, db))
                                        .ToList()
                                        .Select(n => new NoticiaEntity
                                        {
                                            Id = n.Id,
                                            Titulo = n.Titulo,
                                            Copete = n.Copete,
                                            Contenido = n.Contenido,
                                            Imagen = n.Imagen,
                                            FechaAlta = n.FechaAlta,
                                            Destacada = n.Destacada,
                                            Habilitada = n.Habilitada,
                                            ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Noticias, n.Id, db),
                                            TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Noticias, n.Id, db)
                                        })
                                        .ToList();

                    List<Noticia> noticiasResults = new List<Noticia>();
                    foreach (var entity in entities)
                    {
                        Noticia noticia = Mapper.Map<Noticia>(entity);
                        noticia.SetInfoReaccion(new InfoReaccion()
                        {
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        noticiasResults.Add(noticia);
                    }
                    return noticiasResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }

   
    }
}