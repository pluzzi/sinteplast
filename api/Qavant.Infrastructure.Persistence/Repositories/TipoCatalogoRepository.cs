﻿using AutoMapper;
using Qavant.Application;
using Qavant.Application.Repositories;
using Qavant.Domain.Galerias;
using Qavant.Domain.Modulos;
using Qavant.Infrastructure.Persistence.Repositories.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using Qavant.Domain.Reacciones;
using Qavant.Domain.TipoCatalogo;

namespace Qavant.Infrastructure.Persistence.Repositories
{
    public class TipoCatalogoRepository : ITipoCatalogoRepository
    {
        private readonly ITenantHolder _tenantHolder;

        public TipoCatalogoRepository(ITenantHolder tenantHolder)
        {
            _tenantHolder = tenantHolder;
        }

        public TipoCatalogo ObtenerTipoCatalogoPorId(int id)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entity = db.TipoCatalogoProductos
                                    .Where(g => g.Id == id)
                                    .Single();

                    TipoCatalogo tipoCatalogo = Mapper.Map<TipoCatalogo>(entity);
                    
                    return tipoCatalogo;
                }
                catch (InvalidOperationException)
                {
                    return new NullTipoCatalogo();
                }
            }
        }

        public ICollection<TipoCatalogo> ObtenerTipoCatalogos()
        {
            try
            {
                using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
                {

                    var entities = db.TipoCatalogoProductos
                        .ToList();

                    List<TipoCatalogo> tipoCatalogoResults = new List<TipoCatalogo>();
                    foreach (var entity in entities)
                    {
                        TipoCatalogo tipoCatalogo = Mapper.Map<TipoCatalogo>(entity);
                        tipoCatalogoResults.Add(tipoCatalogo);
                    }
                    return tipoCatalogoResults;
                }
                
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public ICollection<TipoCatalogoUsuario> ObtenerTipoCatalogosPorUsuarioId(int usuarioId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entities = db.TipoCatalogoProductosxUsuario
                                    .Where(g => g.Userid == usuarioId)
                                    .ToList();

                    List<TipoCatalogoUsuario> tipoCatalogoUsuarioResults = new List<TipoCatalogoUsuario>();
                    foreach (var entity in entities)
                    {
                        TipoCatalogoUsuario tipoCatalogoUsuario = Mapper.Map<TipoCatalogoUsuario>(entity);
                        var tipoCatalogoEntity = db.TipoCatalogoProductos
                                    .Where(g => g.Id == entity.TipoCatalogoProductoId)
                                    .Single();

                        TipoCatalogo tipoCatalogo = Mapper.Map<TipoCatalogo>(tipoCatalogoEntity);

                        tipoCatalogoUsuario.SetTipoCatalogo(tipoCatalogo);

                        tipoCatalogoUsuarioResults.Add(tipoCatalogoUsuario);
                    }
                    return tipoCatalogoUsuarioResults;
                }
                catch (Exception e)
                {
                    throw;
                }
            }
        }
    }
}
