﻿using Qavant.Application;
using Qavant.Application.Repositories;
using Qavant.Domain.Credencial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity;
using Qavant.Domain.Usuarios;

namespace Qavant.Infrastructure.Persistence.Repositories
{
    public class CredencialRepository : ICredencialRepository
    {
        private readonly ITenantHolder _tenantHolder;

        public CredencialRepository(ITenantHolder tenantHolder)
        {
            _tenantHolder = tenantHolder;
        }

        public Credencial GetCredencialById(int id)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var credencial = db.Credenciales                                  
                                    .Single();

                    var user = db.Usuarios
                        .Include(a => a.Area)            
                        .Where(u => u.Id == id)
                        .Single();
                    
                    Credencial credReemplazo = new Credencial();

                    credReemplazo.Id = credencial.Id;
                                        
                    credReemplazo.Html = credReemplazo.Html.Replace("#nombreUsuario", user.Nombre);        
                    credReemplazo.Html = credReemplazo.Html.Replace("#apellidoUsuario", user.Apellido);
                    credReemplazo.Html = credReemplazo.Html.Replace("#areaUsuario", user.Area.Nombre);

                    return credReemplazo;
                }
                catch (InvalidOperationException)
                {
                    return new NullCredencial();
                }
            }
        }
    }
}