﻿using AutoMapper;
using Qavant.Application;
using Qavant.Application.Repositories;
using Qavant.Domain.Formularios;
using Qavant.Domain.Modulos;
using Qavant.Domain.Reacciones;
using Qavant.Infrastructure.Persistence.Repositories.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Repositories
{
    public class FormularioRepository : IFormularioRepository
    {
        private readonly ITenantHolder _tenantHolder;

        public FormularioRepository(ITenantHolder tenantHolder)
        {
            _tenantHolder = tenantHolder;
        }

        public ICollection<Formulario> GetFormsPorTipo(TipoFormulario tipoForm, int usuarioId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entities = db.Formularios
                                         .Where(x => x.Habilitado == true)
                                         .Where(x => x.FechaDesde <= DateTime.Today)
                                         .Where(x => x.FechaHasta >= DateTime.Today)
                                         .Where(x => x.Tipo == tipoForm)
                                         .OrderByDescending(x => x.FechaCreacion)
                                         .Where(CommonFilters.CustomFilters<Entities.FormsEntity>(_tenantHolder.UsuarioId, ModuloEnum.Formularios, db))
                                         .ToList()
                                         .Select(f => new Entities.FormsEntity {
                                             Id = f.Id,
                                             Titulo = f.Titulo,
                                             Descripcion = f.Descripcion,
                                             Habilitado = f.Habilitado,
                                             RespuestaUnica = f.RespuestaUnica,
                                             Eliminado = f.Eliminado,
                                             Respondido = db.CabeceraRespuestas.Where(r => r.UsuarioId == usuarioId && r.FormularioId == f.Id).Count() > 0,
                                             ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, GetModulo(f.Tipo), f.Id, db),
                                             TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, GetModulo(f.Tipo), f.Id, db)
                                         })
                                         .ToList();

                    List<Formulario> formulariosResults = new List<Formulario>();
                    foreach (var entity in entities)
                    {
                        Formulario form = Mapper.Map<Formulario>(entity);
                        form.SetInfoReaccion(new InfoReaccion()
                        {
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        formulariosResults.Add(form);
                    }
                    return formulariosResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }

        public Formulario GetForm(int formId, int usuarioId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entity = db.Formularios
                                 .Include(f => f.Preguntas)
                                 .Include(f => f.Preguntas.Select(p => p.Opciones))
                                 .Where(f => f.Id == formId)
                                 .Where(f => f.Habilitado == true)
                                 .Where(f => f.Eliminado == false)
                                 .Where(CommonFilters.CustomFilters<Entities.FormsEntity>(_tenantHolder.UsuarioId, ModuloEnum.Formularios, db))
                                 .Single();

                    List<Entities.FormsPreguntaEntity> formTemp = new List<Entities.FormsPreguntaEntity>();
                    foreach (var item in entity.Preguntas)
                    {
                        if (item.Habilitado == true)
                        {
                            if (item.Opciones.Count > 0)
                            {
                                item.Opciones = item.Opciones.OrderBy(o => o.Orden).ToList();
                            }
                            formTemp.Add(item);
                        }
                    }


                    entity.Preguntas = formTemp.OrderBy(o => o.Orden).ToList();

                    entity.Respondido = db.CabeceraRespuestas.Where(r => r.UsuarioId == usuarioId && r.FormularioId == entity.Id).Count() > 0;
                    Formulario form = Mapper.Map<Formulario>(entity);
                    InfoReaccion infoReaccion = new InfoReaccion()
                    {
                        ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, GetModulo(entity.Tipo), entity.Id, db),
                        TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, GetModulo(entity.Tipo), entity.Id, db)
                    };
                    form.SetInfoReaccion(infoReaccion);
                    return form;
                }
                catch (InvalidOperationException ex)
                {
                    return new NullFormulario();
                }
            }
        }

        public void GuardarRespuestas(RespuestasEnFormulario respuestasEnFormulario)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                using (DbContextTransaction dbTran = db.Database.BeginTransaction())
                {
                    try
                    {
                        // Cabecera
                        var formRespCabecera = new Entities.FormsRespCabeceraEntity();
                        formRespCabecera.EstadoId = (int)respuestasEnFormulario.Estado;
                        formRespCabecera.UsuarioId = respuestasEnFormulario.Usuario.Id;
                        formRespCabecera.FormularioId = respuestasEnFormulario.Formulario.Id;
                        formRespCabecera.FechaCreacion = respuestasEnFormulario.FechaCreacion;
                        db.CabeceraRespuestas.Add(formRespCabecera);
                        db.SaveChanges();

                        // Detalle
                        foreach (var respuesta in respuestasEnFormulario.Respuestas)
                        {
                            if (respuesta.Pregunta.Tipo == TipoPregunta.Seleccion)
                            {
                                foreach (var opcion in respuesta.RespuestaSeleccion)
                                {
                                    Entities.FormsRespDetalleEntity respuestaEntity = new Entities.FormsRespDetalleEntity();
                                    respuestaEntity.CabeceraRespuestasFormId = formRespCabecera.Id;
                                    respuestaEntity.PreguntaId = respuesta.Pregunta.Id;
                                    respuestaEntity.OpcionIdSeleccionada = opcion.Id;
                                    db.Respuestas.Add(respuestaEntity);
                                }
                            }
                            else
                            {
                                Entities.FormsRespDetalleEntity respuestaEntity = new Entities.FormsRespDetalleEntity();
                                respuestaEntity.CabeceraRespuestasFormId = formRespCabecera.Id;
                                respuestaEntity.PreguntaId = respuesta.Pregunta.Id;
                                respuestaEntity.RespuestaSimple = respuesta.RespuestaSimple;
                                db.Respuestas.Add(respuestaEntity);
                            }
                        }

                        //saves all above operations within one transaction  
                        db.SaveChanges();

                        //commit transaction  
                        dbTran.Commit();
                    }
                    catch (Exception ex)
                    {
                        //Rollback transaction if exception occurs  
                        dbTran.Rollback();
                        throw;
                    }
                }
            }
        }

        private ModuloEnum GetModulo(TipoFormulario tipoForm)
        {
            ModuloEnum modulo = ModuloEnum.None;
            switch (tipoForm)
            {
                case TipoFormulario.Generico:
                    modulo = ModuloEnum.Formularios;
                    break;
                case TipoFormulario.Encuesta:
                    modulo = ModuloEnum.Encuestas;
                    break;
                case TipoFormulario.Evaluacion:
                    modulo = ModuloEnum.Capacitaciones;
                    break;
            }
            return modulo;
        }


    }
}