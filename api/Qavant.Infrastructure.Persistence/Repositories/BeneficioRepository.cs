﻿using AutoMapper;
using Qavant.Application;
using Qavant.Application.Infrastructure;
using Qavant.Application.Repositories;
using Qavant.Domain.Beneficios;
using Qavant.Domain.Modulos;
using Qavant.Domain.Reacciones;
using Qavant.Infrastructure.Persistence.Entities;
using Qavant.Infrastructure.Persistence.Repositories.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;

namespace Qavant.Infrastructure.Persistence.Repositories
{
    public class BeneficioRepository : IBeneficioRepository
    {
        private readonly ITenantHolder _tenantHolder;
        private readonly IEmailService _emailService;

        public BeneficioRepository(ITenantHolder tenantHolder, IEmailService emailService)
        {
            _tenantHolder = tenantHolder;
            _emailService = emailService;
        }

        public Beneficio GetBeneficioById(int id)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entity = db.Beneficios
                                    .Where(b => b.Id == id)
                                    .Where(b => b.Habilitada)
                                    .Where(CommonFilters.CustomFilters<Entities.BeneficioEntity>(_tenantHolder.UsuarioId, ModuloEnum.Beneficios, db))
                                    .Single();

                    Beneficio beneficio = Mapper.Map<Beneficio>(entity);
                    beneficio.SetInfoReaccion(new InfoReaccion()
                    {
                        ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Beneficios, entity.Id, db),
                        TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Beneficios, entity.Id, db)
                    });
                    return beneficio;
                }
                catch (InvalidOperationException)
                {
                    return new NullBeneficio();
                }
            }
        }

        public ICollection<Beneficio> GetBeneficios(int tipoCatalogoProductosId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entities = db.Beneficios
                        .Include(b => b.Categorias)
                        .Where(x => x.Habilitada == true)
                        .Where(x => x.TipoCatalogoProductosId == tipoCatalogoProductosId)
                        .OrderByDescending(x => x.FechaAlta)
                        .Where(CommonFilters.CustomFilters<Entities.BeneficioEntity>(_tenantHolder.UsuarioId, ModuloEnum.Beneficios, db))
                        .ToList()
                        .Select(b => new BeneficioEntity
                        {
                            Id = b.Id,
                            Titulo = b.Titulo,
                            Copete = b.Copete,
                            Contenido = b.Contenido,
                            Imagen = b.Imagen,
                            FechaDesde = b.FechaDesde,
                            FechaHasta = b.FechaHasta,
                            Puntos = b.Puntos,
                            TipoCatalogoProductosId = b.TipoCatalogoProductosId,
                            EsNuevo = b.EsNuevo,
                            ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Beneficios, b.Id, db),
                            TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Beneficios, b.Id, db)
                        })
                        .ToList();

                    List<Beneficio> beneficiosResults = new List<Beneficio>();
                    foreach (var entity in entities)
                    {
                        Beneficio beneficio = Mapper.Map<Beneficio>(entity);
                        beneficio.SetInfoReaccion(new InfoReaccion()
                        {
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        if (entity.FechaHasta.Date >= DateTime.Now.Date && entity.FechaDesde.Date <= DateTime.Now.Date)
                        {
                            beneficiosResults.Add(beneficio);
                        }
                    }
                    return beneficiosResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }

        public ICollection<Beneficio> GetBeneficios(int page, int tipoCatalogoProductosId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    int recordsPerPage = Convert.ToInt32(_tenantHolder.Tenant.ParametrosPublicos["global"]["RecordsPerPage"]);
                    DateTime desde = DateTime.Now;
                    DateTime hasta = DateTime.Now.AddDays(-1);

                    var entities = db.Beneficios
                        .Include(b => b.Categorias)
                        .Where(x => x.Habilitada == true)
                        .Where(x => x.FechaDesde <= desde && x.FechaHasta >= hasta)
                        .Where(x => x.TipoCatalogoProductosId == tipoCatalogoProductosId)
                        .OrderByDescending(x => x.FechaAlta)
                        .Skip((page - 1) * recordsPerPage).Take(recordsPerPage)
                        .Where(CommonFilters.CustomFilters<Entities.BeneficioEntity>(_tenantHolder.UsuarioId, ModuloEnum.Beneficios, db))
                        .ToList()
                        .Select(b => new BeneficioEntity
                        {
                            Id = b.Id,
                            Titulo = b.Titulo,
                            Copete = b.Copete,
                            Contenido = b.Contenido,
                            Imagen = b.Imagen,
                            FechaDesde = b.FechaDesde,
                            FechaHasta = b.FechaHasta,
                            Puntos = b.Puntos,
                            TipoCatalogoProductosId = b.TipoCatalogoProductosId,
                            EsNuevo = b.EsNuevo,
                            ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Beneficios, b.Id, db),
                            TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Beneficios, b.Id, db)
                        })
                        .ToList();

                    List<Beneficio> beneficiosResults = new List<Beneficio>();
                    foreach (var entity in entities)
                    {
                        Beneficio beneficio = Mapper.Map<Beneficio>(entity);
                        beneficio.SetInfoReaccion(new InfoReaccion()
                        {
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        //if (entity.FechaHasta.Date >= DateTime.Now.Date && entity.FechaDesde.Date <= DateTime.Now.Date)
                        //{
                           beneficiosResults.Add(beneficio);
                        //}
                    }
                    return beneficiosResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }

        public ICollection<Beneficio> GetBeneficiosPorCategoria(int page, int usuarioId, int? categoriaId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    int recordsPerPage = Convert.ToInt32(_tenantHolder.Tenant.ParametrosPublicos["global"]["RecordsPerPage"]);
                    var entities = db.Beneficios
                        .Where(x => x.Habilitada == true)
                        .Where(x => categoriaId == null || x.Categorias.Any(a => a.Id == categoriaId))
                        .OrderByDescending(x => x.FechaAlta)
                        .Skip((page - 1) * recordsPerPage).Take(recordsPerPage)
                        .Where(CommonFilters.CustomFilters<Entities.BeneficioEntity>(_tenantHolder.UsuarioId, ModuloEnum.Beneficios, db))
                        .ToList()
                        .Select(b => new BeneficioEntity
                        {
                            Id = b.Id,
                            Titulo = b.Titulo,
                            Copete = b.Copete,
                            Contenido = b.Contenido,
                            Imagen = b.Imagen,
                            FechaDesde = b.FechaDesde,
                            FechaHasta = b.FechaHasta,
                            ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Beneficios, b.Id, db),
                            TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Beneficios, b.Id, db)
                        })
                        .ToList();


                    List<Beneficio> beneficiosResults = new List<Beneficio>();
                    foreach (var entity in entities)
                    {
                        Beneficio beneficio = Mapper.Map<Beneficio>(entity);
                        beneficio.SetInfoReaccion(new InfoReaccion()
                        {
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        if (entity.FechaHasta.Date >= DateTime.Now.Date && entity.FechaDesde.Date <= DateTime.Now.Date)
                        {
                            beneficiosResults.Add(beneficio);
                        }
                    }
                    return beneficiosResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }

        #region Favoritos
        public void SetLike(int beneficioId, int usuarioId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                var LikesActuales = db.BeneficiosLikes.Where(w => w.BeneficioId == beneficioId && w.UsuarioId == usuarioId).ToList();
                if (LikesActuales.Count() == 0)
                {
                    BeneficioLikeEntity oBenefLike = new BeneficioLikeEntity();
                    oBenefLike.BeneficioId = beneficioId;
                    oBenefLike.UsuarioId = usuarioId;
                    db.BeneficiosLikes.Add(oBenefLike);
                    db.SaveChanges();
                }
                else
                {
                    db.BeneficiosLikes.RemoveRange(LikesActuales);
                    db.SaveChanges();
                }
            }
        }

        public ICollection<Beneficio> GetBeneficiosFavoritosPorCategoria(int page, int usuarioId, int? categoriaId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    int recordsPerPage = Convert.ToInt32(_tenantHolder.Tenant.ParametrosPublicos["global"]["RecordsPerPage"]);
                    var entities = db.Beneficios
                        .Where(x => x.Habilitada == true)
                        .Where(x => x.Likes.Any(a => a.UsuarioId == usuarioId))
                        .Where(x => categoriaId == null || x.Categorias.Any(a => a.Id == categoriaId))
                        .OrderByDescending(x => x.FechaAlta)
                        .Skip((page - 1) * recordsPerPage).Take(recordsPerPage)
                        .ToList()
                        .Select(b => new BeneficioEntity
                        {
                            Id = b.Id,
                            Titulo = b.Titulo,
                            Copete = b.Copete,
                            Contenido = b.Contenido,
                            Imagen = b.Imagen,
                            FechaDesde = b.FechaDesde,
                            FechaHasta = b.FechaHasta,
                            ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Beneficios, b.Id, db),
                            TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Beneficios, b.Id, db)
                        })
                        .ToList();

                    List<Beneficio> beneficiosResults = new List<Beneficio>();
                    foreach (var entity in entities)
                    {
                        Beneficio beneficio = Mapper.Map<Beneficio>(entity);
                        beneficio.SetInfoReaccion(new InfoReaccion()
                        {
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        if (entity.FechaHasta.Date >= DateTime.Now.Date && entity.FechaDesde.Date <= DateTime.Now.Date)
                        {
                            beneficiosResults.Add(beneficio);
                        }
                    }
                    return beneficiosResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }

        public ICollection<Beneficio> GetBeneficiosFavoritos(int usuarioId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entities = db.Beneficios
                        .Where(x => x.Habilitada == true)
                        .Where(x => x.Likes.Any(a => a.UsuarioId == usuarioId))
                        .OrderByDescending(x => x.FechaAlta)
                        .ToList()
                        .Select(b => new BeneficioEntity
                        {
                            Id = b.Id,
                            Titulo = b.Titulo,
                            Copete = b.Copete,
                            Contenido = b.Contenido,
                            Imagen = b.Imagen,
                            FechaDesde = b.FechaDesde,
                            FechaHasta = b.FechaHasta,
                            ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Beneficios, b.Id, db),
                            TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Beneficios, b.Id, db)
                        })
                        .ToList();

                    List<Beneficio> beneficiosResults = new List<Beneficio>();
                    foreach (var entity in entities)
                    {
                        Beneficio beneficio = Mapper.Map<Beneficio>(entity);
                        beneficio.SetInfoReaccion(new InfoReaccion()
                        {
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        if (entity.FechaHasta.Date >= DateTime.Now.Date && entity.FechaDesde.Date <= DateTime.Now.Date)
                        {
                            beneficiosResults.Add(beneficio);
                        }
                    }
                    return beneficiosResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }

        public ICollection<Beneficio> GetBeneficiosFavoritos(int page, int usuarioId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    int recordsPerPage = Convert.ToInt32(_tenantHolder.Tenant.ParametrosPublicos["global"]["RecordsPerPage"]);
                    var entities = db.Beneficios
                        .Where(x => x.Habilitada == true)
                        .Where(x => x.Likes.Any(a => a.UsuarioId == usuarioId))
                        .OrderByDescending(x => x.FechaAlta)
                        .Skip((page - 1) * recordsPerPage).Take(recordsPerPage)
                        .ToList()
                        .Select(b => new BeneficioEntity
                        {
                            Id = b.Id,
                            Titulo = b.Titulo,
                            Copete = b.Copete,
                            Contenido = b.Contenido,
                            Imagen = b.Imagen,
                            FechaDesde = b.FechaDesde,
                            FechaHasta = b.FechaHasta,
                            ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Beneficios, b.Id, db),
                            TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Beneficios, b.Id, db)
                        })
                        .ToList();

                    List<Beneficio> beneficiosResults = new List<Beneficio>();
                    foreach (var entity in entities)
                    {
                        Beneficio beneficio = Mapper.Map<Beneficio>(entity);
                        beneficio.SetInfoReaccion(new InfoReaccion()
                        {
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        if (entity.FechaHasta.Date >= DateTime.Now.Date && entity.FechaDesde.Date <= DateTime.Now.Date)
                        {
                            beneficiosResults.Add(beneficio);
                        }
                    }
                    return beneficiosResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }
        #endregion
        public bool GetLikedByBeneficioId(int id, int usuarioId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var beneficio = (from b in db.Beneficios
                                     where b.Id == id && b.Habilitada && b.Likes.Any(bl => bl.UsuarioId == usuarioId)
                                     select b).Count() > 0;
                    return beneficio;
                }
                catch (InvalidOperationException)
                {
                    return false;
                }
            }
        }

        public ICollection<Beneficio> GetAllBeneficiosPorCategoria(int usuarioId, int? categoriaId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entities = db.Beneficios
                        .Where(x => x.Habilitada == true)
                        .Where(x => categoriaId == null || x.Categorias.Any(a => a.Id == categoriaId))
                        .OrderByDescending(x => x.FechaAlta)
                        .Where(CommonFilters.CustomFilters<Entities.BeneficioEntity>(_tenantHolder.UsuarioId, ModuloEnum.Beneficios, db))
                        .ToList()
                        .Select(b => new BeneficioEntity
                        {
                            Id = b.Id,
                            Titulo = b.Titulo,
                            Copete = b.Copete,
                            Contenido = b.Contenido,
                            Imagen = b.Imagen,
                            FechaDesde = b.FechaDesde,
                            FechaHasta = b.FechaHasta,
                            ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Beneficios, b.Id, db),
                            TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Beneficios, b.Id, db)
                        })
                        .ToList();


                    List<Beneficio> beneficiosResults = new List<Beneficio>();
                    foreach (var entity in entities)
                    {
                        Beneficio beneficio = Mapper.Map<Beneficio>(entity);
                        beneficio.SetInfoReaccion(new InfoReaccion()
                        {
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        var desde = entity.FechaDesde.Date;
                        var hasta = entity.FechaHasta.Date;
                        var hoy = DateTime.Now.Date;
                        if (entity.FechaHasta.Date >= DateTime.Now.Date && entity.FechaDesde.Date <= DateTime.Now.Date)
                        {
                            beneficiosResults.Add(beneficio);
                        }

                    }
                    return beneficiosResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }

        public Exchange Canjear(int userId, int benefitId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                var userIdParameter = new SqlParameter("@UserId", userId);
                var benefitIdParameter = new SqlParameter("@BenefitsId", benefitId);

                var tempResult = db.Database
                    .SqlQuery<Exchange>("can_CanjeProducto @UserId, @BenefitsId", userIdParameter, benefitIdParameter)
                    .ToList();

                var canje = tempResult
                    .AsQueryable()
                    .Single();

                if (canje != null)
                {
                    var benefit = db.Beneficios
                                .Where(b => b.Id == benefitId)
                                .Single();

                    var tipoCatalogo = db.TipoCatalogoProductos
                                    .Where(c => c.Id == benefit.TipoCatalogoProductosId)
                                    .Single();

                    var usuario = db.Usuarios
                                    .Where(u => u.Id == userId)
                                    .Single();

                    string SMTPUsername = ConfigurationManager.AppSettings["SMTPUsername"];
                    string SMTPPassword = ConfigurationManager.AppSettings["SMTPPassword"];
                    string SMTPServer = ConfigurationManager.AppSettings["SMTPServer"];
                    int SMTPPort = int.Parse(ConfigurationManager.AppSettings["SMTPPort"]);
                    bool SMTPEnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["SMTPEnableSsl"]);
                    string emailDisplayName = tipoCatalogo.Descripcion + " - Canje de puntos";
                    string emailFrom = ConfigurationManager.AppSettings["EmailFrom"];
                    _emailService.Configure(SMTPUsername, SMTPPassword, SMTPServer, SMTPPort, SMTPEnableSsl, emailFrom, emailDisplayName);

                    string emailCuerpo = "<h2></strong>" + tipoCatalogo.Descripcion + " - Canje de puntos</strong></h2>";
                    emailCuerpo += "<p style = 'font-size: 18px;' > El cliente <strong>" + usuario.Nombre + " " + usuario.Apellido + "</strong><br>";// con DNI <strong>" + usuario.Dni + "</strong ><br>";
                    emailCuerpo += "Ha solicitado un canje de Puntos.<br>";
                    emailCuerpo += "Los datos de contacto son:<br>";
                    emailCuerpo += "Teléfono: " + usuario.Celular + "<br>";
                    emailCuerpo += "Email: " + usuario.Email + "</p>";
                    emailCuerpo += "<p style = 'font-size: 18px;'><strong> Domicilio de entrega:</strong><br>";
                    emailCuerpo += "Calle: " + usuario.Direccion + "<br>";
                    emailCuerpo += "Localidad: " + usuario.Ciudad + "<br>";
                    //emailCuerpo += "Provincia: " + usuario.Provincia + "<br>";
                    //emailCuerpo += "Código Postal: [código postal]</p>";
                    emailCuerpo += "<p style = 'font-size: 18px;'><strong> La información del canje es:</strong><br>";
                    emailCuerpo += benefit.Titulo + " - "+  benefit.Puntos.ToString() + " puntos</p>";

                    _emailService.SendMail(tipoCatalogo.Mail, emailDisplayName, emailCuerpo);
                    _emailService.SendMail(usuario.Email, emailDisplayName, emailCuerpo);
                }

                return canje;
            }
        }
    }
}