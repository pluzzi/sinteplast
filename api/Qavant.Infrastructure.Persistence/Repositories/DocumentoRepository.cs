﻿using AutoMapper;
using Qavant.Application;
using Qavant.Application.Repositories;
using Qavant.Domain.Documentos;
using Qavant.Domain.Modulos;
using Qavant.Domain.Reacciones;
using Qavant.Infrastructure.Persistence.Entities;
using Qavant.Infrastructure.Persistence.Repositories.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Repositories
{
    public class DocumentoRepository: IDocumentoRepository
    {
        private readonly ITenantHolder _tenantHolder;

        public DocumentoRepository(ITenantHolder tenantHolder)
        {
            _tenantHolder = tenantHolder;
        }

        public ICollection<Documento> GetDocumentos()
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entities = db.Documentos
                                    .Where(x => x.Enabled == true)
                                    .OrderByDescending(x => new { x.CreatedDate })
                                    .Where(CommonFilters.CustomFilters<Entities.DocumentoEntity>(_tenantHolder.UsuarioId, ModuloEnum.Documentos, db))
                                    .ToList()
                                    .Select(n => new DocumentoEntity
                                    {
                                        Id = n.Id,
                                        Title = n.Title,
                                        Description = n.Description,
                                        FilePath = n.FilePath,
                                        ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Documentos, n.Id, db),
                                        TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Documentos, n.Id, db)
                                    })
                                    .ToList();

                    List<Documento> documentosResults = new List<Documento>();
                    foreach (var entity in entities)
                    {
                        Documento documento = Mapper.Map<Documento>(entity);
                        documento.SetInfoReaccion(new InfoReaccion() {
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        documentosResults.Add(documento);
                    }
                    return documentosResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }

        public ICollection<Documento> GetDocumentos(int page)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    int recordsPerPage = Convert.ToInt32(_tenantHolder.Tenant.ParametrosPublicos["global"]["RecordsPerPage"]);
                    var entities = db.Documentos                             
                                    .Where(x => x.Enabled == true)
                                    .OrderByDescending(x => new { x.CreatedDate })
                                    .Skip((page - 1) * recordsPerPage).Take(recordsPerPage)
                                    .Where(CommonFilters.CustomFilters<Entities.DocumentoEntity>(_tenantHolder.UsuarioId, ModuloEnum.Documentos, db))
                                    .ToList()
                                    .Select(n => new DocumentoEntity
                                    {
                                        Id = n.Id,
                                        Title = n.Title,
                                        Description = n.Description,
                                        FilePath = n.FilePath,
                                        ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Documentos, n.Id, db),
                                        TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Documentos, n.Id, db)
                                    })
                                    .ToList();

                    List<Documento> documentosResults = new List<Documento>();
                    foreach (var entity in entities)
                    {
                        Documento documento = Mapper.Map<Documento>(entity);
                        documento.SetInfoReaccion(new InfoReaccion(){
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        documentosResults.Add(documento);
                    }
                    return documentosResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }

        public ICollection<Documento> GetDocumentoPorCategoriaId(int categoriaId, int page)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    int recordsPerPage = Convert.ToInt32(_tenantHolder.Tenant.ParametrosPublicos["global"]["RecordsPerPage"]);
                    var entities = db.Documentos                             
                                    .Where(x => x.CategoriaId == categoriaId)
                                    .Where(x => x.Enabled == true)
                                    .OrderByDescending(x => new { x.CreatedDate })
                                    .Skip((page - 1) * recordsPerPage).Take(recordsPerPage)
                                    .Where(CommonFilters.CustomFilters<Entities.DocumentoEntity>(_tenantHolder.UsuarioId, ModuloEnum.Documentos, db))
                                    .ToList()
                                    .Select(n => new DocumentoEntity
                                    {
                                        Id = n.Id,
                                        Title = n.Title,
                                        Description = n.Description,
                                        FilePath = n.FilePath,
                                        ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Documentos, n.Id, db),
                                        TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Documentos, n.Id, db)
                                    })
                                    .ToList();

                    List<Documento> documentosResults = new List<Documento>();
                    foreach (var entity in entities)
                    {
                        Documento documento = Mapper.Map<Documento>(entity);
                        documento.SetInfoReaccion(new InfoReaccion(){
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        documentosResults.Add(documento);
                    }
                    return documentosResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
               
            }
        }

        public ICollection<Documento> GetAllDocumentoPorCategoriaId(int categoriaId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entities = db.Documentos
                                    .Where(x => x.CategoriaId == categoriaId)
                                    .Where(x => x.Enabled == true)
                                    .OrderByDescending(x => new { x.CreatedDate })
                                    .Where(CommonFilters.CustomFilters<Entities.DocumentoEntity>(_tenantHolder.UsuarioId, ModuloEnum.Documentos, db))
                                    .ToList()
                                    .Select(n => new DocumentoEntity
                                    {
                                        Id = n.Id,
                                        Title = n.Title,
                                        Description = n.Description,
                                        FilePath = n.FilePath,
                                        ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Documentos, n.Id, db),
                                        TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Documentos, n.Id, db)
                                    })
                                    .ToList();

                    List<Documento> documentosResults = new List<Documento>();
                    foreach (var entity in entities)
                    {
                        Documento documento = Mapper.Map<Documento>(entity);
                        documento.SetInfoReaccion(new InfoReaccion()
                        {
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        documentosResults.Add(documento);
                    }
                    return documentosResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }

            }
        }

        public Documento GetDocumentoById(int id)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entity = db.Documentos
                                    .Where(d => d.Id == id)
                                    .Where(d => d.Enabled)
                                    .Where(CommonFilters.CustomFilters<Entities.DocumentoEntity>(_tenantHolder.UsuarioId, ModuloEnum.Documentos, db))
                                    .Single();

                    Documento documento = Mapper.Map<Documento>(entity);
                    documento.SetInfoReaccion(new InfoReaccion()
                    {
                        ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Documentos, entity.Id, db),
                        TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Documentos, entity.Id, db)
                    });
                    return documento;
                }
                catch (InvalidOperationException)
                {
                    return new NullDocumento();
                }
            }
        }
    }
}