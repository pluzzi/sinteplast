﻿using AutoMapper;
using Qavant.Application;
using Qavant.Application.Repositories;
using Qavant.Domain.Modulos;
using Qavant.Domain.Multimedia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Repositories
{
    public class MultimediaRepository : IMultimediaRepository
    {
        private readonly ITenantHolder _tenantHolder;

        public MultimediaRepository(ITenantHolder tenantHolder)
        {
            _tenantHolder = tenantHolder;
        }

        public ICollection<ElementoMultimedia> ObtenerElementosMultimedia(ModuloEnum modulo, int entidadId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entities = db.ElementosMultimedia
                                    .Include(c => c.MultimediaTipo)
                                    .Where(x => x.ModuloId == (int)modulo)
                                    .Where(x => x.EntidadId == entidadId)
                                    .ToList();

                    List<ElementoMultimedia> elementos = new List<ElementoMultimedia>();
                    foreach (var entity in entities)
                    {
                        ElementoMultimedia elemento = Mapper.Map<ElementoMultimedia>(entity);
                        elementos.Add(elemento);
                    }
                    return elementos;
                }
                catch (InvalidOperationException ex)
                {
                    throw;
                }
            }
        }

        public ICollection<ElementoMultimedia> ObtenerElementosMultimedia(ModuloEnum modulo, int entidadId, int? cantidad = 0)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entities = db.ElementosMultimedia
                                    .Include(c => c.MultimediaTipo)
                                    .Where(x => x.ModuloId == (int)modulo)
                                    .Where(x => x.EntidadId == entidadId)
                                    .AsQueryable();

                    if (cantidad > 0)
                    {
                        entities = entities.Take((int)cantidad);
                    }
                    var results = entities.ToList();

                    List<ElementoMultimedia> elementos = new List<ElementoMultimedia>();
                    foreach (var entity in entities)
                    {
                        ElementoMultimedia elemento = Mapper.Map<ElementoMultimedia>(entity);
                        elementos.Add(elemento);
                    }
                    return elementos;
                }
                catch (InvalidOperationException ex)
                {
                    throw;
                }
            }
        }
    }
}