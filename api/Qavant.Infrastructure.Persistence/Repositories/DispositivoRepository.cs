﻿using Qavant.Application;
using Qavant.Application.Repositories;
using Qavant.Domain.Dispositivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Repositories
{
    public class DispositivoRepository : IDispositivoRepository
    {
        private readonly ITenantHolder _tenantHolder;

        public DispositivoRepository(ITenantHolder tenantHolder)
        {
            _tenantHolder = tenantHolder;
        }

        public void RegistrarDispositivo(Dispositivo dispositivo)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var dev = db.Dispositivos.Where(w => w.Uuid == dispositivo.Uuid).ToList();
                    if (dev.Count > 0)
                    {
                        foreach (var item in dev)
                        {
                            db.Dispositivos.Remove(item);
                        }
                        db.SaveChanges();
                    }

                    db.Dispositivos.Add(dispositivo);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }
        public void RemoverDispositivo(Dispositivo dispositivo)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var dev = db.Dispositivos.Where(w => w.Uuid == dispositivo.Uuid).ToList();
                    if (dev.Count > 0)
                    {
                        foreach (var item in dev)
                        {
                            db.Dispositivos.Remove(item);
                        }
                        db.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }


        public List<Dispositivo> ObtenerDispositivos(int usuarioId, string uuid, string token)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {                                       
                    var dispositivos = db.Dispositivos
                                        .Where(x => x.Id == usuarioId)
                                        .Where(x => x.Uuid == uuid)
                                        .Where(x => x.TokenNotificacion == token).ToList();

                    return dispositivos;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public Dispositivo ObtenerDispositivo(int usuarioId, string uuid)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                var dev = db.Dispositivos.Where(w => w.Uuid == uuid).FirstOrDefault();
                return dev;
            }


        }


    }
}