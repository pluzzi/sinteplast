﻿using AutoMapper;
using Qavant.Application;
using Qavant.Application.Repositories;
using Qavant.Domain.Dispositivos;
using Qavant.Domain.Usuarios;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Repositories
{
    public class UsuarioRepository : IUsuarioRepository
    {
        private readonly ITenantHolder _tenantHolder;

        public UsuarioRepository(ITenantHolder tenantHolder)
        {
            _tenantHolder = tenantHolder;
        }

        public bool Exists(string username)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var usuario = (from u in db.Usuarios
                                   where u.UserName == username && u.Habilititado == true
                                   select u).Single();
                    return true;
                }
                catch (InvalidOperationException ex)
                {
                    return false;
                }
            }
        }

        public Usuario Login(string username, string password)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entity = (from c in db.Usuarios
                                  where c.UserName == username && c.Password == password
                                  select c).Single();

                    Usuario usuario = Mapper.Map<Usuario>(entity);
                    return usuario;
                }
                catch (InvalidOperationException ex)
                {
                    return new NullUsuario();
                }
            }
        }

        public Usuario GetUsuarioById(int usuarioId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entity = (from u in db.Usuarios.Include(u => u.Area)
                                  where u.Id == usuarioId && u.Habilititado == true
                                  select u).Single();
                    var listSettings = _tenantHolder.Tenant.ParametrosPublicos;
                    var colorPrincipal = "";
                    var colorSecundario = "";

                    foreach (var item in listSettings)
                    {
                        if (item.Key == "theme")
                        {
                            foreach (var i in item.Value)
                            {
                                if (i.Key == "PrimaryColor")
                                    colorPrincipal = i.Value;
                                if (i.Key == "SecondaryColor")
                                    colorSecundario = i.Value;
                            }
                        }
                    }
                    var primerLetraNombre = entity.Nombre.Substring(0, 1).ToUpper();
                    var primerLetraApellido = entity.Apellido.Substring(0, 1).ToUpper();
                    var texto = primerLetraApellido + primerLetraNombre;

                    Usuario usuario = Mapper.Map<Usuario>(entity);
                    usuario.Iniciales = texto;
                    return usuario;
                }
                catch (InvalidOperationException ex)
                {
                    return new NullUsuario();
                }
            }
        }

        public ICollection<Usuario> GetUsuarios()
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entities = db.Usuarios
                        .Include(u => u.Area)
                        .Where(u => u.Habilititado)
                        .OrderBy(x => x.Nombre)
                        .ToList();

                    var listSettings = _tenantHolder.Tenant.ParametrosPublicos;
                    var colorPrincipal = "";
                    var colorSecundario = "";

                    foreach (var item in listSettings)
                    {
                        if (item.Key == "theme")
                        {
                            foreach (var i in item.Value)
                            {
                                if (i.Key == "PrimaryColor")
                                    colorPrincipal = i.Value;
                                if (i.Key == "SecondaryColor")
                                    colorSecundario = i.Value;
                            }
                        }
                    }
                    List<Usuario> usuariosResults = new List<Usuario>();
                    foreach (var entity in entities)
                    {
                        var primerLetraNombre = entity.Nombre.Substring(0, 1).ToUpper();
                        var primerLetraApellido = entity.Apellido.Substring(0, 1).ToUpper();
                        var texto = primerLetraApellido + primerLetraNombre;

                        Usuario usuario = Mapper.Map<Usuario>(entity);
                        usuario.Iniciales = texto;
                        usuariosResults.Add(usuario);
                    }

                    return usuariosResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }

        public ICollection<Usuario> GetUsuarios(int page)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    int recordsPerPage = Convert.ToInt32(_tenantHolder.Tenant.ParametrosPublicos["global"]["RecordsPerPage"]);
                    var entities = db.Usuarios
                        .Include(u => u.Area)
                        .Where(u => u.Habilititado)
                        .OrderBy(x => x.Nombre)
                        .Skip((page - 1) * recordsPerPage).Take(recordsPerPage)
                        .ToList();

                    var listSettings = _tenantHolder.Tenant.ParametrosPublicos;
                    var colorPrincipal = "";
                    var colorSecundario = "";

                    foreach (var item in listSettings)
                    {
                        if (item.Key == "theme")
                        {
                            foreach (var i in item.Value)
                            {
                                if (i.Key == "PrimaryColor")
                                    colorPrincipal = i.Value;
                                if (i.Key == "SecondaryColor")
                                    colorSecundario = i.Value;
                            }
                        }
                    }
                    List<Usuario> usuariosResults = new List<Usuario>();
                    foreach (var entity in entities)
                    {
                        var primerLetraNombre = entity.Nombre.Substring(0, 1).ToUpper();
                        var primerLetraApellido = entity.Apellido.Substring(0, 1).ToUpper();
                        var texto = primerLetraApellido + primerLetraNombre;


                        Usuario usuario = Mapper.Map<Usuario>(entity);
                        usuario.Iniciales = texto;
                        usuariosResults.Add(usuario);
                    }
                    return usuariosResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }

        public ICollection<Usuario> Search(int page, string searchTerm)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    int recordsPerPage = Convert.ToInt32(_tenantHolder.Tenant.ParametrosPublicos["global"]["RecordsPerPage"]);
                    var entities = db.Usuarios.Where(u => u.Habilititado)
                                      .Where(x => x.Nombre.Contains(searchTerm) || x.Apellido.Contains(searchTerm))
                                      .OrderBy(x => x.Nombre)
                                      .Skip((page - 1) * recordsPerPage).Take(recordsPerPage)
                                      .ToList();
                    var listSettings = _tenantHolder.Tenant.ParametrosPublicos;
                    var colorPrincipal = "";
                    var colorSecundario = "";

                    foreach (var item in listSettings)
                    {
                        if (item.Key == "theme")
                        {
                            foreach (var i in item.Value)
                            {
                                if (i.Key == "PrimaryColor")
                                    colorPrincipal = i.Value;
                                if (i.Key == "SecondaryColor")
                                    colorSecundario = i.Value;
                            }
                        }
                    }
                    List<Usuario> usuariosResults = new List<Usuario>();
                    foreach (var entity in entities)
                    {
                        var primerLetraNombre = entity.Nombre.Substring(0, 1).ToUpper();
                        var primerLetraApellido = entity.Apellido.Substring(0, 1).ToUpper();
                        var texto = primerLetraApellido + primerLetraNombre;

                        Usuario usuario = Mapper.Map<Usuario>(entity);
                        usuario.Iniciales = texto;
                        usuariosResults.Add(usuario);
                    }
                    return usuariosResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }

        public Usuario GetUsuarioByUsername(string username)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entity = (from u in db.Usuarios
                                  where u.UserName == username && u.Habilititado == true
                                  select u).Single();
                    var listSettings = _tenantHolder.Tenant.ParametrosPublicos;
                    var colorPrincipal = "";
                    var colorSecundario = "";

                    foreach (var item in listSettings)
                    {
                        if (item.Key == "theme")
                        {
                            foreach (var i in item.Value)
                            {
                                if (i.Key == "PrimaryColor")
                                    colorPrincipal = i.Value;
                                if (i.Key == "SecondaryColor")
                                    colorSecundario = i.Value;
                            }
                        }
                    }
                    var primerLetraNombre = entity.Nombre.Substring(0, 1).ToUpper();
                    var primerLetraApellido = entity.Apellido.Substring(0, 1).ToUpper();
                    var texto = primerLetraApellido + primerLetraNombre;

                    Usuario usuario = Mapper.Map<Usuario>(entity);
                    return usuario;
                }
                catch (InvalidOperationException ex)
                {
                    return new NullUsuario();
                }
            }
        }

        public void Actualizar(Usuario usuario)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entity = db.Usuarios.SingleOrDefault(u => u.Id == usuario.Id);
                    if (entity == null)
                        throw new Exception("Problemas al actualizar datos de usuario.");

                    db.Entry(entity).CurrentValues.SetValues(usuario);
                    db.SaveChanges();
                }
                catch (InvalidOperationException ex)
                {
                    throw;
                }
            }
        }

        public void ResetPassword(Usuario usuario)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    //usuario.SetActivo(false);
                    usuario.SetPassword(usuario.UserName);

                    var entity = db.Usuarios.SingleOrDefault(u => u.Id == usuario.Id);
                    if (entity == null)
                        throw new Exception("Ha surgido un error interno en el servidor. Inténtelo de nuevo más tarde.");

                    db.Entry(entity).CurrentValues.SetValues(usuario);
                    List<Dispositivo> dispositivos = db.Dispositivos.Where(d => d.UsuarioId == usuario.Id).ToList();
                    db.Dispositivos.RemoveRange(dispositivos);
                    db.SaveChanges();
                }
                catch (InvalidOperationException ex)
                {
                    throw;
                }
            }
        }
    }
}