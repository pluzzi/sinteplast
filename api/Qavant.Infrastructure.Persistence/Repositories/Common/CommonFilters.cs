﻿using Qavant.Domain.Modulos;
using Qavant.Domain.Muro;
using Qavant.Infrastructure.Persistence.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Repositories.Common
{
    public static class CommonFilters
    {


        public static Expression<Func<Publicacion, bool>> CustomFiltersMuro(int usuarioId, QavantDbContext db)
        {
            return entidad =>
                            (db.Segmentos.Where(s => s.EntidadId == entidad.Id && s.ModuloId == entidad.ModuloId).Count() == 0) || //si no hay segmentación, entonces mostrar todo los resultados
                            (db.Segmentos.Where(s => s.EntidadId == entidad.Id && s.ModuloId == entidad.ModuloId).Count() > 0 //si hay segmentación, aplicar los filtros correspondientes
                            &&
                                (
                                //Filtro por Area
                                (db.Segmentos.Where(s => s.EntidadId == entidad.Id && s.ModuloId == entidad.ModuloId)
                                                    .Where(s => s.FiltroId == 4)
                                                    .Where(s => db.Usuarios.Where(u => u.Id == usuarioId).FirstOrDefault().Area.Id == s.FiltroValor)
                                                    .Count() > 0)
                                ||
                                //Filtro por Grupo
                                (db.Segmentos.Where(s => s.EntidadId == entidad.Id && s.ModuloId == entidad.ModuloId)
                                                    .Where(s => s.FiltroId == 2)
                                                    .ToList()
                                                    .Where(s => db.Usuarios
                                                                        .Where(u => u.Id == usuarioId)
                                                                        .Where(u => u.Grupos.Any(a => a.Id == s.FiltroValor))
                                                                        .Count() > 0)
                                                    .Count() > 0)
                                ||
                                //Filtro por Genero
                                (db.Segmentos.Where(s => s.EntidadId == entidad.Id && s.ModuloId == entidad.ModuloId)
                                                    .Where(s => s.FiltroId == 1)
                                                    .Where(s => db.Usuarios.Where(u => u.Id == usuarioId).FirstOrDefault().GeneroCatalogoId == s.FiltroValor)
                                                    .Count() > 0)
                                
                                 ||
                                //Filtro por Region
                                (db.Segmentos.Where(s => s.EntidadId == entidad.Id && s.ModuloId == entidad.ModuloId)
                                                    .Where(s => s.FiltroId == 5)
                                                    .Where(s => db.Usuarios.Where(u => u.Id == usuarioId).FirstOrDefault().RegionId == s.FiltroValor)
                                                    .Count() > 0)


                                ||
                                //Filtro por Sucursal
                                (db.Segmentos.Where(s => s.EntidadId == entidad.Id && s.ModuloId == entidad.ModuloId)
                                                    .Where(s => s.FiltroId == 6)
                                                    .Where(s => db.Usuarios.Where(u => u.Id == usuarioId).FirstOrDefault().SucursalId == s.FiltroValor)
                                                    .Count() > 0)
                                )
                            );
        }

        public static Expression<Func<TEntity, bool>> CustomFilters<TEntity>(int usuarioId, ModuloEnum modulo, QavantDbContext db) where TEntity : class, IEntity
        {
            return entidad =>
                            (db.Segmentos.Where(s => s.EntidadId == entidad.Id && s.ModuloId == (int)modulo).Count() == 0) || //si no hay segmentación, entonces mostrar todo los resultados
                            (db.Segmentos.Where(s => s.EntidadId == entidad.Id && s.ModuloId == (int)modulo).Count() > 0 //si hay segmentación, aplicar los filtros correspondientes
                            &&
                                (
                                //Filtro por Area
                                (db.Segmentos.Where(s => s.EntidadId == entidad.Id && s.ModuloId == (int)modulo)
                                                    .Where(s => s.FiltroId == 4)
                                                    .Where(s => db.Usuarios.Where(u => u.Id == usuarioId).FirstOrDefault().Area.Id == s.FiltroValor)
                                                    .Count() > 0)
                                ||
                                //Filtro por Grupo
                                (db.Segmentos.Where(s => s.EntidadId == entidad.Id && s.ModuloId == (int)modulo)
                                                    .Where(s => s.FiltroId == 2)
                                                    .ToList()
                                                    .Where(s => db.Usuarios
                                                                        .Where(u => u.Id == usuarioId)
                                                                        .Where(u => u.Grupos.Any(a => a.Id == s.FiltroValor))
                                                                        .Count() > 0)
                                                    .Count() > 0)
                                ||
                                //Filtro por Genero
                                (db.Segmentos.Where(s => s.EntidadId == entidad.Id && s.ModuloId == (int)modulo)
                                                    .Where(s => s.FiltroId == 1)
                                                    .Where(s => db.Usuarios.Where(u => u.Id == usuarioId).FirstOrDefault().GeneroCatalogoId == s.FiltroValor)
                                                    .Count() > 0)

                                ||
                                //Filtro por Region
                                (db.Segmentos.Where(s => s.EntidadId == entidad.Id && s.ModuloId == (int)modulo)
                                                    .Where(s => s.FiltroId == 5)
                                                    .Where(s => db.Usuarios.Where(u => u.Id == usuarioId).FirstOrDefault().RegionId == s.FiltroValor)
                                                    .Count() > 0)


                                ||
                                //Filtro por Sucursal
                                (db.Segmentos.Where(s => s.EntidadId == entidad.Id && s.ModuloId == (int)modulo)
                                                    .Where(s => s.FiltroId == 6)
                                                    .Where(s => db.Usuarios.Where(u => u.Id == usuarioId).FirstOrDefault().SucursalId == s.FiltroValor)
                                                    .Count() > 0)
                                )
                            );
        }
    }
}
