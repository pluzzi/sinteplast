﻿using Qavant.Application;
using Qavant.Application.Repositories;
using Qavant.Domain.Tenants;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;

/*
 * Returning datatable using entity framework: https://goo.gl/z5ieCZ
 * 
 */

namespace Qavant.Infrastructure.Persistence.Repositories
{
    public class TenantRepository : ITenantRepository
    {
        public Tenant GetTenantByKey(Expression<Func<Tenant, bool>> predicate)
        {
            using (var db = new QavantTenantDbContext())
            {
                try
                {
                    var tenant = db.Tenants.Where(predicate).Single();
                    return tenant;
                }
                catch (InvalidOperationException)
                {
                    return new NullTenant();
                }
            }
        }

        public Tenant GetTenantById(int tenantId)
        {
            using (var db = new QavantTenantDbContext())
            {
                try
                {
                    var tenant = db.Tenants.Where(t => t.Id == tenantId).Single();
                    return tenant;
                }
                catch (InvalidOperationException)
                {
                    return new NullTenant();
                }
            }
        }

        public Tenant GetCurrentTenant(Canal canal, int tenantId)
        {
            using (var context = new QavantTenantDbContext())
            {
                try
                {
                    var tenant = context.Tenants.Where(t => t.Id == tenantId).Single();
                    SetParametrosInternos(context, canal, tenant);
                    SetParametrosPublicos(context, canal, tenant);
                    return tenant;
                }
                catch (InvalidOperationException)
                {
                    return new NullTenant();
                }
            }
        }

        #region Internal members
        private void SetParametrosInternos(DbContext context, Canal canal, Tenant tenant)
        {
            tenant.AgregarParametroInterno("login", GetParams(context, canal, tenant, "LOGIN"));
            tenant.AgregarParametroInterno("email", GetParams(context, canal, tenant, "EMAIL"));
        }

        private void SetParametrosPublicos(DbContext context, Canal canal, Tenant tenant)
        {
            tenant.AgregarParametroPublico("global", GetParams(context, canal, tenant, "PARAM"));
            tenant.AgregarParametroPublico("theme", GetParams(context, canal, tenant, "THEME"));
            tenant.AgregarParametroPublico("menu", GetMenuParams(context, canal, tenant));
            tenant.AgregarParametroPublico("reacciones", GetReaccionesParams(context, canal, tenant));
        }

        private Dictionary<string, string> GetParams(DbContext context, Canal canal, Tenant tenant, string nemonico)
        {
            Dictionary<string, string> parametros = new Dictionary<string, string>();
            string query = $@"select pa.Nemonico, 
                                     ep.Valor 
                              from ParametrosEmpresasCanal ep 
                              inner join Parametros pa on pa.Id = ep.ParametroId
                              inner join modulos mo on mo.id = pa.ModuloId
                              where ep.empresaid = {tenant.Id} and CanalId={(int)canal} and mo.Nemonico = '{nemonico}'";
            DataTable dt = context.DataTable(query);
            foreach (DataRow row in dt.Rows)
            {
                parametros.Add(row["Nemonico"].ToString(), row["Valor"].ToString());
            }

            #if DEBUG
            parametros["AssemblyPath"] = ConfigurationManager.AppSettings["AssemblyPath"];
            #endif
            return parametros;
        }

        private List<dynamic> GetReaccionesParams(DbContext context, Canal canal, Tenant tenant)
        {
            dynamic reacciones = new List<dynamic>();
            string query = $@"SELECT * FROM [dbo].[Reacciones] ORDER BY Orden";
            DataTable dt = context.DataTable(query);
            foreach (DataRow row in dt.Rows)
            {
                reacciones.Add(new
                {
                    id = row["Id"].ToString(),
                    nombre = row["Nombre"].ToString(),
                    icono = row["Icono"].ToString(),
                    orden = row["Orden"].ToString()
                });
            }
            return reacciones;
        }

        private List<dynamic> GetMenuParams(DbContext context, Canal canal, Tenant tenant)
        {
            dynamic menu = new List<dynamic>();
            string query = $@"SELECT * FROM [dbo].[MenuItems] 
                              WHERE EmpresaId={tenant.Id} and CanalId={(int)canal} 
                              ORDER BY Orden";
            DataTable dt = context.DataTable(query);
            foreach (DataRow row in dt.Rows)
            {
                menu.Add(new
                {
                    label = row["NombreItem"].ToString(),
                    routerLink = row["TargetURL"].ToString(),
                    icon = row["Icono"].ToString(),
                    iconType = row["IconoTipo"].ToString(),
                    order = row["Orden"].ToString()
                });
            }
            return menu;
        }
        #endregion
    }
}