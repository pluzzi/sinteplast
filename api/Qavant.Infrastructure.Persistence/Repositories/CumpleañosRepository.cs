﻿using AutoMapper;
using Qavant.Application;
using Qavant.Application.Repositories;
using Qavant.Domain.Modulos;
using Qavant.Domain.Reacciones;
using Qavant.Domain.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Repositories
{
    public class CumpleañosRepository : ICumpleañosRepository
    {
        private readonly ITenantHolder _tenantHolder;

        public CumpleañosRepository(ITenantHolder tenantHolder)
        {
            _tenantHolder = tenantHolder;
        }

        public ICollection<Cumpleaños> GetCumpleaños()
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    List<Cumpleaños> cumpleañosList = new List<Cumpleaños>();

                    var currentWeekStart = DateTime.Now.StartOfWeek(DayOfWeek.Monday);
                    var currentWeekEnd = DateTime.Now.Next(DayOfWeek.Sunday);
                    var nextThirtyDays = DateTime.Today.AddDays(30);

                    var todayList = db.Usuarios.Include(u => u.Area).Where(w => w.Habilititado)
                               .Where(u => (u.FechaNacimiento.Year != 1900) && (u.FechaNacimiento.Month == DateTime.Now.Month && u.FechaNacimiento.Day == DateTime.Now.Day))
                               .OrderBy(x => new { x.FechaNacimiento.Month, x.FechaNacimiento.Day })
                               .ToList();

                    var thisMonthList = db.Usuarios.Include(u => u.Area).Where(w => w.Habilititado)
                               .Where(u => (u.FechaNacimiento.Year != 1900) && ((u.FechaNacimiento.Month == DateTime.Now.Month) && (u.FechaNacimiento.Day > DateTime.Now.Day)))
                               .OrderBy(x => new { x.FechaNacimiento.Month })
                               .OrderBy(x => new { x.FechaNacimiento.Day })
                               .ToList();

                    var nextMonthList = db.Usuarios.Include(u => u.Area).Where(w => w.Habilititado)
                               .Where(u => (u.FechaNacimiento.Year != 1900) && ((u.FechaNacimiento.Month == nextThirtyDays.Month) && (u.FechaNacimiento.Day <= nextThirtyDays.Day)))
                               .OrderBy(x => new { x.FechaNacimiento.Month })
                               .OrderBy(x => new { x.FechaNacimiento.Day })
                               .ToList();

                    var listSettings = _tenantHolder.Tenant.ParametrosPublicos;
                    var colorPrincipal = "";
                    var colorSecundario = "";

                    foreach (var item in listSettings)
                    {
                        if (item.Key == "theme")
                        {
                            foreach (var i in item.Value)
                            {
                                if (i.Key == "PrimaryColor")
                                    colorPrincipal = i.Value;
                                if (i.Key == "SecondaryColor")
                                    colorSecundario = i.Value;
                            }
                        }
                    }
                    List<Entities.UsuarioEntity> birthdayList = new List<Entities.UsuarioEntity>();
                    List<Entities.UsuarioEntity> birthdayListTemp = new List<Entities.UsuarioEntity>();

                    foreach (var item in todayList)
                    {
                        birthdayListTemp.Add(item);
                    }
                    foreach (var item in thisMonthList)
                    {
                        birthdayListTemp.Add(item);
                    }
                    foreach (var item in nextMonthList)
                    {
                        birthdayListTemp.Add(item);
                    }
                    // eliminates duplicates
                    birthdayList = birthdayListTemp.Distinct().ToList();


                    foreach (var entity in birthdayList)
                    {
                        var primerLetraNombre = entity.Nombre.Substring(0, 1).ToUpper();
                        var primerLetraApellido = entity.Apellido.Substring(0, 1).ToUpper();
                        var texto = primerLetraApellido + primerLetraNombre;

                        Cumpleaños cumpleañosItem = new Cumpleaños();
                        cumpleañosItem.SetUsuario(Mapper.Map<Usuario>(entity));
                        cumpleañosItem.Usuario.Iniciales = texto;
                        cumpleañosItem.SetInfoReaccion(new InfoReaccion() {
                            ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Cumpleaños, entity.Id, db),
                            TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Cumpleaños, entity.Id, db)
                        });
                        cumpleañosList.Add(cumpleañosItem);
                    }
                    return cumpleañosList;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }

        public ICollection<Cumpleaños> GetCumpleaños(int page)
        {
            int recordsPerPage = Convert.ToInt32(_tenantHolder.Tenant.ParametrosPublicos["global"]["RecordsPerPage"]);
            var birthdayList = GetCumpleaños();
            return birthdayList.Skip((page - 1) * recordsPerPage).Take(recordsPerPage).ToList();
        }
    }
}
