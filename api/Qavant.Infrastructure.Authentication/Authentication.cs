﻿using Qavant.Application.Infrastructure;
using Qavant.Infrastructure.Authentication.Nutrien;
using Qavant.Infrastructure.Authentication.PluginInterface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Qavant.Infrastructure.Authentication
{
    public class Authentication : IAuthentication
    {
        private Dictionary<string, string> _parameters;

        public void Configure(Dictionary<string, string> parameters)
        {
            _parameters = parameters;
        }

        public bool Validate(int tenantId, string username, string password)
        {
            try
            {
                IAuthenticationProvider authenticationProvider = GetAuthenticationProvider(_parameters["AssemblyPath"]);
                //IAuthenticationProvider authenticationProvider = new NutrienDirectoryProvider();
                authenticationProvider.SetParameters(_parameters);
                return authenticationProvider.Validate(tenantId, username, password);
            }
            catch (Exception ex)
            {
                throw new AuthenticationProviderException(ex.Message);
            }
        }

        /// <summary>
        /// Factory pattern with reflection
        /// </summary>
        /// <param name="assemblyPath"></param>
        /// <returns></returns>
        private IAuthenticationProvider GetAuthenticationProvider(string assemblyPath)
        {
            string assembly = Path.GetFullPath(assemblyPath);
            Assembly ptrAssembly = Assembly.LoadFile(assembly);
            foreach (Type item in ptrAssembly.GetTypes())
            {
                if (!item.IsClass) continue;
                if (item.GetInterfaces().Contains(typeof(IAuthenticationProvider)))
                {
                    return (IAuthenticationProvider)Activator.CreateInstance(item);
                }
            }
            throw new Exception("Invalid DLL, Interface not found!");
        }
    }
}
