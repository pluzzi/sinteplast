﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantApi.Http.Models
{
    public class Area
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}