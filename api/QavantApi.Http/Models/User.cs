using Microsoft.AspNet.Identity.EntityFramework;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using QavantApi.Http.Controllers;
using System.Globalization;

namespace QavantApi.Http.Models
{
    public class AppRole : IdentityRole
    {
        public AppRole() : base() { }
        public AppRole(string name) : base(name) { }
    }

    public class QavantUser
    {
        public int Id { get; internal set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public DateTime BirthDate { get; set; }
        public bool Active { get; set; }
        public bool Enable { get; set; }
        public DateTime RegistrationDate { get; set; }
        public Area Area { get; set; }
        public int EmpresaId { get; set; }
        public ICollection<UsersGroup> UsersGroup { get; set; }

        public bool IsBirthdayToday
        {
            get
            {
                if (this.BirthDate == null)
                    return false;

                bool result = false;
                if (DateTime.Today.Day == this.BirthDate.Day && DateTime.Today.Month == this.BirthDate.Month)
                {
                    result = true;
                }
                return result;
            }
        }

        public string BirthFriendlyDate
        {
            get
            {
                if (this.BirthDate == null)
                    return string.Empty;
                         
                DateTimeFormatInfo dtinfo = new CultureInfo("es-ES", false).DateTimeFormat;
                string dia = this.BirthDate.Day.ToString();
                string mes = dtinfo.GetMonthName(this.BirthDate.Month);

                string formatted = $"{dia} de {mes}";
                return formatted;
            }
        }
    }
}