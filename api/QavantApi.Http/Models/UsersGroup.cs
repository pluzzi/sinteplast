﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantApi.Http.Models
{
    public  class UsersGroup
    {
        public long Id { get; set; }
        public int UserId { get; set; }
        public int GroupId { get; set; }
        public QavantUser User { get; set; }
    }
}