using System;

namespace QavantApi.Http.Models
{
    public class ActionLog
    {
        public ActionLog()
        {
            Date = DateTime.Now;
        }

        public int Id { get; set; }
        public QavantUser User { get; set; }
        public Byte SectionId { get; set; }
        public int ItemId { get; set; }
        public DateTime Date { get; set; }
        public Byte ActionLogTypeId { get; set; }
    }
}