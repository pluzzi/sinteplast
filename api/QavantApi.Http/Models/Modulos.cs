using System;
using System.Collections.Generic;

namespace QavantApi.Http.Models
{
    public class Modulos
    {

        public int Id { get; set; }
        public string Nombre { get; set; }
        public bool UsoEnCategorias { get; set; }
        public string Nemonico { get; set; }
    }
}