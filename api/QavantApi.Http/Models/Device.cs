﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantApi.Http.Models
{
    public class Device
    {
        public Int64 Id { get; set; }
        public int UserId { get; set; }
        public string Uuid { get; set; }
        public string TokenNotification { get; set; }
    }
}