﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace QavantApi.Http.Models
{
    public class QavantDbContext : DbContext
    {
        public QavantDbContext() : base(ConfigurationManager.ConnectionStrings["QavantDbConnection"].ConnectionString)
        {
            Database.SetInitializer<QavantDbContext>(null);
        }

        public IDbSet<QavantUser> Users { get; set; }

        public IDbSet<ActionLog> ActionLogs { get; set; }

        public IDbSet<Device> Devices { get; set; }

        public IDbSet<Categoria> Categorias { get; set; }

        public IDbSet<UsersGroup> UsersGroup { get; set; }

        public IDbSet<Modulos> Modulos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new QavantUserMap());
        }
    }

    public class QavantUserMap : EntityTypeConfiguration<QavantUser>
    {
        public QavantUserMap()
        {
            ToTable("Users");
            HasKey(x => x.Id);
        }
    }
}