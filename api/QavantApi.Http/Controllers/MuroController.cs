﻿using Qavant.Application.Services.Muro;
using Qavant.Infrastructure.ApplicationLogger;
using Qavant.Infrastructure.Persistence.Repositories;
using QavantApi.Http.Infrastructure.Authorization;
using QavantApi.Http.Infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;

namespace QavantApi.Http.Controllers
{
    [QavantApiAuthorization]
    public class MuroController : QavantApiBaseController
    {
        private readonly IMuroService _muroService;

        public MuroController(IMuroService muroService)
        {
            _muroService = muroService;
        }

        [Route("muro/all")]
        public List<PublicacionOutput> GetPublicaciones()
        {
            var output = _muroService.GetPublicaciones();
            return output.Publicaciones;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}