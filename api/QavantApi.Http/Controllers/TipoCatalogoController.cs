﻿using Qavant.Application.Services.Galerias;
using Qavant.Application.Services.Shared;
using Qavant.Application.Services.TipoCatalogo;
using Qavant.Infrastructure.ApplicationLogger;
using Qavant.Infrastructure.Persistence.Repositories;
using QavantApi.Http.Infrastructure.Authorization;
using QavantApi.Http.Infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;

namespace QavantApi.Http.Controllers
{
    [QavantApiAuthorization]
    public class TipoCatalogoController : QavantApiBaseController
    {
        private readonly ITipoCatalogoService _tipoCatalogoService;

        public TipoCatalogoController(ITipoCatalogoService tipoCatalogoService)
        {
            _tipoCatalogoService = tipoCatalogoService;
        }

        /// <summary>
        /// Obtiene el Tipo de Catalogo de Id referente
        /// </summary>
        /// <param name="id">Identificador del Tipo de Catalogo</param>
        /// <returns></returns>
        [Route("tipoCatalogo/id/{id}")]
        [HttpGet]
        public TipoCatalogoOutput ObtenerTipoCatalogoPorId(int id)
        {
            var output = _tipoCatalogoService.ObtenerTipoCatalogoPorId(id);
            return output;
        }

        /// <summary>
        /// Obtiene la lista de Tipos de Catalogos
        /// </summary>
        /// <returns></returns>
        [Route("tipoCatalogo")]
        [HttpGet]
        public List<TipoCatalogoOutput> ObtenerTipoCatalogos()
        {
            var output = _tipoCatalogoService.ObtenerTipoCatalogos();
            return output;
        }

        /// <summary>
        /// Obtiene los tipos de catalogos relacionados a un usuario
        /// </summary>
        /// <param name="userId">Id de usuario</param>
        /// <returns></returns>
        [Route("tipoCatalogo/usuario/{userId}")]
        [HttpGet]
        public List<TipoCatalogoUsuarioOutput> ObtenerTipoCatalogosPorUsuarioId(int userId)
        {
            var output = _tipoCatalogoService.ObtenerTipoCatalogosPorUsuarioId(userId);
            return output;
        }
        
    }
}

//[Route("galerias/categoria/{categoriaId}/pagina/{pagina}")]