﻿using JWT;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using Qavant.Application.Services.Dispositivos;
using QavantApi.Http.Infrastructure.Authorization;
using QavantApi.Http.Infrastructure.Exceptions;
using QavantApi.Http.Infrastructure.FormObjects;
using QavantApi.Http.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;

namespace QavantApi.Http.Controllers
{
    [QavantApiAuthorization]
    public class DeviceController : QavantApiBaseController
    {
        private readonly IDispositivoService _dispositivoService;

        public DeviceController(IDispositivoService dispositivoService)
        {
            _dispositivoService = dispositivoService;
        }

        [Route("register_device")]
        public async Task<IHttpActionResult> Index(DevicePost devicePost)
        {
            //_dispositivoService.RegistrarDispositivo(UsuarioId, devicePost.Uuid, devicePost.TokenNotification);
            _dispositivoService.RegistrarDispositivo(devicePost.UserId, devicePost.Uuid, devicePost.TokenNotification);
            return Ok();
        }

        [Route("remove_device")]
        public async Task<IHttpActionResult> Remove(DevicePost devicePost)
        {
            _dispositivoService.RemoverDispositivo(devicePost.UserId,devicePost.Uuid);
            return Ok();
        }
    }
}