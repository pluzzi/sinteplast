﻿using Qavant.Application.Services.Credenciales;
using QavantApi.Http.Infrastructure.Authorization;
using System.Collections.Generic;
using System.Web.Http;

namespace QavantApi.Http.Controllers
{
    [QavantApiAuthorization]
    public class CredencialesController : QavantApiBaseController
    {
        private readonly ICredencialService _credencialService;

        public CredencialesController(ICredencialService credencialService)
        {
            _credencialService = credencialService;
        }

        [Route("credencial/{id}")]
        public CredencialOutput GetCredencial(int id)
        {
            var output = _credencialService.GetCredencialById(id);
            return output;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}