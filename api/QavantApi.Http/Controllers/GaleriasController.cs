﻿using Qavant.Application.Services.Galerias;
using Qavant.Application.Services.Shared;
using Qavant.Infrastructure.ApplicationLogger;
using Qavant.Infrastructure.Persistence.Repositories;
using QavantApi.Http.Infrastructure.Authorization;
using QavantApi.Http.Infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;

namespace QavantApi.Http.Controllers
{
    [QavantApiAuthorization]
    public class GaleriasController : QavantApiBaseController
    {
        private readonly IGaleriaService _galeriaService;

        public GaleriasController(IGaleriaService galeriaService)
        {
            _galeriaService = galeriaService;
        }

        /// <summary>
        /// Obtiene todas las galerías
        /// </summary>
        /// <returns></returns>
        [Route("galerias")]
        [HttpGet]
        public List<GaleriaOutput> ObtenerGalerias()
        {
            var output = _galeriaService.ObtenerGalerias();
            return output.Galerias;
        }

        /// <summary>
        /// Obtiene detalles de una galería
        /// </summary>
        /// <param name="galeriaId">Identificador de la galería</param>
        /// <returns></returns>
        [Route("galerias/{galeriaId}")]
        [HttpGet]
        public GaleriaOutput ObtenerGaleriaPorId(int galeriaId)
        {
            var output = _galeriaService.ObtenerGaleriaPorId(galeriaId);
            return output;
        }

        /// <summary>
        /// Obtiene galerías por página
        /// </summary>
        /// <param name="pagina">Número de página</param>
        /// <returns></returns>
        [Route("galerias/pagina/{pagina}")]
        [HttpGet]
        public List<GaleriaOutput> ObtenerGalerias(int pagina)
        {
            var output = _galeriaService.ObtenerGalerias(pagina);
            return output.Galerias;
        }

        /// <summary>
        /// Obtiene galerías filtradas por categoría
        /// </summary>
        /// <param name="categoriaId">Identificador de la categoría</param>
        /// <returns></returns>
        [Route("galerias/categoria/{categoriaId}")]
        [HttpGet]
        public List<GaleriaOutput> ObtenerGaleriasPorCategoria(int categoriaId)
        {
            var output = _galeriaService.ObtenerGaleriasPorCategoria(categoriaId);
            return output.Galerias;
        }


        /// <summary>
        /// Obtiene elementos multimedia de una galería
        /// </summary>
        /// <param name="galeriaId">Identificador de la galería</param>
        /// <returns></returns>
        [Route("galerias/{galeriaId}/multimedia")]
        [HttpGet]
        public List<ElementoMultimediaOutput> ObtenerElementosMultimedia(int galeriaId)
        {
            var output = _galeriaService.ObtenerElementosMultimedia(galeriaId, 5);
            return output.ElementosMultimedia;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

//[Route("galerias/categoria/{categoriaId}/pagina/{pagina}")]