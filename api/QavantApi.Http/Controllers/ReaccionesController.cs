﻿using Qavant.Application.Infrastructure;
using Qavant.Application.Services.Reacciones;
using QavantApi.Http.Infrastructure.Authorization;
using System;
using System.Web.Http;

namespace QavantApi.Http.Controllers
{
    [QavantApiAuthorization]
    public class ReaccionesController : QavantApiBaseController
    {
        private readonly ILogger _logger;
        private readonly IReaccionService _reaccionService;
        private const string ID_MOD = "[REACCIONES]";

        public ReaccionesController(ILogger logger, IReaccionService reaccionService)
        {
            _logger = logger;
            _reaccionService = reaccionService;
        }

        [Route("reacciones/guardar")]
        [HttpPost]
        public IHttpActionResult Guardar([FromBody] ReaccionInput input)
        {
            try
            {
                const string ID_FNC = "[Guardar]";
                _logger.Debug($"{ID_MOD}{ID_FNC}***Guardando reacción. Modulo={input.Modulo}, EntidadId={input.EntidadId}");
                _reaccionService.Guardar(input);
                _logger.Info($"{ID_MOD}{ID_FNC}***La reacción se guardó correctamente.");
                return Ok();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}