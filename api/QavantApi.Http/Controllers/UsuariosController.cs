﻿using Qavant.Application;
using Qavant.Application.Services.Activacion;
using Qavant.Application.Services.ForgotPassword;
using Qavant.Application.Services.MiPerfil;
using Qavant.Application.Services.QuienEsQuien;
using Qavant.Application.Services.Shared;
using Qavant.Application.Services.Tenants;
using QavantApi.Http.Infrastructure.Authorization;
using QavantApi.Http.Infrastructure.FormObjects;
using QavantApi.Http.Infrastructure.Responses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace QavantApi.Http.Controllers
{
    public class UsuariosController : QavantApiBaseController
    {
        private readonly IMiPerfilService _miPerfilService;
        private readonly IQuienEsQuienService _quienEsQuienService;
        private readonly IForgotPasswordService _forgotPasswordService;
        private readonly IActivacionService _activacionService;

        public UsuariosController(IActivacionService activacionService, IForgotPasswordService forgotPasswordService, IMiPerfilService miPerfilService, IQuienEsQuienService quienEsQuienService)
        {
            _activacionService = activacionService;
            _forgotPasswordService = forgotPasswordService;
            _miPerfilService = miPerfilService;
            _quienEsQuienService = quienEsQuienService;
        }

        [QavantApiAuthorization]
        [ResponseType(typeof(UserResponse))]
        [Route("myprofile")]
        public UsuarioOutput GetMyProfile()
        {
            var output = _miPerfilService.GetPerfil(UsuarioId);
            return output;
        }

        [QavantApiAuthorization]
        [Route("users/all")]
        public List<UsuarioOutput> GetAllUsers()
        {
            var output = _quienEsQuienService.GetUsuarios();
            return output.Usuarios;
        }

        [QavantApiAuthorization]
        [Route("users/page/{page}")]
        public List<UsuarioOutput> GetUsers(int page)
        {
            var output = _quienEsQuienService.GetUsuarios(page);
            return output.Usuarios;
        }

        [QavantApiAuthorization]
        [Route("users/search/{searchTerm}/page/{page}")]
        public List<UsuarioOutput> GetUsersBySearchTerm(int page, string searchTerm)
        {
            var output = _quienEsQuienService.Search(page, searchTerm);
            return output.Usuarios;
        }

        [QavantApiAuthorization]
        [ResponseType(typeof(UserResponse))]
        [Route("users/{id}")]
        public UsuarioOutput GetUser(int id)
        {
            var output = _miPerfilService.GetPerfil(id);
            return output;
        }

        #region Activacion
        [HttpPost]
        [Route("users/toactivate")]
        public async Task<IHttpActionResult> Validar([System.Web.Http.FromBody] ActivacionPost postData)
        {
            string passwordHash = System.Configuration.ConfigurationManager.AppSettings["PasswordHash"];
            if (passwordHash != postData.PasswordHash)
                throw new Exception("No está autorizado para realizar esta acción.");                       

            var tenantService = RequestContext.Configuration.DependencyResolver.GetService(typeof(ITenantService)) as ITenantService;
            var tenantHolder = RequestContext.Configuration.DependencyResolver.GetService(typeof(ITenantHolder)) as ITenantHolder;
            var tenant = tenantService.ValidateAndReturn(postData.Canal, postData.TenantKey);
            tenantHolder.Tenant = tenantService.GetCurrentTenant(postData.Canal, tenant.Id);

            var usuario = _activacionService.ValidarUsuario(postData.UserName);
            return Ok(new { Email = usuario.Email });
        }

        [HttpPost]
        [Route("users/activate")]
        public async Task<IHttpActionResult> Activar([System.Web.Http.FromBody] ActivacionPost postData)
        {
            string passwordHash = System.Configuration.ConfigurationManager.AppSettings["PasswordHash"];
            if (passwordHash != postData.PasswordHash)
                throw new Exception("No está autorizado para realizar esta acción.");

            var tenantService = RequestContext.Configuration.DependencyResolver.GetService(typeof(ITenantService)) as ITenantService;
            var tenantHolder = RequestContext.Configuration.DependencyResolver.GetService(typeof(ITenantHolder)) as ITenantHolder;
            var tenant = tenantService.ValidateAndReturn(postData.Canal, postData.TenantKey);
            tenantHolder.Tenant = tenantService.GetCurrentTenant(postData.Canal, tenant.Id);

            _activacionService.ActivarUsuario(postData.UserName, postData.Password);
            return Ok();
        }
        #endregion

        #region ForgotPassword
        [Route("users/forgotpassword")]
        [System.Web.Http.HttpPost]
        public async Task<IHttpActionResult> Reset([System.Web.Http.FromBody] ForgotPasswordPost postData)
        {
            var tenantService = RequestContext.Configuration.DependencyResolver.GetService(typeof(ITenantService)) as ITenantService;
            var tenantHolder = RequestContext.Configuration.DependencyResolver.GetService(typeof(ITenantHolder)) as ITenantHolder;
            var tenant = tenantService.ValidateAndReturn(postData.Canal, postData.TenantKey);
            tenantHolder.Tenant = tenantService.GetCurrentTenant(postData.Canal, tenant.Id);
            _forgotPasswordService.Reset(postData.UserName);
            return Ok();
        }


        [QavantApiAuthorization]
        [Route("users/modifypassword")]
        [System.Web.Http.HttpPost]
        public async Task<IHttpActionResult> PostModifyPassword([System.Web.Http.FromBody] ChangePasswordPost postData)
        {
            _miPerfilService.SetNewPassword(UsuarioId, postData.Password, postData.PasswordConfirm);
            return Ok();
        }

        #endregion


        [QavantApiAuthorization]
        [Route("users/edit")]
        [System.Web.Http.HttpPost]
        public async Task<IHttpActionResult> PostChangeUser([System.Web.Http.FromBody] MiPerfilInput input)
        {
            _miPerfilService.ChangeOldPassword(UsuarioId, input.oldPassword, input.newPassword, input.confirmPassword);
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}