﻿using Qavant.Application.Services.Formularios;
using QavantApi.Http.Infrastructure.Authorization;
using QavantApi.Http.Infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace QavantApi.Http.Controllers
{
    [QavantApiAuthorization]
    public class FormulariosController : QavantApiBaseController
    {
        private readonly IFormularioService _formularioService;

        public FormulariosController(IFormularioService formularioService)
        {
            _formularioService = formularioService;
        }

        [Route("forms/getForms/{type}")]
        public List<FormularioListItemOutput> GetForms(byte type)
        {
            var output = _formularioService.GetFormsPorTipo(type, UsuarioId);
            return output.Formularios;
        }

        [Route("forms/getQuestionsById/{formId}")]
        public FormularioOutput GetQuestionsById(int formId)
        {
            var output = _formularioService.GetForm(formId, UsuarioId);
            return output;
        }

        // PUT: forms/SaveForm
        [ResponseType(typeof(void))]
        [Route("form/saveForm")]
        public IHttpActionResult SaveForm(FormularioInput formInput)
        {
            _formularioService.GuardarRespuestas(formInput, UsuarioId);
            return StatusCode(HttpStatusCode.OK);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}