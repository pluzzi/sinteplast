﻿using Qavant.Application.Services.Noticias;
using QavantApi.Http.Infrastructure.Authorization;
using System.Collections.Generic;
using System.Web.Http;

namespace QavantApi.Http.Controllers
{
    [QavantApiAuthorization]
    public class NoticiasController : QavantApiBaseController
    {
        private readonly INoticiaService _noticiaService;

        public NoticiasController(INoticiaService noticiaService)
        {
            _noticiaService = noticiaService;
        }

        [Route("news/all")]
        [HttpGet]
        public List<NoticiaOutput> ObtenerNoticias()
        {
            var output = _noticiaService.ObtenerNoticias();
            return output.Noticias;
        }

        [Route("news/page/{page}")]
        [HttpGet]
        public List<NoticiaOutput> ObtenerNoticias(int page)
        {
            var output = _noticiaService.ObtenerNoticias(page);
            return output.Noticias;
        }

        [Route("news/{id}")]
        [HttpGet]
        public NoticiaOutput ObtenerNoticiaPorId(int id)
        {
            var output = _noticiaService.ObtenerNoticiaPorId(id);
            return output;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}