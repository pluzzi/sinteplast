﻿using Qavant.Application.Services.Categorias;
using QavantApi.Http.Infrastructure.Authorization;
using QavantApi.Http.Infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;

namespace QavantApi.Http.Controllers
{
    [QavantApiAuthorization]
    public class CategoriasController : QavantApiBaseController
    {
        private readonly ICategoriaService _categoriaService;

        public CategoriasController(ICategoriaService categoriaService)
        {
            _categoriaService = categoriaService;
        }

        [Route("categorias/moduloId/{moduloId}")]
        [HttpGet]
        [ObsoleteAttribute("This method has been deprecated.", true)]
        public List<CategoriaOutput> ObtenerCategoriasPorModuloId(int moduloId)
        {
            var output = _categoriaService.ObtenerCategoriasPorModuloId(moduloId);
            return output.Categorias;
        }

        /// <summary>
        /// Obtiene todas las categorías para el Nemónico especificado como parámetro ,y los nombres de las cartegorías vienen con líneas emulando el formato árbol.
        /// </summary>
        /// <param name="nemonico">Identificador literal de los módulos</param>
        /// <returns>Estructura que contiene el: Id, Nombre, PadreCategoriaId, imagen de cada categoría.</returns>
        [Route("categorias/modulo/{nemonico}")]
        [HttpGet]
        public List<CategoriaOutput> ObtenerCategoriasPorModulo(string nemonico)
        {
            var output = _categoriaService.ObtenerCategoriasPorModulo(nemonico,true);
            return output.Categorias;
        }
        /// <summary>
        /// Obtiene todas las categorías para el Nemónico especificado como parámetro, a demás se puede hacer que los nombres de las cartegorías vengan con líneas emulando el formato árbol.
        /// </summary>
        /// <example> 
        /// Ejemplo de respuesta:
        /// [
        ///{
        ///    "Id": 6,
        ///    "Nombre": "ISO",
        ///   "PadreCategoriaId": null,
        ///    "Imagen": null
        ///},
        ///{
        ///    "Id": 26,
        ///    "Nombre": "--BENCAT",
        ///    "PadreCategoriaId": 6,
        ///    "Imagen": null
        ///},
        ///{
        ///    "Id": 38,
        ///    "Nombre": "IT",
        ///   "PadreCategoriaId": null,
        ///   "Imagen": null
        ///}
        ///]
        /// </example>
        /// <param name="nemonico">Identificador literal de los módulos</param>
        /// <param name="tree">Con valor true, devuelve los nombres de las categorías con líneas emulando el formato árbol. En caso de poner false se devuelven los nombres de las categorías únicamente.</param>
        /// <returns>Estructura que contiene el: Id,Nombre,PadreCategoriaId,imagen de cada categoría.</returns>      
        [Route("categorias/modulo/{nemonico}/{tree}")]
        [HttpGet]
        public List<CategoriaOutput> ObtenerCategoriasPorModulo(string nemonico,bool tree)
        {
            var output = _categoriaService.ObtenerCategoriasPorModulo(nemonico,tree);
            return output.Categorias;
        }
    }
}

