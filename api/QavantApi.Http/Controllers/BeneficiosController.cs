﻿using Qavant.Application.Services.Beneficios;
using QavantApi.Http.Infrastructure.Authorization;
using QavantApi.Http.Infrastructure.FormObjects;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace QavantApi.Http.Controllers
{
    [QavantApiAuthorization]
    public class BeneficiosController : QavantApiBaseController
    {
        private readonly IBeneficioService _beneficioService;

        public BeneficiosController(IBeneficioService beneficioService)
        {
            _beneficioService = beneficioService;
        }

        // GET: api/Benefits/5
        [Route("benefits/{id}")]
        public BeneficioOutput GetBeneficioById(int id)
        {
            var output = _beneficioService.GetBeneficioById(id, UsuarioId);
            return output;
        }

        [Route("benefits/all/{tipoCatalogoProductosId}")]
        public List<BeneficioOutput> GetBeneficios(int tipoCatalogoProductosId)
        {
            var output = _beneficioService.GetBeneficios(tipoCatalogoProductosId, UsuarioId);
            return output.Beneficios;
        }

        [Route("benefits/page/{page}/{tipoCatalogoProductosId}")]
        public List<BeneficioOutput> GetBeneficiosByPage(int page, int tipoCatalogoProductosId)
        {
            var output = _beneficioService.GetBeneficios(page, tipoCatalogoProductosId, UsuarioId);
            return output.Beneficios;
        }

        [Route("benefits/likes/all")]
        public List<BeneficioOutput> GetAllBenefitsLikes()
        {
            var output = _beneficioService.GetBeneficiosFavoritos(UsuarioId);
            return output.Beneficios;
        }

        [Route("benefits/likes/{page}")]
        public List<BeneficioOutput> GetBenefitsLikes(int page)
        {
            var output = _beneficioService.GetBeneficiosFavoritos(page, UsuarioId);
            return output.Beneficios;
        }

        [Route("benefits/category/{categoryId}/page/{page}")]
        public List<BeneficioOutput> GetBenefitsByCategory(int page, int? categoryId)
        {
            var output = _beneficioService.GetBeneficiosPorCategoria(page, UsuarioId, categoryId);
            return output.Beneficios;
        }

        /// <summary>
        /// Obtiene Todos los beneficios de una categoría
        /// </summary>
        /// <param name="categoryId">Identificador literal de la categoría </param>
        /// <returns>Estructura que contiene el: Id, Title, Subtitle, Copete, Content, Image, Liked, To, ReaccionId, TotalReacciones de cada beneficio</returns>
        [Route("benefits/bycategory/{categoryId}")]
        public List<BeneficioOutput> GetAllBenefitsByCategory(int? categoryId)
        {
            var output = _beneficioService.GetAllBeneficiosPorCategoria(UsuarioId, categoryId);
            return output.Beneficios;
        }

        [ResponseType(typeof(BeneficioOutput))]
        [Route("benefits/{id}/like")]
        public BeneficioOutput GetLike(int id, bool dislike = false)
        {
            var output = _beneficioService.SetLike(id, UsuarioId);
            return output;
        }

        [HttpPost]
        [Route("benefits/exchange")]
        public async Task<IHttpActionResult> Exchange([System.Web.Http.FromBody] ExchangePost postData)
        {
            ExchangeOutput output = _beneficioService.Canjear(postData.UserId, postData.BenefitsId);

            if (output == null)
            {
                return StatusCode(System.Net.HttpStatusCode.BadRequest);
            }

            return Ok();
        }
        
    }
}