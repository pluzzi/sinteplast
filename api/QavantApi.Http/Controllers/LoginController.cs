﻿using Qavant.Application;
using Qavant.Application.Infrastructure;
using Qavant.Application.Services.Login;
using Qavant.Application.Services.Tenants;
using QavantApi.Http.Infrastructure.Authorization;
using QavantApi.Http.Infrastructure.FormObjects;
using System.Threading.Tasks;
using System.Web.Http;

namespace QavantApi.Http.Controllers
{
    public class LoginController : ApiController
    {
        private readonly ILogger _logger;
        private readonly ILoginService _loginService;
        private const string ID_MOD = "[AUTH]";

        public LoginController(ILogger logger, ILoginService loginService)
        {
            _logger = logger;
            _loginService = loginService;
        }

        [Route("login")]
        public async Task<IHttpActionResult> Login(LoginRequest request)
        {
            const string ID_FNC = "[Login]";

            _logger.Debug($"{ID_MOD}{ID_FNC}***Iniciando sesión con los siguientes datos: Empresa={request.TenantKey}, Usuario={request.UserName}, Canal={request.Canal}");
            try
            {
                var tenantService = RequestContext.Configuration.DependencyResolver.GetService(typeof(ITenantService)) as ITenantService;
                var tenantHolder = RequestContext.Configuration.DependencyResolver.GetService(typeof(ITenantHolder)) as ITenantHolder;

                // validamos existencia de tenant
                var tenant = tenantService.ValidateAndReturn(request.Canal, request.TenantKey);
                tenantHolder.Tenant = tenant;
                tenantHolder.Canal = request.Canal;
                _logger.Debug($"{ID_MOD}{ID_FNC}***Validación exitosa del tenant '{request.TenantKey}'");

                // realizamos autenticacion de usuario
                var usuario = _loginService.Login(request.UserName, request.Password);
                var encryptedToken = TokenHelper.CreateToken(tenant.Id, usuario.Id, usuario.UserName, request.Canal);
                _logger.Info($"{ID_MOD}{ID_FNC}***El usuario '{usuario.UserName}' de la empresa '{tenant.Nombre} inició sesión correctamente.'");

                return base.Ok(new
                {
                    user = usuario,
                    token = encryptedToken,
                    shouldModifyPassword = usuario.ShouldModifyPassword
                });
            }
            catch (System.Exception ex)
            {
                _logger.Warning($"{ID_MOD}{ID_FNC}***{ex.Message}");
                throw;
            }
        }
    }
}