﻿using Qavant.Application.Services.Documentos;
using Qavant.Infrastructure.ApplicationLogger;
using Qavant.Infrastructure.Persistence.Repositories;
using QavantApi.Http.Infrastructure.Authorization;
using QavantApi.Http.Infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;

namespace QavantApi.Http.Controllers
{
    [QavantApiAuthorization]
    public class DocumentosController : QavantApiBaseController
    {
        private readonly IDocumentoService _documentoService;

        public DocumentosController(IDocumentoService documentoService)
        {
            _documentoService = documentoService;
        }

        [Route("documents/all")]
        public List<DocumentoOutput> GetAllDocuments()
        {
            var output = _documentoService.GetDocumentos();
            return output.Documentos;
        }

        [Route("documents/page/{page}")]
        public List<DocumentoOutput> GetDocuments(int page)
        {
            var output = _documentoService.GetDocumentos(page);                
            return output.Documentos;
        }

        [Route("documentos/{id}")]
        public DocumentoOutput GetDocumentoById(int id)
        {
            var output = _documentoService.GetDocumentoById(id);
            return output;
        }

        [Route("documentos/categoriaId/{id}/{page}")]
        public List<DocumentoOutput> GetDocumentoByCategoryId(int id, int page)
        {
            var output = _documentoService.GetDocumentoByCategoryId(id, page);
            return output.Documentos;
        }

        [Route("documentos/all/bycategoriaId/{id}")]
        public List<DocumentoOutput> GetAllDocumentoByCategoryId(int id)
        {
            var output = _documentoService.GetAllDocumentoByCategoryId(id);
            return output.Documentos;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}