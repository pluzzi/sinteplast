﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using QavantApi.Http.Models;
using QavantApi.Http.Infrastructure.Authorization;

namespace QavantApi.Http.Controllers
{
    [QavantApiAuthorization]
    public class ModulosController : QavantApiBaseController
    {
        // GET: api/modulos
        [Route("modulos")]
        public List<Modulos> GetModulos()
        {
            List<Modulos> list = db.Modulos.Where(x => x.UsoEnCategorias).ToList();
            return list.ToList();
        }


    }
}