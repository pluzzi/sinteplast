﻿using JWT;
using Newtonsoft.Json;
using Qavant.Application;
using Qavant.Application.Exceptions;
using Qavant.Infrastructure.Security;
using QavantApi.Http.Infrastructure.Authorization;
using QavantApi.Http.Infrastructure.Exceptions;
using System;
using System.Configuration;
using System.Web;

namespace QavantApi.Http.Helpers
{
    public class RequestHeadersHelper
    {
        public static string GetToken(HttpContext context)
        {
            string token = string.Empty;
            token = context.Request.Headers["x-token"];
            return token;
        }

        public static string GetTenantKey(HttpContext context)
        {
            string tenantKey = string.Empty;
            Canal canalActual = Canal.None;
            try
            {
                string canal = context.Request.Headers["x-canal"];
                if (canal == "app")
                {
                    canalActual = Canal.App;
                    tenantKey = context.Request.Headers["x-tenant"];
                }
                else
                {
                    canalActual = Canal.Web;
                    string origin = context.Request.Headers["Origin"];
                    if (!string.IsNullOrEmpty(origin))
                    {
                        Uri myUri = new Uri(origin);
                        tenantKey = myUri.Host;
                    }
                }
                if (string.IsNullOrEmpty(tenantKey))
                    throw new Exception($"Canal '{canalActual}': Clave de empresa inválida.");
            }
            catch (Exception ex)
            {
                throw new InvalidTenantKeyException(ex.Message);
            }
            return tenantKey;
        }

        public static Canal GetCanal(HttpContext context)
        {
            //TODO: Perdir que en la web, el Login envie el x-canal 'web'
            Canal canal;
            string value = context.Request.Headers["x-canal"];
            if (value == "app")
                canal = Canal.App;
            else
                canal = Canal.Web;

            return canal;
        }

        public static string GetDeviceChannelName(HttpContext context)
        {
            string name = string.Empty;
            Canal canal = GetCanal(context);
            name = canal.ToString();
            return name;
        }

        public static string GetUserName(HttpContext context)
        {
            string username = string.Empty;
            try
            {
                QavantTripleDES cryptography = new QavantTripleDES();
                var encryptedToken = context.Request.Headers["x-token"];
                if (!string.IsNullOrEmpty(encryptedToken))
                {
                    string jwt = cryptography.Decrypt(encryptedToken, true);
                    var secret = ConfigurationManager.AppSettings.Get("JwtKey");
                    var decodedToken = JsonWebToken.Decode(jwt, secret);
                    dynamic tokenData = JsonConvert.DeserializeObject<dynamic>(decodedToken);
                    var tokenInfo = new TokenInfo { UserName = tokenData.UserName, TenantId = tokenData.EmpresaId };
                    username = tokenInfo.UserName;
                }
            }
            catch (Exception)
            {
                username = string.Empty;
            }
            return username;
        }
    }
}