﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantApi.Http.Infrastructure.FormObjects
{
    public class ContactPost
    {
        public Byte Reason { get; set; }
        public string Comment { get; set; }
    }
}