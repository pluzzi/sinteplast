﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantApi.Http.Infrastructure.FormObjects
{
    public class ReportOperationPost
    {
        public string Event { get; set; }
        public string Date { get; set; }
        public string Comment { get; set; }
    }
}