﻿using Qavant.Application;
using QavantApi.Http.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantApi.Http.Infrastructure.FormObjects
{
    public class ChangePasswordPost
    {
        public string Password { get; set; }
        public string PasswordConfirm { get; set; }
    }
}