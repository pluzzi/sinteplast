﻿namespace QavantApi.Http.Infrastructure.FormObjects
{
    public class ExchangePost
    {
        public int UserId { get; set; }
        public int BenefitsId { get; set; }
    }
}