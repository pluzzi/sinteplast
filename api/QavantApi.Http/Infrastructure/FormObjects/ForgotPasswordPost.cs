﻿using Qavant.Application;
using QavantApi.Http.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantApi.Http.Infrastructure.FormObjects
{
    public class ForgotPasswordPost
    {
        private Canal _canal;
        private string _tenantKey;

        public ForgotPasswordPost()
        {
            Canal = Canal.None;
            TenantKey = string.Empty;
        }
        public string UserName { get; set; }
        public Canal Canal
        {
            get
            {
                return _canal;
            }
            set
            {
                _canal = RequestHeadersHelper.GetCanal(HttpContext.Current);
            }
        }
        public string TenantKey
        {
            get
            {
                return _tenantKey;
            }
            set
            {
                _tenantKey = RequestHeadersHelper.GetTenantKey(HttpContext.Current);
            }
        }
    }
}