﻿using QavantApi.Http.Infrastructure.Responses;
using QavantApi.Http.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;

namespace QavantApi.Http.Infrastructure
{
    public static class Extensions
    {
        //public static IQueryable<FormsResponse> AsResponse(this IDbSet<Form> forms)
        //{
        //    return forms.Select(x => new FormsResponse
        //    {
        //        Id = x.Id,
        //        Title = x.Title,
        //        Description = x.Description
        //    });
        //}

        //public static FormsResponse AsResponse(this Form form)
        //{
        //    return new FormsResponse
        //    {
        //        Id = form.Id,
        //        Title = form.Title,
        //        Description = form.Description
        //    };
        //}

        //public static IQueryable<FormsQuestionDetailsResponse> AsResponse(this IDbSet<FormsQuestion> forms)
        //{
        //    List<FormsQuestionsOptionResponse> lsEmptyOptions = new List<FormsQuestionsOptionResponse>();
        //    return forms.Select(x => new FormsQuestionDetailsResponse
        //    {
        //        Id = x.Id,
        //        Order = x.Order,
        //        Question = x.Question,
        //        QuestionTypeId = x.FormsQuestionsType.Id,
        //        Required = x.Required,
        //        Options = x.FormsQuestionsOptions == null ?
        //                    lsEmptyOptions
        //                    : x.FormsQuestionsOptions.OrderBy(o => o.Order).AsResponse()
        //    });
        //}

        //public static FormsQuestionDetailsResponse AsResponse(this FormsQuestion formQ)
        //{
        //    List<FormsQuestionsOptionResponse> lsEmptyOptions = new List<FormsQuestionsOptionResponse>();
        //    return new FormsQuestionDetailsResponse
        //    {
        //        Id = formQ.Id,
        //        Order = formQ.Order,
        //        Question = formQ.Question,
        //        QuestionTypeId = formQ.FormsQuestionsType.Id,
        //        Required = formQ.Required,
        //        Options = formQ.FormsQuestionsOptions == null?
        //                    lsEmptyOptions
        //                    : formQ.FormsQuestionsOptions.OrderBy(o => o.Order).AsResponse()
        //    };
        //}

        //public static List<FormsQuestionsOptionResponse> AsResponse(this IOrderedEnumerable<FormsQuestionsOption> formsOptions)
        //{
        //    List<FormsQuestionsOptionResponse> lsReturn = formsOptions.Select(x => new FormsQuestionsOptionResponse
        //    {
        //                                                        Id = x.Id,
        //                                                        Description = x.Description,
        //                                                        Order = x.Order,
        //                                                        AnswerSelected = false,
        //                                                        IsRightAnswer = (bool)x.IsRightAnswer,
        //                                                        Enabled = (bool)x.Enabled
        //                                                    }).ToList();
        //    return lsReturn;
        //}
        //public static FormsQuestionsOptionResponse AsResponse(this FormsQuestionsOption formO)
        //{
        //    return new FormsQuestionsOptionResponse
        //    {
        //        Id = formO.Id,
        //        Description = formO.Description,
        //        Order = formO.Order,
        //        AnswerSelected = false,
        //        IsRightAnswer = (bool)formO.IsRightAnswer
        //    };
        //}

        

        public static UserResponse AsResponse(this QavantUser user)
        {
            CultureInfo ci = new CultureInfo("es-AR");

            return new UserResponse
            {
                Id = user.Id,
                Name = user.Name,
                LastName = user.Surname,   
                Email = user.Email,
                Area = user.Area?.Nombre,
                Phone = user.Phone,
                Mobile = user.Mobile,              
                BirthDate = user.BirthDate.ToString("dd DE MMMM", ci).ToUpper(),
                Adress = user.Address,
                City = user.City,
                Username = user.UserName,
                BirthdayType = (user.BirthDate.Day == DateTime.Today.Day && user.BirthDate.Month == DateTime.Today.Month) ? "hoy" : "semana",
                
            };
        }


        public static DateTime NextThirtyDays(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }
            return dt.AddDays(-1 * diff).Date;
        }


    }
}