using System;
using Newtonsoft.Json;
using QavantApi.Http.Models;

namespace QavantApi.Http.Infrastructure.Authorization
{
    public class QavantAuthorizationHelper
    {
        private QavantDbContext _context;

        public QavantAuthorizationHelper(QavantDbContext context)
        {
            _context = context;
        }

        public TokenInfo GetInfoFrom(string decodedToken)
        {
            return TokenHelper.GetTokenInfo(decodedToken);
        }
    }
}