using Qavant.Application;

namespace QavantApi.Http.Infrastructure.Authorization
{
    public class TokenInfo
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public int TenantId { get; set; }
        public Canal Canal { get; set; }
    }
}