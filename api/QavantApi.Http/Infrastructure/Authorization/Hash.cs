﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace QavantApi.Http.Infrastructure.Authorization
{
    public abstract class Hash
    {
        // Key used for encrypting.
        private static string passphrase = ConfigurationManager.AppSettings["passphrase"];

        /// <summary>
        /// Encripta un valor usando el algoritmo TDES.
        /// </summary>
        /// <param name="pValor">Valor a encriptar.</param>
        /// <returns></returns>
        public static string encryptString(string pValor)
        {
            if (pValor == null || pValor == String.Empty)
            {
                throw new Exception("la función encryptString intentó encriptar un valor nulo");
            }
            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();
            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(passphrase));
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;
            byte[] DataToEncrypt = UTF8.GetBytes(pValor);
            try
            {
                ICryptoTransform Encryptor = TDESAlgorithm.CreateEncryptor();
                Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }
            return Convert.ToBase64String(Results);
        }

        /// <summary>
        /// Desencripta un valor encriptado. con algoritmo TDES.
        /// </summary>
        /// <param name="pValor">Valor a desencriptar.</param>
        /// <returns></returns>
        public static string decryptString(string pValor)
        {
            if (pValor == null || pValor == String.Empty)
            {
                throw new Exception("la función encryptString intentó desencriptar un valor nulo");
            }
            byte[] Results;
            byte[] DataToDecrypt;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();
            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(passphrase));
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;
            try
            {
                DataToDecrypt = Convert.FromBase64String(pValor);
                ICryptoTransform Decryptor = TDESAlgorithm.CreateDecryptor();
                Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }
            return UTF8.GetString(Results);
        }
    }
}