﻿using Microsoft.ApplicationInsights;
using Qavant.Application.Exceptions;
using Qavant.Application.Infrastructure;
using Qavant.Infrastructure.Logging;
using Qavant.Infrastructure.Notifications;
using QavantApi.Http.Helpers;
using System;
using System.Configuration;
using System.Web;
using System.Web.Http.ExceptionHandling;
using ApplicationException = Qavant.Application.Exceptions.ApplicationException;

namespace QavantApi.Http.Infrastructure.Exceptions
{
    public class QavantExceptionLogger : ExceptionLogger
    {
        private readonly IEmailService _emailService;
        private readonly TelemetryClient _appInsights;
        private readonly ILogger _logger;

        // poor man's dependency injection
        public QavantExceptionLogger() : this(new Log4NetAdapter(), new SMTPService(), new TelemetryClient())
        {
        }

        public QavantExceptionLogger(ILogger logger, IEmailService emailService, TelemetryClient appInsights)
        {
            _logger = logger;
            _emailService = emailService;
            _appInsights = appInsights;
        }

        public override void Log(ExceptionLoggerContext context)
        {
            var ex = context.Exception;
            if (ex is ApplicationException)
                return;

            if (ex is InvalidTenantKeyException)
            {
                _logger.Error(ex.Message, ex);
            }
            else
            {
                _logger.Error($"{GetInfoContexto(HttpContext.Current)} - {ex.Message}", ex);

                string SMTPUsername = ConfigurationManager.AppSettings["SMTPUsername"];
                string SMTPPassword = ConfigurationManager.AppSettings["SMTPPassword"];
                string SMTPServer = ConfigurationManager.AppSettings["SMTPServer"];
                int SMTPPort = int.Parse(ConfigurationManager.AppSettings["SMTPPort"]);
                bool SMTPEnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["SMTPEnableSsl"]);
                string EmailDisplayName = ConfigurationManager.AppSettings["EmailDisplayName"];
                string emailFrom = ConfigurationManager.AppSettings["EmailFrom"];
                string emailSupport = ConfigurationManager.AppSettings["ITSupport"];

                _emailService.Configure(SMTPUsername, SMTPPassword, SMTPServer, SMTPPort, SMTPEnableSsl, emailFrom, EmailDisplayName);
                _emailService.SendMail(emailSupport, $" Error QMT API - {GetEntorno()} environment", $"{GetInfoContexto(HttpContext.Current)} - {ex}");
            }
        }

        private string GetInfoContexto(HttpContext current)
        {
            string tenant = RequestHeadersHelper.GetTenantKey(HttpContext.Current);
            string canal = RequestHeadersHelper.GetDeviceChannelName(HttpContext.Current);
            string usuario = RequestHeadersHelper.GetUserName(HttpContext.Current);
            string credenciales = $"Canal={canal}, Empresa={tenant}, Usuario={usuario}";
            return credenciales;
        }

        private string GetEntorno()
        {
            return ConfigurationManager.AppSettings["Environment"];
        }
    }
}