﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QavantApi.Http.Models;

namespace QavantApi.Http.Infrastructure.Responses
{
    public class ImageGalleryFileResponse
    {
        public int Id { get; set; }
        public int ImageGalleryId { get; set; }
        public string src { get; set; }
        public string thumb { get; set; }
    }

    

}