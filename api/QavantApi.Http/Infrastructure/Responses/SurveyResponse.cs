﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QavantApi.Http.Models;

namespace QavantApi.Http.Infrastructure.Responses
{
    //public class FormsResponse
    //{
    //    public int Id { get; set; }
    //    public string Title { get;  set; }
    //    public string Description { get; set; }
    //    public bool AlreadyAnswered { get; set; }
    //    public bool UniqueReply { get; set; }
    //    public bool SendMails { get; set; }
    //    public string MailsTo { get; set; }
    //    public string MailSubject { get; set; }
    //    public string MailTitle { get; set; }
    //    public string MailIntro { get; set; }
    //    public string Code { get; set; }
    //    public Byte FormTypeId { get; set; }
    //}



    public class FormsQuestionsTypeResponse
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class FormsResults
    {
        public int FormId { get; set; }
        public int QuestionId { get; set; }
        public int? QuestionOptionId { get; set; }
        public string Comment { get; set; }
    }

}