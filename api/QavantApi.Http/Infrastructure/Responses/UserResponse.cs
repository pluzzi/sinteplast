﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantApi.Http.Infrastructure.Responses
{


    public class UserResponse
    {
        public string Area { get; internal set; }
        public string BirthDate { get; internal set; }
        public string Email { get; internal set; }
        public int Id { get; internal set; }
        public string LastName { get; internal set; }
        public string Mobile { get; internal set; }
        public string Name { get; internal set; }
        public string Phone { get; internal set; }
        public string Adress { get; internal set; }
        public string City { get; internal set; }
        public string Username { get; internal set; }
        public string BirthdayType { get; internal set; }
    }
}