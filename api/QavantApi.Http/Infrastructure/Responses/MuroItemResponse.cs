﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantApi.Http.Infrastructure.Responses
{
    public class MuroItemResponse
    {
        public int Id { get; internal set; }
        public string Title { get; internal set; }
        public string Date { get; internal set; }
        public string Content { get; internal set; }
        public string Image { get; internal set; }
        public int EmpresaId { get; set; }
    }
}