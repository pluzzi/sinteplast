﻿using Ninject;
using Ninject.Web.Common;
using Ninject.Web.Common.WebHost;
using Qavant.Application;
using Qavant.Application.Infrastructure;
using Qavant.Application.Infrastructure.LoggerActivity;
using Qavant.Application.Repositories;
using Qavant.Application.Services.Activacion;
using Qavant.Application.Services.Beneficios;
using Qavant.Application.Services.Capacitaciones;
using Qavant.Application.Services.Categorias;
using Qavant.Application.Services.Credenciales;
using Qavant.Application.Services.Cumpleaños;
using Qavant.Application.Services.Dispositivos;
using Qavant.Application.Services.Documentos;
using Qavant.Application.Services.Galerias;
using Qavant.Application.Services.ForgotPassword;
using Qavant.Application.Services.Formularios;
using Qavant.Application.Services.InfoPaginas;
using Qavant.Application.Services.Login;
using Qavant.Application.Services.MiPerfil;
using Qavant.Application.Services.Muro;
using Qavant.Application.Services.Noticias;
using Qavant.Application.Services.QuienEsQuien;
using Qavant.Application.Services.Reacciones;
using Qavant.Application.Services.Tenants;
using Qavant.Infrastructure.ApplicationLogger;
using Qavant.Infrastructure.Authentication;
using Qavant.Infrastructure.HtmlTemplateGenerator;
using Qavant.Infrastructure.Logging;
using Qavant.Infrastructure.Notifications;
using Qavant.Infrastructure.Persistence;
using Qavant.Infrastructure.Persistence.Repositories;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Qavant.Application.Services.TipoCatalogo;

namespace QavantApi.Http
{
    public class MvcApplication : NinjectHttpApplication
    {

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new
                {
                    controller = "Home",
                    action = "Index",
                    id = UrlParameter.Optional
                });
        }

        protected override IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            RegisterServices(kernel);
            return kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IEmailService>().To<SMTPService>();
            kernel.Bind<IHtmlTemplateGenerator>().To<RazorEngineAdapter>();

            // Tenants
            kernel.Bind<ITenantHolder>().To<TenantHolder>().InRequestScope();
            kernel.Bind<ITenantService>().To<TenantService>();

            // Logging
            kernel.Bind<ILogger>().To<Log4NetAdapter>();

            // Log actividad
            kernel.Bind<ILogHolder>().To<LogHolder>().InRequestScope();
            kernel.Bind<IApplicationLoggerActivity>().To<QavantLoggerActivity>();

            // Security
            kernel.Bind<IAuthentication>().To<Authentication>();
            kernel.Bind<IActivacionService>().To<ActivacionService>();
            kernel.Bind<IForgotPasswordService>().To<ForgotPasswordService>();
            kernel.Bind<ILoginService>().To<LoginService>();
            kernel.Bind<IUsuarioRepository>().To<UsuarioRepository>();

            // Muro
            kernel.Bind<IMuroService>().To<MuroService>();
            kernel.Bind<IMuroRepository>().To<MuroRepository>();

            // Mi perfil
            kernel.Bind<IMiPerfilService>().To<MiPerfilService>();

            // Quien es Quien
            kernel.Bind<IQuienEsQuienService>().To<QuienEsQuienService>();

            // Cumpleaños
            kernel.Bind<ICumpleañosService>().To<CumpleañosService>();
            kernel.Bind<ICumpleañosRepository>().To<CumpleañosRepository>();

            // Noticias
            kernel.Bind<INoticiaService>().To<NoticiaService>();
            kernel.Bind<INoticiaRepository>().To<NoticiaRepository>();

            // Beneficios
            kernel.Bind<IBeneficioService>().To<BeneficioService>();
            kernel.Bind<IBeneficioRepository>().To<BeneficioRepository>();

            // Documentos
            kernel.Bind<IDocumentoService>().To<DocumentoService>();
            kernel.Bind<IDocumentoRepository>().To<DocumentoRepository>();

            // InfoPaginas
            kernel.Bind<IInfoUtilService>().To<InfoUtilService>();
            kernel.Bind<IInfoUtilRepository>().To<InfoUtilRepository>();

            // Categorias
            kernel.Bind<ICategoriaService>().To<CategoriaService>();
            kernel.Bind<ICategoriaRepository>().To<CategoriaRepository>();

            // Dispositivos
            kernel.Bind<IDispositivoService>().To<DispositivoService>();
            kernel.Bind<IDispositivoRepository>().To<DispositivoRepository>();

            // Formularios
            kernel.Bind<IFormularioService>().To<FormularioService>();
            kernel.Bind<IFormularioRepository>().To<FormularioRepository>();

            //Capacitaciones
            kernel.Bind<ICapacitacionService>().To<CapacitacionService>();
            kernel.Bind<ICapacitacionRepository>().To<CapacitacionRepository>();

            // Credenciales
            kernel.Bind<ICredencialService>().To<CredencialService>();
            kernel.Bind<ICredencialRepository>().To<CredencialRepository>();

            //Sentimientos
            kernel.Bind<IReaccionService>().To<ReaccionService>();
            kernel.Bind<IReaccionRepository>().To<ReaccionRepository>();

            // Módulos
            kernel.Bind<ITenantRepository>().To<TenantRepository>();
            kernel.Bind<IModuloRepository>().To<ModuloRepository>();

            // Galerias
            kernel.Bind<IGaleriaService>().To<GaleriaService>();
            kernel.Bind<IGaleriaRepository>().To<GaleriaRepository>();
            kernel.Bind<IMultimediaRepository>().To<MultimediaRepository>();

            // Tipo Catalogos
            kernel.Bind<ITipoCatalogoService>().To<TipoCatalogoService>();
            kernel.Bind<ITipoCatalogoRepository>().To<TipoCatalogoRepository>();

        }

        protected override void OnApplicationStarted()
        {
            base.OnApplicationStarted();
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            GlobalConfiguration.Configuration.Formatters.Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            MvcHandler.DisableMvcResponseHeader = true;

            AutomapperProfile.Run();
        }
    }
}