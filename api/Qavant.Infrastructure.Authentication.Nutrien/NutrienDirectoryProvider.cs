﻿using Newtonsoft.Json.Linq;
using Qavant.Infrastructure.Authentication.PluginInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Authentication.Nutrien
{
    public class NutrienDirectoryProvider : IAuthenticationProvider
    {
        private Dictionary<string, string> _parameters;

        public void SetParameters(Dictionary<string, string> parameters)
        {
            _parameters = parameters;
        }

        public bool Validate(int tenantId, string username, string password)
        {
            string azureTenantId = _parameters["AzureTenantId"];
            string azureClientId = _parameters["AzureClientId"];
            string azureClientSecret = _parameters["AzureClientSecret"];

            string resource = "https%3A%2F%2Fgraph.microsoft.com%2F";
            string grantType = "password";
            string scope = "openid";

            bool result;
            using (HttpClient client = new HttpClient())
            {
                var tokenEndpoint = $"https://login.microsoftonline.com/{azureTenantId}/oauth2/token";
                var accept = "application/json";
                client.DefaultRequestHeaders.Add("Accept", accept);
                string postBody = $"resource={resource}&client_id={azureClientId}&client_secret={azureClientSecret}&grant_type={grantType}&username={username}&password={password}&scope={scope}";
                using (var response = client.PostAsync(tokenEndpoint, new StringContent(postBody, Encoding.UTF8, "application/x-www-form-urlencoded")).GetAwaiter().GetResult())
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var jsonresult = JObject.Parse(response.Content.ReadAsStringAsync().GetAwaiter().GetResult());
                        var token = (string)jsonresult["access_token"];
                        result = true;
                    }
                    else
                    {
                        /*
                        70002: Error validating credentials.
                        50126: Invalid username or password
                         */
                        var error = JObject.Parse(response.Content.ReadAsStringAsync().GetAwaiter().GetResult());
                        var codes = error["error_codes"].ToList();
                        var filter = codes.Where(x => x.Value<int>() == 70002 || x.Value<int>() == 50126).ToList();
                        if (filter.Count > 0)
                        {
                            result = false;
                        }
                        else
                        {
                            string messageError = "Error from Azure AD: " + (string)error["error_description"];
                            throw new Exception("Error de autenticación en Azure AD. Revise sus credenciales.");
                        }
                    }
                }
            }
            return result;
        }
    }
}