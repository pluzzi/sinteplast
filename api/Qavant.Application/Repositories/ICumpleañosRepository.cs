﻿using Qavant.Domain.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Repositories
{
    public interface ICumpleañosRepository
    {
        ICollection<Cumpleaños> GetCumpleaños();

        ICollection<Cumpleaños> GetCumpleaños(int page);
    }
}
