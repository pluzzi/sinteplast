﻿using Qavant.Domain.TipoCatalogo;
using System.Collections.Generic;

namespace Qavant.Application.Repositories
{
    public interface ITipoCatalogoRepository
    {
        TipoCatalogo ObtenerTipoCatalogoPorId(int id);
        ICollection<TipoCatalogo> ObtenerTipoCatalogos();
        ICollection<TipoCatalogoUsuario> ObtenerTipoCatalogosPorUsuarioId(int usuarioId);
    }
}
