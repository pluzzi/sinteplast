﻿using Qavant.Domain.Muro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Repositories
{
    public interface IMuroRepository
    {
        ICollection<Publicacion> GetPublicaciones();
    }
}
