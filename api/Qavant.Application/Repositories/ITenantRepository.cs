﻿using Qavant.Domain.Tenants;
using System;
using System.Linq.Expressions;

namespace Qavant.Application.Repositories
{
    public interface ITenantRepository
    {
        Tenant GetTenantByKey(Expression<Func<Tenant, bool>> predicate);
        Tenant GetTenantById(int tenantId);
        Tenant GetCurrentTenant(Canal canal, int tenantId);
    }
}
