﻿using Qavant.Domain.Beneficios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Repositories
{
    public interface IBeneficioRepository
    {
        ICollection<Beneficio> GetBeneficios(int tipoCatalogoProductosId);
        ICollection<Beneficio> GetBeneficios(int page, int tipoCatalogoProductosId);
        ICollection<Beneficio> GetBeneficiosFavoritos(int usuarioId);
        ICollection<Beneficio> GetBeneficiosFavoritos(int page, int usuarioId);
        ICollection<Beneficio> GetBeneficiosFavoritosPorCategoria(int page, int usuarioId, int? categoriaId);
        ICollection<Beneficio> GetBeneficiosPorCategoria(int page, int usuarioId, int? categoriaId);
        Beneficio GetBeneficioById(int id);
        bool GetLikedByBeneficioId(int beneficioId, int usuarioId);
        void SetLike(int beneficioId, int usuarioId);
        ICollection<Beneficio> GetAllBeneficiosPorCategoria(int usuarioId, int? categoriaId);
        Exchange Canjear(int userId, int benefitId);
    }
}
