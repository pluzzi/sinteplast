﻿using Qavant.Domain.Reacciones;

namespace Qavant.Application.Repositories
{
    public interface IReaccionRepository
    {
        Reaccion GetReaccionById(int id);

        void Guardar(int reaccionId, int moduloId, int entidadId);
    }
}
