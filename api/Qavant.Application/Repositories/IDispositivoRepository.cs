﻿using Qavant.Domain.Dispositivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Repositories
{
    public interface IDispositivoRepository
    {
        void RegistrarDispositivo(Dispositivo dispositivo);
        void RemoverDispositivo(Dispositivo dispositivo);
        List<Dispositivo> ObtenerDispositivos(int usuarioId, string uuid, string token);
        Dispositivo ObtenerDispositivo(int usuarioId,string uuid);
    }
}
