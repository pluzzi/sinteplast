﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Infrastructure
{
    public interface ICryptography
    {
        string Encrypt(string toEncrypt, bool useHashing);
        string Decrypt(string cipherString, bool useHashing);
    }
}
