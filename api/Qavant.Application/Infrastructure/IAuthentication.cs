﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Infrastructure
{
    public interface IAuthentication 
    {
        void Configure(Dictionary<string, string> parameters);
        bool Validate(int tenantId, string username, string password);
    }
}