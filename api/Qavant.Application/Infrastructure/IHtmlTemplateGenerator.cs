﻿using Qavant.Application.Services.Formularios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Infrastructure
{
    public interface IHtmlTemplateGenerator
    {
        string GetTemplateEmail(string template, object vm);
    }
}
