﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Infrastructure.LoggerActivity
{
    public interface ILogHolder
    {
        int TenantId { get; set; }
        int UsuarioId { get; set; }
        Canal Canal { get; set; }
    }
}
