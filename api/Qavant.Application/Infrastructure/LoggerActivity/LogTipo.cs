﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Infrastructure
{
    public enum LogTipo
    {
        VisitaLista = 1,
        VisitaDetalle = 2,
        VisitaPorCategorias = 3,
        AccionExitosa = 4,
        AccionFallida = 5,
        Descarga = 6,
        Respuesta = 7,
        Comentario = 8,
        Reaccion = 9
    }
}