﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Infrastructure
{
    public interface IEmailService
    {
        void Configure(string SMTPUsername, string SMTPPassword, string SMTPServer, int SMTPPort, bool SMTPEnableSsl, string emailFrom, string displayName);
        void SendMail(string email, string subject, string body);
        void SendMail(List<string> emails, string subject, string body);
    }
}