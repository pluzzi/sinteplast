﻿using Qavant.Application.Infrastructure;
using Qavant.Application.Repositories;
using Qavant.Domain.Categorias;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Qavant.Application.Services.Categorias
{
    public class CategoriaService : ICategoriaService
    {
        private ICategoriaRepository _categoriaRepository;
        private IApplicationLoggerActivity _applicationLoggerActivity;

        public CategoriaService(
            ICategoriaRepository CategoriaRepository,
            IApplicationLoggerActivity applicationLoggerActivity)
        {
            _categoriaRepository = CategoriaRepository;
            _applicationLoggerActivity = applicationLoggerActivity;
        }

        public CategoriaListOutput ObtenerCategoriasPorModuloId(int moduloId)
        {
            var categoria = _categoriaRepository.ObtenerCategoriasPorModuloId(moduloId);
            CategoriaListOutput output = new CategoriaListOutput(categoria);
            return output;
        }

        public CategoriaListOutput ObtenerCategoriasPorModulo(string nemonico, bool tree)
        {
            var categorias = _categoriaRepository.ObtenerCategoriasPorModulo(nemonico);
            CategoriaListOutput output = new CategoriaListOutput(categorias,tree);
            return output;
        }
    }
}
