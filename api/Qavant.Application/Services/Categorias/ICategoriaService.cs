﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Categorias
{
    public interface ICategoriaService
    {        
        CategoriaListOutput ObtenerCategoriasPorModuloId(int moduloId);
        CategoriaListOutput ObtenerCategoriasPorModulo(string nemonico,bool tree);
    }
}