﻿using System.Collections.Generic;

namespace Qavant.Application.Services.TipoCatalogo
{
    public class TipoCatalogoUsuarioListOutput
    {
        public List<TipoCatalogoUsuarioOutput> TipoCatalogoUsuario { get; }

        public TipoCatalogoUsuarioListOutput(ICollection<Qavant.Domain.TipoCatalogo.TipoCatalogoUsuario> tipoCatalogosUsuario)
        {
            List<TipoCatalogoUsuarioOutput> tipoCatalogosUsuariosResults = new List<TipoCatalogoUsuarioOutput>();
            foreach (var tipoCatalogoUsuario in tipoCatalogosUsuario)
            {
                TipoCatalogoUsuarioOutput output = new TipoCatalogoUsuarioOutput(tipoCatalogoUsuario);
                tipoCatalogosUsuariosResults.Add(output);
            }
            TipoCatalogoUsuario = tipoCatalogosUsuariosResults;
        }
    }
}
