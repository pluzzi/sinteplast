﻿using System.Collections.Generic;

namespace Qavant.Application.Services.TipoCatalogo
{
    public class TipoCatalogoListOutput
    {
        public List<TipoCatalogoOutput> TipoCatalogo { get; }

        public TipoCatalogoListOutput(ICollection<Qavant.Domain.TipoCatalogo.TipoCatalogo> tipoCatalogos)
        {
            List<TipoCatalogoOutput> tipoCatalogoResults = new List<TipoCatalogoOutput>();
            foreach (var tipoCatalogo in tipoCatalogos)
            {
                TipoCatalogoOutput output = new TipoCatalogoOutput(tipoCatalogo);
                tipoCatalogoResults.Add(output);
            }
            TipoCatalogo = tipoCatalogoResults;
        }
    }
}
