﻿using System.Collections.Generic;

namespace Qavant.Application.Services.TipoCatalogo
{
    public interface ITipoCatalogoService
    {
        TipoCatalogoOutput ObtenerTipoCatalogoPorId(int id);
        List<TipoCatalogoOutput> ObtenerTipoCatalogos();
        List<TipoCatalogoUsuarioOutput> ObtenerTipoCatalogosPorUsuarioId(int usuarioId);
    }
}