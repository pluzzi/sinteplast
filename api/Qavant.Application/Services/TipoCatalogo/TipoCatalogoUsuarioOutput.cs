﻿using System;

namespace Qavant.Application.Services.TipoCatalogo
{
    public class TipoCatalogoUsuarioOutput
    {
        public int UserId { get; }
        public TipoCatalogoOutput TipoCatalogoOutput { get; }
        public decimal PuntosAcumulados { get; }
        public DateTime FechaActualizacionPuntos { get; }


        public TipoCatalogoUsuarioOutput(Qavant.Domain.TipoCatalogo.TipoCatalogoUsuario tipoCatalogoUsuario)
        {
            UserId = tipoCatalogoUsuario.UserId;
            TipoCatalogoOutput = new TipoCatalogoOutput(tipoCatalogoUsuario.TipoCatalogo);
            PuntosAcumulados = tipoCatalogoUsuario.PuntosAcumulados;
            FechaActualizacionPuntos = tipoCatalogoUsuario.FechaActualizacionPuntos;
        }
    }
}