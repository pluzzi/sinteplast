﻿using Qavant.Application.Infrastructure;
using Qavant.Application.Repositories;
using Qavant.Application.Services.Shared;
using Qavant.Domain.Galerias;
using Qavant.Domain.Modulos;
using Qavant.Domain.TipoCatalogo;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Qavant.Application.Services.TipoCatalogo
{
    public class TipoCatalogoService : ITipoCatalogoService
    {
        private ITipoCatalogoRepository _tipoCatalogoRepository;
        private IApplicationLoggerActivity _applicationLoggerActivity;

        public TipoCatalogoService(ITipoCatalogoRepository tipoCatalogoRepository, IApplicationLoggerActivity applicationLoggerActivity)
        {
            _tipoCatalogoRepository = tipoCatalogoRepository;
            _applicationLoggerActivity = applicationLoggerActivity;
        }

        public TipoCatalogoOutput ObtenerTipoCatalogoPorId(int id)
        {
            var tipoCatalogo = _tipoCatalogoRepository.ObtenerTipoCatalogoPorId(id);
            if (tipoCatalogo is NullTipoCatalogo)
                throw new ApplicationException("Tipo de Catálogo no encontrado.");

            var log = LogActividad.CreateNew(ModuloEnum.Catalogos, LogTipo.VisitaDetalle, tipoCatalogo.Id);
            _applicationLoggerActivity.Save(log);

            TipoCatalogoOutput output = new TipoCatalogoOutput(tipoCatalogo);
            return output;
        }

        public List<TipoCatalogoOutput> ObtenerTipoCatalogos()
        {
            var tipoCatalogos = _tipoCatalogoRepository.ObtenerTipoCatalogos();
            if (tipoCatalogos is NullTipoCatalogo)
                throw new ApplicationException("Catálogos no encontrados.");

            var log = LogActividad.CreateNew(ModuloEnum.Catalogos, LogTipo.VisitaLista);
            _applicationLoggerActivity.Save(log);

            List<TipoCatalogoOutput> output = new List<TipoCatalogoOutput>();

            foreach(var elt in tipoCatalogos)
            {
                output.Add(new TipoCatalogoOutput(elt));
            }

            return output;
        }

        public List<TipoCatalogoUsuarioOutput> ObtenerTipoCatalogosPorUsuarioId(int usuarioId)
        {
            var tipoCatalogos = _tipoCatalogoRepository.ObtenerTipoCatalogosPorUsuarioId(usuarioId);
            if (tipoCatalogos is NullTipoCatalogo)
                throw new ApplicationException("Catálogos no encontrados.");

            var log = LogActividad.CreateNew(ModuloEnum.Catalogos, LogTipo.VisitaLista);
            _applicationLoggerActivity.Save(log);

            List<TipoCatalogoUsuarioOutput> output = new List<TipoCatalogoUsuarioOutput>();

            foreach (var elt in tipoCatalogos)
            {
                output.Add(new TipoCatalogoUsuarioOutput(elt));
            }

            return output;
        }
    }
}