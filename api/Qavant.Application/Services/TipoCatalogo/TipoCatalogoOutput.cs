﻿namespace Qavant.Application.Services.TipoCatalogo
{
    public class TipoCatalogoOutput
    {
        public int Id { get; }
        public string Descripcion { get; }
        public string IconoCatalogo { get; }
        public string FondoCatalogo { get; }
        public int EmpresaId { get; }
        public string Mail { get; }

        public TipoCatalogoOutput(Qavant.Domain.TipoCatalogo.TipoCatalogo tipoCatalogo)
        {
            Id = tipoCatalogo.Id;
            Descripcion = tipoCatalogo.Descripcion;
            IconoCatalogo = tipoCatalogo.IconoCatalogo;
            FondoCatalogo = tipoCatalogo.FondoCatalogo;
            EmpresaId = tipoCatalogo.EmpresaId;
            Mail = tipoCatalogo.Mail;
        }
    }
}