﻿using Qavant.Domain.Info;
using System.Collections.Generic;

namespace Qavant.Application.Services.InfoPaginas
{
    public sealed class InfoUtilListOutput
    {
        public List<InfoUtilOutput> InfoPaginas { get; }

        public InfoUtilListOutput(ICollection<InfoUtil> info)
        {
            List<InfoUtilOutput> InfoPaginasResults = new List<InfoUtilOutput>();
            foreach (var item in info)
            {
                InfoUtilOutput output = new InfoUtilOutput(item);
                InfoPaginasResults.Add(output);
            }
            InfoPaginas = InfoPaginasResults;
        }
    }
}