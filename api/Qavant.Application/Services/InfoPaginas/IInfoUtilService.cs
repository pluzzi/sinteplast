﻿namespace Qavant.Application.Services.InfoPaginas
{
    public interface IInfoUtilService
    {
        InfoUtilListOutput GetListInfoUtil();
        InfoUtilListOutput GetListInfoUtilByCategoryId(int id);
        InfoUtilOutput GetInfoUtilById(int id);
    }
}