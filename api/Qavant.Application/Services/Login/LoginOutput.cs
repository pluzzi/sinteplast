﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qavant.Application.Services.Shared;
using Qavant.Domain.Usuarios;

namespace Qavant.Application.Services.Login
{
    public class LoginOutput
    {
        public int Id { get; }
        public string Name { get; set; }
        public string FullName { get; }
        public string UserName { get; }
        public string Email { get; }
        public bool Active { get; }
        public bool ShouldModifyPassword { get; set; }
        public string Pintureria { get; }

        public LoginOutput(Usuario usuario)
        {
            Id = usuario.Id;
            UserName = usuario.UserName;
            Name = usuario.Nombre;
            FullName = $"{usuario.Nombre} {usuario.Apellido}";
            Email = usuario.Email;
            //Image = usuario.Imagen;
            Active = usuario.Activo;
            Pintureria = usuario.Pintureria;
            if (usuario.UserName == usuario.Password)
            {
                ShouldModifyPassword = true;
            }
            else
            {
                ShouldModifyPassword = false;
            }
        }
    }
}
