﻿using Qavant.Application.Services.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Login
{
    public interface ILoginService
    {
        LoginOutput Login(string username, string password);
        LoginOutput Logged(string username);
    }
}
