﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qavant.Application.Infrastructure;
using Qavant.Application.Repositories;
using Qavant.Application.Services.Shared;
using Qavant.Domain.Modulos;
using Qavant.Domain.Usuarios;
using ApplicationException = Qavant.Application.Exceptions.ApplicationException;

namespace Qavant.Application.Services.MiPerfil
{
    public class MiPerfilService : IMiPerfilService
    {
        private IUsuarioRepository _usuarioRepository;
        private IApplicationLoggerActivity _applicationLoggerActivity;

        public MiPerfilService(IUsuarioRepository usuarioRepository, 
            IApplicationLoggerActivity applicationLoggerActivity)
        {
            _usuarioRepository = usuarioRepository;
            _applicationLoggerActivity = applicationLoggerActivity;
        }

        public void SetNewPassword(int usuarioId, string password, string confirmPassword)
        {
            if (usuarioId <= 0)
                throw new ApplicationException(nameof(usuarioId));

            var usuario = _usuarioRepository.GetUsuarioById(usuarioId);
            if (usuario is NullUsuario)
                throw new ApplicationException("No existe el usuario.");

            if (string.IsNullOrEmpty(password))
                throw new ApplicationException("Contraseña nueva en blanco.");

            if (string.IsNullOrEmpty(confirmPassword))
                throw new ApplicationException("Confirmación de contraseña en blanco.");

            if(password.ToLower() != confirmPassword.ToLower())
                throw new ApplicationException("La contraseña y su confirmación deben ser iguales.");

            if(password.ToLower() == usuario.UserName)
                throw new ApplicationException("La contraseña no puede ser igual a su nombre de usuario.");

            usuario.SetPassword(password);
            usuario.SetActivo(true);
            _usuarioRepository.Actualizar(usuario);
        }

        public UsuarioOutput GetPerfil(int usuarioId)
        {
            if (usuarioId <= 0)
                throw new ApplicationException(nameof(usuarioId));

            var usuario = _usuarioRepository.GetUsuarioById(usuarioId);
            if (usuario is NullUsuario)
                throw new ApplicationException("Información de perfil no encontrada.");  

            var log = LogActividad.CreateNew(ModuloEnum.MiPerfil, LogTipo.VisitaDetalle);
            _applicationLoggerActivity.Save(log);

            UsuarioOutput output = new UsuarioOutput(usuario);
            return output;
        }

        public void ChangeOldPassword(int usuarioId, string oldPassword, string newPassword, string confirmPassword)
        {
            if (usuarioId <= 0)
                throw new ApplicationException(nameof(usuarioId));

            var usuario = _usuarioRepository.GetUsuarioById(usuarioId);
            if (usuario is NullUsuario)
                throw new ApplicationException("Información de perfil no encontrada.");

            if (string.IsNullOrEmpty(oldPassword) || string.IsNullOrEmpty(newPassword) || string.IsNullOrEmpty(confirmPassword))
                throw new ApplicationException("Debe completar todos los campos.");

            if (usuario.Password != oldPassword)
                throw new ApplicationException("La constraseña actual es incorrecta.");

            if (newPassword.ToLower() != confirmPassword.ToLower())
                throw new ApplicationException("La contraseña y su confirmación deben ser iguales.");

            if (oldPassword.ToLower() == newPassword.ToLower())
                throw new ApplicationException("Seleccione una contraseña distinta a la anterior.");

            usuario.SetPassword(newPassword);
            _usuarioRepository.Actualizar(usuario);
        }
    }
}
