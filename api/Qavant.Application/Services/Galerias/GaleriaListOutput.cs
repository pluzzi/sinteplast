﻿using Qavant.Domain.Galerias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Galerias
{
    public class GaleriaListOutput
    {
        public List<GaleriaOutput> Galerias { get; }

        public GaleriaListOutput(ICollection<Galeria> galerias)
        {
            List<GaleriaOutput> galeriasResults = new List<GaleriaOutput>();
            foreach (var galeria in galerias)
            {
                GaleriaOutput output = new GaleriaOutput(galeria);
                galeriasResults.Add(output);
            }
            Galerias = galeriasResults;
        }
    }
}
