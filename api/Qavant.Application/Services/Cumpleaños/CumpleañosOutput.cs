﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qavant.Domain.Usuarios;


namespace Qavant.Application.Services.Cumpleaños
{
    public class CumpleañosOutput
    {
        public int Id { get; }
        public string Name { get; }
        public string Surname { get; }
        public string FullName { get; }
        public DateTime BirthDate { get; }
        public string Area { get; }
        public string Iniciales { get; }
        public bool IsBirthdayToday
        {
            get
            {
                if (this.BirthDate == null)
                    return false;

                bool result = false;
                if (DateTime.Today.Day == this.BirthDate.Day && DateTime.Today.Month == this.BirthDate.Month)
                {
                    result = true;
                }
                return result;
            }
        }
        public string BirthFriendlyDate
        {
            get
            {
                if (this.BirthDate == null)
                    return string.Empty;

                DateTimeFormatInfo dtinfo = new CultureInfo("es-ES", false).DateTimeFormat;
                string dia = this.BirthDate.Day.ToString();
                string mes = dtinfo.GetMonthName(this.BirthDate.Month);

                string formatted = $"{dia} de {mes}";
                return formatted;
            }
        }
        public int ReaccionId { get; }
        public int TotalReacciones { get; }

        public CumpleañosOutput(Domain.Usuarios.Cumpleaños cumpleaños)
        {
            Id = cumpleaños.Usuario.Id;
            Name = cumpleaños.Usuario.Nombre;
            Surname = cumpleaños.Usuario.Apellido;
            FullName = $"{cumpleaños.Usuario.Nombre} {cumpleaños.Usuario.Apellido}";
            Area = cumpleaños.Usuario.Area?.Nombre;
            BirthDate = cumpleaños.Usuario.FechaNacimiento;
            ReaccionId = cumpleaños.InfoReaccion.ReaccionId;
            TotalReacciones = cumpleaños.InfoReaccion.TotalReacciones;
            Iniciales = cumpleaños.Usuario.Iniciales;
        }
    }
}