﻿using Qavant.Domain.Beneficios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Beneficios
{
    public sealed class ExchangeOutput
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int BenefitsId { get; set; }
        public DateTime FechaCanje { get; set; }
        public decimal Puntos { get; set; }
        public decimal PuntosUsados { get; set; }

        public ExchangeOutput(Exchange exchange)
        {
            Id = exchange.Id;
            UserId = exchange.UserId;
            BenefitsId = exchange.BenefitsId;
            FechaCanje = exchange.FechaCanje;
            Puntos = exchange.Puntos;
            PuntosUsados = exchange.PuntosUsados;
        }
    }
}