﻿using Qavant.Domain.Beneficios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Beneficios
{
    public sealed class BeneficioOutput
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Copete { get; set; }
        public string Content { get; set; }
        public string Image { get; set; }
        public bool Liked { get; set; }
        public string To { get; set; }
        public int ReaccionId { get; }
        public int TotalReacciones { get; }
        public int Puntos { get; set; }
        public int TipoCatalogoProductosId { get; set; }
        public bool EsNuevo { get; set; }

        public BeneficioOutput(Beneficio beneficio)
        {
            Id = beneficio.Id;
            Title = beneficio.Titulo;
            Subtitle = beneficio.Copete;
            Copete = beneficio.Copete;
            Content = beneficio.Contenido;
            Image = beneficio.Imagen;
            To = beneficio.FechaHasta.ToString("dd/MM/yyyy");
            ReaccionId = beneficio.InfoReaccion.ReaccionId;
            TotalReacciones = beneficio.InfoReaccion.TotalReacciones;
            Puntos = beneficio.Puntos;
            TipoCatalogoProductosId = beneficio.TipoCatalogoProductosId;
            EsNuevo = beneficio.EsNuevo;

        }
    }
}