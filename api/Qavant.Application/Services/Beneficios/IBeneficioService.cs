﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Beneficios
{
    public interface IBeneficioService
    {
        BeneficioOutput SetLike(int beneficioId, int usuarioId);

        BeneficioOutput GetBeneficioById(int id, int usuarioId);

        BeneficioListOutput GetBeneficios(int tipoCatalogoProductosId, int usuarioId);

        BeneficioListOutput GetBeneficios(int page, int tipoCatalogoProductosId, int usuarioId);

        BeneficioListOutput GetBeneficiosFavoritos(int usuarioId);

        BeneficioListOutput GetBeneficiosFavoritos(int page, int usuarioId);

        BeneficioListOutput GetBeneficiosFavoritosPorCategoria(int page, int usuarioId, int? categoriaId);

        BeneficioListOutput GetBeneficiosPorCategoria(int page, int usuarioId, int? categoriaId);

        BeneficioListOutput GetAllBeneficiosPorCategoria(int usuarioId, int? categoriaId);

        ExchangeOutput Canjear(int userId, int benefitsId);

    }
}