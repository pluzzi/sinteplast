﻿using Qavant.Application.Infrastructure;
using Qavant.Application.Repositories;
using Qavant.Domain.Modulos;
using Qavant.Domain.Reacciones;
using System;

namespace Qavant.Application.Services.Reacciones
{
    public class ReaccionService : IReaccionService
    {
        private readonly ITenantHolder _tenantHolder;
        private readonly IModuloRepository _moduloRepository;
        private readonly IReaccionRepository _reaccionRepository;
        private IApplicationLoggerActivity _applicationLoggerActivity;

        public ReaccionService(ITenantHolder tenantHolder, 
            IModuloRepository moduloRepository, 
            IReaccionRepository reaccionRepository,
            IApplicationLoggerActivity applicationLoggerActivity)
        {
            _tenantHolder = tenantHolder;
            _moduloRepository = moduloRepository;
            _reaccionRepository = reaccionRepository;
            _applicationLoggerActivity = applicationLoggerActivity;
        }

        public void Guardar(ReaccionInput input)
        {
            var modulo = _moduloRepository.GetModuloPorNemonico(input.Modulo);
            if (modulo is NullModulo)
                throw new ApplicationException("Módulo no encontrado.");

            var reaccion = _reaccionRepository.GetReaccionById(input.ReaccionId);
            if (reaccion is NullReaccion)
                throw new ApplicationException("Tipo de reacción no registrada.");

            _reaccionRepository.Guardar(reaccion.Id, modulo.Id, input.EntidadId);
            ModuloEnum modEnum = (ModuloEnum)modulo.Id;
            var log = LogActividad.CreateNew(modEnum, LogTipo.Reaccion, input.EntidadId);
            _applicationLoggerActivity.Save(log);
        }
    }
}