﻿using Qavant.Domain.Dispositivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Dispositivos
{
    public sealed class DispositivoOutput
    {
        public Int64 Id { get; }
        public int UserId { get; }
        public string Uuid { get; }
        public string TokenNotification { get; }

        public DispositivoOutput(Dispositivo dispositivo)
        {
            Id = dispositivo.Id;
            UserId = dispositivo.UsuarioId;
            Uuid = dispositivo.Uuid;
            TokenNotification = dispositivo.TokenNotificacion;
            
        }
    }
}