﻿using Qavant.Domain.Dispositivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Dispositivos
{
    public interface IDispositivoService
    {
        void RegistrarDispositivo(int usuarioId, string uuid, string token);
        void RemoverDispositivo(int usuarioId, string uuid );
    }
}