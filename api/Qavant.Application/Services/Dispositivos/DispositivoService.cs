﻿using Qavant.Application.Infrastructure;
using Qavant.Application.Repositories;
using Qavant.Domain.Dispositivos;
using System;
using System.Collections.Generic;

namespace Qavant.Application.Services.Dispositivos
{
    public class DispositivoService : IDispositivoService
    {
        private IDispositivoRepository _dispositivoRepository;
        private IApplicationLoggerActivity _applicationLoggerActivity;

        public DispositivoService(
            IDispositivoRepository dispositivoRepository, 
            IApplicationLoggerActivity applicationLoggerActivity)
        {
            _dispositivoRepository = dispositivoRepository;
            _applicationLoggerActivity = applicationLoggerActivity;
        }

        public void RegistrarDispositivo(int usuarioId, string uuid, string token)
        {
            var dispositivos = _dispositivoRepository.ObtenerDispositivos(usuarioId, uuid, token);
            if (dispositivos.Count == 0)
            {
                Dispositivo dispositivo = new Dispositivo();
                dispositivo.UsuarioId = usuarioId;
                dispositivo.Uuid = uuid;
                dispositivo.TokenNotificacion = token;

                _dispositivoRepository.RegistrarDispositivo(dispositivo);
            }
        }

        public void RemoverDispositivo(int usuarioId, string uuid)
        {
            var dispositivo = _dispositivoRepository.ObtenerDispositivo(usuarioId, uuid);
            if (dispositivo != null)
            {
                _dispositivoRepository.RemoverDispositivo(dispositivo);
            }
        }
    }
}