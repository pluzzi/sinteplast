﻿using Qavant.Application.Exceptions;
using Qavant.Application.Infrastructure;
using Qavant.Application.Repositories;
using Qavant.Domain.Capacitaciones;
using Qavant.Domain.Modulos;
using System;
using ApplicationException = Qavant.Application.Exceptions.ApplicationException;

namespace Qavant.Application.Services.Capacitaciones
{
    public class CapacitacionService : ICapacitacionService
    {
        private ICapacitacionRepository _capacitacionRepository;
        private IApplicationLoggerActivity _applicationLoggerActivity;

        public CapacitacionService(
            ICapacitacionRepository capacitacionRepository,
            IApplicationLoggerActivity applicationLoggerActivity)
        {
            _capacitacionRepository = capacitacionRepository;
            _applicationLoggerActivity = applicationLoggerActivity;
        }

        public CapacitacionOutput GetCapacitacionById(int Id)
        {
            var capacitacion = _capacitacionRepository.GetCapacitacionById(Id);
            if (capacitacion == null)
                throw new ApplicationException("Capacitación no encontrada.");

            var log = LogActividad.CreateNew(ModuloEnum.Capacitaciones, LogTipo.VisitaDetalle, capacitacion.Id);
            _applicationLoggerActivity.Save(log);

            CapacitacionOutput output = new CapacitacionOutput(capacitacion);
            //TODO: Agregar en el output si el usuario ya fue evaluado o no, y si el formulario asociado permite multiple respuestas. Se debe hacer despues del refacto de formularios
            return output;
        }

        public CapacitacionListOutput GetCapacitaciones()
        {
            var capacitaciones = _capacitacionRepository.GetCapacitaciones();
            CapacitacionListOutput output = new CapacitacionListOutput(capacitaciones);
            //TODO: Agregar en el output si el usuario ya fue evaluado o no, y si el formulario asociado permite multiple respuestas. Se debe hacer despues del refacto de formularios
            var log = LogActividad.CreateNew(ModuloEnum.Capacitaciones, LogTipo.VisitaLista);
            _applicationLoggerActivity.Save(log);
            return output;
        }
    }
}
