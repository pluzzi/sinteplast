﻿using Qavant.Domain.Capacitaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Capacitaciones
{
    public sealed class CapacitacionListOutput
    {
        public List<CapacitacionOutput> Capacitaciones { get; }

        public CapacitacionListOutput(ICollection<Capacitacion> capacitacion)
        {
            List<CapacitacionOutput> CapacitacionesResults = new List<CapacitacionOutput>();
            foreach (var item in capacitacion)
            {
                CapacitacionOutput output = new CapacitacionOutput(item);
                CapacitacionesResults.Add(output);
            }
            Capacitaciones = CapacitacionesResults;
        }
    }
}