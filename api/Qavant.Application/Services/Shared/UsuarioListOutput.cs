﻿using Qavant.Domain.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Shared
{
    public class UsuarioListOutput
    {
        public List<UsuarioOutput> Usuarios { get; }

        public UsuarioListOutput(ICollection<Usuario> usuarios)
        {
            List<UsuarioOutput> usuariosResults = new List<UsuarioOutput>();
            foreach (var usuario in usuarios)
            {
                UsuarioOutput output = new UsuarioOutput(usuario);
                usuariosResults.Add(output);
            }
            Usuarios = usuariosResults;
        }
    }
}
