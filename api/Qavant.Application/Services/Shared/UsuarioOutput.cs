﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qavant.Domain.Usuarios;

namespace Qavant.Application.Services.Shared
{
    public class UsuarioOutput
    {
        public int Id { get; }
        public string Name { get; }
        public string Surname { get; }
        public string FullName { get; }
        public string UserName { get; }
        public string Email { get; }   
        public string Phone { get; }
        public string Mobile { get; }
        public string Address { get; }
        public DateTime BirthDate { get; }
        public bool Active { get; }
        public bool Enable { get; }
        public int GeneroCatalogoId { get; }
        public string Area { get; }
        public string Pintuteria { get; }
        public string CP { get; }
        public string Iniciales { get; }  
        
        public bool IsBirthdayToday
        {
            get
            {
                if (this.BirthDate == null)
                    return false;

                bool result = false;
                if (DateTime.Today.Day == this.BirthDate.Day && DateTime.Today.Month == this.BirthDate.Month)
                {
                    result = true;
                }
                return result;
            }
        }
        public string BirthFriendlyDate
        {
            get
            {
                if (this.BirthDate == null)
                    return string.Empty;

                DateTimeFormatInfo dtinfo = new CultureInfo("es-ES", false).DateTimeFormat;
                string dia = this.BirthDate.Day.ToString();
                string mes = dtinfo.GetMonthName(this.BirthDate.Month);

                string formatted = $"{dia} de {mes}";
                return formatted;
            }
        }
        public UsuarioOutput(Usuario usuario)
        {
            Id = usuario.Id;
            Name = usuario.Nombre;
            UserName = usuario.UserName;
            Surname = usuario.Apellido;
            FullName = $"{usuario.Nombre} {usuario.Apellido}";
            Email = usuario.Email;
            Phone = usuario.Telefono;
            Mobile = usuario.Celular;
            Address = usuario.Direccion;
            BirthDate = usuario.FechaNacimiento;
            Active = usuario.Activo;
            Enable = usuario.Habilititado;
            Area = usuario.Area?.Nombre;
            Iniciales = usuario.Iniciales;
            Pintuteria = usuario.Pintureria;
            CP = usuario.CP;
      
            

        }
    }
}
