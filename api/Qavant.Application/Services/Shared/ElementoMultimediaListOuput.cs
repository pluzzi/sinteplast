﻿using Qavant.Domain.Multimedia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Shared
{
    public class ElementoMultimediaListOuput
    {
        public List<ElementoMultimediaOutput> ElementosMultimedia { get; }

        public ElementoMultimediaListOuput(ICollection<ElementoMultimedia> elementos)
        {
            List<ElementoMultimediaOutput> elementosResults = new List<ElementoMultimediaOutput>();
            foreach (var elemento in elementos)
            {
                ElementoMultimediaOutput output = new ElementoMultimediaOutput(elemento);
                elementosResults.Add(output);
            }
            ElementosMultimedia = elementosResults;
        }
    }
}
