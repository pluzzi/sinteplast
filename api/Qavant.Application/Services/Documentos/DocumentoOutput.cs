﻿using Qavant.Domain.Documentos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Documentos
{
    public class DocumentoOutput
    {
        public int Id { get; }
        public string Title { get; }
        public string Description { get; }
        public string FilePath { get; }
        public string Extension { get; }
        public string FileTypeLabel { get; }
        public int ReaccionId { get; }
        public int TotalReacciones { get; }

        public DocumentoOutput(Documento documento)
        {
            Id = documento.Id;
            Title = documento.Title;
            Description = documento.Description;
            FilePath = documento.FilePath;
            TotalReacciones = documento.InfoReaccion.TotalReacciones;
            ReaccionId = documento.InfoReaccion.ReaccionId;

            string ext = Path.GetExtension(documento.FilePath).Replace(".", "");
            string docType = "";

            switch (ext)
            {
                case "pdf":
                    docType = "PDF";                    
                    break;
                case "jpg":
                case "jpeg":
                case "png":
                    docType = "IMAGEN";
                    break;
                case "xls":
                case "xlsx":
                    docType = "EXCEL";
                    break;
                case "doc":
                case "docx":
                    docType = "WORD";
                    break;
                case "ppt":
                case "pptx":
                case "pps":
                case "ppsx":
                    docType = "POWER POINT";
                    break;
            }

            Extension = ext;
            FileTypeLabel = docType;            
        }
    }
}
