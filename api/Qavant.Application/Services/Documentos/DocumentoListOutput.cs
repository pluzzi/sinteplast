﻿using Qavant.Domain.Documentos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Documentos
{
    public class DocumentoListOutput
    {
        public List<DocumentoOutput> Documentos { get; }

        public DocumentoListOutput(ICollection<Documento> documentos)
        {
            List<DocumentoOutput> documentosResults = new List<DocumentoOutput>();
            foreach (var documento in documentos)
            {
                DocumentoOutput output = new DocumentoOutput(documento);
                documentosResults.Add(output);
            }
            Documentos = documentosResults;
        }
    }
}
