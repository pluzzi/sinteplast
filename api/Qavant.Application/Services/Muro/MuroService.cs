﻿using Qavant.Application.Infrastructure;
using Qavant.Application.Repositories;
using Qavant.Domain.Modulos;

namespace Qavant.Application.Services.Muro
{
    public class MuroService : IMuroService
    {
        private IMuroRepository _muroRepository;
        private IApplicationLoggerActivity _applicationLoggerActivity;

        public MuroService(IMuroRepository muroRepository, IApplicationLoggerActivity applicationLoggerActivity)
        {
            _muroRepository = muroRepository;
            _applicationLoggerActivity = applicationLoggerActivity;
        }

        public PublicacionListOutput GetPublicaciones()
        {
            var publicaciones = _muroRepository.GetPublicaciones();
            PublicacionListOutput output = new PublicacionListOutput(publicaciones);
            var log = LogActividad.CreateNew(ModuloEnum.Muro, LogTipo.VisitaLista);
            _applicationLoggerActivity.Save(log);
            return output;
        }
    }
}