﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qavant.Domain.Muro;

namespace Qavant.Application.Services.Muro
{
    public class PublicacionListOutput
    {
        public List<PublicacionOutput> Publicaciones { get; }

        public PublicacionListOutput(ICollection<Publicacion> publicaciones)
        {
            List<PublicacionOutput> publicacionesResults = new List<PublicacionOutput>();
            foreach (var publicacion in publicaciones)
            {
                PublicacionOutput output = new PublicacionOutput(publicacion);
                publicacionesResults.Add(output);
            }
            Publicaciones = publicacionesResults;
        }
    }
}
