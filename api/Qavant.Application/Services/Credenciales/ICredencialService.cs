﻿using Qavant.Application.Services.Noticias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Credenciales
{
    public interface ICredencialService
    {
        CredencialOutput GetCredencialById(int id);
    }
}