﻿using Qavant.Application.Infrastructure;
using Qavant.Application.Repositories;
using Qavant.Domain.Credencial;
using Qavant.Domain.Modulos;
using ApplicationException = Qavant.Application.Exceptions.ApplicationException;

namespace Qavant.Application.Services.Credenciales
{
    public class CredencialService : ICredencialService
    {
        private readonly ITenantHolder _tenantHolder;
        private readonly ICredencialRepository _credencialRepository;
        private IApplicationLoggerActivity _applicationLoggerActivity;

        public CredencialService(
            ITenantHolder tenantHolder,
            ICredencialRepository credencialRepository, 
            IApplicationLoggerActivity applicationLoggerActivity)
        {
            _tenantHolder = tenantHolder;
            _credencialRepository = credencialRepository;
            _applicationLoggerActivity = applicationLoggerActivity;
        }

        public CredencialOutput GetCredencialById(int credencialId)
        {
            var credencial = _credencialRepository.GetCredencialById(credencialId);
            if (credencial is NullCredencial)
                throw new ApplicationException("Credencial no encontrada.");

            var log = LogActividad.CreateNew(ModuloEnum.Credenciales, LogTipo.VisitaDetalle, credencial.Id);
            _applicationLoggerActivity.Save(log);

            CredencialOutput output = new CredencialOutput(credencial);
            return output;
        }

    }
}