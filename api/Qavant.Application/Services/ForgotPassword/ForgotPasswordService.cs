﻿using Qavant.Application.Exceptions;
using Qavant.Application.Repositories;
using Qavant.Application.Services.Shared;
using Qavant.Domain.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.ForgotPassword
{
    public class ForgotPasswordService : IForgotPasswordService
    {
        private readonly ITenantHolder _tenantHolder;
        private readonly IUsuarioRepository _usuarioRepository;

        public ForgotPasswordService(ITenantHolder tenantHolder, IUsuarioRepository usuarioRepository)
        {
            _tenantHolder = tenantHolder;
            _usuarioRepository = usuarioRepository;
        }

        public void Reset(string username)
        {
            var usuario = _usuarioRepository.GetUsuarioByUsername(username);
            if (usuario is NullUsuario)
                throw new UsuarioNoRegistradoException("No encontramos el usuario ingresado en el sistema.");

            if (!usuario.Activo)
                throw new UsuarioInactivoException("Su cuenta de usuario ha sido desactivada.");

            _usuarioRepository.ResetPassword(usuario);
        }
    }
}
