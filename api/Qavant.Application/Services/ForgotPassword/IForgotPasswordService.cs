﻿using Qavant.Application.Services.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.ForgotPassword
{
    public interface IForgotPasswordService
    {
        void Reset(string username);
    }
}
