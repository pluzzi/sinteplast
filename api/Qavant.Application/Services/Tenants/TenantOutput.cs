﻿using Qavant.Domain.Tenants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Tenants
{
    public class TenantOutput
    {
        public int Id { get;}
        public string Nombre { get; }
        public dynamic ParametrosPublicos { get; }
        public dynamic ParametrosInternos { get; }

        public TenantOutput(Tenant tenant)
        {
            Id = tenant.Id;
            Nombre = tenant.Nombre;
            ParametrosPublicos = tenant.ParametrosPublicos;
            ParametrosInternos = tenant.ParametrosInternos;
        }
    }
}
