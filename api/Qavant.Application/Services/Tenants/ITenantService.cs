﻿using Qavant.Application.Services.Shared;
using Qavant.Domain.Tenants;

namespace Qavant.Application.Services.Tenants
{
    public interface ITenantService
    {
        TenantOutput ValidateAndReturn(Canal canal, string tenantKey);
        TenantOutput GetCurrentTenant(Canal canal, int tenantId);
        dynamic GetClientSettings(Canal canal, int tenantId);
    }
}
