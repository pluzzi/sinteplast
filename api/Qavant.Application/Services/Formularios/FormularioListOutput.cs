﻿using Qavant.Domain.Formularios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Formularios
{
    public class FormularioListOutput
    {
        public List<FormularioListItemOutput> Formularios { get; }

        public FormularioListOutput(ICollection<Formulario> formularios)
        {
            List<FormularioListItemOutput> formulariosResults = new List<FormularioListItemOutput>();
            foreach (var form in formularios)
            {
                FormularioListItemOutput output = new FormularioListItemOutput(form);
                formulariosResults.Add(output);
            }
            Formularios = formulariosResults;
        }
    }
}