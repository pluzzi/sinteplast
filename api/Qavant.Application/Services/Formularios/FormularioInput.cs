﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Formularios
{
    public class FormularioInput
    {
        public int FormId { get; set;  }
        public List<PreguntaInput> Questions { get; set; }
    }

    public class PreguntaInput
    {
        public int Id { get; set; }
        public string QuestionTypeId { get; set; }
        public string Answer { get; set; }
        public int AnswerOptionId { get; set; }
        public List<OpcionInput> Options { get; set; }
    }

    public class OpcionInput
    {
        public int Id { get; set; }
        public bool AnswerSelected { get; set; }
    }

}
