﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qavant.Domain.Formularios;
using Qavant.Domain.Usuarios;

namespace Qavant.Application.Services.Formularios
{
    public class FormularioEmailViewModel
    {
        public string Titulo { get; set; }
        public string Introduccion { get; set; }
        public string UsuarioNombre { get; set; }
        public string UsuarioApellido { get; set; }
        public string UsuarioEmail { get; set; }
        public IList<PreguntaEmailViewModel> Preguntas { get; set; }

        public FormularioEmailViewModel(Usuario usuario, Formulario form, Domain.Formularios.RespuestasEnFormulario respuestasForm)
        {
            Titulo = form.EmailTitulo;
            Introduccion = form.EmailIntroduccion;
            UsuarioNombre = usuario.Nombre;
            UsuarioApellido = usuario.Apellido;
            UsuarioEmail = usuario.Email;

            List<PreguntaEmailViewModel> preguntasTemp = new List<PreguntaEmailViewModel>();
            foreach (var respuesta in respuestasForm.Respuestas)
            {
                List<OpcionEmailViewModel> opcionesResults = new List<OpcionEmailViewModel>();
                foreach (var opcion in respuesta.RespuestaSeleccion)
                {
                    opcionesResults.Add(new OpcionEmailViewModel {
                        Descripcion = opcion.Descripcion
                    });
                }
                preguntasTemp.Add(new PreguntaEmailViewModel {
                    Enunciado = respuesta.Pregunta.Enunciado,
                    RespuestaTexto = respuesta.RespuestaSimple,
                    RespuestasSeleccion = opcionesResults
                });
            }
            Preguntas = preguntasTemp;
        }
    }

    public class PreguntaEmailViewModel
    {
        public string Enunciado { get; set; }
        public string RespuestaTexto { get; set; }
        public List<OpcionEmailViewModel> RespuestasSeleccion { get; set; }
    }

    public class OpcionEmailViewModel
    {
        public string Descripcion { get; set; }
    }
}
