﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Noticias
{
    public interface INoticiaService
    {
        NoticiaOutput ObtenerNoticiaPorId(int id);
        NoticiaListOutput ObtenerNoticias();
        NoticiaListOutput ObtenerNoticias(int page);
    }
}