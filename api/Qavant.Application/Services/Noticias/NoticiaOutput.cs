﻿using Qavant.Domain.Noticias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Noticias
{
    public sealed class NoticiaOutput
    {
        public int Id { get; }
        public string Title { get; }
        public string Copete { get; set; }
        public string Content { get; }
        public string Date { get; }
        public string Image { get; }
        public bool Featured { get;}
        public int ReaccionId { get; }
        public int TotalReacciones { get; }

        public NoticiaOutput(Noticia noticia)
        {
            Id = noticia.Id;
            Title = noticia.Titulo;
            Copete = noticia.Copete;
            Content = noticia.Contenido;
            Date = noticia.FechaAlta.ToString("dd/MM/yyyy");
            Image = noticia.Imagen;
            Featured = noticia.Destacada;
            ReaccionId = noticia.InfoReaccion.ReaccionId;
            TotalReacciones = noticia.InfoReaccion.TotalReacciones;
        }
    }
}