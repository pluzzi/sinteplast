﻿using Qavant.Application.Services.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.QuienEsQuien
{
    public interface IQuienEsQuienService
    {
        UsuarioListOutput GetUsuarios();
        UsuarioListOutput GetUsuarios(int page);
        UsuarioListOutput Search(int page, string searchTerm);
    }
}
