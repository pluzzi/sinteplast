﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qavant.Application.Infrastructure;
using Qavant.Application.Repositories;
using Qavant.Application.Services.Shared;
using Qavant.Domain.Modulos;

namespace Qavant.Application.Services.QuienEsQuien
{
    public class QuienEsQuienService : IQuienEsQuienService
    {
        private IUsuarioRepository _usuarioRepository;
        private IApplicationLoggerActivity _applicationLoggerActivity;

        public QuienEsQuienService(IUsuarioRepository usuarioRepository, IApplicationLoggerActivity applicationLoggerActivity)
        {
            _usuarioRepository = usuarioRepository;
            _applicationLoggerActivity = applicationLoggerActivity;
        }

        public UsuarioListOutput GetUsuarios()
        {
            var usuarios = _usuarioRepository.GetUsuarios();
            UsuarioListOutput output = new UsuarioListOutput(usuarios);
            var log = LogActividad.CreateNew(ModuloEnum.QuienEsQuien, LogTipo.VisitaLista);
            _applicationLoggerActivity.Save(log);
            return output;
        }

        public UsuarioListOutput GetUsuarios(int page)
        {
            var usuarios = _usuarioRepository.GetUsuarios(page);
            UsuarioListOutput output = new UsuarioListOutput(usuarios);

            if (page == 1)
            {
                var log = LogActividad.CreateNew(ModuloEnum.QuienEsQuien, LogTipo.VisitaLista);
                _applicationLoggerActivity.Save(log);
            }
            return output;
        }

        public UsuarioListOutput Search(int page, string searchTerm)
        {
            var usuarios = _usuarioRepository.Search(page, searchTerm);
            UsuarioListOutput output = new UsuarioListOutput(usuarios);
            return output;
        }
    }
}
