﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Exceptions
{
    public class UsuarioInactivoException : ApplicationException
    {
        public UsuarioInactivoException(string message) : base(message)
        { }
    }
}
