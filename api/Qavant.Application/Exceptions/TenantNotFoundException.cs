﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Exceptions
{
    public class TenantNotFoundException : ApplicationException
    {
        internal TenantNotFoundException(string message) : base(message)
        { }
    }
}
