﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Exceptions
{
    public class ApplicationException : Exception
    {
        internal ApplicationException(string businessMessage) : base(businessMessage)
        {
        }

        internal ApplicationException(string businessMessage, string detail) : base(businessMessage)
        {
            base.Data["Detail"] = detail;
        }
    }
}