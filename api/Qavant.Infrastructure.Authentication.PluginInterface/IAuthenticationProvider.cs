﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Authentication.PluginInterface
{
    public interface IAuthenticationProvider
    {
        void SetParameters(Dictionary<string, string> parameters);
        bool Validate(int tenantId, string username, string password);
    }
}
