﻿using Qavant.Domain.Reacciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Usuarios
{
    public class Cumpleaños : IReaccionable
    {
        public Usuario Usuario { get; private set; }
        public InfoReaccion InfoReaccion { get; private set; }

        public void SetInfoReaccion(InfoReaccion infoReaccion)
        {
            InfoReaccion = infoReaccion;
        }

        public void SetUsuario(Usuario usuario)
        {
            Usuario = usuario;
        }
    }
}
