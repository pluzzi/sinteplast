﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Usuarios
{
    public class Area
    {
        public int Id { get; private set; }
        public int TenantId { get; private set; }
        public string Nombre { get; private set; }
        public bool Habilitada { get; private set; }
    }
}