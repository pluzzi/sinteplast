﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Multimedia
{
    public class ElementoMultimedia
    {
        public int Id { get; private set; }
        public string Tipo { get; private set; }
        public DateTime FechaAlta { get; private set; }
        public string RutaArchivo { get; private set; }
    }
}