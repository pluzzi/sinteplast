﻿using Qavant.Domain.Reacciones;
using System;

namespace Qavant.Domain.Noticias
{
    public class Noticia : IReaccionable
    {
        public int Id { get; private set; }

        public string Titulo { get; private set; }

        public string Copete { get; private set; }

        public string Contenido { get; private set; }

        public string Imagen { get; private set; }

        public DateTime FechaAlta { get; private set; }

        public bool Destacada { get; private set; }

        public InfoReaccion InfoReaccion { get; private set; }

        public void SetInfoReaccion(InfoReaccion infoReaccion)
        {
            InfoReaccion = infoReaccion;
        }
    }
}