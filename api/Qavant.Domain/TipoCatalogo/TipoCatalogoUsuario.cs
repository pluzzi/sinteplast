﻿using Qavant.Domain.Categorias;
using Qavant.Domain.Multimedia;
using Qavant.Domain.Reacciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.TipoCatalogo
{
    public class TipoCatalogoUsuario
    {
        public TipoCatalogoUsuario(){}

        public int UserId { get; private set; }
        public TipoCatalogo TipoCatalogo { get; private set; }
        public decimal PuntosAcumulados { get; private set; }
        public DateTime FechaActualizacionPuntos { get; private set; }

        public void SetTipoCatalogo(TipoCatalogo tipoCatalogo)
        {
            TipoCatalogo = tipoCatalogo;
        }

    }
}
