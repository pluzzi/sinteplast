﻿using Qavant.Domain.Categorias;
using Qavant.Domain.Multimedia;
using Qavant.Domain.Reacciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.TipoCatalogo
{
    public class TipoCatalogo
    {
        public TipoCatalogo(){}

        public int Id { get; set; }
        public string Descripcion { get; set; }
        public string IconoCatalogo { get; set; }
        public string FondoCatalogo { get; set; }
        public int EmpresaId { get; set; }
        public string Mail { get; set; }

    }
}
