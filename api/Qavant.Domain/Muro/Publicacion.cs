﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Muro
{
    public class Publicacion
    {
        public int Id { get; private set; }

        public string TipoRegistro { get; private set; }

        public int ModuloId { get; private set; }

        public string DescRegistro { get; private set; }

        public string Titulo { get; private set; }

        public string Copete { get; private set; }

        public string Contenido { get; private set; }

        public string Imagen { get; private set; }

        public DateTime FechaAlta { get; private set; }

        public bool Destacada { get; private set; }

        public int TotalReacciones { get; private set; }

        public int ReaccionId { get; set; }
    }
}