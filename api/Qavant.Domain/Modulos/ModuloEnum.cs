﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Modulos
{
    public enum ModuloEnum
    {
        None = 0,
        Documentos = 1,
        Capacitaciones = 2,
        Muro = 3,
        MiPerfil = 4,
        Noticias = 5,
        QuienEsQuien = 6,
        Cumpleaños = 7,
        Beneficios = 8,
        Encuestas = 9,
        InfoUtil = 10,
        TerminosCondiciones = 11,
        Login = 13,
        Activacion = 14,
        OlvidoPass = 15,
        Formularios = 16,
        Credenciales = 17,
        Galerias = 26,
        Catalogos = 30,
        Canje = 31

    }
}