﻿using Qavant.Domain.Categorias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Dispositivos
{
    public class Dispositivo
    {
        public Int64 Id { get; set; }
        public int UsuarioId { get; set; }
        public string Uuid { get; set; }
        public string TokenNotificacion { get; set; }

    }
}