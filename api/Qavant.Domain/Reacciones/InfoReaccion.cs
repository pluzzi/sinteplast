﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Reacciones
{
    public class InfoReaccion
    {
        public int TotalReacciones { get; set; }

        public int ReaccionId { get; set; }
    }
}
