﻿using Qavant.Domain.Categorias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Credencial
{
    public class Credencial
    {
        public int Id { get; set; }
        public int TenantId { get; set; }
        public string Html { get; set; }

    }
}