﻿using Qavant.Domain.Beneficios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Categorias
{
    public class Categoria
    {

        public int Id { get; private set; }

        public int ModuloId { get; private set; }

        public string Nombre { get; private set; }

        public int? PadreId { get; private set; }

        public string Imagen { get; private set; }
    }
}