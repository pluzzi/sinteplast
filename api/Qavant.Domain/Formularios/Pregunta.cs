using System;
using System.Collections.Generic;
using System.Linq;

namespace Qavant.Domain.Formularios
{
    public class Pregunta
    {
        public int Id { get; private set; }
        public Int16 Orden { get; private set; }
        public string Enunciado { get; private set; }
        public string HtmlInputType { get; private set; }
        public bool Habilitado { get; private set; }
        public bool Requerido { get; private set; }
        public ICollection<Opcion> Opciones { get; private set; }
        public TipoPregunta Tipo
        {
            get
            {
                TipoPregunta tipo;

                switch (HtmlInputType)
                {
                    case "DROPDOWN":
                    case "MULTIPLE":
                    case "OPTION":
                        tipo = TipoPregunta.Seleccion;
                        break;
                    default:
                        tipo = TipoPregunta.Simple;
                        break;
                }
                return tipo;
            }
        }
    }
}