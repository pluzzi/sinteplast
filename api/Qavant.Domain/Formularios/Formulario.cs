using Qavant.Domain.Reacciones;
using System;
using System.Collections.Generic;

namespace Qavant.Domain.Formularios
{
    public class Formulario : IReaccionable
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public bool RespuestaUnica { get; set; }
        public bool Respondido { get; set; }
        public bool EnviaEmail { get; set; }
        public TipoFormulario Tipo { get; set; }
        public string EmailPara { get; set; }
        public string EmailAsunto { get; set; }
        public string EmailTitulo { get; set; }
        public string EmailIntroduccion { get; set; }
        public bool Habilitado { get; set; }
        public ICollection<Pregunta> Preguntas { get; set; }
        public InfoReaccion InfoReaccion { get; private set; }

        public List<string> GetEmailsNotificacion()
        {
            List<string> emails = new List<string>();
            if (!string.IsNullOrEmpty(EmailPara))
            {
                foreach (var email in EmailPara.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    emails.Add(email.Trim());
                }
            }
            return emails;
        }
        public void SetInfoReaccion(InfoReaccion infoReaccion)
        {
            InfoReaccion = infoReaccion;
        }
    }
}