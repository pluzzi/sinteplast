﻿using Qavant.Domain.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Formularios
{
    public class RespuestasEnFormulario
    {
        public Usuario Usuario { get; private set; }
        public Formulario Formulario { get; private set; }
        public DateTime FechaCreacion { get; private set; }
        public List<Respuesta> Respuestas { get; private set; }
        public FormEstado Estado { get; private set; }

        public RespuestasEnFormulario(Usuario usuario, Formulario formulario, List<Respuesta> respuestas)
        {
            Usuario = usuario;
            Formulario = formulario;
            Respuestas = respuestas;
            FechaCreacion = DateTime.Now;
            Estado = FormEstado.None;
        }

        public int GetCantidadPreguntasRespondidas()
        {
            int count = 0;
            foreach (var respuesta in Respuestas)
            {
                if (respuesta.Pregunta.Tipo == TipoPregunta.Simple)
                {
                    if (!string.IsNullOrEmpty(respuesta.RespuestaSimple))
                    {
                        count++;
                    }
                }
                else
                {
                    if (respuesta.RespuestaSeleccion.Count > 0)
                    {
                        count++;
                    }
                }
            }
            return count;
        }

        public int GetCantidadPreguntasObligatoriasSinRespuesta()
        {
            int count = 0;
            foreach (var respuesta in Respuestas)
            {
                if (respuesta.Pregunta.Requerido)
                {
                    if (respuesta.Pregunta.Tipo == TipoPregunta.Simple)
                    {
                        if (string.IsNullOrEmpty(respuesta.RespuestaSimple))
                        {
                            count++;
                        }
                    }
                    else
                    {
                        if (respuesta.RespuestaSeleccion.Count == 0)
                        {
                            count++;
                        }
                    }
                }
            }
            return count;
        }

        public void RemoverRespuestasVacias()
        {
            List<Respuesta> opcionalesTemp = new List<Respuesta>();
            foreach (var respuesta in Respuestas)
            {
                if (!respuesta.Pregunta.Requerido)
                {
                    if (respuesta.Pregunta.Tipo == TipoPregunta.Simple)
                    {
                        if (string.IsNullOrEmpty(respuesta.RespuestaSimple))
                        {
                            opcionalesTemp.Add(respuesta);
                        }
                    }
                    else
                    {
                        if (respuesta.RespuestaSeleccion.Count == 0)
                        {
                            opcionalesTemp.Add(respuesta);
                        }
                    }
                }
            }
            Respuestas = Respuestas.Except(opcionalesTemp).ToList();
        }
    }
}