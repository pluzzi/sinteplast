﻿using Qavant.Domain.Categorias;
using Qavant.Domain.Reacciones;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Beneficios
{
    public class Beneficio : IReaccionable
    {
        public int Id { get; private set; }

        public string Titulo { get; private set; }

        public string Copete { get; private set; }

        public string Contenido { get; private set; }

        public string Imagen { get; private set; }

        public DateTime FechaDesde { get; private set; }

        public DateTime FechaHasta { get; private set; }

        public InfoReaccion InfoReaccion { get; set; }

        public int Puntos { get; private set; }

        public int TipoCatalogoProductosId { get; private set; }

        public bool EsNuevo { get; private set; }

        public void SetInfoReaccion(InfoReaccion infoReaccion)
        {
            InfoReaccion = infoReaccion;
        }
    }
}