﻿using Qavant.Domain.Categorias;
using Qavant.Domain.Reacciones;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Beneficios
{
    public class Exchange
    {
        public int Id { get; private set; }

        public int UserId { get; private set; }

        public int BenefitsId { get; private set; }

        public DateTime FechaCanje { get; private set; }

        public decimal Puntos { get; private set; }

        public decimal PuntosUsados { get; private set; }
    }
}