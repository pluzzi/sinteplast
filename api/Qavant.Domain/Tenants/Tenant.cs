﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Tenants
{
    public class Tenant
    {
        public Tenant()
        {
            ParametrosPublicos = new Dictionary<string, object>();
            ParametrosInternos = new Dictionary<string, object>();
        }

        #region Props
        public int Id { get; private set; }
        public string Nombre { get; private set; }
        public string TenantKeyWeb { get; private set; }
        public string TenantKeyApp { get; private set; }
        public Dictionary<string, object> ParametrosInternos { get; private set; }
        public Dictionary<string, object> ParametrosPublicos { get; private set; }
        #endregion

        #region Behavior
        public Tenant AgregarParametroPublico(string key, object param)
        {
            ParametrosPublicos.Add(key, param);
            return this;
        }

        public Tenant AgregarParametroInterno(string key, object param)
        {
            ParametrosInternos.Add(key, param);
            return this;
        }
        #endregion
    }
}
