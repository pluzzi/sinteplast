﻿using Qavant.Infrastructure.Authentication.PluginInterface;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Authentication.Basic
{
    public class BasicProvider : IAuthenticationProvider
    {
        private Dictionary<string, string> _parameters;

        public void SetParameters(Dictionary<string, string> parameters)
        {
            _parameters = parameters;
        }

        public bool Validate(int tenantId, string username, string password)
        {
            bool result = default(bool);
            string connectionString = _parameters["ConnectionString"];
            string queryString = "SELECT Id, UserName from dbo.Users WHERE EmpresaId = @tenantId AND UserName = @username AND Password = @password";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(queryString, connection))
                {
                    command.Parameters.AddWithValue("@tenantId", tenantId);
                    command.Parameters.AddWithValue("@username", username);
                    command.Parameters.AddWithValue("@password", password);

                    try
                    {
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();

                        if (reader.HasRows)
                            result = true;
                        else
                            result = false;

                        reader.Close();
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
            }
            return result;
        }
    }
}
